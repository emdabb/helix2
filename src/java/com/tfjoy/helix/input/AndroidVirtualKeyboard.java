package com.tfjoy.helix.input;

class AndroidVirtualKeyboard extends Activity {
    public static void Show() {
        InputMethodManager m = (InputMethodManager)NativeActivitySubclass.Get().getSystemService(Context.INPUT_METHOD_SERVICE);
        m.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void Hide() {
        InputMethodManager m = (InputMethodManager)NativeActivitySubclass.Get().getSystemService(Context.INPUT_METHOD_SERVICE);
        m.toggleSoftInput(0, 0);
    }
    
}
