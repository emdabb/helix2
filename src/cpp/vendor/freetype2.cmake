cmake_minimum_required(VERSION 3.0)

project(freetype2 C)

include_directories(
    freetype2/include
    freetype2/include/freetype

)
ADD_DEFINITIONS(-DFT2_BUILD_LIBRARY)


file(GLOB SDL_TTF_SOURCES
    freetype2/src/base/ftsystem.c
    freetype2/src/base/ftdebug.c
    freetype2/src/base/ftinit.c
    freetype2/src/base/ftbbox.c
    freetype2/src/base/ftbitmap.c
    freetype2/src/base/ftcid.c
    freetype2/src/base/fthash.c
freetype2/src/base/ftadvanc.c
freetype2/src/base/ftcalc.c
freetype2/src/base/ftdbgmem.c
freetype2/src/base/ftgloadr.c
freetype2/src/base/ftobjs.c
freetype2/src/base/ftoutln.c
freetype2/src/base/ftrfork.c
freetype2/src/base/ftsnames.c
freetype2/src/base/ftstream.c
freetype2/src/base/fttrigon.c
freetype2/src/base/ftutil.c
freetype2/src/base/ftfstype.c
freetype2/src/base/ftgasp.c
freetype2/src/base/ftglyph.c
freetype2/src/base/ftgxval.c
freetype2/src/base/ftlcdfil.c
freetype2/src/base/ftmm.c
freetype2/src/base/ftotval.c
freetype2/src/base/ftpatent.c
freetype2/src/base/ftpfr.c
freetype2/src/base/ftstroke.c
freetype2/src/base/ftsynth.c
freetype2/src/base/fttype1.c
freetype2/src/base/ftwinfnt.c
freetype2/src/base/ftxf86.c
freetype2/src/truetype/truetype.c
freetype2/src/type1/type1.c
freetype2/src/cff/cff.c
freetype2/src/cid/type1cid.c
freetype2/src/pfr/pfr.c
freetype2/src/type42/type42.c
freetype2/src/winfonts/winfnt.c
freetype2/src/pcf/pcf.c
freetype2/src/bdf/bdf.c
freetype2/src/sfnt/sfnt.c
freetype2/src/autofit/autofit.c
freetype2/src/pshinter/pshinter.c
freetype2/src/raster/raster.c
freetype2/src/smooth/smooth.c
freetype2/src/cache/ftcache.c
freetype2/src/gzip/ftgzip.c
freetype2/src/lzw/ftlzw.c
freetype2/src/bzip2/ftbzip2.c
freetype2/src/psaux/psaux.c
freetype2/src/psnames/psmodule.c
)

add_library(${PROJECT_NAME} ${SDL_TTF_SOURCES})

if(HELIX_PLATFORM_IOS)
    set_target_properties(${PROJECT_NAME} PROPERTIES
        XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY "1,2"
        XCODE_ATTRIBUTE_ENABLE_BITCODE "NO"
        XCODE_ATTRIBUTE_COMPRESS_PNG_FILES "YES"
        XCODE_ATTRIBUTE_VALIDATE_PRODUCT "YES"
        XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET "7.0"
    )

endif(HELIX_PLATFORM_IOS)

target_link_libraries(${PROJECT_NAME})
