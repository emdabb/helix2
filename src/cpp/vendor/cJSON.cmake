cmake_minimum_required(VERSION 3.0)
project(cJSON)

file(GLOB CJSON_SOURCES
    cJSON/*.c
)

add_library(cJSON ${CJSON_SOURCES})

if(HELIX_PLATFORM_IOS)

set_target_properties(cJSON PROPERTIES
XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY "1,2"
XCODE_ATTRIBUTE_ENABLE_BITCODE "NO"
XCODE_ATTRIBUTE_COMPRESS_PNG_FILES "YES"
XCODE_ATTRIBUTE_VALIDATE_PRODUCT "YES"
XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET "7.0"
)

endif(HELIX_PLATFORM_IOS)


link_libraries(cJSON)





