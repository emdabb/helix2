cmake_minimum_required(VERSION 3.0)

project(lodepng CXX)


file( GLOB lodepng_SOURCES    
    "lodepng/lodepng.cpp"
    "lodepng/lodepng_util.cpp"
    "lodepng/pngdetail.cpp"
)

add_library(lodepng STATIC ${lodepng_SOURCES})

