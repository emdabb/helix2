cmake_minimum_required(VERSION 3.0)

project(liblua C)
# the following two variables are defined for the use of packages
# that wish to link or compile against lua
set (LUA_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/lua/src")
set (LUA_LIBRARIES lua)
file (GLOB LUA_CORE_SRCS
    lua/src/lapi.c
    lua/src/lauxlib.c
    lua/src/lbaselib.c
    lua/src/lbitlib.c
    lua/src/lcode.c
    lua/src/lcode.h
    lua/src/lcorolib.c
    lua/src/lctype.c
    lua/src/lctype.h
    lua/src/ldblib.c
    lua/src/ldebug.c
    lua/src/ldo.c
    lua/src/ldump.c
    lua/src/lfunc.c
    lua/src/lfunc.h
    lua/src/lgc.c
    lua/src/lgc.h
    lua/src/linit.c
    lua/src/liolib.c
    lua/src/llex.c
    lua/src/lmathlib.c
    lua/src/lmem.c
    lua/src/loadlib.c
    lua/src/loadlib_rel.c
    lua/src/lobject.c
    lua/src/lobject.h
    lua/src/lopcodes.c
    lua/src/lopcodes.h
    lua/src/loslib.c
    lua/src/lparser.c
    lua/src/lstate.c
    lua/src/lstring.c
    lua/src/lstrlib.c
    lua/src/ltable.c
    lua/src/ltablib.c
    lua/src/ltm.c
    lua/src/lua.c
    lua/src/luac.c
    #lua/src/luaconf.h.in
    #lua/src/luaconf.h.orig
    #lua/src/luac.rc
    #lua/src/lua.rc
    lua/src/lundump.c
    lua/src/lvm.c
    lua/src/lzio.c
    #lua/src/wmain.c

 )
if(HELIX_PLATFORM_IOS)
    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -std=gnu99 -O2 -Wall -Wextra -DLUA_COMPAT_5_2 -DLUA_USE_IOS" )
endif(HELIX_PLATFORM_IOS)

add_library (liblua STATIC ${LUA_CORE_SRCS})
target_link_libraries (liblua)
