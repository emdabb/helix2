#ifndef HELIX_CORE_FACTORY_H_
#define HELIX_CORE_FACTORY_H_

#include <map>
#include <string>
#include <typeinfo>
#include <helix/core/Singleton.h>

namespace helix {
namespace core {

template <typename T, typename CLASSID=std::string>
class Factory : public Singleton<Factory<T, CLASSID> >{
  typedef T product_t;
  typedef T*(*create_fun)();
  typedef CLASSID classid_t;
  typedef std::map<classid_t, create_fun> registry_t;
  registry_t reg;
public:
  virtual ~Factory() {
  }

  bool registerClass(const classid_t& id, create_fun fun) {
    typename registry_t::iterator it = reg.find(id);
    if (it == reg.end()) {
      reg[id] = fun;
      return true;
    }
    return false;
  }

  T* create(const classid_t& id) {
    typename registry_t::iterator it = reg.find(id);
    if (it != reg.end()) {
      T* result = (*it).second();
      return result;
    }
    return 0;
  }

  T* create() {
    return new T;
  }
};

    
template<typename BASE, typename TYPE, typename CLASSID = std::string>
class RegisterFactory {
  typedef CLASSID classid_t;
  typedef BASE base_t;
  typedef TYPE specialized_t;
  typedef Factory<base_t, classid_t> factory_t;

  static base_t* createInstance() {
    return new specialized_t;
  }
public:

  RegisterFactory(const CLASSID& id) {
    factory_t::getInstance().register_class(id, this->createInstance);
  }

  RegisterFactory() {
    std::string id = typeid(TYPE).name();
    Factory<BASE, classid_t>::getInstance().registerClass(id, createInstance);
  }
};


}
}

#endif

