/*
 * Int2.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_CORE_INT2_H_
#define HELIX_CORE_INT2_H_

#include <helix/Types.h>

namespace helix {
namespace core {

__declare_aligned(struct, 8) Int2 {
	int X, Y;
};

}
}

typedef helix::core::Int2 hxInt2;



#endif /* HELIX_CORE_INT2_H_ */
