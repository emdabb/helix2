#ifndef HELIX_CORE_ITHREAD_H_
#define HELIX_CORE_ITHREAD_H_

#include <helix/Types.h>
#include <helix/core/IRunnable.h>

namespace helix {
namespace core {

struct IThread : public IRunnable {
    virtual ~IThread() {}
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void suspend() = 0;

    static void yield();
};

}
}
typedef helix::core::IRunnable  hxRunnable;
typedef helix::core::IThread    hxThread;
HXAPI hxThread* new_Thread(hxRunnable*);

#endif
