#ifndef HELIX_CORE_VECTOR2_H_
#define HELIX_CORE_VECTOR2_H_

#include <helix/Types.h>
#include <helix/core/MathHelper.h>

namespace helix {
    namespace core {

        __declare_aligned(struct, 8) HXEXPORT Vector2 {
            typedef const Vector2& const_ref;
            typedef Vector2* ptr;

            float X, Y;

            static const Vector2 One;
            static const Vector2 Zero;
            static const Vector2 Up;
            static const Vector2 Right;

            __forceinline static void createFromAngle(const float& in, ptr out) {
                out->X = real_cos(in);
                out->Y = real_sin(in);
            }

            __forceinline static Vector2 createFromAngle(const float& in) {
                Vector2 out;
                createFromAngle(in, &out);
                return out;
            }

            static float dot(const_ref a, const_ref b) {
                float res;
                dot(a, b, &res);
                return res;
            }
            static void dot(const_ref a, const_ref b, float* out) {
                *out = a.X * b.X + a.Y * b.Y;
            }

            static void normalOf(const_ref a, ptr b) {
            	b->X = -a.Y;
            	b->Y =  a.X;
            }

            static Vector2 normalOf(const_ref a) {
            	Vector2 res;
            	normalOf(a, &res);
            	return res;
            }

            static void normalized(const_ref a, Vector2* out);

            static Vector2  normalized(const_ref a);

            static void normalize(ptr out);

            static float length(const_ref in) {
                float out;
                length(in, &out);
                return out;
            }

            static inline void lengthSq(const_ref in, float* out) {
                *out = in.X * in.X + in.Y * in.Y;
            }

            static inline float lengthSq(const_ref in) {
                float out;
                lengthSq(in, &out);
                return out;
            }

            static Vector2 createRandom(int seed);
            static void     createRandom(int seed, ptr out);

            __forceinline static void lerp(const_ref a, const_ref b, float const& t, ptr c) {
                c->X = MathHelper::lerp(a.X, b.X, t);
                c->Y = MathHelper::lerp(a.Y, b.Y, t);
            }

            static void length(const_ref in, float* out) {
                *out = real_sqrt(in.X * in.X + in.Y * in.Y);
            }

            static void toAngle(const_ref in, float* out);
            static float toAngle(const_ref);

            inline Vector2 operator - () const {
                Vector2 copy(*this);
                copy.X = -copy.X;
                copy.Y = -copy.Y;
                return copy;
            }

            inline Vector2 operator + (const_ref b) const {
                Vector2 copy(*this);
                copy += b;
                return copy;
            }
            inline Vector2 operator - (const_ref b) const {
                Vector2 copy(*this);
                copy -= b;
                return copy;
            }
            inline Vector2 operator * (const_ref b) const {
                Vector2 copy(*this);
                copy *= b;
                return copy;
            }
            inline Vector2 operator / (const_ref b) const {
                Vector2 copy(*this);
                copy /= b;
                return copy;
            }
            inline Vector2& operator += (const_ref b) {
                X += b.X;
                Y += b.Y;
                return *this;
            }
            inline Vector2& operator -= (const_ref b) {
                X -= b.X;
                Y -= b.Y;
                return *this;
            }
            inline Vector2& operator *= (const_ref b) {
                X *= b.X;
                Y *= b.Y;
                return *this;
            }
            inline Vector2& operator /= (const_ref b) {
                X /= b.X;
                Y /= b.Y;
                return *this;
            }

            inline Vector2 operator * (float const& a) const {
                Vector2 copy(*this);
                copy *= a;
                return copy;
            }

            inline Vector2& operator *= (float const& a) {
                X *= a;
                Y *= a;
                return *this;
            }

            inline Vector2 operator / (float const& a) const {
                Vector2 copy(*this);
                copy /= a;
                return copy;
            }

            inline Vector2& operator /= (float const& a) {
                // TODO: reciprocal?
                X /= a;
                Y /= a;
                return *this;
            }
        };

    }
}

typedef helix::core::Vector2 hxVector2;

#endif
