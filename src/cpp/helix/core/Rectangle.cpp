/*
 * Rectangle.cpp
 *
 *  Created on: Jul 17, 2015
 *      Author: miel
 */

#include <helix/core/Rectangle.h>

using namespace helix::core;

const bool hxRectangle::contains(const hxRectangle& rc, int x, int y) {
   if (x >= rc.X) {
	   if (y >= rc.Y) {
		   if (x <= Rectangle::getRight(rc)) {
			   if (y <= Rectangle::getBottom(rc)) {
				   return true;
			   }
			   return false;
		   }
		   return false;
	   }
	   return false;
   }
   return false;
}


