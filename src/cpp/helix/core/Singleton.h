#ifndef HELIX_CORE_SINGLETON_H_
#define HELIX_CORE_SINGLETON_H_

#include <helix/Types.h>

namespace helix {
namespace core {

template <typename T>
class HXEXPORT Singleton {
    static T* mInstance;
public:
    __forceinline static T* getInstancePtr() {
        if(!mInstance) {
            mInstance = new T;
        }
        return mInstance;
    }

    __forceinline static T& getInstance() {
        return *getInstancePtr();
    }
};

template <typename T>
HXEXPORT T* Singleton<T>::mInstance = NULL;

}
}

template <typename T>
struct hxSingleton {
    typedef helix::core::Singleton<T> Type;
};

#endif
