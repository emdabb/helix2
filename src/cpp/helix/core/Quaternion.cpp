#include <helix/core/Quaternion.h>
#include <helix/core/Vector3.h>

using namespace helix::core;

const Quaternion Quaternion::Identity = { 0., 0., 0., 1. };

Quaternion Quaternion::createFromAxisAngle(const Vector3& axis, float angle) {
	Quaternion q;
	real num2 = angle * 0.5f;
	real num = real_sin(num2);
	real num3 = real_cos(num2);
	q.X = axis.X * num;
	q.Y = axis.Y * num;
	q.Z = axis.Z * num;
	q.W = num3;
	return q;
}

Quaternion Quaternion::createFromVectors(const Vector3& u, const Vector3& v) {
	/**
	 * source: http://lolengine.net/blog/2013/09/18/beautiful-maths-quaternion-from-vectors
	 */
	Vector3 w = Vector3::cross(u, v);
	Quaternion q;
	q.W = 1.f + Vector3::dot(u, v);
	q.X = w.X;
	q.Y = w.Y;
	q.Z = w.Z;
	return normalized(q);
}

void Quaternion::getDirectionVector(const Quaternion& q, Vector3* out)
{

    out->X = 2 * ((q.Z * q.X) + (q.Y * q.W));
    out->Y = 2 * ((q.Y * q.Z) - (q.X * q.W));
    out->Z = 1 - (2 * ((q.Y * q.Y) + (q.X * q.X)));

}
