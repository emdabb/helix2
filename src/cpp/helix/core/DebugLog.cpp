#include "DebugLog.h"
#include <cstdio>
#include <cstdarg>
#include <string>
#include <sstream>
#include <helix/Config.h>

#if defined(HELIX_PLATFORM_WIN32)
#pragma warning(disable:4996)
#endif

using namespace helix::core;

#if defined(HELIX_PLATFORM_IOS)
#ifdef __OBJC__
#include <Foundation/Foundation.h>
#endif
template <typename T, typename TRAITS=std::char_traits<T> >
class basic_ns_logbuf : public std::basic_streambuf<T, TRAITS> {
    static const int mBufferSize    = 128;
    char mBuffer[mBufferSize];
private:
    int overflow(int c) {
        if(c == TRAITS::eof()) {
            *this->pptr() = TRAITS::to_char_type(c);
            this->sbumpc();
        }
        return this->sync() ? TRAITS::eof() : TRAITS::not_eof(c);
    }
    int sync() {
        if(this->pbase() != this->pptr()) {
            std::string str(this->pbase(), (size_t)(this->pptr() - this->pbase()));
            NSString* nssString = @(str.c_str());
            NSLog(nssString);
                  
            this->setp(mBuffer, mBuffer + mBufferSize - 1);
        }
        return 0;
    }
public:
    basic_ns_logbuf() {
        this->setp(mBuffer, mBuffer + mBufferSize - 1);
    }
};

template <typename T, typename TRAITS=std::char_traits<T> >
class basic_ns_logstream : public std::basic_ostream<T, TRAITS> {
    //basic_anroid_logbuf* mBuffer;
public:
    basic_ns_logstream() : std::ostream(new basic_ns_logbuf<T>){
    }
};


typedef basic_ns_logstream<char>    ns_logstream;
typedef basic_ns_logbuf<char>       ns_logbuf;

#elif defined(HELIX_PLATFORM_ANDROID)
#include <android/log.h>
#define LOG_TAG     "helix.debug"
template <typename T, typename TRAITS=std::char_traits<T> >
class basic_android_logbuf : public std::basic_streambuf<T, TRAITS> {
    static const int mBufferSize    = 128;
    char mBuffer[mBufferSize];
private:
    int overflow(int c) {
        if(c == TRAITS::eof()) {
            *this->pptr() = TRAITS::to_char_type(c);
            this->sbumpc();
        }
        return this->sync() ? TRAITS::eof() : TRAITS::not_eof(c);
    }
    int sync() {
        if(this->pbase() != this->pptr()) {
            std::string str(this->pbase(), (size_t)(this->pptr() - this->pbase()));
            __android_log_print(
                ANDROID_LOG_DEBUG, 
                LOG_TAG, 
                "%s",
                str.c_str() 
            );
            this->setp(mBuffer, mBuffer + mBufferSize - 1);
        }
        return 0;
    }
public:
    basic_android_logbuf() {
        this->setp(mBuffer, mBuffer + mBufferSize - 1);
    }
};

template <typename T, typename TRAITS=std::char_traits<T> >
class basic_android_logstream : public std::basic_ostream<T, TRAITS> {
    //basic_anroid_logbuf* mBuffer;
public:
    basic_android_logstream() : std::ostream(new basic_android_logbuf<T>){
    }
};

typedef basic_android_logbuf<char>      android_logbuf;
typedef basic_android_logstream<char>   android_logstream;

static android_logstream AndroidLogOutput;
#else
#endif

int DebugLog::mContextLevel = 0;
IMutex* DebugLog::mLock = new_Mutex();

DebugLog::DebugLog(const std::string& val) 
: mContext(val)
#if defined(HELIX_PLATFORM_ANDROID)
, sOutputStream(new android_logbuf)
#elif defined(HELIX_PLATFORM_IOS)
, sOutputStream(new ns_logbuf)
//, sOutputStream(std::cout.rdbuf())
#else
, sOutputStream(std::cout.rdbuf()) 
#endif
{
    mLock->acquire();
    //msg(std::string("+") + mContext);
    writeIndent();
    sOutputStream << "+" << mContext << std::endl << std::flush;
    mContextLevel++;
    mLock->release();
}

DebugLog::~DebugLog() {
    mLock->acquire();

    mContextLevel--;

    writeIndent();
    sOutputStream << "-" << mContext << std::endl << std::flush;
    //msg(std::string("-") + mContext);
    mLock->release();

}

void DebugLog::writeIndent(char c) {
    for(int i=0; i < mContextLevel; i++) {
        sOutputStream << c;
    }
}

void DebugLog::msg(const std::string& str) {
    mLock->acquire();

    writeIndent();

    sOutputStream << mContext << ":" << str << std::endl << std::flush;
    //sOutputStream.flush();
    mLock->release();

}

void DebugLog::msg(const char* str, ...) {
    mLock->acquire();

    writeIndent();

    char b[4096];
    va_list args;
    va_start(args, str);
    vsprintf(b, str, args);
    va_end(args);
    sOutputStream << mContext << ":" << b << std::endl << std::flush;
    //sOutputStream.flush();
    mLock->release();


}

//void DebugLog::setOutputStream(std::ostream* s) {
//    sOutputStream.flush();
//    sOutputStream.rdbuf(s.rdbuf());
//}
