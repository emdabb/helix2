#ifndef HELIX_CORE_MATRIX_H_
#define HELIX_CORE_MATRIX_H_

#include <helix/Types.h>

#if defined(HELIX_HAVE_SSE)
#include <xmmintrin.h>
#endif

namespace helix {
    namespace core {

        struct Vector3;
        struct Quaternion;

        __declare_aligned(struct, 16) Matrix {
            typedef const Matrix& const_ref;
            typedef Matrix* ptr;

            static const Matrix Identity;
#if defined(HELIX_HAVE_SSE) || defined(HELIX_HAVE_NEON)
            union {
#endif
            	struct {
					real M11, M12, M13, M14;
					real M21, M22, M23, M24;
					real M31, M32, M33, M34;
					real M41, M42, M43, M44;
            	};
#if defined(HELIX_HAVE_SSE)
            	__m128 row[4];
            };
#elif defined(HELIX_HAVE_NEON)
            float32x4 row[4];
        	};
#endif
            static Matrix mul(const_ref a, const_ref b);
            static void mul(const_ref a, const_ref b, ptr out);
            static Matrix inverted(const_ref in);
            static void invert(ptr inout);
            static void invert(const_ref in, ptr out);
            static void transpose(const_ref in, ptr out);
            static void createLookat(Vector3 const& pos, Vector3 const& at, Vector3 const& up, ptr out);
            static void determinant(const_ref a, real*);
            static void createPerspectiveFov(real const& fov, real const& ratio, real const& tnear, real const& tfar, ptr out);
            static void createOrthoOffcentre(real const& left, real const& right, real const& bottom, real const& top, real const& zNearPlane, real const& zFarPlane, ptr out);
            static void createRotation(real const& rx, real const& ry, real const& rz, ptr out);
            static void createRotationX(real const& a, ptr out);
            static void createRotationY(real const& a, ptr out);
            static void createRotationZ(real const& a, ptr out);
            static void createWorld(Vector3 const& pos, Vector3 const& dir, Vector3 const& up, ptr out);
            static void createScale(Vector3 const&, ptr);
            static void createScale(real const&, real const&, real const&, ptr);
            static void createTranslation(Vector3 const&, ptr);
            static void createTranslation(real const& x, real const& y, real const& z, ptr out);
            static void createRotation(Quaternion const&, ptr);

        };

    }
}

typedef helix::core::Matrix hxMatrix;

#endif
