#ifndef HELIX_CORE_DEBUGLOG_H_
#define HELIX_CORE_DEBUGLOG_H_

#include <helix/Types.h>
#include <helix/core/IMutex.h>
#include <iostream>
#include <typeinfo>
#include <string>
#include <cstdarg>


namespace helix {
namespace core {

#define DEBUG_METHOD()              helix::core::DebugLog __LOG__(__FUNCTION__)
#define DEBUG_MESSAGE(...)          __LOG__.msg(__VA_ARGS__)
#define DEBUG_VALUE_OF(x)           __LOG__.valueOf(#x, x, false)
#define DEBUG_VALUE_AND_TYPE_OF(x)  __LOG__.valueOf(#x, x, true)

struct IMutex;

class HXEXPORT DebugLog {
    std::string     mContext;
    std::ostream    sOutputStream;
    static int      mContextLevel;
    static IMutex*  mLock;
protected:
    void writeIndent(char c = 0x20);

public:
    explicit DebugLog(const std::string&);
    virtual ~DebugLog();
    void msg(const char*, ...);
    void msg(const std::string&);

    template <typename T>
    void valueOf(const std::string& name, const T& val, bool typeOutput) {
        writeIndent();
        sOutputStream << mContext << ":"<< name;
        if(typeOutput) {
            sOutputStream << "(" << typeid(val).name() << ")";
        }
        sOutputStream << "=[" << val << "]" << std::endl;
        sOutputStream.flush();
    }


    //void setOutputStream(std::ostream*);
};

}
}

#endif

