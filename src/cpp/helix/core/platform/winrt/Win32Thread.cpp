#include <helix/core/IThread.h>
#include <windows.h>

namespace helix {
namespace core {
namespace impl {

class Win32Thread : public IThread {
    IRunnable* mRunnable;
	HANDLE mHandle;

	static DWORD WINAPI callback(LPVOID param) {
		Win32Thread* t = (Win32Thread*)param;
		return (DWORD)t->run();
	}

public:
    Win32Thread(hxRunnable* val) {
		DWORD id;
		mHandle = ::CreateThread(NULL, 0, &callback, this, CREATE_SUSPENDED, &id);
    }

    virtual ~Win32Thread() {
		done();
    }

	int run(){
		if (mRunnable != NULL) {
			return mRunnable->run();

		}
		return -1;
	}

    void start() {
		::ResumeThread(mHandle);
    }

    void stop() {
		::CloseHandle(mHandle);
    }

    void done() {
		stop();
    }

	void suspend() {
		::SuspendThread(mHandle);
	}
};

}

void IThread::yield() {
	YieldProcessor();
}

}
}

HXAPI hxThread* new_Thread(hxRunnable* val) {
    return new helix::core::impl::Win32Thread(val);
}
