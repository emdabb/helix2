#include <helix/core/IMutex.h>
#include <windows.h>

namespace helix {
    namespace core {
        namespace impl {

            class Win32Mutex : public IMutex {
				CRITICAL_SECTION mHandle;
            public:
				Win32Mutex() {
					InitializeCriticalSectionEx(&mHandle, 0, 0);
				}

				~Win32Mutex() {
					::DeleteCriticalSection(&mHandle);
				}
                int acquire() {
					return TryEnterCriticalSection(&mHandle) != FALSE;
                }

                void release() {
					::LeaveCriticalSection(&mHandle);
                }
            };

        }
    }
}

HXAPI HXEXPORT hxMutex* new_Mutex() {
    return new helix::core::impl::Win32Mutex();
}
