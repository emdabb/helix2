#include <helix/core/IMutex.h>
#include <pthread.h>

namespace helix {
namespace core {
namespace impl {

class PosixMutex : public IMutex {
    pthread_mutex_t mHandle;
public:
    PosixMutex() {
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_DEFAULT);
        pthread_mutex_init(&mHandle, &attr);
        pthread_mutexattr_destroy(&attr);
    }

    virtual ~PosixMutex() {
    }

    virtual int acquire() {
        return pthread_mutex_lock(&mHandle);
    }

    virtual void release() { 
        pthread_mutex_unlock(&mHandle);
    }

};

}
}
}

HXAPI HXEXPORT hxMutex* new_Mutex() {
    return new helix::core::impl::PosixMutex;
}
