#include <helix/core/IWaitCondition.h>
#include <pthread.h>

namespace helix {
namespace core {
namespace impl {

class PosixWaitCondition : public IWaitCondition {
    pthread_mutex_t     _lock;
    pthread_cond_t      _cond;
    bool                _set;
public:
    PosixWaitCondition() 
    : _set(false)
    {
        pthread_condattr_t   attr;
        pthread_condattr_init(&attr);
        pthread_cond_init(&_cond, &attr);
        pthread_condattr_destroy(&attr);

        pthread_mutexattr_t  mattr;
        pthread_mutexattr_init(&mattr);
        pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_DEFAULT);
        pthread_mutex_init(&_lock, &mattr);
        pthread_mutexattr_destroy(&mattr);
    }

    virtual ~PosixWaitCondition() {
        set();
        pthread_cond_destroy(&_cond);
        pthread_mutex_destroy(&_lock);
    }

    void wait() {
        pthread_mutex_lock(&_lock);
        while(!_set) {  
            pthread_cond_wait(&_cond, &_lock);
        }
        pthread_mutex_unlock(&_lock);
    }

    void set() {
        _set = true;
        pthread_cond_signal(&_cond);
    }

    void reset() {
        pthread_mutex_lock(&_lock);
        _set = false;
        pthread_mutex_unlock(&_lock);
    }

};

}
}
}

HXAPI HXEXPORT hxWaitCondition* new_WaitCondition() {
    return new helix::core::impl::PosixWaitCondition();
}
