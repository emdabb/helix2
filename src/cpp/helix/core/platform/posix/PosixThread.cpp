#include <helix/core/IThread.h>
#include <pthread.h>

namespace helix {
namespace core {
namespace impl {

class PosixThread : public IThread {
    IRunnable*      mRunnable;
    pthread_t       mHandle;
    pthread_mutex_t mLock;
    pthread_cond_t  mWaitCondition;
    bool            mIsSuspended;
private:
    static void* callback(void* args) {
        PosixThread* thiz = static_cast<PosixThread*>(args);
        pthread_mutex_lock(&thiz->mLock);
        while(thiz->mIsSuspended) {
            pthread_cond_wait(&thiz->mWaitCondition, &thiz->mLock);
        }
        pthread_mutex_unlock(&thiz->mLock);
        thiz->run();
        pthread_exit(NULL);
        return args;
    }
public:
    explicit PosixThread(IRunnable* val)
    : mRunnable(val)
    , mIsSuspended(true)
    {
        pthread_mutex_init(&mLock, NULL);
        pthread_cond_init(&mWaitCondition, NULL);

        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_create(&mHandle, &attr, &PosixThread::callback, this);
        pthread_attr_destroy(&attr);
    }

    virtual ~PosixThread() {
    }

    void start() {
        pthread_mutex_lock(&mLock);
        mIsSuspended = false;
        pthread_cond_signal(&mWaitCondition);
        pthread_mutex_unlock(&mLock);
    }

    void stop() {
        pthread_join(mHandle, NULL);
    }

    void suspend() {
    }

    int run() {
        if(mRunnable) {
            return mRunnable->run();
        }
        return 0;
    }

    void done() {
      stop();
    }
};

}
void IThread::yield() {
  sched_yield();
}
}
}

HXAPI HXEXPORT hxThread* new_Thread(hxRunnable* arg) {
    return new helix::core::impl::PosixThread(arg);
}
