#ifndef HELIX_CORE_RECTANGLE_H_
#define HELIX_CORE_RECTANGLE_H_

#include <helix/Types.h>

namespace helix {
namespace core {

__declare_aligned(struct, 16) HXEXPORT Rectangle {
    int X, Y;
    unsigned int W, H;

    __forceinline static int32_t getRight(const Rectangle& rc) {
        return rc.X + rc.W;
    }

    __forceinline static int32_t getBottom(const Rectangle& rc) {
        return rc.Y + rc.H;
    }

    static const bool contains(const Rectangle& rc, int x, int y);


    __forceinline static real getArea(const Rectangle& rc) {
        return (real)rc.W * (real)rc.H;
    }

};

}
}

typedef helix::core::Rectangle hxRectangle;

#endif
