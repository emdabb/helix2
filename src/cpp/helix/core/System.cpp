#include <helix/core/System.h>
#include <helix/Config.h>

using namespace helix::core;

#if defined(HELIX_PLATFORM_POSIX)
typedef void* Library;
#elif defined(HELIX_PLATFORM_WINRT)
#include <windows.h>
typedef HMODULE Library;
#endif


int System::getNumProcessors() {
#if defined(HELIX_PLATFORM_POSIX)
    return 0;
#elif defined(HELIX_PLATFORM_WINRT)
	return 0;
#endif
}
