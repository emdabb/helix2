/** Socket.h
 *
 *  Created on: Oct 21, 2013
 *      Author: miel
 */

#ifndef SOCKET_H_
#define SOCKET_H_

#include <helix/Types.h>
#include <string>
#include <vector>
//#include <core/event.h>

struct sockaddr_in;


namespace helix {
namespace core {

class Socket;

struct SocketEventArgs {
    Socket* thiz;
};
struct SocketType {
    enum {
        Stream,
        Datagram
    };
};

class Socket {
    int         mHandle;
    bool        mIsBlocking;
    uint32_t    mHostAddr;
    uint16_t    mHostPort;
public:
private:
    int error();
    const int error() const;
public:
    Socket(const int type = SocketType::Stream);
    Socket(int, sockaddr_in*);
    virtual ~Socket();
    bool isValid() const;
    int bind(int, unsigned short);
    int bind(uint16_t);
    Socket* accept();
    bool open(std::string const&, uint16_t);
    bool open(int, uint16_t);
    bool close();
    bool isClosed() const;
    bool listen();
    int  receive(void*, size_t);
    const int  receive(void*, size_t) const;
    int  receiveFrom(void*, size_t, int*, uint16_t*);
    int  send(const void*, size_t);
    const int send(const void*, size_t) const;
    int  sendTo(const void*, size_t, int, uint16_t);
    bool setTimeout(int msec);
    const bool setTimeout(int msec) const;
    bool setBroadcast(bool broadcast);
    bool setBlocking(bool);

    uint32_t getHostAddress() const {
        return mHostAddr;
    }

    static const char* ip2string(const uint32_t);
    static uint32_t string2ip(std::string const&);
    static bool getHostAdaptorsByName(std::string const&, std::vector<uint32_t>*);
    static bool getHostAdaptorsByAddress(uint32_t const,  std::vector<uint32_t>*);
    static bool getLocalAdaptors(std::vector<uint32_t>*);

    //EventHandler<SocketEventArgs> OnDisconnect;

};
}
}

typedef helix::core::Socket hxSocket;



#endif /* SOCKET_H_ */
