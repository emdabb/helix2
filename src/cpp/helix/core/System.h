#ifndef HELIX_CORE_SYSTEM_H_
#define HELIX_CORE_SYSTEM_H_

#include <helix/Types.h>

namespace helix {
namespace core {

struct System {
    static int getNumProcessors();
};

}
}

#endif
