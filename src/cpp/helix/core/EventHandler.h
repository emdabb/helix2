#ifndef HELIX_CORE_EVENTHANDLER_H_
#define HELIX_CORE_EVENTHANDLER_H_

#include <vector>
#include <algorithm>
#include <helix/Types.h>

namespace helix {
namespace core {

struct EventArgs {
    virtual ~EventArgs() {}
    static EventArgs Empty;
};


template <typename T>
struct IHandlerFunction {
    virtual ~IHandlerFunction() {}
    virtual void call(const T&) = 0;
};

template <typename T>
struct HandlerFunction : public IHandlerFunction<T> {
    typedef void(*Delegate)(const T&);
    Delegate mFunctionPtr;

    explicit HandlerFunction(Delegate pFn)
    : mFunctionPtr(pFn) {
    }

    virtual ~HandlerFunction() {
    }

    virtual void call(const T& args) {
        mFunctionPtr(args);
    }
};

template <typename S, typename T>
    struct HandlerMethod : public IHandlerFunction<T>{
    typedef void(S::*Delegate)(const T&);
    S*          mClass;
    Delegate    mFunctionPtr;

    HandlerMethod(S* clazz, Delegate pFn)
    : mClass(clazz)
    , mFunctionPtr(pFn)
    {
    }

    virtual ~HandlerMethod() {
    }

    virtual void call(const T& args) {
        ((*mClass).*mFunctionPtr)(args);
    }
};

template <typename S, typename T>
    IHandlerFunction<T>* event(S* clazz, void(S::*pFn)(const T&)) { //)typename HandlerMethod<S, T>::Delegate pFn) {
    return new HandlerMethod<S, T>(clazz, pFn);
}

template <typename T>
IHandlerFunction<T>* event(void(*pFn)(const T&)) { //typename HandlerFunction<T>::Delegate pFn) {
    return new HandlerFunction<T>(pFn);
}



template <typename T>
class EventHandler {
    std::vector<IHandlerFunction<T>* > mHandlerFunctions;
public:
    EventHandler() {
    }

    virtual ~EventHandler() {
        mHandlerFunctions.clear();
    }

    void operator += (IHandlerFunction<T>* ptr) {
        mHandlerFunctions.push_back(ptr);
    }
    int operator () (const T& args) {
        for(size_t i=0; i < mHandlerFunctions.size(); i++) {
            mHandlerFunctions[i]->call(args);
        }
        return (int)mHandlerFunctions.size();
    }

    void operator -= (IHandlerFunction<T>* ptr) {
    	typename std::vector<IHandlerFunction<T>* >::iterator it = std::find(mHandlerFunctions.begin(), mHandlerFunctions.end(), ptr);
    	if(it != mHandlerFunctions.end()) {
    		mHandlerFunctions.erase(it);
    	}
    }

    void clear() {
    	mHandlerFunctions.clear();
    }
};

}
}

template <typename T>
struct hxEventHandler {
    typedef helix::core::EventHandler<T> Type;
};

typedef helix::core::EventArgs hxEventArgs;

#endif
