/*
 * math.h
 *
 *  Created on: Jun 3, 2014
 *      Author: miel
 */

#ifndef MATH_H_
#define MATH_H_

#include <helix/Types.h>
#include <limits.h>
#include <cmath>
#include <cstring>
#include <algorithm>

#define real_sqrt(x)		sqrtf(((x)))
#define real_tan(x)     	tanf (((x)))
#define real_cos(x)			cosf (((x)))
#define real_sin(x)			sinf (((x)))
#define real_acos(x)		acosf(((x)))
#define real_atan(x)		atanf(((x)))
#define real_atan2(y, x)	atan2f(((y)), ((x)))
#define real_abs(x)			fabsf(((x)))
#define real_fmod(x,y)		fmodf(((x)), ((y)))
#define real_pow(x, y)		powf(((x)), ((y)))
#define real_exp(x)			expf(((x)))
#define real_round(x)		roundf(((x)))
#define real_floor(x)		floorf(((x)))

namespace helix {
    namespace core {
        struct MathHelper {

            template<unsigned long N, typename P = void>
                struct factorial {
                    static const unsigned long value = N * factorial<N - 1, P>::value;
                };

            template<typename P>
                struct factorial<1, P> {
                    static const unsigned long value = 1;
                };

            __forceinline static float rsqrt(float x) {
#if 1
                float xhalf = 0.5f * x;
                uint32_t i;
                memcpy(&i, &x, sizeof(i));
                i = 0x5f3759df - (i >> 1);  // what the ...?
                memcpy(&x, &i, sizeof(x));
                x = x*(1.5f-(xhalf*x*x));
                return x;
#else
                float xhalf = 0.5f * x;
                uint32_t i = *reinterpret_cast<uint32_t*>(&x);
                i = 0x5f3759df - (i >> 1);
                x = *reinterpret_cast<float*>(&i);
                x = x * (1.5f - (xhalf * x * x));
                return x;
#endif
            }
            // r1 = (-b - sqrt(d)) / (2 * a)
            // r2 = -2 * c / (b + sqrt(d))
            template<typename T>
                inline static bool solve_quadratic(const T& a, const T& b, const T& c,
                        T* t0, T* t1) {
                    T d = b * b - (T) 4. * a * c;
                    if (d < (T) 0.) {
                        return false;
                    } else if (d == (T) 0.) {
                        *t0 = *t1 = (T) -.5 * b / a;
                    } else {
                        T q = (b > (T) 0.) ?
                            (T) -.5 * (b + real_sqrt(d)) : (T) -.5 * (b - real_sqrt(d));
                        *t0 = q / a;
                        *t1 = c / q;
                    }
                    if (*t0 > *t1) {
                        std::swap(*t0, *t1);
                    }
                    return true;
                }

            template<typename T>
                static inline T next_pow2(const T& value) {
                    T n = value;
                    n--;
                    n |= n >> 1;   // Divide by 2^k for consecutive doublings of k up to 32,
                    n |= n >> 2;   // and then or the results.
                    n |= n >> 4;
                    n |= n >> 8;
                    n |= n >> 16;
                    n++;           // The result is a number of 1 bits equal to the number
                    // of bits in the original number, plus 1. That's the
                    // next highest power of 2.
                    return n;
                }

            static bool within_epsilon(real const& a) {
                return (((real) -1.401298E-45f <= a) && (a <= EPSILON));
            }

            inline static real to_rad(const real& a) {
                return a * PI / (real) 180.;
            }

            inline static real to_deg(const real& a) {
                return a * (real) 180. * ONE_OVER_PI;
            }

            inline static unsigned int iabs(int v) {
                unsigned int r;
                int const mask = (v >> sizeof(int)) * (CHAR_BIT - 1);
                r = (v ^ mask) - mask;
                return r;
            }

            inline static bool is_pow2(unsigned int v) {
                return v && !(v & (v - 1));
            }

            inline static uint32_t toggle_bit(uint32_t word, uint32_t mask, bool b) {
                uint32_t w = word;
                w ^= (-b ^ word) & mask;
                return w;
            }

            inline static real sigmoid(real const& x, real const& alpha = ONE) {
                //
                // f(x) = 0.5 * (x * alpha / (1 + abs(x*alpha)) + 0.5
                //
                return 0.5f * (x * alpha / (ONE + real_abs(x * alpha)) + (real) 0.5f);
            }

            __forceinline static float lerp(float a, float b, float t) {
            	return (1.f - t) * a + t * b;
            }

            static const real ZERO;
            static const real ONE;
            static const real TWO;
            static const real EPSILON; /* e */
            static const real LOG2E; /* log_2 e */
            static const real LOG10E; /* log_10 e */
            static const real LOGE2; /* log_e 2 */
            static const real LOGE10; /* log_e 10 */
            static const real PI; /* pi */
            static const real TWO_PI; /* 2 pi */
            static const real PI_OVER_2; /* pi/2 */
            static const real PI_OVER_4; /* pi/4 */
            static const real PI_OVER_180; /* pi/180 */
            static const real ONE_OVER_PI; /* 1/pi */
            static const real TWO_OVER_PI; /* 2/pi */
            static const real TWO_OVER_SQRT_PI; /* 2/sqrt(pi) */
            static const real SQRT_2; /* sqrt(2) */
            static const real ONE_OVER_SQRT_2; /* 1/sqrt(2) */
            static const real PHI;

        };
    }
}

typedef helix::core::MathHelper hxMathHelper;

#endif /* MATH_H_ */
