/*
 * Easing.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#include <helix/core/Easing.h>
#include <helix/core/MathHelper.h>

using namespace helix::core;

Easing::EasingFunction Easing::Delegate[NumEasingFunctions] = {
	&Easing::EaseLinearIn,
	&Easing::EaseLinearOut,
	&Easing::EaseLinearInOut,
	&Easing::EaseSineIn,
	&Easing::EaseSineOut,
	&Easing::EaseSineInOut,
	&Easing::EaseQuadraticIn,
	&Easing::EaseQuadraticOut,
	&Easing::EaseQuadraticInOut,
	&Easing::EaseCubicIn,
	&Easing::EaseCubicOut,
	&Easing::EaseCubicInOut,
	&Easing::EaseQuinticIn,
	&Easing::EaseQuinticOut,
	&Easing::EaseQuinticInOut,
	&Easing::EaseExponentialIn,
	&Easing::EaseExponentialOut,
	&Easing::EaseExponentialInOut,
	&Easing::EaseCircleIn,
	&Easing::EaseCircleOut,
	&Easing::EaseCircleInOut,
	&Easing::EaseBackIn,
	&Easing::EaseBackOut,
	&Easing::EaseBackInOut,
	&Easing::EaseElasticIn,
	&Easing::EaseElasticOut,
	&Easing::EaseElasticInOut,
	&Easing::EaseBounceIn,
	&Easing::EaseBounceOut,
	&Easing::EaseBounceInOut
};

float Easing::EaseLinearIn(float p) {
	return p;
}
float Easing::EaseLinearOut(float p) {
	return  1.f - p;
}
float Easing::EaseLinearInOut(float p) {
	return p;
}
float Easing::EaseSineIn(float p) {
	return real_sin((p - 1) * MathHelper::PI_OVER_2) + 1;
}
float Easing::EaseSineOut(float p) {
	return real_sin(p * MathHelper::PI_OVER_2);
}
float Easing::EaseSineInOut(float p) {
	return 0.5f * (1.f - real_cos(p * MathHelper::PI));
}
float Easing::EaseQuadraticIn(float p) {
	return p * p;
}
float Easing::EaseQuadraticOut(float p) {
	/**
	 * Modeled after the parabola y = -x^2 + 2x
	 */
	return -(p * (p - 2));
}
float Easing::EaseQuadraticInOut(float p) {
	/**
	 * Modeled after the piecewise quadratic
	 * y = (1/2)((2x)^2)             ; [0, 0.5)
	 * y = -(1/2)((2x-1)*(2x-3) - 1) ; [0.5, 1]
	 */
	if(p < 0.5f) {
		return 2 * p * p;
	} else {
		return (-2 * p * p) + (4 * p) - 1;
	}
}

float Easing::EaseCubicIn(float p) {
	return p * p * p;
}

float Easing::EaseCubicOut(float p) {
	float f = (p - 1);
	return f * f * f + 1.0f;
}

float Easing::EaseCubicInOut(float p) {
	if(p < 0.5) {
		return 4 * p * p * p;
	} else {
		float f = ((2 * p) - 2);
		return 0.5f * f * f * f + 1;
	}
}
float Easing::EaseQuarticIn(float p) {
	return p * p * p * p;
}
float Easing::EaseQuarticOut(float p) {
	float f = (p - 1);
	return f * f * f * (1 - p) + 1;
}
float Easing::EaseQuarticInOut(float p) {
	if(p < 0.5)
	{
		return 8 * p * p * p * p;
	}
	else
	{
		float f = (p - 1);
		return -8 * f * f * f * f + 1;
	}
}
float Easing::EaseQuinticIn(float p) {
	return p * p * p * p * p;
}
float Easing::EaseQuinticOut(float p) {
	float f = (p - 1);
	return f * f * f * f * f + 1;
}
float Easing::EaseQuinticInOut(float p) {
	if(p < 0.5)
	{
		return 16 * p * p * p * p * p;
	}
	else
	{
		float f = ((2 * p) - 2);
		return  0.5 * f * f * f * f * f + 1;
	}
}
float Easing::EaseExponentialIn(float p) {
	return (p == 0.0) ? p : real_pow(2, 10 * (p - 1));
}
float Easing::EaseExponentialOut(float p) {
	return (p == 1.0) ? p : 1 - real_pow(2, -10 * p);
}
float Easing::EaseExponentialInOut(float p) {
	if(p == 0.0 || p == 1.0) return p;
	if(p < 0.5)	{
		return 0.5 * real_pow(2, (20 * p) - 10);
	} else {
		return -0.5 * real_pow(2, (-20 * p) + 10) + 1;
	}
}

float Easing::EaseCircleIn(float p) {
	return 1 - real_sqrt(1 - (p * p));
}

float Easing::EaseCircleOut(float p) {
	return real_sqrt((2 - p) * p);
}

float Easing::EaseCircleInOut(float p) {
	if(p < 0.5)
	{
		return 0.5 * (1 - real_sqrt(1 - 4 * (p * p)));
	}
	else
	{
		return 0.5 * (real_sqrt(-((2 * p) - 3) * ((2 * p) - 1)) + 1);
	}
}

float Easing::EaseBackIn(float p) {
	return p * p * p - p * real_sin(p * MathHelper::PI);
}
float Easing::EaseBackOut(float p) {
	float f = (1 - p);
	return 1 - (f * f * f - f * real_sin(f * MathHelper::PI));
}
float Easing::EaseBackInOut(float p) {
	if(p < 0.5)
	{
		float f = 2 * p;
		return 0.5 * (f * f * f - f * real_sin(f * MathHelper::PI));
	}
	else
	{
		float f = (1 - (2*p - 1));
		return 0.5 * (1 - (f * f * f - f * real_sin(f * MathHelper::PI))) + 0.5;
	}
}

float Easing::EaseElasticIn(float p) {
	return real_sin(13 * MathHelper::PI_OVER_2 * p) * real_pow(2, 10 * (p - 1));
}

float Easing::EaseElasticOut(float p) {
	return real_sin(-13 * MathHelper::PI_OVER_2 * (p + 1)) * real_pow(2, -10 * p) + 1;
}

float Easing::EaseElasticInOut(float p) {
	if(p < 0.5)	{
		return 0.5 * real_sin(13 * MathHelper::PI_OVER_2 * (2 * p)) * real_pow(2, 10 * ((2 * p) - 1));
	} else	{
		return 0.5 * (real_sin(-13 * MathHelper::PI_OVER_2 * ((2 * p - 1) + 1)) * real_pow(2, -10 * (2 * p - 1)) + 2);
	}
}

float Easing::EaseBounceIn(float p) {
	return 1 - EaseBounceOut(1 - p);
}

float Easing::EaseBounceOut(float p) {
	if(p < 4/11.0) {
		return (121 * p * p)/16.0;
	} else if(p < 8/11.0) {
		return (363/40.0 * p * p) - (99/10.0 * p) + 17/5.0;
	} else if(p < 9/10.0) {
		return (4356/361.0 * p * p) - (35442/1805.0 * p) + 16061/1805.0;
	} else {
		return (54/5.0 * p * p) - (513/25.0 * p) + 268/25.0;
	}
}

float Easing::EaseBounceInOut(float p) {
	if(p < 0.5) {
		return 0.5 * EaseBounceIn(p*2);
	} else	{
		return 0.5 * EaseBounceOut(p * 2 - 1) + 0.5;
	}
}
