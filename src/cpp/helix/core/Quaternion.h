#ifndef HELIX_CORE_QUATERNION_H_
#define HELIX_CORE_QUATERNION_H_

#include <helix/Types.h>
#include <helix/core/MathHelper.h>

namespace helix {
namespace core{

struct Vector3;

__declare_aligned(struct, 16) HXEXPORT Quaternion {
    real X, Y, Z, W;

  typedef 		Quaternion* ptr;
  typedef 		Quaternion& ref;
  typedef const Quaternion& const_ref;

  static const Quaternion Identity;

  inline static void createfromYawPitchRoll(const real& yaw, const real& pitch, const real& roll, ptr out) {

     float y_half = yaw 	* (real).5;
     float p_half = pitch 	* (real).5;
     float r_half = roll 	* (real).5;

     float r_sin = real_sin(r_half);
     float r_cos = real_cos(r_half);

     float p_sin = real_sin(p_half);
     float p_cos = real_cos(p_half);

     float y_sin = real_sin(y_half);
     float y_cos = real_cos(y_half);

     out->X = ((y_cos * p_sin) * r_cos) + ((y_sin * p_cos) * r_sin);
     out->Y = ((y_sin * p_cos) * r_cos) - ((y_cos * p_sin) * r_sin);
     out->Z = ((y_cos * p_cos) * r_sin) - ((y_sin * p_sin) * r_cos);
     out->W = ((y_cos * p_cos) * r_cos) + ((y_sin * p_sin) * r_sin);
  }

  static Quaternion createFromAxisAngle(const Vector3& axis, float angle);
  /**
   *  @brief finds a quaternion representing the rotation between two 3D vectors.
   */
  static Quaternion createFromVectors(const Vector3& a, const Vector3& b);

  inline static void dot(const_ref a, const_ref b, real* out) {
    *out = a.X * b.X + a.Y * b.Y + a.Z * b.Z + a.W * b.W;
  }

  inline static real dot(const_ref a, const_ref b) {
    real out;
    dot(a, b, &out);
    return out;
  }

  inline static real length(const_ref a) {
    real out;
    length(a, &out);
    return out;
  }

  inline static void length(const_ref a, real* len) {
    *len = real_sqrt(dot(a, a));
  }

  inline static void normalize(ptr a) {
    real t = (real)1. / length(*a);
    a->X *= t;
    a->Y *= t;
    a->Z *= t;
    a->W *= t;
  }

  inline static Quaternion normalized(const_ref a) {
    Quaternion out;
    normalized(a, &out);
    return out;
  }

  static void getDirectionVector(const Quaternion& q, Vector3*);

  __forceinline static void normalized(const_ref a, ptr out) {
    real t = (real)1. / dot(a, a);
    out->X = a.X * t;
    out->Y = a.Y * t;
    out->Z = a.Z * t;
    out->W = a.W * t;
  }

  __forceinline static void slerp(const_ref a, const_ref b, real const& c, ptr d) {
    if (c <= (real)0.) {
      *d = a;
      return;
    }
    if (c >= (real)1.) {
      *d = b;
      return;
    }

    Quaternion q3 = b;
    real t;
    dot(a, q3, &t);

    if (t < (real)0.) {
      q3 = -q3;
      t = -t;
    }

    if (c > (real)1. - MathHelper::EPSILON) {
      normalized(lerp(a, q3, t), d); // Lerp() = q1 + t * (q2 - q1)
      return;
    }

    real ac = real_acos(c); // ACos() clamps input to [-1, 1]
    *d = (a * real_sin((1 - t) * ac) + q3 * real_sin(t * ac)) / real_sin(ac);
  }

  __forceinline  static Quaternion lerp(const_ref a, const_ref b, const real& c) {
    Quaternion out;
    lerp(a, b, c, &out);
    return out;
  }

  __forceinline  static void lerp(const_ref a, const_ref b, const real& c, ptr out) {
    out->X = MathHelper::lerp(a.X, b.X, c);
    out->Y = MathHelper::lerp(a.Y, b.Y, c);
    out->Z = MathHelper::lerp(a.Z, b.Z, c);
    out->W = MathHelper::lerp(a.W, b.W, c);
  }

  __forceinline  static Quaternion conjugate(const_ref a) {
    Quaternion out;
    conjugate(a, &out);
    return out;
  }

  __forceinline  static void conjugate(const_ref a, ptr out) {
    out->X = -a.X;
    out->Y = -a.Y;
    out->Z = -a.Z;
    out->W =  a.W;
  }

  __forceinline Quaternion& operator = (const Quaternion& b) {
    X = b.X;
    Y = b.Y;
    Z = b.Z;
    W = b.W;
    return *this;
  }

  __forceinline  Quaternion operator - () const {
    Quaternion copy(*this);
    copy.X = -copy.X;
    copy.Y = -copy.Y;
    copy.Z = -copy.Z;
    copy.W = -copy.W;
    return copy;
  }

  __forceinline  Quaternion operator * (const real& a) const {
    Quaternion copy(*this);
    copy *= a;
    return copy;
  }

  __forceinline  Quaternion& operator *= (const real& a) {
    this->X *= a;
    this->Y *= a;
    this->Z *= a;
    this->W *= a;
    return *this;
  }

  __forceinline  Quaternion operator * (const_ref a) const {
    Quaternion copy(*this);
    copy *= a;
    return copy;
  }

  __forceinline  Quaternion& operator *= (const_ref a) {
    real xx = W * a.X + X * a.W + Z * a.Y - Y * a.Z;
    real yy = W * a.Y + Y * a.W + X * a.Z - Z * a.X;
    real zz = W * a.Z + Z * a.W + Y * a.X - X * a.Y;
    real ww = W * a.W - X * a.X - Y * a.Y - Z * a.Z;

    this->X = xx;
    this->Y = yy;
    this->Z = zz;
    this->W = ww;
    return *this;
  }

  __forceinline  Quaternion operator + (const_ref a) const {
    Quaternion copy(*this);
    copy += a;
    return copy;
  }

  __forceinline  Quaternion& operator += (const_ref a) {
    this->X += a.X;
    this->Y += a.Y;
    this->Z += a.Z;
    this->W += a.W;
    return *this;
  }

  __forceinline  Quaternion operator / (const real& a) const {
    Quaternion copy(*this);
    copy /= a;
    return copy;
  }

  __forceinline  Quaternion& operator /= (const real& a) {
    this->X /= a;
    this->Y /= a;
    this->Z /= a;
    this->W /= a;
    return *this;
  }

};

}
}

typedef helix::core::Quaternion hxQuaternion;

#endif
