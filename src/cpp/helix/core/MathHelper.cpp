/*
 * math.cpp
 *
 *  Created on: Jun 3, 2014
 *      Author: miel
 */

#include <helix/core/MathHelper.h>
#include <limits>

using namespace helix;
using namespace helix::core;

const real MathHelper::ZERO= (real)0.;
const real MathHelper::ONE= (real)1.;
const real MathHelper::TWO= (real)2.;
const real MathHelper::EPSILON= std::numeric_limits<real>::epsilon(); 	/* e */
const real MathHelper::LOG2E 	= 1.4426950408889634074f;		 	/* log_2 e */
const real MathHelper::LOG10E = 0.4342944819032518277f;		 	/* log_10 e */
const real MathHelper::LOGE2 	= 0.6931471805599453094f;		 	/* log_e 2 */
const real MathHelper::LOGE10 = 2.3025850929940456840f;		 	/* log_e 10 */
const real MathHelper::PI 	= 3.1415926535897932385f;	 		/* pi */
const real MathHelper::TWO_PI = 6.2831853071795864769f; 			/* 2 pi */
const real MathHelper::PI_OVER_2	= 1.57079632679489661923f;		/* pi/2 */
const real MathHelper::PI_OVER_4	= 0.78539816339744830962f; 		/* pi/4 */
const real MathHelper::PI_OVER_180= 0.01745329251994329576f;		/* pi/180 */
const real MathHelper::ONE_OVER_PI= 0.31830988618379067154f; 		/* 1/pi */
const real MathHelper::TWO_OVER_PI= 0.63661977236758134308f; 		/* 2/pi */
const real MathHelper::SQRT_2 	= 1.41421356237309504880f;		/* sqrt(2) */
const real MathHelper::TWO_OVER_SQRT_PI 	= 1.12837916709551257390f; /* 2/sqrt(pi) */
const real MathHelper::ONE_OVER_SQRT_2 	= 0.70710678118654752440f; /* 1/sqrt(2) */
const real MathHelper::PHI = (real)1.f + real_sqrt(5.f / 2.f);

/**
#if QQQ_PRECISION == QQQ_PRECISION_FIXED
template<typename T, uint8_t I, uint8_t F>
const fixed<T, I, F> fixed<T, I, F>::PI = fixed<T, I, F>(math::PI);

template<typename T, uint8_t I, uint8_t F>
const fixed<T, I, F> fixed<T, I, F>::TWO_PI= fixed<T, I, F>(math::TWO_PI);

template<typename T, uint8_t I, uint8_t F>
const fixed<T, I, F> fixed<T, I, F>::ONE = static_cast<T>(1.);//fixed<T, I, F>::pow2<F>::value);
*/

//#endif

