/*
 * Curve.h
 *
 *  Created on: Aug 19, 2015
 *      Author: miel
 */

#ifndef HELIX_CORE_CURVE_H_
#define HELIX_CORE_CURVE_H_

#include <helix/Types.h>
#include <vector>

namespace helix {
namespace core {

struct CurveKey {
	real position;
	real value;
};

class Curve {
	std::vector<CurveKey> mPoints;
	real mDeltaTime;
	real mTimeRange;
protected:
	real interpolate(real const& t, real const& p1, real const& p2, real const& p3, real const& p4);
public:
	Curve();
	virtual ~Curve();
	void addPoint(real const& time, real const& vec);
	real evaluate(real const& at);
	size_t size() const;
	real timeRange() const;
	real timeRangeInv() const;

};

}
}


#endif /* HELIX_CORE_CURVE_H_ */
