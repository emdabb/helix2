#include <helix/core/Vector2.h>
#include <helix/core/RandomNumber.h>

using namespace helix::core;

const Vector2 Vector2::Zero = { 0, 0 };
const Vector2 Vector2::One  = { 1, 1 };
const Vector2 Vector2::Up   = { 0, 1 };
const Vector2 Vector2::Right= { 1, 0 };


Vector2 Vector2::createRandom(int seed) {
    Vector2 out;
    createRandom(seed, &out);
    return out;
}
void Vector2::createRandom(int seed, ptr out) {
    RandomNumber rnd(seed);
    out->X = rnd.next() * (float) 2. - (float) 1.;
    out->Y = rnd.next() * (float) 2. - (float) 1.;
}
float Vector2::toAngle(const_ref in) {
    float out;
    toAngle(in, &out);
    return out;
}
void Vector2::toAngle(const_ref in, float* out) {
    *out = real_atan2(in.Y, in.X);
}

void Vector2::normalized(const_ref a, Vector2* out) {
	float dp   = a.X * a.X + a.Y * a.Y;
    float fLen = (float) 1. / real_sqrt(dp);
    out->X = a.X * fLen;
    out->Y = a.Y * fLen;
}

void Vector2::normalize(ptr out) {
	normalized(*out, out);
//	float r  = (float)1.f / length(*out);
//	out->X *= r;
//	out->Y *= r;
}

Vector2 Vector2::normalized(const_ref a) {
    Vector2 res;
    normalized(a, &res);
    return res;
}
