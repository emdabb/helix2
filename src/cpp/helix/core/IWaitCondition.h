#ifndef HELIX_CORE_IWAITCONDITION_H_
#define HELIX_CORE_IWAITCONDITION_H_

#include <helix/Types.h>

namespace helix {
namespace core {

struct IWaitCondition {
    virtual ~IWaitCondition() {}
    virtual void wait() = 0;
    virtual void set() = 0;
    virtual void reset() = 0;
};

}
}

typedef helix::core::IWaitCondition hxWaitCondition;

HXAPI HXEXPORT hxWaitCondition* new_WaitCondition();

#endif
