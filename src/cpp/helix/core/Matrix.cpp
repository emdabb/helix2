#include "Matrix.h"
#include "Vector3.h"
#include "MathHelper.h"
#include "Quaternion.h"

using namespace helix::core;

#if defined(HELIX_HAVE_SSE)

static inline __m128 lincomb_sse(const __m128 &a, const Matrix &B)
{
    __m128 result;
    result = _mm_mul_ps(_mm_shuffle_ps(a, a, 0x00), B.row[0]);
    result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(a, a, 0x55), B.row[1]));
    result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(a, a, 0xaa), B.row[2]));
    result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(a, a, 0xff), B.row[3]));
    return result;
}

// this is the right approach for SSE ... SSE4.2
void matmul4_sse(const Matrix &A, const Matrix &B, Matrix *out)
{
    __m128 out0x = lincomb_sse(A.row[0], B);
    __m128 out1x = lincomb_sse(A.row[1], B);
    __m128 out2x = lincomb_sse(A.row[2], B);
    __m128 out3x = lincomb_sse(A.row[3], B);

    out->row[0] = out0x;
    out->row[1] = out1x;
    out->row[2] = out2x;
    out->row[3] = out3x;
}

#endif

static void matmul4_c(const float* m0, const float* m1, float* d) {
	d[ 0] = (m0[ 0] * m1[0]) + (m0[ 1] * m1[4]) + (m0[ 2] * m1[ 8]) + (m0[ 3] * m1[12]);
	d[ 1] = (m0[ 0] * m1[1]) + (m0[ 1] * m1[5]) + (m0[ 2] * m1[ 9]) + (m0[ 3] * m1[13]);
	d[ 2] = (m0[ 0] * m1[2]) + (m0[ 1] * m1[6]) + (m0[ 2] * m1[10]) + (m0[ 3] * m1[14]);
	d[ 3] = (m0[ 0] * m1[3]) + (m0[ 1] * m1[7]) + (m0[ 2] * m1[11]) + (m0[ 3] * m1[15]);

	d[ 4] = (m0[ 4] * m1[0]) + (m0[ 5] * m1[4]) + (m0[ 6] * m1[ 8]) + (m0[ 7] * m1[12]);
	d[ 5] = (m0[ 4] * m1[1]) + (m0[ 5] * m1[5]) + (m0[ 6] * m1[ 9]) + (m0[ 7] * m1[13]);
	d[ 6] = (m0[ 4] * m1[2]) + (m0[ 5] * m1[6]) + (m0[ 6] * m1[10]) + (m0[ 7] * m1[14]);
	d[ 7] = (m0[ 4] * m1[3]) + (m0[ 5] * m1[7]) + (m0[ 6] * m1[11]) + (m0[ 7] * m1[15]);

	d[ 8] = (m0[ 8] * m1[0]) + (m0[ 9] * m1[4]) + (m0[10] * m1[ 8]) + (m0[11] * m1[12]);
	d[ 9] = (m0[ 8] * m1[1]) + (m0[ 9] * m1[5]) + (m0[10] * m1[ 9]) + (m0[11] * m1[13]);
	d[10] = (m0[ 8] * m1[2]) + (m0[ 9] * m1[6]) + (m0[10] * m1[10]) + (m0[11] * m1[14]);
	d[11] = (m0[ 8] * m1[3]) + (m0[ 9] * m1[7]) + (m0[10] * m1[11]) + (m0[11] * m1[15]);

	d[12] = (m0[12] * m1[0]) + (m0[13] * m1[4]) + (m0[14] * m1[ 8]) + (m0[15] * m1[12]);
	d[13] = (m0[12] * m1[1]) + (m0[13] * m1[5]) + (m0[14] * m1[ 9]) + (m0[15] * m1[13]);
	d[14] = (m0[12] * m1[2]) + (m0[13] * m1[6]) + (m0[14] * m1[10]) + (m0[15] * m1[14]);
	d[15] = (m0[12] * m1[3]) + (m0[13] * m1[7]) + (m0[14] * m1[11]) + (m0[15] * m1[15]);
}

const Matrix Matrix::Identity = {
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1
};

void Matrix::mul(const_ref a, const_ref b, ptr c) {
#if defined(HELIX_HAVE_SSE)
    matmul4_sse(a, b, c);
#elif defined(HELIX_HAVE_ARM_NEON)
    matmul4_neon(&a.M11, &b.M11, &c->M11);
#else
    matmul4_c(&a.M11, &b.M11, &c->M11);
#endif
}

Matrix Matrix::mul(const_ref a, const_ref b) {
    Matrix c;
    mul(a, b, &c);
    return c;
}

void Matrix::transpose(const_ref in, ptr out) {
    out->M11 = in.M11;
    out->M12 = in.M21;
    out->M13 = in.M31;
    out->M14 = in.M41;

    out->M21 = in.M12;
    out->M22 = in.M22;
    out->M23 = in.M32;
    out->M24 = in.M42;

    out->M31 = in.M13;
    out->M32 = in.M23;
    out->M33 = in.M33;
    out->M34 = in.M43;

    out->M41 = in.M14;
    out->M42 = in.M24;
    out->M43 = in.M34;
    out->M44 = in.M44;
}


void Matrix::createLookat(Vector3 const& eye, Vector3 const& at, Vector3 const& up, ptr out) {
    Vector3 vz = Vector3::normalized(eye - at);

    Vector3 vx = Vector3::cross(up, vz);

    if (Vector3::dot(vx, vx) < MathHelper::EPSILON)
        vx = Vector3::Right;
    else
        vx = Vector3::normalized(vx);

    Vector3 vy = Vector3::normalized(Vector3::cross(vz, vx));


    out->M11 = vx.X;  out->M12 = vy.X;  out->M13 = vz.X; out->M14 = (float)0.;
    out->M21 = vx.Y;  out->M22 = vy.Y;  out->M23 = vz.Y; out->M24 = (float)0.;
    out->M31 = vx.Z;  out->M32 = vy.Z;  out->M33 = vz.Z; out->M34 = (float)0.;

    out->M41 = Vector3::dot(vx, -eye);
    out->M42 = Vector3::dot(vy, -eye);
    out->M43 = Vector3::dot(vz, -eye);

    out->M44 = (float)1.;
}

void Matrix::determinant(const_ref a, float* out) {
    float num22 = a.M11;
    float num21 = a.M12;
    float num20 = a.M13;
    float num19 = a.M14;
    float num12 = a.M21;
    float num11 = a.M22;
    float num10 = a.M23;
    float num9  = a.M24;
    float num8  = a.M31;
    float num7  = a.M32;
    float num6  = a.M33;
    float num5  = a.M34;
    float num4  = a.M41;
    float num3  = a.M42;
    float num2  = a.M43;
    float num   = a.M44;
    float num18 = (num6 * num) - (num5 * num2);
    float num17 = (num7 * num) - (num5 * num3);
    float num16 = (num7 * num2) - (num6 * num3);
    float num15 = (num8 * num) - (num5 * num4);
    float num14 = (num8 * num2) - (num6 * num4);
    float num13 = (num8 * num3) - (num7 * num4);
    *out =    ((num22 * ((((num11 * num18)) - ((num10 * num17))) + ((num9 * num16))))
            - ((num21 * ((((num12 * num18)) - ((num10 * num15))) + ((num9 * num14)))))
            + ((num20 * ((((num12 * num17)) - ((num11 * num15))) + ((num9 * num13)))))
            - ((num19 * ((((num12 * num16)) - ((num11 * num14))) + ((num10 * num13))))));
}

void Matrix::createPerspectiveFov(float const& fov, float const& ratio, float const& tnear, float const& tfar, ptr out) {
    float num = 1 / real_tan(fov * 0.5f);
    float num9 = num / ratio;
    out->M11 = num9;
    out->M12 = (float)0.;
    out->M13 = (float)0.;
    out->M14 = (float)0.;

    out->M21 = (float)0.;
    out->M22 = num;
    out->M23 = (float)0.;
    out->M24 = (float)0.;

    out->M31 = (float)0.;
    out->M32 = (float)0.;
    out->M33 = tfar / (tnear - tfar);
    out->M34 = -1.0f;

    out->M41 = (float)0.;
    out->M42 = (float)0.;
    out->M43 = (tnear * tfar) / (tnear - tfar);
    out->M44 = (float)0.;
}

void Matrix::createOrthoOffcentre(float const& lt, float const& rt, float const& bt, float const& tp, float const& tnear, float const& tfar, ptr out) {
    out->M11 = (float)2. / (rt - lt);
    out->M12 = (float)0.;
    out->M13 = (float)0.;
    out->M14 = (float)0.;
    out->M21 = (float)0.;
    out->M22 = (float)2. / (tp - bt);
    out->M23 = (float)0.;
    out->M24 = (float)0.;
    out->M31 = (float)0.;
    out->M32 = (float)0.;
    out->M33 = (float)1.f / (tnear - tfar);
    out->M34 = (float)0.;
    out->M41 = (lt + rt) / (lt - rt);
    out->M42 = (bt + tp) / (bt - tp);
    out->M43 = tnear / (tnear - tfar);
    out->M44 = (float)1.;
}

void Matrix::createRotation(float const& rx, float const& ry, float const& rz, ptr out) {
    Matrix mrx, mry, mrz;
    Matrix::createRotationX(rx, &mrx);
    Matrix::createRotationY(ry, &mry);
    Matrix::createRotationZ(rz, &mrz);
    Matrix mrw;
    Matrix::mul(mrx, mry, &mrw);
    Matrix::mul(mrw, mrz,  out);
}

void Matrix::createRotationX(float const& a, ptr out) {
    *out = Matrix::Identity;
    float ct = real_cos(a);
    float st = real_sin(a);
    out->M22 =  ct;
    out->M23 = -st;
    out->M32 =  st;
    out->M33 =  ct;
}

void Matrix::createRotationY(float const& a, ptr out) {
    *out = Matrix::Identity;
    float ct = real_cos(a);
    float st = real_sin(a);
    out->M11 = ct;
    out->M13 = st;
    out->M31 = -st;
    out->M33 = ct;

}

void Matrix::createRotationZ(float const& a, ptr out) {
    *out = Matrix::Identity;
    float ct = real_cos(a);
    float st = real_sin(a);
    out->M11 =  ct;
    out->M12 = -st;
    out->M21 =  st;
    out->M22 =  ct;
}

void Matrix::createWorld(Vector3 const& pos, Vector3 const& dir, Vector3 const& up, ptr out){
    Vector3 x, y, z;
    z = Vector3::normalized(dir);
    x = Vector3::normalized(Vector3::cross(dir, up));
    y = Vector3::normalized(Vector3::cross(x, dir));

    out->M11 =  x.X;
    out->M12 =  x.Y;
    out->M13 =  x.Z;
    out->M14 =  (float)0.;
    out->M21 =  y.X;
    out->M22 =  y.Y;
    out->M23 =  y.Z;
    out->M24 = (float)0.;
    out->M31 = -z.X;
    out->M32 = -z.Y;
    out->M33 = -z.Z;
    out->M34 = (float)0.;
    out->M41 =  pos.X;
    out->M42 =  pos.Y;
    out->M43 =  pos.Z;
    out->M44 =  (float)1.;
}

void Matrix::createScale(Vector3 const& in, ptr out) {
    *out = Matrix::Identity;
    out->M11 = in.X;
    out->M22 = in.Y;
    out->M33 = in.Z;
}

void Matrix::createScale(float const& x, float const& y, float const& z, ptr out) {
    *out = Matrix::Identity;
    out->M11 = x;
    out->M22 = y;
    out->M33 = z;
}

void Matrix::createTranslation(Vector3 const& in, ptr out) {
    *out = Matrix::Identity;
    out->M41 = in.X;
    out->M42 = in.Y;
    out->M43 = in.Z;
}

void Matrix::createTranslation(float const& x, float const& y, float const& z, ptr out) {
    *out = Matrix::Identity;
    out->M41 = x;
    out->M42 = y;
    out->M43 = z;
}

void Matrix::createRotation(Quaternion const& in, ptr out) {
    *out = Matrix::Identity;
    float xx = in.X * in.X;
    float yy = in.Y * in.Y;
    float zz = in.Z * in.Z;
    float xy = in.X * in.Y;
    float zw = in.Z * in.W;
    float zx = in.Z * in.X;
    float yw = in.Y * in.W;
    float yz = in.Y * in.Z;
    float xw = in.X * in.W;

    out->M11 = MathHelper::ONE - (MathHelper::TWO * (yy + zz));
    out->M12 = MathHelper::TWO * (xy + zw);
    out->M13 = MathHelper::TWO * (zx - yw);
    out->M14 = MathHelper::ZERO;
    out->M21 = MathHelper::TWO * (xy - zw);
    out->M22 = MathHelper::ONE - (MathHelper::TWO * (zz + xx));
    out->M23 = MathHelper::TWO * (yz + xw);
    out->M24 = MathHelper::ZERO;
    out->M31 = MathHelper::TWO * (zx + yw);
    out->M32 = MathHelper::TWO * (yz - xw);
    out->M33 = MathHelper::ONE - (MathHelper::TWO * (yy + xx));
    out->M34 = MathHelper::ZERO;
    out->M41 = MathHelper::ZERO;
    out->M42 = MathHelper::ZERO;
    out->M43 = MathHelper::ZERO;
    out->M44 = MathHelper::ONE;
}

Matrix Matrix::inverted(const_ref in) {
    Matrix out;
    invert(in, &out);
    return out;
}

void Matrix::invert(const_ref in, ptr out) {
    real num23 = (in.M33 * in.M44) - (in.M34 * in.M43);
    real num22 = (in.M32 * in.M44) - (in.M34 * in.M42);
    real num21 = (in.M32 * in.M43) - (in.M33 * in.M42);
    real num20 = (in.M31 * in.M44) - (in.M34 * in.M41);
    real num19 = (in.M31 * in.M43) - (in.M33 * in.M41);
    real num18 = (in.M31 * in.M42) - (in.M32 * in.M41);
    real num39 = ((in.M22 * num23) - (in.M23 * num22)) + (in.M24 * num21);
    real num38 = -(((in.M21 * num23) - (in.M23 * num20)) + (in.M24 * num19));
    real num37 = ((in.M21 * num22) - (in.M22 * num20)) + (in.M24 * num18);
    real num36 = -(((in.M21 * num21) - (in.M22 * num19)) + (in.M23 * num18));
    real num = (real) 1 / ((((in.M11 * num39) + (in.M12 * num38)) + (in.M13 * num37)) + (in.M14 * num36));
    out->M11 = num39 * num;
    out->M21 = num38 * num;
    out->M31 = num37 * num;
    out->M41 = num36 * num;
    out->M12 = -(((in.M12 * num23) - (in.M13 * num22)) + (in.M14 * num21)) * num;
    out->M22 = (((in.M11 * num23) - (in.M13 * num20)) + (in.M14 * num19)) * num;
    out->M32 = -(((in.M11 * num22) - (in.M12 * num20)) + (in.M14 * num18)) * num;
    out->M42 = (((in.M11 * num21) - (in.M12 * num19)) + (in.M13 * num18)) * num;
    real num35 = (in.M23 * in.M44) - (in.M24 * in.M43);
    real num34 = (in.M22 * in.M44) - (in.M24 * in.M42);
    real num33 = (in.M22 * in.M43) - (in.M23 * in.M42);
    real num32 = (in.M21 * in.M44) - (in.M24 * in.M41);
    real num31 = (in.M21 * in.M43) - (in.M23 * in.M41);
    real num30 = (in.M21 * in.M42) - (in.M22 * in.M41);
    out->M13 = (((in.M12 * num35) - (in.M13 * num34)) + (in.M14 * num33)) * num;
    out->M23 = -(((in.M11 * num35) - (in.M13 * num32)) + (in.M14 * num31)) * num;
    out->M33 = (((in.M11 * num34) - (in.M12 * num32)) + (in.M14 * num30)) * num;
    out->M43 = -(((in.M11 * num33) - (in.M12 * num31)) + (in.M13 * num30)) * num;
    real num29 = (in.M23 * in.M34) - (in.M24 * in.M33);
    real num28 = (in.M22 * in.M34) - (in.M24 * in.M32);
    real num27 = (in.M22 * in.M33) - (in.M23 * in.M32);
    real num26 = (in.M21 * in.M34) - (in.M24 * in.M31);
    real num25 = (in.M21 * in.M33) - (in.M23 * in.M31);
    real num24 = (in.M21 * in.M32) - (in.M22 * in.M31);
    out->M14 = -(((in.M12 * num29) - (in.M13 * num28)) + (in.M14 * num27)) * num;
    out->M24 = (((in.M11 * num29) - (in.M13 * num26)) + (in.M14 * num25)) * num;
    out->M34 = -(((in.M11 * num28) - (in.M12 * num26)) + (in.M14 * num24)) * num;
    out->M44 = (((in.M11 * num27) - (in.M12 * num25)) + (in.M13 * num24)) * num;
}
