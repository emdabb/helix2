#ifndef HELIX_CORE_SHORT4_H_
#define HELIX_CORE_SHORT4_H_

#include <helix/Types.h>

namespace helix {
namespace core {
    __declare_aligned(struct, 8) Short4 {
        short X, Y, Z, W;
    };
}
}

#endif
