#ifndef HELIX_CORE_DICTIONARY_H_
#define HELIX_CORE_DICTIONARY_H_

#include <map>
#include <string>

namespace helix {
namespace core {
template <typename T = std::string>
struct Dictionary {
    typedef std::map<std::string, T> Type;
    typedef std::pair<std::string, T> Pair;
};
}
}

template <typename T = std::string>
struct hxDictionary {
    typedef typename helix::core::Dictionary<T>::Type Type;
    typedef typename helix::core::Dictionary<T>::Pair Pair;
};

#endif
