#include "Vector3.h"
#include "Matrix.h"
#include "Quaternion.h"

using namespace helix::core;

const Vector3 Vector3::One      = {  1.,  1.,  1. };
const Vector3 Vector3::Zero     = {  0.,  0.,  0. };
const Vector3 Vector3::Up       = {  0.,  1.,  0. };
const Vector3 Vector3::Right    = {  1.,  0.,  0. };
const Vector3 Vector3::Forward  = {  0.,  0., -1. }; 

Vector3 Vector3::transform(const_ref v, const Quaternion& q) {
    Vector3 r;
    transform(v, q, &r);
    return r;
}
void Vector3::transform(const_ref v, const Quaternion& q, ptr c) {
    real X = +q.Y * v.Z - q.Z * v.Y + q.W * v.X;
    real Y = -q.X * v.Z + q.Z * v.X + q.W * v.Y;
    real Z = +q.X * v.Y - q.Y * v.X + q.W * v.Z;
    real W = -q.X * v.X - q.Y * v.Y - q.Z * v.Z;

    c->X = +X * +q.W + Y * -q.Z - Z * -q.Y + W * -q.X;
    c->Y = -X * -q.Z + Y * +q.W + Z * -q.X + W * -q.Y;
    c->Z = +X * -q.Y - Y * -q.X + Z * +q.W + W * -q.Z;
}

Vector3 Vector3::transform(const_ref a, const Matrix& b) {
    Vector3 out;
    transform(a, b, &out);
    return out;
}

void Vector3::transform(const_ref a, const Matrix& b, ptr c) {
    c->X = (a.X * b.M11) + (a.Y * b.M21) + (a.Z * b.M31) + b.M41;
    c->Y = (a.X * b.M12) + (a.Y * b.M22) + (a.Z * b.M32) + b.M42;
    c->Z = (a.X * b.M13) + (a.Y * b.M23) + (a.Z * b.M33) + b.M43;
}

void Vector3::rotate(const_ref a, const Matrix& b, ptr c) {
    c->X = (a.X * b.M11) + (a.Y * b.M21) + (a.Z * b.M31);
    c->Y = (a.X * b.M12) + (a.Y * b.M22) + (a.Z * b.M32);
    c->Z = (a.X * b.M13) + (a.Y * b.M23) + (a.Z * b.M33);
}

