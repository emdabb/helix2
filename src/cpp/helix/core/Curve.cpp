/*
 * Curve.cpp
 *
 *  Created on: Aug 19, 2015
 *      Author: miel
 */


#include <helix/core/Curve.h>
#include <helix/core/Vector3.h>
#include <helix/core/MathHelper.h>
#include <helix/core/DebugLog.h>

using namespace helix;
using namespace helix::core;

real Curve::interpolate(real const& t, real const& p1, real const& p2, real const& p3, real const& p4)
{
    real t2 = t * t;
    real t3 = t2 * t;
#if 0
    real b1 = .5 * (-t3 + 2 * t2 - t);
    real b2 = .5 * (3 * t3 - 5 * t2 + 2);
    real b3 = .5 * (-3 * t3 + 4 * t2 + t);
    real b4 = .5 * (t3 - t2);
#else
    real it = 1 - t;

    real b1 = (it * it * it) / 6.0f;
    real b2 = (3 * t * t * t - 6 * t * t + 4) / 6.0f;
    real b3 = (-3 * t * t * t + 3 * t * t + 3 * t + 1) / 6.0f;
    real b4 = t * t * t / 6.0f;
#endif
    return p1 * b1 + p2 * b2 + p3 * b3 + p4 * b4;
}

Curve::Curve() :
        mDeltaTime(0), mTimeRange(0)
{

}

Curve::~Curve()
{

}

void Curve::addPoint(real const& time, real const& vec)
{

    CurveKey key;// = new CurveKey;
    key.position = time;
    key.value = vec;

    mPoints.push_back(key);
    mDeltaTime = (float) 1 / (float) mPoints.size();
    mTimeRange = mPoints[mPoints.size() - 1].position - mPoints[0].position;

}

real Curve::evaluate(real const& at)
{
    if (mPoints.size() == 0 || mTimeRange == 0)
    {
        return (real) 0;
    }
    real tt = at;
    CurveKey* prev = &mPoints[0];
    CurveKey* next;
    for (size_t i = 1; i < mPoints.size(); i++)
    {
        next = &mPoints[i];
        if (next->position >= tt)
        {
            float t = (tt - prev->position) / (next->position - prev->position); //to have t in [0,1]
            float ts = t * t;
            float tss = ts * t;
            /* After a lot of search on internet I have found all about spline function
             * and bezier (phi'sss ancien) but finaly use hermite curve
             * http://en.wikipedia.org/wiki/Cubic_Hermite_spline
             * P(t) = (2*t^3 - 3t^2 + 1)*P0 + (t^3 - 2t^2 + t)m0 + (-2t^3 + 3t^2)P1 + (t^3-t^2)m1
             * with P0.value = prev.value , m0 = prev.tangentOut, P1= next.value, m1 = next.TangentIn
             */
            return (2 * tss - 3 * ts + 1.f) * prev->value + (tss - 2 * ts + t) * prev->value + (3 * ts - 2 * tss) * next->value + (tss - ts) * next->value;
        }
        prev = next;
    }
    return 0;
}

size_t Curve::size() const
{
    return mPoints.size();
}

real Curve::timeRange() const
{
    return mTimeRange;
}

real Curve::timeRangeInv() const
{
    return 1.0f / mTimeRange;
}
