#ifndef HELIX_CORE_THREADPOOL_H_
#define HELIX_CORE_THREADPOOL_H_

#include <queue>
#include <helix/core/Singleton.h>

namespace helix {
namespace core {

struct IRunnable;
struct IMutex;

class Threadpool : public Singleton<Threadpool> {
    struct InternalThread;
    InternalThread**        mThreads;
    std::queue<IRunnable*>  mQueue;
    IMutex*                 mLock;
public:
    Threadpool();
    virtual ~Threadpool();
    void create();
    void destroy();
    IRunnable* next();
    void add(IRunnable*);
};

}
}

#endif
