#ifndef HELIX_CORE_IRUNNABLE_H_
#define HELIX_CORE_IRUNNABLE_H_

namespace helix {
namespace core {

struct IRunnable {
    virtual ~IRunnable() {}
    virtual void done() {}
    virtual int  run() = 0;
};

}
}

typedef helix::core::IRunnable hxRunnable;

#endif
