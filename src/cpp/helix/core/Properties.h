/*
 * Properties.h
 *
 *  Created on: Jul 29, 2015
 *      Author: miel
 */

#ifndef HELIX_CORE_PROPERTIES_H_
#define HELIX_CORE_PROPERTIES_H_

#include <string>
#include <map>
#include <sstream>
#include <iomanip>
#include <vector>
#include <cassert>
#include <helix/Types.h>

namespace helix {
namespace core {

template <typename S, typename T>
inline S lexical_cast(const T& in) {
	std::stringstream s;
	S out;
	s << std::setprecision(type_length<T>::value) << in;
	s >> std::setprecision(type_length<S>::value) >> out;
	return out;
}
/**
 * Specialization for lexically casting string -> string (nonsense cast, but still...)
 * @param in
 * @return
 */
template <>
inline std::string lexical_cast<std::string, std::string>(const std::string& in) {
	return in;
}

class Properties {
	typedef std::string key_t;
	typedef std::string value_t;
	typedef std::multimap<key_t, value_t> list_type;
	list_type mData;
public:
	Properties();
	virtual ~Properties();
	void insert(const key_t&, const value_t&);
	bool has(const key_t&) const;

	template <typename T>
	T get(const key_t& key, T defaultValue) {
		list_type::iterator it = mData.find(key);
		if(it != mData.end()) {
			return lexical_cast<T>(it->second);
		}
		return defaultValue;
	}

	template <typename T>
	void get(const key_t& key, std::vector<T>* values, bool appendTo = false) {
		assert(values);
		if(!appendTo) {
			values->clear();
		}
		list_type::iterator first = mData.find(key);
		if(first != mData.end()) {
			list_type::iterator last = mData.upper_bound(key);
			for(; first != last; first++) {
				values->push_back(lexical_cast<T>(first->second));
			}
		}
	}


};

}
}

typedef helix::core::Properties hxProperties;

#endif /* HELIX_CORE_PROPERTIES_H_ */
