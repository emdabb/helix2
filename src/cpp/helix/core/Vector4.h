#ifndef HELIX_CORE_VECTOR4_H_
#define HELIX_CORE_VECTOR4_H_

#include <helix/Types.h>

namespace helix {
namespace core {
__declare_aligned(struct, 16) Vector4 {
    typedef const Vector4& const_ref;
    typedef Vector4* ptr;
    real X, Y, Z, W;
};
}
}

typedef helix::core::Vector4 hxVector4;

#endif
