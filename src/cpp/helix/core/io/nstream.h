#ifndef HELIX_CORE_IO_NSTREAM_H_
#define HELIX_CORE_IO_NSTREAM_H_

#include <iostream>
#include <helix/core/net/Socket.h>

namespace helix {
namespace core {

template <typename T, typename TR=std::char_traits<T> >
class basic_inbuf : public std::basic_streambuf<T, TR> {
    const static size_t BUFFER_SIZE = 256;

    typedef typename TR::int_type   int_type;

    T       gbuf[BUFFER_SIZE];
    Socket* mSocket;
public:
    int_type underflow() {
        if(this->gptr() < this->egptr()) {
            return *(this->gptr());
        }
        int num = mSocket->receive(reinterpret_cast<void*>(gbuf), BUFFER_SIZE * sizeof(T));
        if(num == 0) {
            return TR::eof();
        }
        this->setg(gbuf, gbuf, gbuf + num);
        return *(this->gptr());
    }

    void setSocket(Socket* s) {
        mSocket = s;
    }

};

template <typename T, typename TR=std::char_traits<T> >
class basic_onbuf : public std::basic_streambuf<T, TR> {
    const static size_t BUFFER_SIZE = 256;
    typedef typename TR::int_type   int_type;
    T       pbuf[BUFFER_SIZE];
    Socket* mSocket;
public:
    int flush() {
        int num = this->pptr() - this->pbase();
        if(mSocket->send(reinterpret_cast<void*>(pbuf), num * sizeof(T)) != num) {
            return TR::eof();
        }
        this->pbump(-num);
        return num;
    }

    int_type overflow(int_type c = TR::eof()) {
        if(c == TR::eof()) {
            *(this->pptr()) = c;
            this->pbump(1);
        }
        if(flush() == TR::eof()) {
            return TR::eof();
        }
        return c;
    }

    int_type sync() {
        if(flush() == TR::eof()) {
            return TR::eof();
        }
        return 0;
    }

    void setSocket(Socket* s) {
        mSocket = s;
    }
};

template <typename T, typename TR=std::char_traits<T> >
class basic_instream : public std::basic_istream<T, TR> {
    typedef basic_inbuf<T, TR>  buffer_type;
    buffer_type buf;
    Socket      socket;
public:
    basic_instream()
    : std::basic_istream<T, TR>(&buf)
    {

    }

    void accept(unsigned short port) {
        socket.bind(port);
        socket.listen();
        Socket* cs = socket.accept();
        buf.setSocket(cs);
    }
};

template <typename T, typename TR=std::char_traits<T> >
class basic_onstream : public std::basic_ostream<T, TR> {
    typedef basic_onbuf<T, TR>  buffer_type;

    buffer_type buf;
    Socket*     _socket;
public:

    basic_onstream()
    : std::basic_ostream<T, TR>(&buf) {
        buf.setSocket(new Socket);
    }

    basic_onstream(Socket* s)
    : std::basic_ostream<T, TR>(&buf)
    , _socket(s)
    {
        buf.setSocket(_socket);
    }
    void open(const char* ip, unsigned short port) {
        if(!_socket) {
            if(_socket->open(ip, port)) {
                buf.setSocket(_socket);
            } else {
                this->setstate(std::ios::failbit);
            }
        }
    }
};

typedef basic_onstream<char>    onstream;
typedef basic_instream<char>    instream;

}
}

#endif
