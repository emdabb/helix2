#ifndef HELIX_CORE_BINARY_READER_H_
#define HELIX_CORE_BINARY_READER_H_

#include <helix/Types.h>
#include <iostream>

namespace helix {
namespace core {

class HXEXPORT BinaryReader {
    std::istream* mStream;
public:
    BinaryReader(std::istream*);
    virtual ~BinaryReader();

    int             read7BitEncodedInt();
    std::string     readString();
    int32_t         readInt32();
    std::istream*   getStream() const;
    void            readBytes(void*, size_t);
};

}
}

#endif
