#include <helix/core/io/BinaryReader.h>

using namespace helix::core;

BinaryReader::BinaryReader(std::istream* is) 
: mStream(is) 
{
}

BinaryReader::~BinaryReader() {
}

int BinaryReader::read7BitEncodedInt() {
    int res = 0;
    for(int i=0; i < 5; i++) {
        char b;
        readBytes(&b, 1);
        res |= (b & 127) << (7 * i);
        if(!(b & 128)) {
            break;
        } 
    }
    return res;
}

std::string BinaryReader::readString() {
    int cap = read7BitEncodedInt();
    std::string res;
    res.resize(cap);
    readBytes(&res[0], cap);
    return res;
}



int32_t BinaryReader::readInt32() {
    int32_t res;
    readBytes(reinterpret_cast<char*>(&res), sizeof(int32_t));   
	return res;
}

void BinaryReader::readBytes(void* out, size_t len) {
    mStream->read(static_cast<char*>(out), len);
}

std::istream* BinaryReader::getStream() const {
    return mStream; 
}
