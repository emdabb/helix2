#ifndef HELIX_CORE_IMUTEX_H_
#define HELIX_CORE_IMUTEX_H_

#include <helix/Types.h>
#include <helix/core/ILockable.h>

namespace helix {
namespace core {

struct IMutex : public ILockable {
    virtual ~IMutex() {}
    virtual int acquire() = 0;//virtual int  lock() = 0;    // <-- TODO: rename to acquire
    virtual void release()= 0;//void unlock() = 0;  // <-- TODO: rename to release
};

}
}

typedef helix::core::IMutex hxMutex;

HXAPI HXEXPORT hxMutex* new_Mutex();

#endif
