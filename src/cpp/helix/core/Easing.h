/*
 * Easing.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_CORE_EASING_H_
#define HELIX_CORE_EASING_H_

#include <helix/Types.h>

namespace helix {
namespace core {

struct Easing {
	enum {
		LinearIn		= 0,
		LinearOut,
		LinearInOut,

		SineIn,
		SineOut,
		SineInOut,

		QuadraticIn,
		QuadraticOut,
		QuadraticInOut,

		CubicIn,
		CubicOut,
		CubicInOut,

		QuinticIn,
		QuinticOut,
		QuinticInOut,

		ExponentialIn,
		ExponentialOut,
		ExponentialInOut,

		CircleIn,
		CircleOut,
		CircleInOut,

		BackIn,
		BackOut,
		BackInOut,

		ElasticIn,
		ElasticOut,
		ElasticInOut,

		BounceIn,
		BounceOut,
		BounceInOut,

		NumEasingFunctions
	};

	typedef float(*EasingFunction)(float);
	static EasingFunction Delegate[NumEasingFunctions];

	/**
	 *  Linear interpolation (no easing)
	 * @param easing
	 * @return
	 */
	static float EaseLinearIn(float);
	static float EaseLinearOut(float);
	static float EaseLinearInOut(float);
	/**
	 * Sine wave easing; sin(p * PI/2).
	 * @param in
	 * @return
	 */
	static float EaseSineIn(float);
	static float EaseSineOut(float);
	static float EaseSineInOut(float);
	/**
	 * Quadratic easing; p^2
	 * @param in
	 * @return
	 */
	static float EaseQuadraticIn(float);
	static float EaseQuadraticOut(float);
	static float EaseQuadraticInOut(float);
	/**
	 * Quadratic easing; p^3
	 * @param in
	 * @return
	 */
	static float EaseCubicIn(float);
	static float EaseCubicOut(float);
	static float EaseCubicInOut(float);
	/**
	 * Quartic easing; p^4
	 * @param in
	 * @return
	 */
	static float EaseQuarticIn(float);
	static float EaseQuarticOut(float);
	static float EaseQuarticInOut(float);
	/**
	 * Quintic easing; p^5
	 * @param in
	 * @return
	 */
	static float EaseQuinticIn(float);
	static float EaseQuinticOut(float);
	static float EaseQuinticInOut(float);
	/**
	 * Exponential easing, base 2
	 * @param in
	 * @return
	 */
	static float EaseExponentialIn(float);
	static float EaseExponentialOut(float);
	static float EaseExponentialInOut(float);
	/**
	 * Circular easing; sqrt(1 - p^2)
	 * @param in
	 * @return
	 */
	static float EaseCircleIn(float);
	static float EaseCircleOut(float);
	static float EaseCircleInOut(float);
	/**
	 * Overshooting cubic easing;
	 * @param in
	 * @return
	 */
	static float EaseBackIn(float);
	static float EaseBackOut(float);
	static float EaseBackInOut(float);
	/**
	 * Exponentially-damped sine wave easing
	 * @param in
	 * @return
	 */
	static float EaseElasticIn(float);
	static float EaseElasticOut(float);
	static float EaseElasticInOut(float);
	/**
	 * Exponentially-decaying bounce easing
	 * @param in
	 * @return
	 */
	static float EaseBounceIn(float);
	static float EaseBounceOut(float);
	static float EaseBounceInOut(float);
};

}
}



#endif /* HELIX_CORE_EASING_H_ */
