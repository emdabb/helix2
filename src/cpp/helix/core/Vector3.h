#ifndef HELIX_CORE_VECTOR3_H_
#define HELIX_CORE_VECTOR3_H_

#include <helix/Types.h>
#include <helix/core/MathHelper.h>

namespace helix {
    namespace core {

        struct Matrix;
        struct Quaternion;

        __declare_aligned(struct, 16) Vector3 {
            typedef const Vector3&  const_ref;
            typedef Vector3* ptr;

            float X, Y, Z;

            static const Vector3 One;
            static const Vector3 Zero;
            static const Vector3 Up;
            static const Vector3 Right;
            static const Vector3 Forward;

            inline static void dot(const_ref a, const_ref b, float* c) {
                *c = a.X * b.X + a.Y * b.Y + a.Z * b.Z;
            }

            inline static float dot(const_ref a, const_ref b) {
                float res;
                dot(a, b, &res);
                return res;
            }

            inline static void cross(const_ref a, const_ref b, ptr c) {
                c->X = a.Y * b.Z - b.Y * a.Z;
                c->Y = b.X * a.Z - a.X * b.Z;
                c->Z = a.X * b.Y - b.X * a.Y;
            }

            inline static Vector3 cross(const_ref a, const_ref b) {
                Vector3 res;
                cross(a, b, &res);
                return res;
            }

            inline static void length(const_ref a, float* b) {
                *b = real_sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
            }

            inline static float length(const_ref a) {
                float res;
                length(a, &res);
                return res;
            }

            inline static void lengthSquared(const_ref a, float* b) {
                *b = a.X * a.X + a.Y * a.Y + a.Z * a.Z;
            }

            inline static float lengthSquared(const_ref a) {
                float b;
                lengthSquared(a, &b);
                return b;
            }

            inline static void normalized(const_ref a, ptr b) {
                float len;
                length(a, &len);
                len = (float)1. / len;
                b->X = a.X * len;
                b->Y = a.Y * len;
                b->Z = a.Z * len;
            }

            inline static Vector3 normalized(const_ref a) {
                Vector3 out;
                normalized(a, &out);
                return out;
            }

            inline static void normalize(ptr a) {
                float len;
                length(*a, &len);
                len = (float)1. / len;
                a->X *= len;
                a->Y *= len;
                a->Z *= len;
            }

            inline static void limit(const_ref vec, float const& l, ptr out) {
                Vector3 copy(vec);
                float len = dot(vec, vec);
                float limit2 = (l * l);
                if (len > limit2)
                {
                    Vector3::normalized(vec, &copy);
                    copy *= l;
                }
                *out = copy;
            }

            static Vector3    transform(const_ref a, const Quaternion& b);
            static void     transform(const_ref a, const Quaternion& b, ptr c);
            static Vector3    transform(const_ref a, const Matrix& b);
            static void         transform(const_ref a, const Matrix& b, ptr c);
            static void rotate(const_ref a, const Matrix& b, ptr c);

            __forceinline static void mul(const_ref a, const_ref b, ptr c) {
                c->X = a.X * b.Y;
                c->Y = a.Y * b.Y;
                c->Z = a.Z * b.Z;
            }

            __forceinline static void mul(ptr a, const_ref b) {
                a->X *= b.Y;
                a->Y *= b.Y;
                a->Z *= b.Z;
            }

            __forceinline static void add(const_ref a, const_ref b, ptr c) {
                c->X = a.X + b.X;
                c->Y = a.Y + b.Y;
                c->Z = a.Z + b.Z;
            }

            __forceinline static void add(ptr a, const_ref b) {
                a->X += b.X;
                a->Y += b.Y;
                a->Z += b.Z;
            }
            __forceinline static void sub(const_ref a, const_ref b, ptr c) {
                c->X = a.X - b.X;
                c->Y = a.Y - b.Y;
                c->Z = a.Z - b.Z;
            }

            inline static void sub(ptr a, const_ref b) {
                a->X -= b.X;
                a->Y -= b.Y;
                a->Z -= b.Z;
            }
            __forceinline static void scale(const_ref a, float const& b, ptr c) {
                c->X = a.X * b;
                c->Y = a.Y * b;
                c->Z = a.Z * b;
            }

            __forceinline static void scale(ptr a, float const& c) {
                a->X *= c;
                a->Y *= c;
                a->Z *= c;
            }

            __forceinline static void lerp(const_ref a, const_ref b, float const& d, ptr c) {
                c->X = MathHelper::lerp(a.X, b.X, d);
                c->Y = MathHelper::lerp(a.Y, b.Y, d);
                c->Z = MathHelper::lerp(a.Z, b.Z, d);
            }


            inline Vector3 operator - () const {
                Vector3 copy(*this);
                copy.X = -copy.X;
                copy.Y = -copy.Y;
                copy.Z = -copy.Z;
                return copy;
            }

            inline Vector3 operator + (const_ref b) const {
                Vector3 copy(*this);
                copy += b;
                return copy;
            }
            inline Vector3 operator - (const_ref b) const {
                Vector3 copy(*this);
                copy -= b;
                return copy;
            }
            inline Vector3 operator * (const_ref b) const {
                Vector3 copy(*this);
                copy *= b;
                return copy;
            }
            inline Vector3 operator / (const_ref b) const {
                Vector3 copy(*this);
                copy /= b;
                return copy;
            }
            inline Vector3& operator += (const_ref b) {
                X += b.X;
                Y += b.Y;
                Z += b.Z;
                return *this;
            }
            inline Vector3& operator -= (const_ref b) {
                X -= b.X;
                Y -= b.Y;
                Z -= b.Z;
                return *this;
            }
            inline Vector3& operator *= (const_ref b) {
                X *= b.X;
                Y *= b.Y;
                Z *= b.Z;
                return *this;
            }
            inline Vector3& operator /= (const_ref b) {
                X /= b.X;
                Y /= b.Y;
                Z /= b.Z;
                return *this;
            }

            inline Vector3 operator * (float const& a) const {
                Vector3 copy(*this);
                copy *= a;
                return copy;
            }

            inline Vector3& operator *= (float const& a) {
                X *= a;
                Y *= a;
                Z *= a;
                return *this;
            }

            inline Vector3 operator / (float const& a) const {
                Vector3 copy(*this);
                copy /= a;
                return copy;
            }

            inline Vector3& operator /= (float const& a) {
                // TODO: reciprocal?
                X /= a;
                Y /= a;
                Z /= a;
                return *this;
            }
        };
    }
}

typedef helix::core::Vector3 hxVector3;

#endif

