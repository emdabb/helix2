#include <helix/core/Threadpool.h>
#include <helix/core/IThread.h>
#include <helix/core/IMutex.h>

using namespace helix::core;

template <typename T>
    class ScopedLock {
        T* _lock;
    public:
        explicit ScopedLock(T* lock) : _lock(lock) {
            _lock->acquire();
        }

        virtual ~ScopedLock() {
            _lock->release();
        }
    };

struct Threadpool::InternalThread : public IThread {
    Threadpool* mThreadpool;

    explicit InternalThread(Threadpool* pool)
    : mThreadpool(pool)
    {
    }
    virtual int run() {
        for(;;) {
            IRunnable* r = mThreadpool->next();
            if(r) {
                r->run();
            }
        }
        return 0;
    }
};

Threadpool::Threadpool() {

}

Threadpool::~Threadpool() {
}

void Threadpool::add(IRunnable* val) {
	ScopedLock<IMutex> guard(mLock);
	mQueue.push(val);
}

IRunnable* Threadpool::next() {
    ScopedLock<IMutex> guard(mLock);
    if(mQueue.size() > 0) {
       return mQueue.front();
    }
    return NULL;
}

void Threadpool::create() {

}

void Threadpool::destroy() {
}
