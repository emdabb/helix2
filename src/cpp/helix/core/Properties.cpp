/*
 * Properties.cpp
 *
 *  Created on: Jul 29, 2015
 *      Author: miel
 */

#include <helix/core/Properties.h>

using namespace helix::core;

Properties::Properties() {

}
Properties::~Properties() {

}
void Properties::insert(const key_t& key, const value_t& val) {
	mData.insert(std::pair<key_t, value_t>(key, val));
}
bool Properties::has(const key_t& key) const {
	list_type::const_iterator it = mData.find(key);
	return it != mData.end();
}



