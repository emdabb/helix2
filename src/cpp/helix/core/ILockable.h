#ifndef HELIX_CORE_ILOCKABLE_H_
#define HELIX_CORE_ILOCKABLE_H_

namespace helix {
namespace core {

    struct ILockable {
        virtual ~ILockable() {}
        virtual int acquire() = 0;
        virtual void release() = 0;
    };


    template <typename T>
    class ScopedLock {
        T* mLock;
    public:
        explicit ScopedLock(T* lock) : mLock(lock) {
            mLock->acquire();
        }

        ~ScopedLock() {
            mLock->release();
        }
    };
}
}

typedef helix::core::ILockable hxLockable;

template <typename T>
struct hxScopedLock {
    typedef helix::core::ScopedLock<T> Type;
};

#endif
