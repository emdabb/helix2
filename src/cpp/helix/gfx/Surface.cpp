#include <helix/gfx/Surface.h>
#include <helix/gfx/PixelFormat.h>
#include <cstring>

using namespace helix::gfx;

Surface::Surface(size_t w, size_t h, const int pixelFormat)
: mWidth(w)
, mHeight(h)
, mPixelFormat(pixelFormat)
{
    int pixelSize = PixelFormat::Bpp[pixelFormat] >> 3;
    mPitch = pixelSize * w;
    mData = malloc(mHeight * mPitch);
}

Surface::~Surface() {
}

const size_t Surface::getWidth() const{
    return mWidth;
}
const size_t Surface::getHeight() const {
    return mHeight;
}
const void* Surface::getData() const {
    return mData;
}
void Surface::setData(const void* src) {
    memcpy(mData, src, mPitch * mHeight);
}

const int Surface::getPixelFormat() const {
    return mPixelFormat;
}

