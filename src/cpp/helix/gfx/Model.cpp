/**
 * @file Model.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 21, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#include <helix/gfx/Model.h>
//#include "index_buffer.h"
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/VertexStream.h>
#include <helix/core/io/BinaryReader.h>
#include <helix/core/Vector4.h>
#include <helix/core/Vector3.h>
#include <helix/core/Vector2.h>
#include <helix/gfx/PrimitiveType.h>
#include <helix/gfx/VertexData.h>

using namespace helix::gfx;
using namespace helix::core;

Model::Model(IGraphicsDevice* pGraphicsDevice) :
        mGraphicsDevice(pGraphicsDevice) {

}

Model::~Model() {

}

void Model::addMesh(ModelMesh* pModelMesh) {
    mMeshes.push_back(pModelMesh);
}

void Model::draw() {
    for (size_t i = 0; i < mMeshes.size(); i++) {
        mGraphicsDevice->setStreamSource(mMeshes[i]->Vertices);
        //mGraphicsDevice->setIndexBuffer(mMeshes[i]->Indices);
        // mGraphicsDevice->drawIndexedPrimitives(PrimitiveType::Triangles, 0,
        //         0, mMeshes[i]->Indices->count(), 0, 1);
    }
}

void Model::createFromStream(Model** dst, std::istream* is) {
    // BinaryReader reader(is);
    //
    // std::string name = reader.readString();
    //
    // int32_t hasBones = reader.readInt32();;
    // int32_t hasAnimations = reader.readInt32();
    //
    // int32_t numMaterials = reader.readInt32();
    //
    // for(int i=0; i < numMaterials; i++) {
    //     std::string textureFilename=reader.readString();
    // }
    //
    // int32_t numVertices = reader.readInt32();

    // size_t sz_pos = numVertices * sizeof(Vector3);
    // size_t sz_nrm = numVertices * sizeof(Vector3);
    // size_t sz_tex = numVertices * sizeof(Vector2);
    // size_t sz_bix = numVertices * sizeof(Short4);
    // size_t sz_bwe = numVertices * sizeof(Vector4);
    //
    // VertexData data;





}
