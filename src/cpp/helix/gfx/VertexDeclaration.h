#ifndef HELIX_GFX_VERTEXDECLARATION_H_
#define HELIX_GFX_VERTEXDECLARATION_H_

#include <helix/Types.h>

namespace helix {
namespace gfx {

struct VertexElement;

class VertexDeclaration {
    VertexElement*  mVertexElements;
    size_t          mNumVertexElements;
    size_t          mVertexStride;
protected:
    //DISALLOW_COPY_AND_ASSIGN(VertexDeclaration);
public:
    /**
     *
     * @param vertexElements
     * @param numElements
     * @param stride
     */
    VertexDeclaration(const VertexElement*, size_t, size_t);
    /**
     *
     */
    virtual ~VertexDeclaration();
    /**
     *
     * @param n
     * @return
     */
    const VertexElement& at(size_t n) const;
    /**
     *
     * @return
     */
    const size_t size() const;
    /**
     *
     * @return
     */
    const size_t stride() const;

    /**
     *
     * @param elements
     * @param numElements
     * @param stride
     * @return
     */
    static const VertexDeclaration* create(const VertexElement* elements, size_t numElements, size_t stride);
};

}
}

typedef helix::gfx::VertexDeclaration hxVertexDeclaration;

#endif
