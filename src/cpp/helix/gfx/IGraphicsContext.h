#ifndef HELIX_GFX_IGRAPHICSCONTEXT_H_
#define HELIX_GFX_IGRAPHICSCONTEXT_H_

#include <helix/Types.h>
#include <helix/core/ILockable.h>

namespace helix {
namespace gfx {
struct IGraphicsContext : public hxLockable {
    virtual ~IGraphicsContext() {}

    virtual bool isCurrent() const = 0;
    virtual bool swap() = 0;
    virtual bool create(void*, void*) = 0;
    virtual void destroy() = 0;
    // virtual int  acquire() = 0;
    // virtual void release() = 0;
    //virtual void createDevice();
protected:
    virtual bool makeCurrent() = 0;
};

}
}

typedef helix::gfx::IGraphicsContext hxGraphicsContext;

#endif
