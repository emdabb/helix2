#ifndef HELIX_GFX_SURFACE_H_
#define HELIX_GFX_SURFACE_H_

#include <helix/Types.h>

namespace helix {
namespace gfx {

class Surface {
    size_t      mWidth;
    size_t      mHeight;    
    size_t      mPitch;
    const int   mPixelFormat;
    void*       mData;
public:
    Surface(size_t w, size_t h, const int pixelFormat);
    virtual ~Surface();

    const size_t    getWidth() const;
    const size_t    getHeight() const;
    const void*     getData() const;
    void            setData(const void*);
    const int       getPixelFormat() const;
};

}
}

typedef helix::gfx::Surface hxSurface;

#endif
