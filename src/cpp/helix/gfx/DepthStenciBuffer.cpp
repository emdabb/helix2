/*
 * DepthStenciBuffer.cpp
 *
 *  Created on: Jul 20, 2015
 *      Author: miel
 */

#include <helix/gfx/DepthStencilBuffer.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/core/Factory.h>
#include <cassert>

using namespace helix::gfx;
using namespace helix::core;

DepthStencilBuffer::DepthStencilBuffer(IGraphicsDevice* device, int w, int h, const int dsf)
: mPimpl(NULL)
, mWidth(w)
, mHeight(h)
, mDepthStencilFormat(dsf)
{
	create();
}

DepthStencilBuffer::~DepthStencilBuffer() {
	destroy();
}
int DepthStencilBuffer::getWidth() const {
	return mWidth;
}

int DepthStencilBuffer::getHeight() const {
	return mHeight;
}

const int DepthStencilBuffer::getDepthStencilFormat() const {
	return mDepthStencilFormat;
}
const DepthStencilBuffer::Pimpl* DepthStencilBuffer::getImpl() const {
	return mPimpl;
}

void DepthStencilBuffer::create() {
	if(NULL == mPimpl) {
		mPimpl = Factory<Pimpl>::getInstance().create(mGraphicsDevice->api());
		assert(mPimpl);
		mPimpl->create(this);
	}
}

void DepthStencilBuffer::destroy() {
	if(NULL != mPimpl) {
		delete mPimpl;
		mPimpl =NULL;
	}
}




