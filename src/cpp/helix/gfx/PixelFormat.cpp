#include <helix/gfx/PixelFormat.h>

using namespace helix::gfx;

const int PixelFormat::Bpp[] = {
    32,     // < Color
    16,     // < Bgr565
    16,     // < Bgra5551
    16,     // < Bgra4444
    0,      // < Dxt1
    0,      // < Dxt3
    0,      // < Dxt5
    16,     // < NormalizedByte2
    32,     // < NormalizedByte4
    32,     // < Rgba1010102
    32,     // < Rg32
    64,     // < Rgba64
    8,      // < Alpha8
    32,     // < Single
    64,     // < Vector2
    128,    // < Vector4
    16,     // < HAlfSingle
    32,     // < HalfVector2
    64,     // < HalfVector4
    128     // < HdrRenderable
};

const int PixelFormat::RedBits[] = {
    8,      // < Color
    5,      // < Bgr565
    5,      // < Bgra5551
    4,      // < Bgra4444
    0,      // < Dxt1
    0,      // < Dxt3
    0,      // < Dxt5
    8,      // < NormalizedByte2
    8,      // < NormalizedByte4
    10,     // < Rgba1010102
    16,     // < Rg32
    16,     // < Rgba64
    0,      // < Alpha8
    32,     // < Single
    32,     // < Vector2
    32,     // < Vector4
    16,     // < HAlfSingle
    16,     // < HalfVector2
    16,     // < HalfVector4
    32      // < HdrRenderable
};

const int PixelFormat::GreenBits[] = {
    8,      // < Color
    6,      // < Bgr565
    5,      // < Bgra5551
    4,      // < Bgra4444
    0,      // < Dxt1
    0,      // < Dxt3
    0,      // < Dxt5
    8,      // < NormalizedByte2
    8,      // < NormalizedByte4
    10,     // < Rgba1010102
    16,     // < Rg32
    16,     // < Rgba64
    0,      // < Alpha8
    0,      // < Single
    32,     // < Vector2
    32,     // < Vector4
    0,      // < HAlfSingle
    16,     // < HalfVector2
    16,     // < HalfVector4
    32      // < HdrRenderable
};

const int PixelFormat::BlueBits[] = {
    8,      // < Color
    5,      // < Bgr565
    5,      // < Bgra5551
    4,      // < Bgra4444
    0,      // < Dxt1
    0,      // < Dxt3
    0,      // < Dxt5
    0,      // < NormalizedByte2
    8,      // < NormalizedByte4
    10,     // < Rgba1010102
    0,      // < Rg32
    16,     // < Rgba64
    0,      // < Alpha8
    0,      // < Single
    0,      // < Vector2
    32,     // < Vector4
    0,      // < HAlfSingle
    0,      // < HalfVector2
    16,     // < HalfVector4
    32      // < HdrRenderable
};

const int PixelFormat::AlphaBits[] = {
    8,      // < Color
    0,      // < Bgr565
    1,      // < Bgra5551
    4,      // < Bgra4444
    0,      // < Dxt1
    0,      // < Dxt3
    0,      // < Dxt5
    0,      // < NormalizedByte2
    8,      // < NormalizedByte4
    2,      // < Rgba1010102
    0,      // < Rg32
    16,     // < Rgba64
    8,      // < Alpha8
    0,      // < Single
    0,      // < Vector2
    32,     // < Vector4
    0,      // < HAlfSingle
    0,      // < HalfVector2
    16,     // < HalfVector4
    32      // < HdrRenderable
};

