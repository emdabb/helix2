#ifndef HELIX_GFX_SHADERPROGRAM_H_
#define HELIX_GFX_SHADERPROGRAM_H_

#include <helix/Types.h>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <helix/gfx/ShaderParameter.h>
#include <helix/core/ILockable.h>

namespace helix {
namespace gfx {

class  ShaderParameterBase;
struct IGraphicsDevice;
class  ShaderProgram;

namespace impl {
struct IShaderProgram {
    virtual ~IShaderProgram() {}
    //virtual void cacheUniforms(ShaderProgram*) = 0;
    virtual void create(ShaderProgram*) = 0;
    //virtual bool compile(const char*) = 0;
    //virtual bool link() = 0;
    virtual int  acquire() = 0;
    virtual void release() =0;
};
}

class ShaderProgram : public hxLockable {
protected:
    std::vector<ShaderParameterBase*> mUniform;
    std::vector<int> mLocation;
    impl::IShaderProgram* pimpl;
    std::map<std::string, ShaderParameterBase*> mUniformNames;
    bool mIsActive;
    std::string mSource;
public:
    ShaderProgram(IGraphicsDevice*, const std::string& source);
    virtual ~ShaderProgram();
//    void compileAndLink(const char*);
    void onInvalidate(const UniformEventArgs& args);
    void addUniform(ShaderParameterBase* param, int loc);
    int  acquire();
    void release();
    size_t numUniforms() const {
        return mUniform.size();
    }

    ShaderParameterBase* getUniform(size_t n) const {
        return mUniform[n];
    }

    template <typename T>
    ShaderParameter<T>* getUniformByName(const char* name) {
        return reinterpret_cast<ShaderParameter<T>* >(mUniformNames[name]);
    }

    const std::string& getSource() const;

    static ShaderProgram* createFromStream(IGraphicsDevice*, std::istream*);
};

}
}

typedef helix::gfx::ShaderProgram hxShaderProgram;

#endif
