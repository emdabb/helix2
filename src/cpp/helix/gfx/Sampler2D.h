/**
 * @file sampler2d.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef SAMPLER2D_H_
#define SAMPLER2D_H_

#include <helix/gfx/TextureAddressMode.h>
#include <helix/gfx/TextureFilter.h>

namespace helix {
namespace gfx {

class Texture2D;

struct Sampler2D {
    Sampler2D()
    : TextureRef(NULL)
    , SamplerID(0)
    , Filter(TextureFilter::Point)
    , AddressU(TextureAddressMode::Clamp)
    , AddressV(TextureAddressMode::Clamp)
    , AddressW(TextureAddressMode::Clamp)
    {

    }
    Texture2D* TextureRef;
    int SamplerID;
    int Filter;
    int AddressU;
    int AddressV;
    int AddressW;
};
}
}

typedef helix::gfx::Sampler2D hxSampler2D;


#endif /* SAMPLER2D_H_ */
