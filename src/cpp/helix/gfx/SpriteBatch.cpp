/*
 * sprite_batch.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#include <helix/gfx/SpriteBatch.h>
#include <helix/gfx/SpriteFont.h>
#include <assert.h>
#include <helix/core/MathHelper.h>
#include <helix/gfx/PrimitiveType.h>
#include <helix/gfx/VertexStream.h>
#include <helix/gfx/Viewport.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/Sampler2D.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/ShaderParameter.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/VertexStream.h>
#include <helix/gfx/VertexElement.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/TextureFilter.h>

using namespace helix::core;
using namespace helix::gfx;

const VertexElement SpriteVertex::el[] = {
    { offsetof(SpriteVertex, pos),      VertexElementFormat::Vector3,   VertexStream::Position },
    { offsetof(SpriteVertex, coord),    VertexElementFormat::Vector2,   VertexStream::TextureCoordinate0 },
    { offsetof(SpriteVertex, tint),     VertexElementFormat::Color,     VertexStream::PrimaryColor }
};

const VertexDeclaration* SpriteVertex::decl = VertexDeclaration::create(SpriteVertex::el, 3, sizeof(SpriteVertex));


SpriteBatch::SpriteBatch(IGraphicsDevice* dev, int capacity)
: mNumSpritesToDraw(0)
, mVertexBuffer(NULL)
, mWorldViewProjParameter(NULL)
, mBaseSamplerParameter(NULL)
, mGraphicsDevice(dev)
, mShader(NULL)
, mCapacity(capacity)
{
  assert(capacity > MinBatchSize && capacity <= MaxBatchSize);
  mVertexBuffer = new VertexBuffer(dev, SpriteVertex::decl, 2 * capacity * VerticesPerSprite * sizeof(SpriteVertex));
}

SpriteBatch::~SpriteBatch() {
  delete mVertexBuffer;
  mVertexBuffer = NULL;
}

void SpriteBatch::begin() {
    begin(0, BlendState::AlphaBlend, Matrix::Identity, NULL);
}

void SpriteBatch::begin(const int fx, const BlendState& blend, const Matrix& transform, ShaderProgram* sh = NULL) {
  mCachedBlendState = mGraphicsDevice->getBlendState();
  mCachedRasterizerState = mGraphicsDevice->getRasterizerState();
  RasterizerState rasterizerState = mCachedRasterizerState;
  rasterizerState.CullMode= CullMode::CullNone;
  mGraphicsDevice->setBlendState(blend);
  mGraphicsDevice->setRasterizerState(rasterizerState);

  Viewport vp = mGraphicsDevice->getViewport();
  Matrix orthoTransform;
  Matrix::createOrthoOffcentre(0, vp.W, vp.H, 0, -1, 1, &orthoTransform);
  Matrix::mul(transform, orthoTransform, &mTransform);

  mShader = sh;

  if(mShader != NULL) {
      mBaseSamplerParameter   = mShader->getUniformByName<Sampler2D>("BaseSampler");
      mWorldViewProjParameter = mShader->getUniformByName<Matrix>("_WorldViewProj");
  } else {
      mBaseSamplerParameter = NULL;
      mWorldViewProjParameter = NULL;
  }
}

void SpriteBatch::draw(Texture2D* tex, const Rectangle& dst, const Color& col) {
  draw(tex, dst, NULL, col);
}
void SpriteBatch::draw(Texture2D* tex, const Rectangle& dst, Rectangle* src, const Color& col) {
  draw(tex, dst, src, col, (float)0., Vector2::Zero, 0, (float)0.);
}

void SpriteBatch::draw(Texture2D* tex, const Rectangle& dst, Rectangle* src, const Color& col, float rot, const Vector2& pivot, const int fx, float depth) {
  draw(tex, dst, src, col, rot, pivot, Vector2::One, fx, depth);
}

void SpriteBatch::draw(
    Texture2D* tex,
    const Rectangle& dst,
    Rectangle* src,
    const Color& col,
    float rot,
    const Vector2& pivot,
    const Vector2& scale,
    const int fx,
    float depth) {
  SpriteInfo* info = &mQueue[mNumSpritesToDraw];
  float sx, sy, sw, sh;
  if(src) {
    sx = (float)src->X;
    sy = (float)src->Y;
    sw = (float)src->W;
    sh = (float)src->H;
  } else {
    sx = (float)0.;
    sy = (float)0.;
    sw = (float)tex->getWidth();
    sh = (float)tex->getHeight();
  }

  info->tex = tex;
  info->dst.X = (float) dst.X;
  info->dst.Y = (float) dst.Y;
  info->dst.Z = (float) dst.W * scale.X;
  info->dst.W = (float) dst.H * scale.Y;
  info->orig = pivot;
  info->rot = rot;
  info->depth = depth;
  info->fx = fx;
  info->tint = col;
  info->src.X = (float) sx;
  info->src.Y = (float) sy;
  info->src.Z = (float) sw;
  info->src.W = (float) sh;

  mNumSpritesToDraw++;
  if(mNumSpritesToDraw == mCapacity) {
      flush();
  }
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& pos, Rectangle* src, const Color& col, float rot, const Vector2& pivot, float scale, const int fx, float depth) {
    Vector2 vScale = { scale, scale };
    draw(tex, pos, src, col, rot, pivot, vScale, fx, depth);
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& pos, Rectangle* src, const Color& col, float rot, const Vector2& pivot, const Vector2& scale, const int fx, float depth) {
  SpriteInfo* info = &mQueue[mNumSpritesToDraw];
  float sx, sy, sw, sh;
  if(src) {
    sx = (float)src->X;
    sy = (float)src->Y;
    sw = (float)src->W;
    sh = (float)src->H;
  } else {
    sx = (float)0.;
    sy = (float)0.;
    sw = (float)tex->getWidth();
    sh = (float)tex->getHeight();
  }

  info->tex = tex;
  info->dst.X = (float) pos.X;
  info->dst.Y = (float) pos.Y;
  info->dst.Z = (float) sw * scale.X;
  info->dst.W = (float) sh * scale.Y;
  info->orig = pivot;
  info->rot = rot;
  info->depth = depth;
  info->fx = fx;
  info->tint = col;
  info->src.X = (float) sx;
  info->src.Y = (float) sy;
  info->src.Z = (float) sw;
  info->src.W = (float) sh;

  mNumSpritesToDraw++;
  if(mNumSpritesToDraw == mCapacity) {
    flush();
  }
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& at, const Color& col) {
  Rectangle dst;
  dst.X = (int32_t)at.X;
  dst.Y = (int32_t)at.Y;
  dst.W = tex->getWidth();
  dst.H = tex->getHeight();
  draw(tex, dst, NULL, col, (float)0., Vector2::Zero, Vector2::One, SpriteEffect::NoEffect, (float)0.);
}

void SpriteBatch::draw(Texture2D* tex, const Vector2& at, Rectangle* src, const Color& col) {
    Rectangle dst;
    dst.X = (int32_t)at.X;
    dst.Y = (int32_t)at.Y;
    dst.W = tex->getWidth();
    dst.H = tex->getHeight();
    draw(tex, dst, src, col, (float)0., Vector2::Zero, Vector2::One, SpriteEffect::NoEffect, (float)0.);
}

void SpriteBatch::drawString(SpriteFont* font, const char* text, const Vector2& at, const Color& color) {
    drawString(font, text, at, color, 0.0f, Vector2::Zero, 1.f, 0, 0.0f);
}

void SpriteBatch::drawString(SpriteFont* font, const char* text, const Vector2& at, const Color& color, float rot, const Vector2& origin, float scale, const int fx, float depth) {
    Vector2 cursor  = at;
    Texture2D* tex  = font->getTexture();
    Vector2 dir     = Vector2::createFromAngle(rot);
    for (size_t i = 0; i < strlen(text); i++) {
        if(text[i] == '\n') {
            cursor.Y += (float)font->getLineHeight();
            cursor.X  = at.X;
        }
        GlyphInfo& info = font->getGlyphInfo((int) text[i]);
        float px = (cursor.X + info.Bearing.X);
        float py = (cursor.Y - info.Rect.H + (info.Rect.H - info.Bearing.Y));
        Rectangle dst = {
            (int32_t)px,
            (int32_t)py,
            info.Rect.W,
            info.Rect.H
        };
        draw(tex, dst, &info.Rect, color, rot, Vector2::Zero, SpriteEffect::NoEffect, 0.0f);
        cursor.X += dir.X * info.Advance;
        cursor.Y += dir.Y * font->getLineHeight();
    }
}

void SpriteBatch::end() {
  flush();
  mGraphicsDevice->setBlendState(mCachedBlendState);
}

void SpriteBatch::getTransform(Matrix* out) {
    *out = mTransform;
}

//__attribute__((optimize("unroll-loops")))
void SpriteBatch::copyToVertices(const SpriteInfo& src, SpriteVertex* dst) {
  float dx = (float)1. / (float)src.tex->getWidth();
  float dy = (float)1. / (float)src.tex->getHeight();

  static const Vector2 corner_offsets[] = {
      Vector2::Zero,
      Vector2::Right,
      Vector2::One,
      Vector2::Up
  };

  Vector2 rot = src.rot != (float)1. ?
      Vector2::createFromAngle(src.rot) :
      Vector2::Right;

  Vector2 orig = src.orig;
  orig.X /= src.src.Z == (float)0. ? MathHelper::EPSILON : src.src.Z;
  orig.Y /= src.src.W == (float)0. ? MathHelper::EPSILON : src.src.W;

  for(int i=0; i < VerticesPerSprite; i++) {
    Vector2 corner = corner_offsets[i];
    Vector2 pos;
    pos.X = (corner.X - orig.X) * src.dst.Z;
    pos.Y = (corner.Y - orig.Y) * src.dst.W;

    dst->pos.X = src.dst.X + (pos.X * rot.X) - (pos.Y * rot.Y);
    dst->pos.Y = src.dst.Y + (pos.X * rot.Y) + (pos.Y * rot.X);
    dst->pos.Z = src.depth;
    dst->tint  = src.tint;

    corner = corner_offsets[i ^ src.fx];
    dst->coord.X = (src.src.X + corner.X * src.src.Z) * dx;
    dst->coord.Y = (src.src.Y + corner.Y * src.src.W) * dy;
    dst++;
  }
}

void SpriteBatch::flush() {
  if(mNumSpritesToDraw <= 0) {
    return;
  }
  /**
   * copy sprite information into respective vertices
   */
  for(int i=0; i < mNumSpritesToDraw; i++) {
    copyToVertices(mQueue[i], &mVertices[i * VerticesPerSprite]);
  }
  /**
   *
   * set the vertex buffer data
   *
   */
  mVertexBuffer->setData<SpriteVertex>(&mVertices[0], 0, mNumSpritesToDraw * VerticesPerSprite);
  mGraphicsDevice->setStreamSource(mVertexBuffer);
  int beg = 0;
  Texture2D* tex = NULL;
  for(int i=0; i < mNumSpritesToDraw; i++) {
    if(tex != mQueue[i].tex) {
      if(i > beg) {
        renderBatch(tex, beg, i - beg);
      }
      tex = mQueue[i].tex;
      beg = i;
    }
  }
  renderBatch(tex, beg, mNumSpritesToDraw - beg);
  mNumSpritesToDraw = 0;

}
void SpriteBatch::renderBatch(Texture2D* tex, int off, size_t count) {
  size_t beg = off * VerticesPerSprite;
  //sampler2d smp0 = { tex, 0, 0, 0 };
  mSampler2D.TextureRef = tex;
  mSampler2D.SamplerID = 0;
  mSampler2D.Filter = TextureFilter::Linear;


  if(mShader) {
      mBaseSamplerParameter->setValue(&mSampler2D);
      mWorldViewProjParameter->setValue(&mTransform);
      mShader->acquire();
  }

  mGraphicsDevice->drawPrimitives(PrimitiveType::TriangleFan, beg, VerticesPerSprite, count);
  if(mShader) {
      mShader->release();
  }
}
