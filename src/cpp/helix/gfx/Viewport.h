#ifndef HELIX_GFX_VIEWPORT_H_
#define HELIX_GFX_VIEWPORT_H_

#include <helix/core/Rectangle.h>
#include <helix/core/Vector3.h>
#include <helix/core/Matrix.h>

namespace helix {
namespace gfx {

struct Viewport : public core::Rectangle {
    real MinDepth, MaxDepth;
    static void project(const Viewport&, const hxVector3& in, const hxMatrix& wvp, hxVector3* out);
    static void unproject(const Viewport&, const hxVector3& in, const hxMatrix& wvp, hxVector3* out);
};

}
}

typedef helix::gfx::Viewport hxViewport;

#endif
