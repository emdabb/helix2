#include <helix/gfx/VertexDeclarationBuilder.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/VertexElement.h>

using namespace helix::gfx;

VertexDeclarationBuilder::VertexDeclarationBuilder()
: mStride(0) {
    mElements.clear();
}

VertexDeclarationBuilder::~VertexDeclarationBuilder() {

}

VertexDeclarationBuilder& VertexDeclarationBuilder::element(size_t id, size_t offset, const int type, const int streamId) {
    if(id >= mElements.size()) {
        mElements.resize(id + 1);
    }
    mElements[id].offset              = offset;
    mElements[id].vertexElementFormat = type;
    mElements[id].vertexElementUsage  = streamId;
    return *this;
}

//VertexDeclarationBuilder& VertexDeclarationBuilder::element(const VertexElement& a) {
//    mElements.push_back(a);
//    return *this;
//}
VertexDeclarationBuilder& VertexDeclarationBuilder::stride(size_t a) {
    mStride = a;
    return *this;
}

VertexDeclaration* VertexDeclarationBuilder::build() {
    return new VertexDeclaration(&mElements[0], mElements.size(), mStride);
}
