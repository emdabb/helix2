/**
 * @file AnimationInfo.cpp
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Apr 21, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */


#include "AnimationInfo.h"
// #include <helix/core/Vector3.h>
// #include <helix/core/Quaternion.h>

using namespace helix;
using namespace helix::core;
using namespace helix::gfx;

void Transform::lerp(const_ref a, const_ref b, float c, ptr d) {
    Vector3::lerp(a.Position, b.Position, c, &d->Position);
    Vector3::lerp(a.Scale, b.Scale, c, &d->Scale);
    Quaternion::slerp(a.Orientation, b.Orientation, c, &d->Orientation);
}
