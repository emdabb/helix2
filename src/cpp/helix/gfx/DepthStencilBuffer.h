/*
 * DepthStencilBuffer.h
 *
 *  Created on: Jul 20, 2015
 *      Author: miel
 */

#ifndef HELIX_GFX_DEPTHSTENCILBUFFER_H_
#define HELIX_GFX_DEPTHSTENCILBUFFER_H_

#include <helix/gfx/IGraphicsResource.h>
#include <helix/core/ILockable.h>

namespace helix {
namespace gfx {

class  DepthStencilBuffer;
struct IGraphicsDevice;

namespace impl {
struct IDepthStencilBuffer : public hxLockable {
	virtual ~IDepthStencilBuffer() {}
	virtual void create(const DepthStencilBuffer*) = 0;
	virtual void destroy() = 0;
};
}

struct DepthStencilFormat {
	enum {
		Depth16,
		Depth24,
		Depth24Stencil8,
		Depth32
	};
};

class DepthStencilBuffer : public IGraphicsResource {
	typedef impl::IDepthStencilBuffer Pimpl;
	IGraphicsDevice* mGraphicsDevice;
	Pimpl* mPimpl;
	int mWidth;
	int mHeight;
	const int mDepthStencilFormat;
public:
	DepthStencilBuffer(IGraphicsDevice*, int, int, const int);
	virtual ~DepthStencilBuffer();
	int getWidth() const;
	int getHeight() const;
	const int getDepthStencilFormat() const;
	void create();
	void destroy();
	const Pimpl* getImpl() const;
};

}
}



#endif /* HELIX_GFX_DEPTHSTENCILBUFFER_H_ */
