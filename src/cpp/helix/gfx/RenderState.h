/*
 * render_state.h
 *
 *  Created on: Jun 27, 2014
 *      Author: miel
 */

#ifndef RENDER_STATE_H_
#define RENDER_STATE_H_

#include <helix/Types.h>

namespace helix {
namespace gfx {

struct FillMode {
    enum {
        Solid,		//	Draw solid faces for each primitive.
        WireFrame	//	Draw lines connecting the vertices that define a primitive face.
    };

};

struct CompareFunction {
    enum {
        AlwaysPass,			//	Always pass the test.
        Equal,			//	Accept the new pixel if its value is equal to the value of the current pixel.
        Greater,		//	Accept the new pixel if its value is greater than the value of the current pixel.
        GreaterEqual,	//	Accept the new pixel if its value is greater than or equal to the value of the current pixel.
        Less,			//	Accept the new pixel if its value is less than the value of the current pixel.
        LessEqual,		//	Accept the new pixel if its value is less than or equal to the value of the current pixel.
        Never,			//	Always fail the test.
        NotEqual		//	Accept the new pixel if its value does not equal the value of the current pixel.

    };
};

struct StencilOperation {
    enum {
        Decrement,				//	Decrements the stencil-buffer entry, wrapping to the maximum value if the new value is less than 0.
        DecrementSaturation,	//	Decrements the stencil-buffer entry, clamping to 0.
        Increment,				//	Increments the stencil-buffer entry, wrapping to 0 if the new value exceeds the maximum value.
        IncrementSaturation,	//	Increments the stencil-buffer entry, clamping to the maximum value.
        Invert,					//	Inverts the bits in the stencil-buffer entry.
        Keep,					//	Does not update the stencil-buffer entry. This is the default value.
        Replace,				//	Replaces the stencil-buffer entry with a reference value.
        Zero					//	Sets the stencil-buffer entry to 0.
    };
};

struct Blend {
    enum {
        Zero,					//	Each component of the color is multiplied by (0, 0, 0, 0).
        One,					//	Each component of the color is multiplied by (1, 1, 1, 1).
        SourceColor,			//	Each component of the color is multiplied by the source color. This can be represented as (Rs, Gs, Bs, As), where R, G, B, and A respectively stand for the red, green, blue, and alpha source values.
        InverseSourceColor,		//	Each component of the color is multiplied by the inverse of the source color. This can be represented as (1 − Rs, 1 − Gs, 1 − Bs, 1 − As) where R, G, B, and A respectively stand for the red, green, blue, and alpha destination values.
        SourceAlpha,			//	Each component of the color is multiplied by the alpha value of the source. This can be represented as (As, As, As, As), where As is the alpha source value.
        InverseSourceAlpha,		//	Each component of the color is multiplied by the inverse of the alpha value of the source. This can be represented as (1 − As, 1 − As, 1 − As, 1 − As), where As is the alpha destination value.
        DestinationAlpha,		//	Each component of the color is multiplied by the alpha value of the destination. This can be represented as (Ad, Ad, Ad, Ad), where Ad is the destination alpha value.
        InverseDestinationAlpha,//	Each component of the color is multiplied by the inverse of the alpha value of the destination. This can be represented as (1 − Ad, 1 − Ad, 1 − Ad, 1 − Ad), where Ad is the alpha destination value.
        DestinationColor,		//	Each component color is multiplied by the destination color. This can be represented as (Rd, Gd, Bd, Ad), where R, G, B, and A respectively stand for red, green, blue, and alpha destination values.
        InverseDestinationColor,//	Each component of the color is multiplied by the inverse of the destination color. This can be represented as (1 − Rd, 1 − Gd, 1 − Bd, 1 − Ad), where Rd, Gd, Bd, and Ad respectively stand for the red, green, blue, and alpha destination values.
        SourceAlphaSaturation,	//	Each component of the color is multiplied by either the alpha of the source color, or the inverse of the alpha of the source color, whichever is greater. This can be represented as (f, f, f, 1), where f = min(A, 1 − Ad).
        BlendFactor,			//	Each component of the color is multiplied by a constant set in BlendFactor.
        InverseBlendFactor		//	Each component of the color is multiplied by the inverse of a constant set in BlendFactor.

    };
};

struct BlendFunction {
    enum {
        /**
         * The result is the destination added to the source.
         * Result = (Source Color * Source Blend) + (Destination Color * Destination Blend)
         */
        Add,
        /**
         * The result is the maximum of the source and destination.
         * Result = max( (Source Color * Source Blend), (Destination Color * Destination Blend) )
         */
        Max,
        /**
         * The result is the minimum of the source and destination.
         * Result = min( (Source Color * Source Blend), (Destination Color * Destination Blend) )
         */
        Min,
        /**
         * The result is the source subtracted from the destination.
         * Result = (Destination Color * Destination Blend) − (Source Color * Source Blend)
         */
        ReverseSubtract,
        /**
         * The result is the destination subtracted from the source.
         * Result = (Source Color * Source Blend) − (Destination Color * Destination Blend)
         */
        Subtract
    };
};

struct CullMode {
    enum {
        CullNone, //	Do not cull back faces.
        CullClockwiseFace,//	Cull back faces with clockwise vertices.
        CullCounterClockwiseFace//	Cull back faces with counterclockwise vertices.

    };
};

struct ColorWriteChannels {
    enum {
        WriteToNone,
        Red,
        Green,
        Blue,
        Alpha,
        All
    };
};

struct BlendState {
    int AlphaBlendFunction;		//	The arithmetic operation when blending alpha values. The default is BlendFunction.Add.
    int AlphaDestinationBlend;	//	The blend factor for the destination alpha, which is the percentage of the destination alpha included in the blended result. The default is Blend.One.
    int AlphaSourceBlend;		//	The alpha blend factor. The default is Blend.One.
    uint32_t BlendFactor;		//	The four-component (RGBA) blend factor for alpha blending.
    int ColorBlendFunction;		//	The arithmetic operation when blending color values. The default is BlendFunction.Add.
    int ColorDestinationBlend;	//	The blend factor for the destination color. The default is Blend.One.
    int ColorSourceBlend;		//	The blend factor for the source color. The default is Blend.One.
    int ColorWriteChannels;		//	Which color channels (RGBA) are enabled for writing during color blending. The default value is ColorWriteChannels.None.
    int ColorWriteChannels1;	//	Which color channels (RGBA) are enabled for writing during color blending. The default value is ColorWriteChannels.None.
    int ColorWriteChannels2;	//	Which color channels (RGBA) are enabled for writing during color blending. The default value is ColorWriteChannels.None.
    int ColorWriteChannels3;	//	Which color channels (RGBA) are enabled for writing during color blending. The default value is ColorWriteChannels.None.
    uint32_t MultiSampleMask;	// Defines which samples can be written during multisampling. The default is 0xffffffff.

    static const BlendState Default;
    static const BlendState Additive;
    static const BlendState AlphaBlend;
    static const BlendState NonPremultiplied;
    static const BlendState Opaque;
};

struct RasterizerState {
    int CullMode;				//	Specifies the conditions for culling or removing triangles. The default value is CullMode.CounterClockwise.
    real DepthBias;				//	Sets or retrieves the depth bias for polygons, which is the amount of bias to apply to the depth of a primitive to alleviate depth testing problems for primitives of similar depth. The default value is 0.
    int FillMode;				//	The fill mode, which defines how a triangle is filled during rendering. The default is FillMode.Solid.
    int MultiSampleAntiAlias;	//	Enables or disables multisample antialiasing. The default is true.
    int ScissorTestEnable;		//	Enables or disables scissor testing. The default is false.
    real SlopeScaleDepthBias;	//	Gets or sets a bias value that takes into account the slope of a polygon. This bias value is applied to coplanar primitives to reduce aliasing and other rendering artifacts caused by z-fighting. The default is 0.

    static const RasterizerState Default;
    static const RasterizerState CullClockwise;
    static const RasterizerState CullCounterClockwise;
    static const RasterizerState CullNone;
};

struct DepthStencilState {
    int CounterClockwiseStencilDepthBufferFail;	//	The stencil operation to perform if the stencil test passes and the depth-buffer test fails for a counterclockwise triangle. The default is StencilOperation.Keep.
    int CounterClockwiseStencilFail;			//	The stencil operation to perform if the stencil test fails for a counterclockwise triangle. The default is StencilOperation.Keep.
    int CounterClockwiseStencilFunction;		//	The comparison function to use for counterclockwise stencil tests. The default is CompareFunction.Always.
    int CounterClockwiseStencilPass;			//	The stencil operation to perform if the stencil and depth-tests pass for a counterclockwise triangle. The default is StencilOperation.Keep.
    int DepthBufferEnable;						//	Enables or disables depth buffering. The default is true.
    int DepthBufferFunction;					//	The comparison function for the depth-buffer test. The default is CompareFunction.LessEqual
    int DepthBufferWriteEnable;					//	Enables or disables writing to the depth buffer. The default is true.
    int ReferenceStencil;						//	Specifies a reference value to use for the stencil test. The default is 0.
    int StencilDepthBufferFail;					//	The stencil operation to perform if the stencil test passes and the depth-test fails. The default is StencilOperation.Keep.
    int StencilEnable;							//	Stencil enabling. The default is false.
    int StencilFail;							//	The stencil operation to perform if the stencil test fails. The default is StencilOperation.Keep.
    int StencilFunction;						//	The comparison function for the stencil test. The default is CompareFunction.Always.
    int StencilMask;							//	The mask applied to the reference value and each stencil buffer entry to determine the significant bits for the stencil test. The default mask is Int32.MaxValue.
    int StencilPass;							//	The stencil operation to perform if the stencil test passes. The default is StencilOperation.Keep.
    int StencilWriteMask;						//	The write mask applied to values written into the stencil buffer. The default mask is Int32.MaxValue.
    int TwoSidedStencilMode;					//	Enables or disables two-sided stenciling. The default is false.

    static const DepthStencilState Default;
    static const DepthStencilState DepthRead;
    static const DepthStencilState NoDepthStencilBuffer;
};

}
}

typedef helix::gfx::BlendState hxBlendState;
typedef helix::gfx::RasterizerState hxRasterizerState;
typedef helix::gfx::DepthStencilState hxDepthStencilState;


#endif /* RENDER_STATE_H_ */
