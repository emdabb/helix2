#ifndef HELIX_GFX_IGRAPHICS_DEVICE_H_
#define HELIX_GFX_IGRAPHICS_DEVICE_H_

#include <helix/Types.h>

namespace helix {
namespace gfx {

struct IGraphicsContext;
struct BlendState;
struct DepthStencilState;
struct RasterizerState;
struct Color;
class  VertexBuffer;
struct Viewport;
class  Texture2D;

struct IGraphicsDevice {
	virtual ~IGraphicsDevice(){}
    virtual void  createWithContext(IGraphicsContext*) = 0;
    virtual IGraphicsContext* getContext() const = 0;
    virtual void  clear(const Color&, const int, const float = 1.0f, const int = 0) = 0;
    virtual bool  startRendering() = 0;
    virtual void  stopRendering() = 0;
    virtual const char* api() const = 0;
    virtual const BlendState& getBlendState() const = 0;
    virtual const RasterizerState& getRasterizerState() const = 0;
    virtual const DepthStencilState& getDepthStencilState() const = 0;
    virtual void  setBlendState(const BlendState&) = 0;
    virtual void  setRasterizerState(const RasterizerState&) = 0;
    virtual void  setDepthStencilState(const DepthStencilState&) = 0;
    /**
     *
     * @param primitiveType
     * @param start
     * @param count
     * @param primitiveCount
     */
    virtual void  drawPrimitives(const int primitiveType, int start, size_t count, size_t primitiveCount) = 0;
    virtual void  drawIndexedPrimitives() = 0;
    virtual void  setStreamSource(VertexBuffer*) = 0;
    virtual const Viewport& getViewport() const = 0;
    virtual void  setViewport(const Viewport&) = 0;

    virtual void setRenderTarget(Texture2D*, int) = 0;
    virtual void resolveRenderTarget(int) = 0;
};

}
}

typedef helix::gfx::IGraphicsDevice hxGraphicsDevice;

#endif //HELIX_GFX_IGRAPHICS_DEVICE_H_


