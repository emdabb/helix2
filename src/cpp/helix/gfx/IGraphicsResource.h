#ifndef HELIX_GFX_IGRAPHICSRESOURCE_H_
#define HELIX_GFX_IGRAPHICSRESOURCE_H_

namespace helix {
namespace gfx {

struct IGraphicsResource;

struct GraphicsResourceEventArgs {
    IGraphicsResource* thiz;
};

struct IGraphicsResource {
    virtual ~IGraphicsResource() {}
    virtual void onContextLost(const GraphicsResourceEventArgs&) = 0;
    virtual void onContextReset(const GraphicsResourceEventArgs&) = 0;

};
}
}

#endif
