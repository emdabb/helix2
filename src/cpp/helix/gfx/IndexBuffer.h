#ifndef HELIX_GFX_INDEXBUFFER_H_
#define HELIX_GFX_INDEXBUFFER_H_

#include <helix/Types.h>
#include <helix/gfx/IGraphicsResource.h>

namespace helix {
namespace gfx {

class IndexBuffer;
class IGraphicsDevice;
    
    struct IndexType {
        enum {
            UnsignedByte,
            UnsignedShort,
            UnsignedInt
        };
    };

namespace impl {
struct IIndexBufferImpl {
    virtual ~IIndexBufferImpl() {}
    virtual void create(IndexBuffer*) = 0;
    virtual int  acquire() = 0;
    virtual void release() = 0;
    virtual void setData(const void*, size_t, size_t) = 0;
    virtual void getData(void*, size_t, size_t) = 0;
};
}

class IndexBuffer : public IGraphicsResource {
    typedef impl::IIndexBufferImpl Impl;
    IGraphicsDevice*    mGraphicsDevice;
    const int           mIndexFormat;
    size_t              mSizeInBytes;
    Impl*               mPimpl;
    void*               mBackup;
public:
    IndexBuffer(IGraphicsDevice*, const int, size_t);
    virtual ~IndexBuffer();

    void create();
    void destroy();

    virtual void onContextLost(const GraphicsResourceEventArgs&);
    virtual void onContextReset(const GraphicsResourceEventArgs&);

    Impl& getImpl() const {
        return *mPimpl;
    }    

    template <typename T>
    void setData(const T* src, size_t beg, size_t end) {
        if(mPimpl->acquire()) {
            mPimpl->setData(src, beg, end * sizeof(T));
            mPimpl->release();
        }

    }

    template <typename T>
    void getData(T* dst, size_t beg, size_t end) {
        if(mPimpl->acquire()) {
            mPimpl->getData(dst, beg, end * sizeof(T));
            mPimpl->release();
        }

    }

    
};

}
}

#endif
