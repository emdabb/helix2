#ifndef HELIX_GFX_VERTEXDATA_H_
#define HELIX_GFX_VERTEXDATA_H_

#include <helix/Types.h>
#include <vector>

namespace helix {
namespace gfx {
    struct IVertexChannel {
    virtual ~IVertexChannel() {}
    virtual const size_t offset() const = 0;
    virtual const int    type() const = 0;
    virtual const int    binding() const = 0;
    virtual const size_t size() const = 0;
};

template <typename T>
class VertexChannel : public IVertexChannel {
    size_t 			mOffset;
    size_t 			mSize;
    const int		mType;
    const int 		mBinding;
    std::vector<T> 	mData;
public:
    VertexChannel(size_t off, size_t sz, const int t, const int b)
    : mOffset(off)
    , mSize(sz)
    , mType(t)
    , mBinding(b)
    {

    }

    virtual ~VertexChannel() {

    }

    virtual const size_t offset() const {
        return mOffset;
    }

    virtual const size_t size() const {
        return mSize;
    }

    virtual const int type() const {
        return mType;
    }

    virtual const int binding() const {
        return mBinding;
    }
};

class VertexDeclaration;

class VertexData {
    size_t 							mOffset;
    std::vector<IVertexChannel*> 	mChannels;
public:
    VertexData();

    virtual ~VertexData();

    template <typename T>
    size_t add_channel(size_t off, const int type, const int bind) {
        IVertexChannel* c = new VertexChannel<T>(off, sizeof(T), type, bind);
        mOffset += sizeof(T);
        mChannels.push_back(c);
        return mOffset;
    }

    template <typename T>
    VertexChannel<T>* getChannel(const int bind) {
        for(size_t i=0; i < mChannels.size(); i++){
            if(mChannels[i]->binding() == bind) {
                return reinterpret_cast<VertexChannel<T>* >(mChannels[i]);
            }
        }
        return NULL;
    }

    const VertexDeclaration* getDeclaration();
};
}
}

#endif
