#ifndef HELIX_GFX_SPRITEFONT_H_
#define HELIX_GFX_SPRITEFONT_H_

#include <iostream>
#include <vector>
#include <helix/core/Vector2.h>
#include <helix/core/Rectangle.h>


namespace helix {
namespace gfx {

class  Texture2D;
struct IGraphicsDevice;

struct GlyphInfo {
    real            Advance;
    real            Ascent;
    core::Vector2   Bearing;
    core::Rectangle Rect;
};

struct SpriteFontMetrics {
    int Ascent;
    int CapHeight;
    int MedianHeight;
    int Descent;
    int LineHeight;
    int MaxAdvance;
};

class SpriteFont {
    Texture2D*              mTexture;
    SpriteFontMetrics       mMetrics;
    std::vector<GlyphInfo>  mInfos;
public:
    SpriteFont(GlyphInfo[], size_t, Texture2D*);
    virtual ~SpriteFont();
    void setLineHeight(int);
    int  getLineHeight() const;
    real getSpacing() const;
    char getDefaultCharacter() const;
    core::Vector2 measureString(const char*);
    Texture2D* getTexture() const;
    GlyphInfo& getGlyphInfo(int);
    const GlyphInfo& getGlyphInfo(int) const;
    static SpriteFont* createFromStream(IGraphicsDevice*, std::istream*, size_t);


};

}
}

typedef helix::gfx::SpriteFont  hxSpriteFont;

#endif
