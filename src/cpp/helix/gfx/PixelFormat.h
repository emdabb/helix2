#ifndef HELIX_GFX_PIXELFORMAT_H_
#define HELIX_GFX_PIXELFORMAT_H_

namespace helix {
namespace gfx {

struct PixelFormat {

    static const int Bpp[];
    static const int RedBits[];
    static const int GreenBits[];
    static const int BlueBits[];
    static const int AlphaBits[];

enum {
    Color,//   (Unsigned format) 32-bit ARGB pixel format with alpha, using 8 bits per channel.
    Bgr565,//  (Unsigned format) 16-bit BGR pixel format with 5 bits for blue, 6 bits for green, and 5 bits for red.
    Bgra5551,//    (Unsigned format) 16-bit BGRA pixel format where 5 bits are reserved for each color and 1 bit is reserved for alpha.
    Bgra4444,//    (Unsigned format) 16-bit BGRA pixel format with 4 bits for each channel.
    Dxt1,//    DXT1 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
    Dxt3,//    DXT3 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
    Dxt5,//    DXT5 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
    NormalizedByte2,// (Signed format) 16-bit bump-map format using 8 bits each for u and v data.
    NormalizedByte4,// (Signed format) 32-bit bump-map format using 8 bits for each channel.
    Rgba1010102,// (Unsigned format) 32-bit RGBA pixel format using 10 bits for each color and 2 bits for alpha.
    Rg32,//    (Unsigned format) 32-bit pixel format using 16 bits each for red and green.
    Rgba64,//  (Unsigned format) 64-bit RGBA pixel format using 16 bits for each component.
    Alpha8,//  (Unsigned format) 8-bit alpha only.
    Single,//  (IEEE format) 32-bit float format using 32 bits for the red channel.
    Vector2,// (IEEE format) 64-bit float format using 32 bits for the red channel and 32 bits for the green channel.
    Vector4,// (IEEE format) 128-bit float format using 32 bits for each channel (alpha, blue, green, red).
    HalfSingle,//  (Floating-point format) 16-bit float format using 16 bits for the red channel.
    HalfVector2,// (Floating-point format) 32-bit float format using 16 bits for the red channel and 16 bits for the green channel.
    HalfVector4,// (Floating-point format) 64-bit float format using 16 bits for each channel (alpha, blue, green, red).
    //HdrBlendable    (Floating-point format) for high dynamic range data.
};
};

}
}

#endif
