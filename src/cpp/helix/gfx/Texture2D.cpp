#include <helix/gfx/Texture2D.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/Surface.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/core/Factory.h>
#include <cassert>

using namespace helix::gfx;
using namespace helix::core;

Texture2D::Texture2D(IGraphicsDevice* device, size_t w, size_t h, const int pixelFormat, bool hasLevels)
: mGraphicsDevice(device)
, mWidth(w)
, mHeight(h)
, mPixelFormat(pixelFormat)
, mHasLevels(hasLevels)
, mPimpl(NULL)
, mBackup(NULL)
{
    create();
}

Texture2D::~Texture2D() {
    destroy();
}

void Texture2D::create() {
    if(!mPimpl) {
        mPimpl = Factory<impl::ITexture2DImpl>::getInstance().create(mGraphicsDevice->api());
        assert(mPimpl);
        mPimpl->create(this);
    }
}

void Texture2D::destroy() {
    if(mPimpl) {
        delete mPimpl;
        mPimpl = NULL;
    }
}


size_t Texture2D::getWidth() const {
    return mWidth;
}

size_t Texture2D::getHeight() const{
    return mHeight;
}

const int Texture2D::getPixelFormat() const {
    return mPixelFormat;
}

bool Texture2D::hasLevels() const {
    return mHasLevels;
}


void Texture2D::onContextLost(const GraphicsResourceEventArgs&) {
    if(NULL == mBackup) {
        mBackup = malloc(mWidth * mHeight * PixelFormat::Bpp[mPixelFormat] << 3);
        mPimpl->getData(mBackup, 0, 0, 0, mWidth, mHeight);
        destroy();
    }
}

void Texture2D::onContextReset(const GraphicsResourceEventArgs&) {
    if(mBackup != NULL) {
        create();
        mPimpl->setData(mBackup, 0, 0, 0, mWidth, mHeight);
        free(mBackup);
        mBackup = NULL;
    }
}

Texture2D* Texture2D::createFromSurface(IGraphicsDevice* device, const Surface& surface) {
    size_t w = surface.getWidth();
    size_t h = surface.getHeight();
    const int pfd = surface.getPixelFormat();
    Texture2D* tex = new Texture2D(device, w, h, pfd, true);
    tex->setData<char>(static_cast<const char*>(surface.getData()), 0, 0, 0, w, h);
    return tex;
}
