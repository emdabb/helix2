#ifndef HELIX_GFX_TEXTURE_2D_H_
#define HELIX_GFX_TEXTURE_2D_H_

#include <helix/Types.h>
#include <helix/gfx/IGraphicsResource.h>

namespace helix {
namespace gfx {

class  Texture2D;
class  Surface;
struct IGraphicsDevice;

namespace impl {
    struct ITexture2DImpl {
        virtual ~ITexture2DImpl() {}
        virtual void create(Texture2D*) = 0;
        virtual void setData(const void*, size_t, int, int, size_t, size_t) = 0;
        virtual void getData(void*, size_t, int, int, size_t, size_t) = 0;
        virtual int  acquire() = 0;
        virtual void release() = 0;
    };
}

class Texture2D {
    typedef impl::ITexture2DImpl* pimpl;

    IGraphicsDevice* mGraphicsDevice;
    size_t      mWidth;
    size_t      mHeight;
    const int   mPixelFormat;
    bool        mHasLevels;
    pimpl       mPimpl;
    void*       mBackup;
public:
    Texture2D(IGraphicsDevice*, size_t, size_t, const int, bool);
    virtual ~Texture2D();
    size_t getWidth() const;
    size_t getHeight() const;
    const int getPixelFormat() const;
    bool hasLevels() const;
    template <typename T>
    void setData(const T* src, size_t level, int x, int y, size_t w, size_t h) {
        if(mPimpl && mPimpl->acquire()) {
            mPimpl->setData((const void*)src, level, x, y, w, h);
            mPimpl->release();
        }
    }
    template <typename T>
    void getData(T* dst, size_t level, int x, int y, size_t w, size_t h) {
        if(mPimpl && mPimpl->acquire()) {
            mPimpl->getData((void*)dst, level, x, y, w, h);
            mPimpl->release();
        }

    }
    void create();
    void destroy();

    virtual void onContextLost(const GraphicsResourceEventArgs&);
    virtual void onContextReset(const GraphicsResourceEventArgs&);

    static Texture2D* createFromSurface(IGraphicsDevice*, const Surface&);

    impl::ITexture2DImpl& getImpl() const {
        return *mPimpl;
    }
};

}
}

typedef helix::gfx::Texture2D hxTexture2D;

#endif
