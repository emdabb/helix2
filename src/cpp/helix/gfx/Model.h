/**
 * @file model.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Jun 9, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <helix/core/Matrix.h>
#include <helix/core/Short4.h>
#include <vector>
#include <string>

namespace helix {
namespace gfx {

class  VertexBuffer;
class  IndexBuffer;
class  Texture2D;
struct IGraphicsDevice;

struct ModelMesh {
    VertexBuffer* 	Vertices;
    IndexBuffer* 	Indices;
};

__declare_aligned(struct, 16)
ModelBone {
    std::string Name;
    hxMatrix WorldToBone;
    hxMatrix BindPoseInv;
    int ParentId;

    __forceinline bool hasParent() const {
        return ParentId >= 0;
    }
};

struct Material {
    Texture2D* map_Kd;
    Texture2D* map_Ks;
};

class Model {
    IGraphicsDevice* mGraphicsDevice;
    std::vector<ModelMesh*> mMeshes;
public:
    explicit Model(IGraphicsDevice* pGraphicsDevice);
    virtual ~Model();

    void addMesh(ModelMesh*);

    const ModelMesh* getMesh(size_t meshIndex);

    void draw();

    static void createFromStream(Model**, std::istream*);
    static void saveToStream(Model*, std::ostream);
};
}
}

typedef helix::gfx::Model hxModel;

#endif /* MODEL_H_ */
