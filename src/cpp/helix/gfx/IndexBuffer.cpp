#include <helix/gfx/IndexBuffer.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/core/Factory.h>
#include <assert.h>

using namespace helix::core;
using namespace helix::gfx;

IndexBuffer::IndexBuffer(IGraphicsDevice* graphicsDevice, const int indexFormat, size_t sizeInBytes) 
: mGraphicsDevice(graphicsDevice)
, mIndexFormat(indexFormat)
, mSizeInBytes(sizeInBytes)
, mPimpl(NULL)
, mBackup(NULL)
{
    create();
}

IndexBuffer::~IndexBuffer() {
    destroy();
}

void IndexBuffer::create() {
    if(NULL == mPimpl) {
        mPimpl = Factory<impl::IIndexBufferImpl>::getInstance().create(mGraphicsDevice->api());
        assert(mPimpl);
        mPimpl->create(this);
    }
}

void IndexBuffer::destroy() {
    if(mPimpl) {
        delete mPimpl;
        mPimpl = NULL;
    }
}

void IndexBuffer::onContextLost(const GraphicsResourceEventArgs& args) {
    if(mBackup == NULL) {
        mBackup = malloc(mSizeInBytes);
        mPimpl->getData(mBackup, 0, mSizeInBytes);
        destroy();
    }
}

void IndexBuffer::onContextReset(const GraphicsResourceEventArgs& args) {
    if(mBackup != NULL) {
        create();
        mPimpl->setData(mBackup, 0, mSizeInBytes);
        free(mBackup);
        mBackup = NULL;
    }
}

