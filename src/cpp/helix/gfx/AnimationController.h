/**
 * @file AnimationController.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Apr 21, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef JNI_HELIX2_GFX_ANIMATIONCONTROLLER_H_
#define JNI_HELIX2_GFX_ANIMATIONCONTROLLER_H_

#include "AnimationInfo.h"
#include <helix/core/EventHandler.h>
#include <helix/core/Matrix.h>

namespace helix {
namespace gfx {

class ModelBone;
class AnimationController;
struct AnimationClip;

struct AnimationEventArgs {
    AnimationController* 	Controller;
    AnimationInfo* 			Info;
    AnimationClip*			Animation;
};

struct AnimationClip {
    size_t 		Begin;
    size_t 		End;
    const char Name[256];
    bool		Loop;
};

class AnimationController {
    ModelBone* 	mBones;
    size_t		mNumBones;
    Transform*	mPoses;

    AnimationClip* 	mAnimationClip;
    AnimationClip* 	mCrossfadeAnimationClip;
    AnimationInfo*	mAnimationInfo;

    bool mIsFinished;
    bool mIsPlaying;
    bool mIsCrossfadeEnabled;

    float mCurrentTime;
    float mCrossfadeLerp;
    float mCrossfadeElapsed;
    float mCrossfadeTime;
public:
    hxEventHandler<AnimationEventArgs>::Type OnAnimationStart;
    hxEventHandler<AnimationEventArgs>::Type OnAnimationDone;
    hxEventHandler<AnimationEventArgs>::Type OnCrossfadeStart;
    hxEventHandler<AnimationEventArgs>::Type OnCrossfadeDone;
public:
    AnimationController(AnimationInfo* info);
    virtual ~AnimationController();
    void start(AnimationClip* animationClip);
    void crossfade(AnimationClip* crossfadeAnimationClip, float t);
    void update(int t, int dt);
    void copyAbsoluteBoneTransformsTo(hxMatrix array[]);
protected:
    void updateAnimationTime(float dt);
    void updateCrossfadeTime(float dt);
    void updateBonePoses();
    void updateAbsoluteBoneTransforms();
    void interpolateTransform(const AnimationChannel& animationChannel, float at, Transform* out);
};


}
}



#endif /* JNI_HELIX2_GFX_ANIMATIONCONTROLLER_H_ */
