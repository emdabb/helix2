#ifndef HELIX_GFX_SPRITEBATCH_H_
#define HELIX_GFX_SPRITEBATCH_H_

#include <helix/core/Vector2.h>
#include <helix/core/Vector3.h>
#include <helix/core/Vector4.h>
#include <helix/core/Rectangle.h>
#include <helix/core/Matrix.h>
#include <helix/core/Quaternion.h>
#include <helix/gfx/Color.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/VertexBuffer.h>
#include <helix/gfx/RenderState.h>
#include <helix/gfx/Sampler2D.h>

namespace helix {
namespace gfx {

template <typename T>
class ShaderParameter;
class ShaderProgram;
class Texture2D;
class SpriteFont;

struct SpriteVertex {
    core::Vector3   pos;
	core::Vector2   coord;
    Color			tint;
    static const VertexDeclaration* decl;
    static const VertexElement el[];
};

struct SpriteEffect {
enum {
    NoEffect = 0, 
    FlipHorizontal = 1,
    FlipVertical = 2,
    FlipBoth = 3
};
};


class SpriteBatch {
    //
    // internal classes.
    //
    __declare_aligned(struct, 16) SpriteInfo {
		core::Vector4   src;
		core::Vector4   dst;
		core::Vector2   orig;
        float			rot;
        float			depth;
        int				fx;
        Color			tint;
        Texture2D*		tex;
    };
private:
    const static int MaxBatchSize = 1024;
    const static int MinBatchSize = 128;
    const static int InitialQueueSize = 64;
    const static int VerticesPerSprite = 4;
    const static int IndicesPerSprite = 6;
    const static int MaxVertexCount = MaxBatchSize * VerticesPerSprite;
    const static int MaxIndexCount = MaxBatchSize * IndicesPerSprite;

    int mNumSpritesToDraw;
    SpriteInfo mQueue[MaxBatchSize];
    SpriteVertex mVertices[MaxVertexCount];
    VertexBuffer* mVertexBuffer;

	core::Matrix mTransform;

    ShaderParameter<core::Matrix>*  mWorldViewProjParameter;
    ShaderParameter<Sampler2D>* mBaseSamplerParameter;

    Sampler2D mSampler2D;

    BlendState mCachedBlendState;
    RasterizerState mCachedRasterizerState;
    DepthStencilState mCachedDepthStencilState;

    IGraphicsDevice* mGraphicsDevice;
    ShaderProgram* mShader;
    int mCapacity;
public:

    SpriteBatch(IGraphicsDevice*, int capacity = MaxBatchSize);
    virtual ~SpriteBatch();
    void begin(); 
    void begin(const int, const BlendState&, const core::Matrix&, ShaderProgram*);

    void draw(Texture2D* tex, const core::Rectangle& dst, const Color& col);
    void draw(Texture2D* tex, const core::Rectangle& dst, core::Rectangle* src, const Color& col);
    void draw(Texture2D* tex, const core::Rectangle& dst, core::Rectangle* src, const Color& col, float rot, const core::Vector2& pivot, const int fx, float depth);
    void draw(Texture2D* tex, const core::Rectangle& dst, core::Rectangle* src, const Color& col, float rot, const core::Vector2& pivot, const core::Vector2& scale, const int fx, float depth);

    void draw(Texture2D* tex, const core::Vector2& pos, const Color& col);
    void draw(Texture2D* tex, const core::Vector2& pos, core::Rectangle* src, const Color& col);
    void draw(Texture2D* tex, const core::Vector2& pos, core::Rectangle* src, const Color& col, float rot, const core::Vector2& pivot, float scale, const int fx, float depth);
    void draw(Texture2D* tex, const core::Vector2& pos, core::Rectangle* src, const Color& col, float rot, const core::Vector2& pivot, const core::Vector2& scale, const int fx, float depth);
    void drawString(SpriteFont*, const char*, const core::Vector2&, const Color&);
    void drawString(SpriteFont*, const char*, const core::Vector2&, const Color&, float rot, const core::Vector2& pivot, float scale, const int fx, float depth);
    void end();
    void getTransform(core::Matrix*);
protected:
  void flush();
  void renderBatch(Texture2D*, int off, size_t count);
  void copyToVertices(const SpriteInfo&, SpriteVertex*);
};

}
}

typedef helix::gfx::SpriteBatch hxSpriteBatch;

#endif
