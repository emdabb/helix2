#ifndef HELIX_GFX_VERTEXDECLARATIONBUILDER_H_
#define HELIX_GFX_VERTEXDECLARATIONBUILDER_H_

#include <helix/Types.h>
#include <vector>

namespace helix {
namespace gfx {

class  VertexDeclaration;
struct VertexElement;

class VertexDeclarationBuilder {
    std::vector<VertexElement> mElements;
    size_t mStride;
public:
    /**
     *
     */
    VertexDeclarationBuilder();
    /**
     *
     */
    virtual ~VertexDeclarationBuilder();
    /**
     *
     * @param n
     * @param offset
     * @param type
     * @param streamId
     * @return
     */
    VertexDeclarationBuilder& element(size_t n, size_t offset, const int type, const int streamId);
    //VertexDeclarationBuilder& element(const VertexElement& a);
    /**
     *
     * @param a
     * @return
     */

    VertexDeclarationBuilder& stride(size_t a);
    /**
     *
     * @return
     */
    VertexDeclaration* build();
};

}
}

#endif
