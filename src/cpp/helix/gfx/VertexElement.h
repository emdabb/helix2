#ifndef HELIX_GFX_VERTEXELEMENT_H_
#define HELIX_GFX_VERTEXELEMENT_H_

namespace helix {
namespace gfx {

 struct VertexElement {
     int offset;
     int vertexElementFormat;
     int vertexElementUsage;
 };

}
}

typedef helix::gfx::VertexElement hxVertexElement;

#endif
