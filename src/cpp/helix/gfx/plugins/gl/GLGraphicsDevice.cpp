#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/IGraphicsContext.h>
#include <helix/gfx/Color.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/VertexBuffer.h>
#include <helix/core/Factory.h>
#include <helix/gfx/RenderState.h>
#include <helix/gfx/Viewport.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/VertexElement.h>
#include <helix/gfx/plugins/gl/OpenGL.h>
#include <stack>

#define BUFFER_OFFSET(n)    ((char*)NULL + ((n)))

using namespace helix::core;

namespace helix {
namespace gfx {
namespace impl {

HXAPI HXEXPORT ITexture2DImpl*      new_GLTexture2D();
HXAPI HXEXPORT IVertexBufferImpl*   new_GLVertexBuffer();
HXAPI HXEXPORT IShaderProgram*      new_GLShaderProgram();
//HXAPI HXEXPORT IIndexBufferImpl*    new_GLIndexBuffer;

class GLStateManager {
    std::map<GLenum, bool> mBooleanState;
    float mLineWidth;
    GLenum mSrcBlendRGB, mDstBlendRGB;
    GLenum mSrcBlendAlpha, mDstBlendAlpha;
    GLenum mDepthFunc;

    GLenum mStencilFunc;
    GLint mStencilRef;
    GLuint mStencilMask;
    GLenum mStencilOpFail;
    GLenum mStencilOpDepthFail;
    GLenum mStencilOpPass;
    std::stack<GLuint> mTexture2D;
    std::vector<bool> mVertexAttribArray;
    bool mWriteToDepthBuffer;
    std::map<GLenum, GLuint> mFrameBufferObjects;
    Color mBlendColor;
    GLenum mBlendEquationRGB;
    GLenum mBlendEquationA;
    Viewport mViewport;
    GLenum mCullFace;
    GLenum mFrontFace;
protected:
    void initBoolean(GLenum which) {
        GLboolean isEnabled = glIsEnabled(which);
        mBooleanState[which] = isEnabled != GL_FALSE ? true : false;
    }
public:
    void init() {
        /** Optimization: guarantees a stack size of at least 1 */
        mTexture2D.push(GL_NONE);

        //glBlendEquation(GL_FUNC_ADD);
        /**
         *
         * Get initial blend values.
         *
         */
        glGetIntegerv(GL_BLEND_SRC_RGB, (GLint*) &mSrcBlendRGB);
        glGetIntegerv(GL_BLEND_SRC_ALPHA, (GLint*) &mSrcBlendAlpha);
        glGetIntegerv(GL_BLEND_DST_RGB, (GLint*) &mDstBlendRGB);
        glGetIntegerv(GL_BLEND_DST_ALPHA, (GLint*) &mDstBlendAlpha);
        /**
         *
         * Get initial float values.
         *
         */
        glGetFloatv(GL_LINE_WIDTH, (GLfloat*) &mLineWidth);
        /**
         *
         * Get initial boolean values. (WIP)
         *
         */
        initBoolean(GL_BLEND); //   glBlendFunc, glLogicOp
        initBoolean(GL_CULL_FACE); //   glCullFace
        initBoolean(GL_DEPTH_TEST); //  glDepthFunc, glDepthRange
        initBoolean(GL_DITHER); //  glEnable
        initBoolean(GL_POLYGON_OFFSET_FILL); //     glPolygonOffset
        initBoolean(GL_SAMPLE_ALPHA_TO_COVERAGE); //    glSampleCoverage
        initBoolean(GL_SAMPLE_COVERAGE); //     glSampleCoverage
        initBoolean(GL_SCISSOR_TEST); //    glScissor
        initBoolean(GL_STENCIL_TEST); //    glStencilFunc, glStencilOp

#if defined(QQQ_HAVE_OPENGL_CORE)
        initBoolean(GL_COLOR_LOGIC_OP); //  glLogicOp
        initBoolean(GL_DEPTH_CLAMP);//  glEnable
        initBoolean(GL_DEBUG_OUTPUT);//     glEnable
        initBoolean(GL_DEBUG_OUTPUT_SYNCHRONOUS);//     glEnable
        //initBoolean(GL_CLIP_DISTANCEi);//     glEnable
        initBoolean(GL_FRAMEBUFFER_SRGB);//     glEnable
        initBoolean(GL_LINE_SMOOTH);//  glLineWidth
        initBoolean(GL_MULTISAMPLE);//  glSampleCoverage
        initBoolean(GL_POLYGON_SMOOTH);//   glPolygonMode
        initBoolean(GL_POLYGON_OFFSET_LINE);//  glPolygonOffset
        initBoolean(GL_POLYGON_OFFSET_POINT);//     glPolygonOffset
        initBoolean(GL_PROGRAM_POINT_SIZE);//   glEnable
        initBoolean(GL_PRIMITIVE_RESTART);//    glEnable, glPrimitiveRestartIndex
        initBoolean(GL_SAMPLE_ALPHA_TO_ONE);//  glSampleCoverage
        initBoolean(GL_SAMPLE_MASK);//  glEnable
        initBoolean(GL_TEXTURE_CUBE_MAP_SEAMLESS);//
#endif
        /**
         *
         * Get depth-write mask seperately... :(
         *
         */
        glGetIntegerv(GL_DEPTH_WRITEMASK, (GLint*) &mWriteToDepthBuffer);
        /**
         *
         * Get the stencil buffer's state
         *
         */
        glGetIntegerv(GL_STENCIL_FAIL, (GLint*) &mStencilOpFail);
        glGetIntegerv(GL_STENCIL_PASS_DEPTH_FAIL,
                (GLint*) &mStencilOpDepthFail);
        glGetIntegerv(GL_STENCIL_PASS_DEPTH_PASS, (GLint*) &mStencilOpPass);
        glGetIntegerv(GL_STENCIL_FUNC, (GLint*) &mStencilFunc);
        glGetIntegerv(GL_STENCIL_VALUE_MASK, (GLint*) &mStencilMask);
        glGetIntegerv(GL_STENCIL_REF, (GLint*) &mStencilRef);

        glGetIntegerv(GL_DEPTH_FUNC, (GLint*)&mDepthFunc);
        /**
         *
         * Get initial vertex attribute array values and caps.
         *
         */
        GLuint maxVertexAttribs;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, (GLint*) &maxVertexAttribs);
        mVertexAttribArray.resize(maxVertexAttribs);
        GLint isEnabled = 0;
        for (GLuint i = 0; i < maxVertexAttribs; i++) {
            glGetVertexAttribiv(i, GL_VERTEX_ATTRIB_ARRAY_ENABLED, &isEnabled);
            mVertexAttribArray[i] = isEnabled > 0 ? true : false;
        }
        /**
         *
         * FBO stuff.
         *
         */
        this->bindFramebuffer(GL_FRAMEBUFFER, 0);
        //this->bindFramebuffer(GL_FRAMEBUFFER, 0);

        glGetIntegerv(GL_CULL_FACE_MODE, (GLint*)&mCullFace);
        glGetIntegerv(GL_FRONT_FACE, (GLint*)&mFrontFace);


        mViewport.MinDepth = 1.f;
        mViewport.MaxDepth = 1000.f;

        mViewport.X = -1;
        mViewport.Y = -1;
        mViewport.W = -1;
        mViewport.H = -1;

        assert(glGetError() == GL_NO_ERROR);
    }

    bool bindFramebuffer(GLenum dst, GLuint src) {
        if (mFrameBufferObjects[dst] != src) {
            mFrameBufferObjects[dst] = src;
            glCallChecked(glBindFramebuffer(dst, src));
            return true;
        }
        return false;
    }

    bool setBoolean(GLenum which, bool value) {
        bool prevValue = mBooleanState[which];
        if (prevValue != value) {
            value ? glEnable(which) : glDisable(which);
            mBooleanState[which] = value;
            return true;
        }
        return false;
    }

    bool setBlendColor(const uint32_t cc) {
        if (mBlendColor.Value != cc) {
            mBlendColor.Value = cc;
            GLfloat fR = mBlendColor.R / 255.f;
            GLfloat fG = mBlendColor.G / 255.f;
            GLfloat fB = mBlendColor.B / 255.f;
            GLfloat fA = mBlendColor.A / 255.f;

            glCallChecked(glBlendColor(fR, fG, fB, fA));
            return true;
        }
        return false;
    }

    bool setBlendFunc(GLenum src, GLenum dst) {
        bool isValid = true;
        if (mSrcBlendRGB != src) {
            mSrcBlendRGB = src;
            isValid = false;
        }
        if (mDstBlendRGB != dst) {
            mDstBlendRGB = dst;
            isValid = false;
        }

        if (isValid != true) {
        	glCallChecked(glBlendFuncSeparate(src, dst, src, dst));
            return true;
        }
        return false;
    }

    bool setLineWidth(float val) {
        if (mLineWidth != val) {
            mLineWidth = val;
            glCallChecked(glLineWidth(mLineWidth));
            return true;
        }
        return false;
    }

    bool pushTexture2D(GLuint id) {
        bool isValid = true;
        if (mTexture2D.top() != id) {
            isValid = false;
        }

        mTexture2D.push(id);
        if (!isValid) {
        	glCallChecked(glBindTexture(GL_TEXTURE_2D, id));
            return true;
        }
        return false;
    }

    bool popTexture2D() {
        GLuint id = mTexture2D.top();
        mTexture2D.pop();
        if (id != mTexture2D.top()) {
        	glCallChecked(glBindTexture(GL_TEXTURE_2D, mTexture2D.top()));
            return true;
        }
        return false;
    }

    bool setVertexAttribArray(GLuint which, bool val) {
        if (mVertexAttribArray[which] != val) {
            mVertexAttribArray[which] = val;
            val ? glEnableVertexAttribArray(which) : glDisableVertexAttribArray(which);
            assert(glGetError() == GL_NO_ERROR);
            return true;
        }
        return false;
    }

    bool setDepthWrite(bool val) {
        if (val != mWriteToDepthBuffer) {
            mWriteToDepthBuffer = val;
            glCallChecked(glDepthMask(mWriteToDepthBuffer ? GL_TRUE : GL_FALSE));
            return true;
        }
        return false;
    }

//    bool setBlendState(BlendState const& state) {
//        bool res = setBlendFunc(state.AlphaSourceBlend, state.AlphaDestinationBlend);
//        return res;
//    }
//
//    bool setDepthStencilState(DepthStencilState const& state) {
//        bool res = setBoolean(GL_DEPTH_TEST, (bool) state.DepthBufferEnable);
//        res &= setDepthWrite((bool) state.DepthBufferWriteEnable);
//        res &= setBoolean(GL_STENCIL_TEST, (bool) state.StencilEnable);
//        return res;
//    }
//
//    bool setRasterizerState(RasterizerState const& state) {
//        bool res = setBoolean(GL_CULL_FACE, (bool) state.CullMode);
//        return res;
//    }

    bool setBlendEquation(const GLenum eRGB, const GLenum eA) {
        bool isValid = true;
        if (mBlendEquationRGB != eRGB) {
            mBlendEquationRGB = eRGB;
            isValid = false;
        }

        if (mBlendEquationA != eA) {
            mBlendEquationA = eA;
            isValid = false;
        }

        if (!isValid) {
        	glCallChecked(glBlendEquationSeparate(mBlendEquationRGB, mBlendEquationA));
            return true;
        }
        return false;
    }

    bool stencilOp(GLenum sfail, GLenum dfail, GLenum pass) {
        bool isValid = true;
        if (mStencilOpFail != sfail) {
            mStencilOpFail = sfail;
            isValid = false;
        }

        if (mStencilOpDepthFail != dfail) {
            mStencilOpDepthFail = dfail;
            isValid = false;
        }

        if (mStencilOpPass != pass) {
            mStencilOpPass = pass;
            isValid = false;
        }

        if (!isValid) {
        	glCallChecked(glStencilOp(mStencilOpFail, mStencilOpDepthFail, mStencilOpPass));
            return true;
        }
        return false;
    }

    bool depthFunc(GLenum func) {
        if(mDepthFunc != func) {
            mDepthFunc = func;
            glCallChecked(glDepthFunc(func));
            return true;
        }
        return false;
    }

    bool stencilFunc(GLenum func, GLint ref, GLuint mask) {
        bool isValid = true;
        if (mStencilFunc != func) {
            mStencilFunc = func;
            isValid = false;
        }
        if (mStencilRef != ref) {
            mStencilRef = ref;
            isValid = false;
        }
        if (mStencilMask != mask) {
            mStencilMask = mask;
            isValid = false;
        }

        if (!isValid) {
        	glCallChecked(glStencilFunc(mStencilFunc, mStencilRef, mStencilMask));
            return true;
        }
        return false;
    }

    bool setViewport(const Viewport& a) {
//        if(       (a.rect.X != mViewport.rect.X) ||
//                (a.rect.Y != mViewport.rect.Y) ||
//                (a.rect.W != mViewport.rect.W) ||
//                (a.rect.H != mViewport.rect.H)) {
//            mViewport.X = a.X;
//            mViewport.Y = a.Y;
//            mViewport.W = a.W;
//            mViewport.H = a.H;

    	glCallChecked(glViewport(a.X, a.Y, a.W, a.H));
    	glCallChecked(glScissor(a.X, a.Y, a.W, a.H));

            return true;
        //}


        //return false;
    }

    bool cullFace(GLenum val) {
        if(mCullFace != val) {
            mCullFace = val;
            glCallChecked(glCullFace(mCullFace));
            return true;
        }
        return false;
    }

    bool frontFace(GLenum val) {
        if(mFrontFace != val) {
            mFrontFace = val;
            glCallChecked(glFrontFace(mFrontFace));
            return true;
        }
        return false;
    }
};

class HXEXPORT GLGraphicsDevice : public IGraphicsDevice {
    IGraphicsContext*   mContext;
    BlendState          mBlendState;
    RasterizerState     mRasterizerState;
    DepthStencilState   mDepthStencilState;

    Viewport            mViewport;
    VertexBuffer*       mVertexBuffer;
    GLStateManager      mStateManager;

    int     mNumRenderTargetsAttached;
    GLuint  mFbo;
    GLuint  mCachedFbo;
public:
    GLGraphicsDevice()
    :mContext(NULL)
    ,mVertexBuffer(NULL)
	,mNumRenderTargetsAttached(0)
	,mFbo(0)
	,mCachedFbo(0)
//    , mVAO(0)
    {
        Factory<ITexture2DImpl>::getInstance().registerClass(api(), &new_GLTexture2D);
        Factory<IVertexBufferImpl>::getInstance().registerClass(api(), &new_GLVertexBuffer);
        Factory<IShaderProgram>::getInstance().registerClass(api(), &new_GLShaderProgram);


    }
    virtual ~GLGraphicsDevice() {
    }

    std::vector<std::string> split(std::string str, char delimiter) {
        std::vector<std::string> internal;
        std::stringstream ss(str); // Turn the string into a stream.
        std::string tok;

        while(std::getline(ss, tok, delimiter)) {
            internal.push_back(tok);
        }

        return internal;
    }

    void createWithContext(IGraphicsContext* pContext) {
        
        ScopedLock<IGraphicsContext> guard(pContext);
        DEBUG_METHOD();
        mContext = pContext;

        glEnable(GL_BLEND);

        mStateManager.init();

//        glGenVertexArrays(1, &mVAO);

        std::string ext = (const char*)glGetString(GL_EXTENSIONS);

        std::vector<std::string> extList = split(ext, ' ');
        for(size_t i=0; i < extList.size(); i++)
            DEBUG_MESSAGE("GL_EXT: %s", extList[i].c_str());

        setBlendState(BlendState::Default);
        setRasterizerState(RasterizerState::Default);
        setDepthStencilState(DepthStencilState::Default);

        glGenFramebuffers(1, &mFbo);
    }

    const char* api() const {
        return "<helix.gfx.api.OpenGL>";
    }

    void clear(const Color& color, const int clearOptions, const float clearDepth, const int clearStencil) {
        float fR = (float)color.R / 255.f;
        float fG = (float)color.G / 255.f;
        float fB = (float)color.B / 255.f;
        float fA = (float)color.A / 255.f;
        //glClearColor(color.R / 255.f, color.G / 255.f, color.B / 255.f, color.A / 255.f);
        glCallChecked(glClearColor(fR, fG, fB, fA));
        glCallChecked(glClearDepth(1.0f));
        glCallChecked(glClearStencil(0));

        glCallChecked(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));
    }

    bool startRendering() {
        return mContext->acquire();
        //return true;
    }

    void stopRendering() {
        mContext->swap();
        mContext->release();
    }

    virtual const BlendState& getBlendState() const {
        return mBlendState;
    }

    virtual const RasterizerState& getRasterizerState() const {
        return mRasterizerState;
    }

    virtual const DepthStencilState& getDepthStencilState() const {
        return mDepthStencilState;
    }

    virtual void setBlendState(const BlendState& a) {
        static const GLenum GLBLEND[] = {
            GL_ZERO,      //E_ZERO,
            GL_ONE,//E_ONE,
            GL_SRC_COLOR,//E_SRC_COLOR,
            GL_ONE_MINUS_SRC_COLOR,//E_SRC_COLOR_INV,
            GL_SRC_ALPHA,//E_SRC_ALPHA,
            GL_ONE_MINUS_SRC_ALPHA,//E_SRC_ALPHA_INV,
            GL_DST_ALPHA,//E_DST_ALPHA,
            GL_ONE_MINUS_DST_ALPHA,//E_DST_ALPHA_INV,
            GL_DST_COLOR,//E_DST_COLOR,
            GL_ONE_MINUS_DST_COLOR,//E_DST_COLOR_INV,
            GL_SRC_ALPHA_SATURATE,//E_SRC_ALPHA_SAT,
            GL_CONSTANT_COLOR,
            GL_ONE_MINUS_CONSTANT_COLOR
        };

        static const GLenum GLBLENDEQ[] = {
            GL_FUNC_ADD,
            GL_FUNC_ADD,//GL_MAX,
            GL_FUNC_SUBTRACT,//GL_MIN,
            GL_FUNC_REVERSE_SUBTRACT,
            GL_FUNC_SUBTRACT
        };

        mBlendState = a;
        mStateManager.setBlendFunc(GLBLEND[a.AlphaSourceBlend], GLBLEND[a.AlphaDestinationBlend]);
        mStateManager.setBlendColor(a.BlendFactor);
        mStateManager.setBlendEquation(GLBLENDEQ[a.ColorBlendFunction], GLBLENDEQ[a.AlphaBlendFunction]);
    }

    virtual void setRasterizerState(const RasterizerState& val) {
        static const GLenum OGL_FRONT_FACE[] = {
                GL_CCW,
                GL_CCW,
                GL_CW
        };


        //CullNone, //  Do not cull back faces.
        //CullClockwiseFace,//  Cull back faces with clockwise vertices.
        //CullCounterClockwiseFace//    Cull back faces with counterclockwise vertices.

        mRasterizerState = val;
        mStateManager.setBoolean(GL_CULL_FACE, val.CullMode != CullMode::CullNone);
        mStateManager.frontFace(OGL_FRONT_FACE[val.CullMode]);
        mStateManager.setBoolean(GL_SCISSOR_TEST, val.ScissorTestEnable);
    }

    virtual void setDepthStencilState(const DepthStencilState& val) {
        mDepthStencilState=val;

        mStateManager.setDepthWrite(val.DepthBufferEnable != 0);
        mStateManager.setBoolean(GL_DEPTH_TEST, val.DepthBufferEnable != 0);

        static const GLenum GL_STENCILOP[] = {
                GL_DECR,//Decrement,                //  Decrements the stencil-buffer entry, wrapping to the maximum value if the new value is less than 0.
                GL_DECR_WRAP,//DecrementSaturation, //  Decrements the stencil-buffer entry, clamping to 0.
                GL_INCR,//Increment,                //  Increments the stencil-buffer entry, wrapping to 0 if the new value exceeds the maximum value.
                GL_INCR_WRAP,//IncrementSaturation, //  Increments the stencil-buffer entry, clamping to the maximum value.
                GL_INVERT,//Invert,                 //  Inverts the bits in the stencil-buffer entry.
                GL_KEEP,//Keep,                 //  Does not update the stencil-buffer entry. This is the default value.
                GL_REPLACE,//Replace,               //  Replaces the stencil-buffer entry with a reference value.
                GL_ZERO//Zero                   //  Sets the stencil-buffer entry to 0.
        };

        static const GLenum GL_COMPAREFUNC[] = {
                GL_ALWAYS,//AlwaysPass,         //  Always pass the test.
                GL_EQUAL,//Equal,           //  Accept the new pixel if its value is equal to the value of the current pixel.
                GL_GREATER,//Greater,       //  Accept the new pixel if its value is greater than the value of the current pixel.
                GL_GEQUAL,//GreaterEqual,   //  Accept the new pixel if its value is greater than or equal to the value of the current pixel.
                GL_LESS,//Less,         //  Accept the new pixel if its value is less than the value of the current pixel.
                GL_LEQUAL,//LessEqual,      //  Accept the new pixel if its value is less than or equal to the value of the current pixel.
                GL_NEVER,//Never,           //  Always fail the test.
                GL_NOTEQUAL//NotEqual       //  Accept the new pixel if its value does not equal the value of the current pixel.
        };

        mStateManager.depthFunc(GL_COMPAREFUNC[val.DepthBufferFunction]);
        mStateManager.stencilFunc(GL_COMPAREFUNC[val.StencilFunction], val.ReferenceStencil, val.StencilMask);
        mStateManager.stencilOp(GL_STENCILOP[val.StencilFail], GL_STENCILOP[val.StencilPass], GL_STENCILOP[val.StencilDepthBufferFail]);

    }

    virtual const Viewport& getViewport() const {
        return mViewport;
    }

    virtual void setViewport(const Viewport& vp) {
        mViewport = vp;
        glViewport(vp.X, vp.Y, vp.W, vp.H);
    }

    virtual void drawPrimitives(const int primitiveType, int start, size_t count, size_t primCount) {
        for(size_t i=0; i < primCount; i++) {
            glDrawArrays(__glPrimitiveType[primitiveType], start + count * i, count);
        }
    }

    virtual void drawIndexedPrimitives() {
    }

    virtual void setStreamSource(VertexBuffer* src) {
        if(mVertexBuffer != src) {
            VertexBuffer* prev = mVertexBuffer;

            if(prev) {
                if(&prev->getVertexDeclaration() != &src->getVertexDeclaration()) {
                    disableStreams(prev->getVertexDeclaration());
                }
                prev->getImpl().release();
            }
            //glBindVertexArray(mVAO);
            mVertexBuffer = src;
            if(mVertexBuffer != NULL) {
                mVertexBuffer->getImpl().acquire();
                enableStreams(mVertexBuffer->getVertexDeclaration());
            } else {
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                //glBindVertexArray(0);
            }
        }
    }

    void disableStreams(const VertexDeclaration& d) {
        //glBindVertexArray(mVAO);
        for(uint16_t i=0; i < d.size(); i++) {
            mStateManager.setVertexAttribArray(d.at(i).vertexElementUsage, false);
        }
        //glBindVertexArray(0);
    }

    void enableStreams(const VertexDeclaration& d) {
        //glBindVertexArray(mVAO);
        for(size_t i=0; i < d.size(); i++) {

            mStateManager.setVertexAttribArray(d.at(i).vertexElementUsage, true);

            glVertexAttribPointer(
                    d.at(i).vertexElementUsage,
                    __glVertexFormatSize[d.at(i).vertexElementFormat], // count
                    __glVertexFormatType[d.at(i).vertexElementFormat],// type
                    __glVertexFormatIsNormalized[d.at(i).vertexElementFormat],// normalized
                    d.stride(),
                    BUFFER_OFFSET(d.at(i).offset)
            );
        }
        //glBindVertexArray(0);
    }

    virtual void setRenderTarget(Texture2D* dst, int n) {
    	//assert(n < mMaxRenderTargets);
		GLuint texID;

		if(mNumRenderTargetsAttached == 0) {
			glCallChecked(glGetIntegerv(GL_FRAMEBUFFER_BINDING, (GLint*)&mCachedFbo));
			mStateManager.bindFramebuffer(GL_FRAMEBUFFER, mFbo);
		}
		GLenum at = GL_COLOR_ATTACHMENT0 + n;
		dst->getImpl().acquire();
		glCallChecked(glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&texID));
		dst->getImpl().release();
		glCallChecked(glFramebufferTexture2D(GL_FRAMEBUFFER, at, GL_TEXTURE_2D, texID, 0));
//		if(this->mDepthStencilBuffer) {
//			mDepthStencilBuffer->get_impl().lock();
//			GLuint depthBuffer = 0;
//			glGetIntegerv(GL_RENDERBUFFER_BINDING, (GLint*)&depthBuffer);
//			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
//		} else {
//			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
//			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, 0);
//		}

		GLenum err = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if(err != GL_FRAMEBUFFER_COMPLETE) {
			assert(0);
		}

		mNumRenderTargetsAttached++;
    }
    virtual void resolveRenderTarget(int n) {
    	mNumRenderTargetsAttached--;
		/**
		 *
		 * Set drawbuffer n to GL_NONE, removing it from the draw targets.
		 *
		 */
		//mDrawBuffers[mNumRenderTargetsAttached] = GL_NONE;
		//glDrawBuffers(mNumRenderTargetsAttached, mDrawBuffers);
    	glCallChecked(glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + n, GL_TEXTURE_2D, 0, 0));
		/**
		 *
		 * If there are no more render targets attached, detach the framebuffer object as well. This
		 * returns the GL state machine to the default setting (back buffer or front buffer, depending
		 * on whether or not double buffering is enabled (default)).
		 *
		 */

		if(mNumRenderTargetsAttached == 0) {
			mStateManager.bindFramebuffer(GL_FRAMEBUFFER, mCachedFbo);
		}
    }
    
    virtual IGraphicsContext* getContext() const {
        return mContext;
    }
};

HXAPI HXEXPORT IGraphicsDevice* new_GLGraphicsDevice() {
    return new GLGraphicsDevice;
}

}
}
}
