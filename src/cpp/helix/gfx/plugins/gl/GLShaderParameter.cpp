#include <helix/gfx/ShaderParameter.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/Sampler2D.h>
#include <helix/gfx/VertexStream.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/Color.h>
#include <helix/core/Vector2.h>
#include <helix/core/Vector3.h>
#include <helix/core/Vector4.h>
#include <helix/core/Matrix.h>
#include <helix/core/DebugLog.h>
#include "OpenGL.h"

using namespace helix::core;

namespace helix {
namespace gfx {
namespace impl {
template <typename T, size_t N>
class GLUniformT : public ShaderParameter<T> {
    GLuint mId;
public:
    GLUniformT(ShaderProgram* program, const std::string& name, int location, size_t count)
    : ShaderParameter<T>(program, name, location, count)
    , mId((GLuint)location)
    {

    }

    virtual ~GLUniformT() {
    }

    virtual void validate() {
        // unimplemented
    }
};

typedef GLUniformT<float, 1> GLUniform1f;
typedef GLUniformT<float, 2> GLUniform2f;
typedef GLUniformT<float, 3> GLUniform3f;
typedef GLUniformT<float, 4> GLUniform4f;

typedef GLUniformT<Sampler2D, 1> GLUniformSampler2D;
typedef GLUniformT<Matrix, 1> GLUniformMatrix;
typedef GLUniformT<Vector2, 1> GLUniformVector2;
typedef GLUniformT<Vector3, 1> GLUniformVector3;
typedef GLUniformT<Vector4, 1> GLUniformVector4;

typedef GLUniformT<Color, 1> GLUniformColor;

template <>
void GLUniformColor::validate() {
	for(size_t i=0; i < mCount; i++) {

	}
}

template <>
void GLUniformSampler2D::validate() {
	static const GLenum __glAddressMode[] = {
		GL_CLAMP_TO_EDGE,
		GL_MIRRORED_REPEAT,
		GL_REPEAT
	};

	const static GLenum __glTextureFilterMinMag[][2] = {
		{ GL_LINEAR,  GL_LINEAR }, 					//Linear,						//	Use linear filtering.
		{ GL_NEAREST, GL_NEAREST },					//Point,						//	Use point filtering.
		{ GL_LINEAR,  GL_LINEAR },					//Anisotropic,					//	Use anisotropic filtering.
		{ GL_LINEAR,  GL_LINEAR_MIPMAP_NEAREST },	//LinearMipPoint,				//	Use linear filtering to shrink or expand, and point filtering between mipmap levels (mip).
		{ GL_NEAREST, GL_NEAREST_MIPMAP_LINEAR  },	//PointMipLinear,				//	Use point filtering to shrink (minify) or expand (magnify), and linear filtering between mipmap levels.
		{ GL_LINEAR,  GL_NEAREST_MIPMAP_LINEAR },	//MinLinearMagPointMipLinear,	//	Use linear filtering to shrink, point filtering to expand, and linear filtering between mipmap levels.
		{ GL_LINEAR,  GL_NEAREST_MIPMAP_NEAREST },	//MinLinearMagPointMipPoint,	//	Use linear filtering to shrink, point filtering to expand, and point filtering between mipmap levels.
		{ GL_NEAREST, GL_LINEAR_MIPMAP_LINEAR },	//MinPointMagLinearMipLinear,	//	Use point filtering to shrink, linear filtering to expand, and linear filtering between mipmap levels.
		{ GL_NEAREST, GL_LINEAR_MIPMAP_NEAREST }	//MinPointMagLinearMipPoint
	};

    for(size_t i=0; i < mCount; i++) {
        if(mValue[i].TextureRef) {
            glCallChecked(glUniform1i(mId, mValue[i].SamplerID));
            glCallChecked(glActiveTexture(GL_TEXTURE0 + mValue[i].SamplerID));
            mValue[i].TextureRef->getImpl().acquire();
            glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, __glAddressMode[mValue[i].AddressU]));
			glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, __glAddressMode[mValue[i].AddressV]));
			glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, __glTextureFilterMinMag[mValue[i].Filter][0]));
			glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, __glTextureFilterMinMag[mValue[i].Filter][1]));
        }
    }
}

template <>
void GLUniform1f::validate() {
    glCallChecked(glUniform1fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniform2f::validate() {
    glCallChecked(glUniform2fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniform3f::validate() {
    glCallChecked(glUniform3fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniform4f::validate() {
    glCallChecked(glUniform4fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniformVector2::validate() {
    glCallChecked(glUniform2fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniformVector3::validate() {
    glCallChecked(glUniform3fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniformVector4::validate() {
    glCallChecked(glUniform4fv(mId, mCount, (const GLfloat*)&mValue[0]));
}

template <>
void GLUniformMatrix::validate() {
    Matrix T;
    Matrix::transpose(mValue[0], &T);
    glCallChecked(glUniformMatrix4fv(mId, mCount, GL_FALSE, (const GLfloat*)&T));
}

class GLShaderProgram : public IShaderProgram {
    GLuint mId;
    GLuint mVS;
    GLuint mPS;
public:
    GLShaderProgram()
    : mId(0)
    , mVS(0)
    , mPS(0)
    {
        create(NULL);
    }
    virtual ~GLShaderProgram() {
        destroy();
    }

    virtual void compile(const char* src) {
        const char* pSourceVS[3]= {
                "#undef     FRAGMENT     \n",
                "#define    VERTEX      1\n",
                src
        };

        glCallChecked(glShaderSource(mVS, 3, pSourceVS, NULL));
        compileShader(mVS);

        const char* pSourcePS[3] = {
                "#define    FRAGMENT    1\n",
                "#undef     VERTEX       \n",
                src
        };

        glCallChecked(glShaderSource(mPS, 3, pSourcePS, NULL));
        compileShader(mPS);

    }

     bool compileShader(const GLuint sh) {
    	 DEBUG_METHOD();
        glCallChecked(glCompileShader(sh));
        GLint success;
        glCallChecked(glGetShaderiv(mVS, GL_COMPILE_STATUS, &success));
        //if(!success)
        //{
            GLchar error[1024];
            glGetShaderInfoLog(sh, 1024, NULL, error);
            DEBUG_MESSAGE("Shader compilation output: %s", error);
            //throw std::runtime_error(std::string("Error compiling shader!") + std::string(error));

        //}
        return success > 0;
    }

    virtual bool link() {
        DEBUG_METHOD();

        glCallChecked(glAttachShader(mId, mVS));
        glCallChecked(glAttachShader(mId, mPS));
        static const char* SEMANTIC[] = {
                "VertexCoord",
                "BlendWeight",
                "Normal",
                "Color0",
                "Color1",
                "FogCoord",
                "PointSize",
                "BlendIndex0",
                "TexCoord0",
                "TexCoord1",
                "TexCoord2",
                "TexCoord3",
                "TexCoord4",
                "TexCoord5",
                "TexCoord6",
                "TexCoord7"
        };

        for(int i=0; i < VertexStream::MaxElementAttributes; i++) {
            const char* str = SEMANTIC[i];
            glCallChecked(glBindAttribLocation(mId, i, str));
        }

        glCallChecked(glLinkProgram(mId));
        GLint success = GL_FALSE;
        glCallChecked(glGetProgramiv(mId, GL_LINK_STATUS, &success));
        if(success == GL_FALSE)
        {
            GLchar errorLog[1024] = {0};
            glCallChecked(glGetProgramInfoLog(mId, 1024, NULL, errorLog));
            DEBUG_MESSAGE("error linking program: %s", errorLog);
            return false;
        }
        glCallChecked(glValidateProgram(mId));
        success = GL_FALSE;
        glCallChecked(glValidateProgram(mId));
        glCallChecked(glGetProgramiv(mId, GL_VALIDATE_STATUS, &success));
        if(success == GL_FALSE)
        {
            GLchar errorLog[1024] = {0};
            glGetProgramInfoLog(mId, 1024, NULL, errorLog);
            DEBUG_MESSAGE("error validating program: %s", errorLog);
            return false;
        }
        return true;
    }

    virtual void create(ShaderProgram* program) {
        if(!mId) {
            mId = glCreateProgram();
        }
        if(!mVS) {
            mVS = glCreateShader(GL_VERTEX_SHADER);
        }
        if(!mPS) {
            mPS = glCreateShader(GL_FRAGMENT_SHADER);
        }
        if(program) {
            compile(program->getSource().c_str());
            link();
            cacheUniforms(program);
        }

    }

    virtual int acquire() {
        glUseProgram(mId);
        return (int)mId;
    }

    virtual void release() {
        glUseProgram(0);
    }


    virtual void cacheUniforms(ShaderProgram* sh) {
        DEBUG_METHOD();
        int count;
        glGetProgramiv( mId, GL_ACTIVE_UNIFORMS, &count);
        static const int BUFF_SIZE = 1024;

        for (int i=0; i < count; ++i)
        {
            char name[BUFF_SIZE]; // for holding the variable name
            GLint size = BUFF_SIZE;
            GLenum type;
            GLsizei length;
            GLsizei bufSize=BUFF_SIZE;
            glGetActiveUniform( mId, i, bufSize, &length, &size, &type, name );
            int location = glGetUniformLocation(mId, name);

            //glGetUniformfv(mId, location, params);

            //DEBUG_MESSAGE("uniform %s @ 0x%x", name, location);
            DEBUG_VALUE_AND_TYPE_OF(name);
            DEBUG_VALUE_AND_TYPE_OF(type);
            DEBUG_VALUE_AND_TYPE_OF(size);
            DEBUG_VALUE_AND_TYPE_OF(location);

            ShaderParameterBase* pUniform = NULL;


            switch(type) {
                case GL_FLOAT:
                    pUniform = new GLUniformT<float, 1>(sh, name, location, size);
                    break;
                case GL_FLOAT_VEC2:
                    pUniform = new GLUniformT<Vector2, 1>(sh, name, location, size);
                    break;
                case GL_FLOAT_VEC3:
                    pUniform = new GLUniformT<Vector3, 1>(sh, name, location, size);
                    break;
                case GL_FLOAT_VEC4:
                    pUniform = new GLUniformT<Vector4, 1>(sh, name, location, size);
                    break;
                case GL_INT:
                    pUniform = new GLUniformT<int,1>(sh, name, location, size);
                    break;
                case GL_INT_VEC2:
                    pUniform = new GLUniformT<int,2>(sh, name, location, size * 2);
                    break;
                case GL_INT_VEC3:
                    pUniform = new GLUniformT<int,3>(sh, name, location, size * 3);
                    break;
                case GL_INT_VEC4:
                    pUniform = new GLUniformT<int,4>(sh, name, location, size * 4);
                    break;
                case GL_BOOL:
                    pUniform = new GLUniformT<bool,1>(sh, name, location, size);
                    break;
                case GL_BOOL_VEC2:
                    pUniform = new GLUniformT<bool,2>(sh, name, location, size * 2);
                    break;
                case GL_BOOL_VEC3:
                    pUniform = new GLUniformT<bool,3>(sh, name, location, size * 3);
                    break;
                case GL_BOOL_VEC4:
                    pUniform = new GLUniformT<bool,4>(sh, name, location, size * 4);
                    break;
                case GL_FLOAT_MAT2:
                    pUniform = new GLUniformT<float,1>(sh, name, location, size * 4);
                    break;
                case GL_FLOAT_MAT3:
                    pUniform = new GLUniformT<float,1>(sh, name, location, size * 9);
                    break;
                case GL_FLOAT_MAT4:
                    pUniform = new GLUniformT<Matrix,1>(sh, name, location, size);
                    break;
                case GL_SAMPLER_2D:
                    pUniform = new GLUniformT<Sampler2D,1>(sh, name, location, size);
                    break;
    #if 0
                case GL_SAMPLER_3D:
                    pUniform = new GLUniformT<texture3d, 1>(sh, name, location, size);
                    break;
    #endif
                case GL_SAMPLER_CUBE:
                    //pUniform = new GLUniformT<TextureCUBE*,1>(sh, name, location, size);
                    break;
                default:
                    break;
                }

            assert(pUniform);
            sh->addUniform(pUniform, location);
        }
    }

    void updateUniforms(ShaderProgram* sh) {
        glUseProgram(mId);
        for(unsigned int i=0; i < sh->numUniforms(); i++) {
            ShaderParameterBase* pUniform = sh->getUniform(i);
            if(pUniform->isValid()){
                pUniform->validate();
            }
        }
    }


    virtual void destroy() {
    }
};

HXAPI HXEXPORT IShaderProgram* new_GLShaderProgram() {
    return new GLShaderProgram;
}

}
}
}
