#include <helix/gfx/VertexBuffer.h>
#include "OpenGL.h"

namespace helix {
namespace gfx {
namespace impl {

class GLVertexBuffer : public IVertexBufferImpl {
    GLuint mId;
    GLenum mBufferUsage;
    size_t mBufferSize;
public:

    GLVertexBuffer()
    : mId(0)
    , mBufferUsage(GL_DYNAMIC_DRAW)
    , mBufferSize(0) {

        glCallChecked(glGenBuffers(1, &mId));

    }

    virtual ~GLVertexBuffer() {
        glCallChecked(glDeleteBuffers(1, &mId));

    }

    void orphan() {

        glCallChecked(glBufferData(
                     GL_ARRAY_BUFFER,
                     mBufferSize,
                     NULL,
                     mBufferUsage
                     ));

    }

    virtual void create(VertexBuffer* src) {
        mBufferSize = src->size();

        glCallChecked(glBindBuffer(GL_ARRAY_BUFFER, mId));
        orphan();
        glCallChecked(glBindBuffer(GL_ARRAY_BUFFER, 0));
    }

    virtual void setData(const void* src, size_t beg, size_t end) {
        orphan();
        glCallChecked(glBufferSubData(GL_ARRAY_BUFFER, beg, end - beg, src));

    }

    virtual void getData(void* dst, size_t beg, size_t end) {
    	//glGetBufferSubData(GL_ARRAY_BUFFER, beg, end - beg, dst);
//    	const void* src = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
//    	if(NULL != src) {
//    		memcpy(dst, src, end - beg);
//    		glUnmapBuffer(GL_ARRAY_BUFFER);
//    	}
    }

    virtual int acquire() {
        glCallChecked(glBindBuffer(GL_ARRAY_BUFFER, mId));
        return static_cast<int>(mId);
    }

    virtual void release() {
        glCallChecked(glBindBuffer(GL_ARRAY_BUFFER, 0));
    }

};

HXAPI HXEXPORT IVertexBufferImpl* new_GLVertexBuffer() {
    return new GLVertexBuffer;
}

}
}
}
