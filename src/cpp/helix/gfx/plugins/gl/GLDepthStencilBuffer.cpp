/*
 * GLDepthStencilBuffer.cpp
 *
 *  Created on: Jul 20, 2015
 *      Author: miel
 */

#include <helix/gfx/DepthStencilBuffer.h>
#include <helix/gfx/plugins/gl/OpenGL.h>

namespace helix {
namespace gfx {
namespace impl {

class GLDepthStencilBuffer : public IDepthStencilBuffer {
	GLuint mId;
	GLenum mType;
	GLenum mAttachment;
public:
	GLDepthStencilBuffer()
	: mId(0)
	, mType(0)
	, mAttachment(0)
	{
		glGenBuffers(1, &mId);

	}
	virtual ~GLDepthStencilBuffer() {
		glDeleteBuffers(1, &mId);
	}

	virtual void create(const DepthStencilBuffer* src) {
		static const GLenum __glRenderBufferStorageType[] = {
			GL_DEPTH_COMPONENT16,
			GL_DEPTH_COMPONENT24,
			GL_DEPTH24_STENCIL8,
			GL_DEPTH_COMPONENT32
		};

		static GLenum __glRenderBufferAttachment[] = {
			GL_DEPTH_ATTACHMENT,
			GL_DEPTH_ATTACHMENT,
			GL_DEPTH_ATTACHMENT | GL_STENCIL_ATTACHMENT,
			GL_DEPTH_ATTACHMENT
		};
		const int dsf = src->getDepthStencilFormat();
		mAttachment = __glRenderBufferAttachment[dsf];
		mType		= __glRenderBufferStorageType[dsf];


		glBindBuffer(GL_RENDERBUFFER, mId);
		glRenderbufferStorage(GL_RENDERBUFFER, mType, src->getWidth(), src->getHeight());
		glBindBuffer(GL_RENDERBUFFER, 0);
	}

	virtual void destroy() {
		glDeleteBuffers(1, &mId);
	}

	virtual int acquire() {
		glBindRenderbuffer(GL_RENDERBUFFER, mId);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, mAttachment, GL_RENDERBUFFER, mId);
		return (int)mId;
	}

	virtual void release() {
		/**
		 * This effectively resets the GL's depth/stencil buffer.
		 * i.e.: Bad practice, TODO: cache states.
		 */
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, mAttachment, GL_RENDERBUFFER, 0);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}
};

}
}
}


