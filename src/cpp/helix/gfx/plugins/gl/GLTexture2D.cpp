#include <helix/gfx/Texture2D.h>
#include <helix/core/MathHelper.h>
#include "OpenGL.h"

namespace helix {
namespace gfx {
namespace impl {

class GLTexture2D : public ITexture2DImpl {
    GLuint  mId;
    GLenum  mSurfaceFormat;
    GLenum  mSurfaceFormatInternal;
    GLenum  mType;
    bool    mHasLevels;
public:
    GLTexture2D()
	: mId(0)
	{
        glGenTextures(1, &mId);

    }
    virtual ~GLTexture2D() {
        glDeleteTextures(1, &mId);

    }

    virtual void destroy() {

    }

    virtual void create(Texture2D* src) {
        mSurfaceFormat          = __glSurfaceFormat[src->getPixelFormat()];
        mSurfaceFormatInternal  = __glSurfaceFormatInternal[src->getPixelFormat()];
        mType                   = __glSurfaceFormatType[src->getPixelFormat()];
        GLuint pixelStore       = __glPixelStoreSize[src->getPixelFormat()];
        mHasLevels              = src->hasLevels();
        size_t width            = src->getWidth();//MathHelper::next_pow2(src->getWidth());
        size_t height           = src->getHeight();//MathHelper::next_pow2(src->getHeight());

        glCallChecked(glBindTexture(GL_TEXTURE_2D, mId));
        glCallChecked(glPixelStorei(GL_PACK_ALIGNMENT, pixelStore));
        glCallChecked(glPixelStorei(GL_UNPACK_ALIGNMENT, pixelStore));
        glCallChecked(glTexImage2D(
            GL_TEXTURE_2D,
            0, // miplevel
            mSurfaceFormatInternal, // internalFormat
            width,
            height,
            0, // no border
            mSurfaceFormat, // format
            mType, // type
            NULL));

        glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
        glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
        glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
        glCallChecked(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
        if(src->hasLevels()) {
            // only POT textures... *sigh*
//            glGenerateMipmap(GL_TEXTURE_2D);

        }
        glCallChecked(glBindTexture(GL_TEXTURE_2D, 0));


    }

    virtual void setData(const void* src, size_t level, int x, int y, size_t w, size_t h) {
        glCallChecked(glTexSubImage2D(
            GL_TEXTURE_2D,
            level,
            x, y,
            w, h,
            mSurfaceFormat,
            mType,
            src));

        if(mHasLevels) {
            //glGenerateMipmap(GL_TEXTURE_2D);
        }
    }
    virtual void getData(void*, size_t, int, int, size_t, size_t) {
    }
    virtual int  acquire() {
        glBindTexture(GL_TEXTURE_2D, mId);
        return static_cast<int>(mId);
    }
    virtual void release() {
        glBindTexture(GL_TEXTURE_2D, 0); // <-- NOTE: this could result in not-so-obvious errors, perhaps store last bound before 'acquire()'
    }

};

HXAPI HXEXPORT ITexture2DImpl* new_GLTexture2D() {
    return new GLTexture2D;
}

}
}
}
