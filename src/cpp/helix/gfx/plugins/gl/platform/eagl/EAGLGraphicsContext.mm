#include <helix/gfx/IGraphicsContext.h>
#include <helix/gfx/plugins/gl/OpenGL.h>
#include <helix/core/DebugLog.h>
#include <helix/core/IMutex.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include <OpenGLES/EAGL.h>
#include <OpenGLES/EAGLDrawable.h>
#include <QuartzCore/QuartzCore.h>

#import <UIKit/UIKit.h>

using namespace helix::core;

namespace helix {
namespace gfx {
namespace impl {

class EAGLGraphicsContext : public IGraphicsContext {
	CAEAGLLayer* 	mDrawable;
	EAGLContext*	mContext;
	GLuint          mRenderBufferId;
    GLuint          mFboId;
    GLuint          mDepthBufferId;
    int             mWidth;
    int             mHeight;
    IMutex*         mLock;
public:
    EAGLGraphicsContext()
    : mLock(new_Mutex())
    {
        
    }

	virtual ~EAGLGraphicsContext() {
        destroy();
        delete mLock;
        mLock = NULL;
    }

    virtual bool create(void* display, void* layer) {
        createContextAndRenderbuffers((__bridge CAEAGLLayer*)layer);
        return true;
    }

    void createContextAndRenderbuffers(CAEAGLLayer* drawable) {

        mWidth      = [drawable bounds].size.width;
        mHeight     = [drawable bounds].size.height;
        mDrawable   = drawable;
        //mDrawable.opaque = YES;
        mDrawable.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:YES],
                                        kEAGLDrawablePropertyRetainedBacking,
                                        kEAGLColorFormatRGBA8, // or kEAGLColorFormatRGB565
                                        kEAGLDrawablePropertyColorFormat,
                                        nil
                                        ];

        EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
        mContext = [[EAGLContext alloc] initWithAPI:api];
        int width;
        int height;
        makeCurrent();
        glGenFramebuffers(1, &mFboId);
        glBindFramebuffer(GL_FRAMEBUFFER, mFboId);
        glGenRenderbuffers(1, &mRenderBufferId);
        glBindRenderbuffer(GL_RENDERBUFFER, mRenderBufferId);
        [mContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:drawable];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mRenderBufferId);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
        glGenRenderbuffers(1, &mDepthBufferId);
        glBindRenderbuffer(GL_RENDERBUFFER, mDepthBufferId);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepthBufferId);
        glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }

    virtual void destroy() {
        if(mContext != nil) {
            EAGLContext* oldContext = [EAGLContext currentContext];
            if(oldContext != mContext) {
                [EAGLContext setCurrentContext:mContext];
            }

            if(mFboId != 0) {
                glDeleteFramebuffers(1, &mFboId);
            }

            if(mRenderBufferId !=0 ) {
                glDeleteRenderbuffers(1, &mRenderBufferId);
            }
            if(mDepthBufferId != 0) {
                glDeleteRenderbuffers(1, &mDepthBufferId);
            }

            if(oldContext != mContext) {
                [EAGLContext setCurrentContext:oldContext];
            }
            [mContext release];
            mContext = nil;
        }
    }

	virtual bool swap() {
        glBindRenderbuffer(GL_RENDERBUFFER, mRenderBufferId);
        return [mContext presentRenderbuffer:GL_RENDERBUFFER];
	}

    virtual int acquire() {
        mLock->acquire();
        if([EAGLContext setCurrentContext:mContext] == YES) {
            glBindFramebuffer(GL_FRAMEBUFFER, mFboId);
            return 1;
        }
        return 0;
    }

    virtual void release() {

        mLock->release();
    }

    virtual bool makeCurrent() {
        return [EAGLContext setCurrentContext:mContext];
    }

    virtual bool isCurrent() const {
        return mContext == [EAGLContext currentContext];
    }
};

HXAPI HXEXPORT IGraphicsContext* new_EAGLGraphicsContext() {
    return new EAGLGraphicsContext;
}

}
}
}
