#include <helix/gfx/IGraphicsContext.h>
#include <helix/core/DebugLog.h>
#include <helix/core/IMutex.h>
#include <EGL/egl.h>

using namespace helix::core;

namespace helix {
namespace gfx {
namespace impl {

class EGLGraphicsContext : public IGraphicsContext {
	EGLContext	mContext;
	EGLDisplay	mDisplay;
	EGLSurface	mSurface;
    EGLConfig   mConfig;
    IMutex*     mLock;
public:
    EGLGraphicsContext()
    : mContext(EGL_NO_CONTEXT)
    , mDisplay(EGL_NO_DISPLAY)
    , mSurface(EGL_NO_SURFACE)
    , mConfig(0)
    , mLock(new_Mutex()){

    }
	virtual ~EGLGraphicsContext() {
        destroy();
        delete mLock;
        mLock = NULL;
	}

    virtual bool create(void* display, void* surface) {
        DEBUG_METHOD();
        DEBUG_VALUE_AND_TYPE_OF(display);
        DEBUG_VALUE_AND_TYPE_OF(surface);
        static const EGLint attrib[] = {
            EGL_SURFACE_TYPE,   EGL_WINDOW_BIT,
            EGL_RED_SIZE,       8,
            EGL_GREEN_SIZE,     8,
            EGL_BLUE_SIZE,      8,
            EGL_ALPHA_SIZE,     8,
            EGL_DEPTH_SIZE,     16,
            EGL_STENCIL_SIZE,   8,
            EGL_RENDERABLE_TYPE,EGL_OPENGL_ES2_BIT,
            EGL_NONE
        };

        EGLint numConfigs = 0;

        EGLNativeDisplayType dpy = (EGLNativeDisplayType)display;
        EGLNativeWindowType  win = (EGLNativeWindowType)surface;

        mDisplay = eglGetDisplay(dpy);
        if(mDisplay == EGL_NO_DISPLAY) {
            destroy();
            throw std::runtime_error("eglGetDisplay() failed!");
        }
        if(!eglInitialize(mDisplay, NULL, NULL)) {
            destroy();
            throw std::runtime_error("eglInitialize() failed!");
        }
        if(!eglChooseConfig(mDisplay, attrib, &mConfig, 1, &numConfigs)) {
            destroy();
            throw std::runtime_error("eglChooseConfig() failed!");
        }

        static const EGLint contextAttribs[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE
        };

        mContext = eglCreateContext(mDisplay, mConfig, EGL_NO_CONTEXT, contextAttribs);

        if(mContext == EGL_NO_CONTEXT) {
            throw std::runtime_error("eglCreateContext() failed!");
        }

#if defined(HELIX_PLATFORM_ANDROID)
        EGLint nativeVisualId;
        if(!eglGetConfigAttrib(mDisplay, mConfig, EGL_NATIVE_VISUAL_ID, &nativeVisualId)) {
            throw std::runtime_error("eglGetConfigAttrib() failed!");
        }
        ANativeWindow_setBuffersGeometry((ANativeWindow*)surface, 0, 0, nativeVisualId);
#endif
        mSurface = eglCreateWindowSurface(mDisplay, mConfig, win, NULL);
        if(mSurface == EGL_NO_SURFACE) {
            DEBUG_MESSAGE("eglCreateWindowSurface failed with error code: 0x%x", eglGetError());
            throw std::runtime_error("eglCreateWindowSurface() failed!");
        }


        return eglMakeCurrent(mDisplay, mSurface, mSurface, mContext);
    }

    virtual void destroy() {
        if(mDisplay != EGL_NO_DISPLAY) {
            eglMakeCurrent(mDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
            if(mContext != EGL_NO_CONTEXT) {
                eglDestroyContext(mDisplay, mContext);
                mContext = EGL_NO_CONTEXT;
            }

            if(mSurface != EGL_NO_SURFACE) {
                eglDestroySurface(mDisplay, mSurface);
                mSurface = EGL_NO_SURFACE;
            }

            eglTerminate(mDisplay);
            mDisplay = EGL_NO_DISPLAY;
        }
    }

	virtual bool swap() {
		return (bool)eglSwapBuffers(mDisplay, mSurface);
	}

    virtual int acquire() {
        mLock->acquire();
        return (int)eglMakeCurrent(mDisplay, mSurface, mSurface, mContext) != EGL_FALSE;
    }
    virtual void release() {
    	eglMakeCurrent(mDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        mLock->release();
    }

    bool makeCurrent(){
        return eglMakeCurrent(mDisplay, mSurface, mSurface, mContext) != EGL_FALSE;
    }

    bool isCurrent() const {
        return eglGetCurrentContext() == mContext;
    }
};

HXAPI HXEXPORT IGraphicsContext* new_EGLGraphicsContext() {
    return new EGLGraphicsContext;
}

}
}
}
