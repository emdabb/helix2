#ifndef HELIX_GFX_PLUGINS_GL_OPENGL_H_
#define HELIX_GFX_PLUGINS_GL_OPENGL_H_

#include <helix/Config.h>

#define GL_GLEXT_PROTOTYPES     1

#if defined(HELIX_PLATFORM_IOS)
# include <OpenGLES/ES2/gl.h>
# include <OpenGLES/ES2/glext.h>
#elif defined(HELIX_PLATFORM_ANDROID) || defined(HELIX_PLATFORM_X11) || defined(HELIX_PLATFORM_WINRT)
# include <GLES2/gl2.h>
# include <GLES2/gl2ext.h>
#endif
#include <helix/core/DebugLog.h>
#include <cassert>
#include <sstream>
#include <stdexcept>
using helix::core::DebugLog;
#if defined(_DEBUG)
#define glCallChecked(x) {\
    x;\
    GLenum err = glGetError();\
    if(GL_NO_ERROR != err) { \
        std::stringstream ss;\
        ss << "OpenGL error: @" << __FILE__ << ":" << __LINE__ << " > 0x" << std::hex << err << std::endl; \
        throw std::runtime_error(ss.str()); \
    }\
}
#endif

#define glClearDepth    glClearDepthf
#define GL_HALF_FLOAT   GL_HALF_FLOAT_OES

#ifndef GL_DEPTH_COMPONENT24
#define GL_DEPTH_COMPONENT24 GL_DEPTH_COMPONENT24_OES
#endif

#ifndef GL_DEPTH_COMPONENT32
#define GL_DEPTH_COMPONENT32 GL_DEPTH_COMPONENT32_OES
#endif

/**
 * #define GL_DEPTH_STENCIL_OES                                    0x84F9
#define GL_UNSIGNED_INT_24_8_OES                                0x84FA
#define GL_DEPTH24_STENCIL8_OES                                 0x88F0
 */

#ifndef GL_DEPTH_STENCIL
#define GL_DEPTH_STENCIL	GL_DEPTH_STENCIL_OES
#endif

#ifndef GL_UNSIGNED_INT_24_8
#define GL_UNSIGNED_INT_24_8 GL_UNSIGNED_INT_24_8_OES
#endif

#ifndef GL_DEPTH24_STENCIL8
#define GL_DEPTH24_STENCIL8 GL_DEPTH24_STENCIL8_OES
#endif

#ifndef GL_EXT_texture_rg
#define GL_EXT_texture_rg 1
#define GL_RED_EXT                        0x1903
#define GL_RG_EXT                         0x8227
#define GL_R8_EXT                         0x8229
#define GL_RG8_EXT                        0x822B
#endif /* GL_EXT_texture_rg */
#define GL_RED              GL_RED_EXT
#define GL_RG               GL_RG_EXT
#define GL_BGR              GL_RGB
#ifndef  GL_BGRA
# define GL_BGRA             GL_RGBA
#endif
#define glBindVertexArray    //glBindVertexArrayOES
#define glGenVertexArrays    //glGenVertexArraysOES
#define glDeleteVertexArrays //glDeleteVertexArraysOES

static const GLenum __glPrimitiveType[] = {
    GL_POINTS,
    GL_LINES,
    GL_LINE_STRIP,
    GL_TRIANGLES,
    GL_TRIANGLE_STRIP,
    GL_TRIANGLE_FAN
};

static const GLint __glVertexFormatType[] = {
    GL_FLOAT,           // single
    GL_FLOAT,           // vec2
    GL_FLOAT,           // vec3
    GL_FLOAT,           // vec4
    GL_HALF_FLOAT,      // half
    GL_HALF_FLOAT,      // vec2h
    GL_HALF_FLOAT,      // vec3h
    GL_HALF_FLOAT,      // vec4h
    GL_UNSIGNED_BYTE,
    GL_SHORT,
    GL_SHORT,
    GL_SHORT,
    GL_SHORT,
    GL_BYTE

};

static const GLenum __glVertexFormatSize[] = {
    1,      2,      3,      4,
    1,      2,      3,      4,
    4,      2,      4,      2,
    4,      4
};

static const GLboolean __glVertexFormatIsNormalized[] = {
    GL_FALSE, // vec
    GL_FALSE,
    GL_FALSE,
    GL_FALSE,
    GL_FALSE, // vech
    GL_FALSE,
    GL_FALSE,
    GL_FALSE,
    GL_TRUE,
    GL_FALSE,
    GL_FALSE, // short2, 4
    GL_TRUE,
    GL_TRUE, // short2, 4 norm
    GL_FALSE // byte4

};

static const GLenum __glIndexFormatType[] = {
    GL_UNSIGNED_BYTE, //E_TYPE_UNSIGNED_INT8,
    GL_UNSIGNED_SHORT, //E_TYPE_UNSIGNED_INT16,
    GL_UNSIGNED_INT, //E_TYPE_UNSIGNED_INT32,
    GL_NONE
};

const GLenum __glSurfaceFormat[] = {
GL_RGBA,   //	 Color,//   (Unsigned format) 32-bit ARGB pixel format with alpha, using 8 bits per channel.
GL_BGR,    //    Bgr565,//  (Unsigned format) 16-bit BGR pixel format with 5 bits for blue, 6 bits for green, and 5 bits for red.
GL_BGRA,   //    Bgra5551,//    (Unsigned format) 16-bit BGRA pixel format where 5 bits are reserved for each color and 1 bit is reserved for alpha.
GL_BGRA,   //    Bgra4444,//    (Unsigned format) 16-bit BGRA pixel format with 4 bits for each channel.
GL_RGB,    //    Dxt1,//    DXT1 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_RGB,    //    Dxt3,//    DXT3 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_RGB,    //    Dxt5,//    DXT5 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_RG,     //    NormalizedByte2,// (Signed format) 16-bit bump-map format using 8 bits each for u and v data.
GL_RGBA,   //    NormalizedByte4,// (Signed format) 32-bit bump-map format using 8 bits for each channel.
GL_RGBA,   //    Rgba1010102,// (Unsigned format) 32-bit RGBA pixel format using 10 bits for each color and 2 bits for alpha.
GL_RG,     //    Rg32,//    (Unsigned format) 32-bit pixel format using 16 bits each for red and green.
GL_RGBA,   //    Rgba64,//  (Unsigned format) 64-bit RGBA pixel format using 16 bits for each component.
GL_ALPHA,  //    Alpha8,//  (Unsigned format) 8-bit alpha only.
GL_RED,    //    Single,//  (IEEE format) 32-bit float format using 32 bits for the red channel.
GL_RG,     //    Vector2,// (IEEE format) 64-bit float format using 32 bits for the red channel and 32 bits for the green channel.
GL_RGBA,   //    Vector4,// (IEEE format) 128-bit float format using 32 bits for each channel (alpha, blue, green, red).
GL_RED,    //    HalfSingle,//  (Floating-point format) 16-bit float format using 16 bits for the red channel.
GL_RG,     //    HalfVector2,// (Floating-point format) 32-bit float format using 16 bits for the red channel and 16 bits for the green channel.
GL_RGBA,   //    HalfVector4,// (Floating-point format) 64-bit float format using 16 bits for each channel (alpha, blue, green, red).
};  //

const GLint __glPixelStoreSize[] = {
		4, 4, 4, 4,
		4, 4, 4, 4,
		1, 1, 4, 1,
		1, 4,
        4, 4, 2, 1,
		4, 4, 2, 1 };

const GLenum __glSurfaceFormatInternal[] = {
GL_RGBA,     //     Color,//   (Unsigned format) 32-bit ARGB pixel format with alpha, using 8 bits per channel.
GL_BGR,      //     Bgr565,//  (Unsigned format) 16-bit BGR pixel format with 5 bits for blue, 6 bits for green, and 5 bits for red.
GL_BGRA,     //     Bgra5551,//    (Unsigned format) 16-bit BGRA pixel format where 5 bits are reserved for each color and 1 bit is reserved for alpha.
GL_BGRA,     //     Bgra4444,//    (Unsigned format) 16-bit BGRA pixel format with 4 bits for each channel.
GL_RGB,      //     Dxt1,//    DXT1 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_RGB,      //     Dxt3,//    DXT3 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_RGB,      //     Dxt5,//    DXT5 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_RG,       //     NormalizedByte2,// (Signed format) 16-bit bump-map format using 8 bits each for u and v data.
GL_RGBA,     //     NormalizedByte4,// (Signed format) 32-bit bump-map format using 8 bits for each channel.
GL_RGBA,     //     Rgba1010102,// (Unsigned format) 32-bit RGBA pixel format using 10 bits for each color and 2 bits for alpha.
GL_RG,       //     Rg32,//    (Unsigned format) 32-bit pixel format using 16 bits each for red and green.
GL_RGBA,     //     Rgba64,//  (Unsigned format) 64-bit RGBA pixel format using 16 bits for each component.
GL_ALPHA,    //     Alpha8,//  (Unsigned format) 8-bit alpha only.
GL_RED,      //     Single,//  (IEEE format) 32-bit float format using 32 bits for the red channel.
GL_RG,       //     Vector2,// (IEEE format) 64-bit float format using 32 bits for the red channel and 32 bits for the green channel.
GL_RGBA,     //     Vector4,// (IEEE format) 128-bit float format using 32 bits for each channel (alpha, blue, green, red).
GL_RED,      //     HalfSingle,//  (Floating-point format) 16-bit float format using 16 bits for the red channel.
GL_RG,       //     HalfVector2,// (Floating-point format) 32-bit float format using 16 bits for the red channel and 16 bits for the green channel.
GL_RGBA,     //     HalfVector4,// (Floating-point format) 64-bit float format using 16 bits for each channel (alpha, blue, green, red).
};

const GLenum __glSurfaceFormatType[] = {



GL_UNSIGNED_BYTE,             // 	 Color,//   (Unsigned format) 32-bit ARGB pixel format with alpha, using 8 bits per channel.
GL_UNSIGNED_SHORT_5_6_5,      //     Bgr565,//  (Unsigned format) 16-bit BGR pixel format with 5 bits for blue, 6 bits for green, and 5 bits for red.
GL_UNSIGNED_SHORT_5_5_5_1,    //     Bgra5551,//    (Unsigned format) 16-bit BGRA pixel format where 5 bits are reserved for each color and 1 bit is reserved for alpha.
GL_UNSIGNED_SHORT_4_4_4_4,    //     Bgra4444,//    (Unsigned format) 16-bit BGRA pixel format with 4 bits for each channel.
GL_UNSIGNED_BYTE,			  //     Dxt1,//    DXT1 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_UNSIGNED_BYTE,             //     Dxt3,//    DXT3 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_UNSIGNED_BYTE,             //     Dxt5,//    DXT5 compression texture format. The runtime will not allow an application to create a surface using a DXTn format unless the surface dimensions are multiples of 4. This applies to offscreen-plain surfaces, render targets, 2D textures, cube textures, and volume textures.
GL_UNSIGNED_SHORT,            //     NormalizedByte2,// (Signed format) 16-bit bump-map format using 8 bits each for u and v data.
GL_UNSIGNED_INT,              //     NormalizedByte4,// (Signed format) 32-bit bump-map format using 8 bits for each channel.
GL_UNSIGNED_INT,   //     Rgba1010102,// (Unsigned format) 32-bit RGBA pixel format using 10 bits for each color and 2 bits for alpha.
GL_UNSIGNED_INT,              //     Rg32,//    (Unsigned format) 32-bit pixel format using 16 bits each for red and green.
GL_UNSIGNED_SHORT,            //     Rgba64,//  (Unsigned format) 64-bit RGBA pixel format using 16 bits for each component.
GL_UNSIGNED_BYTE,             //     Alpha8,//  (Unsigned format) 8-bit alpha only.
GL_FLOAT,                     //     Single,//  (IEEE format) 32-bit float format using 32 bits for the red channel.
GL_FLOAT,                     //     Vector2,// (IEEE format) 64-bit float format using 32 bits for the red channel and 32 bits for the green channel.
GL_FLOAT,		              //     Vector4,// (IEEE format) 128-bit float format using 32 bits for each channel (alpha, blue, green, red).
GL_HALF_FLOAT,                //     HalfSingle,//  (Floating-point format) 16-bit float format using 16 bits for the red channel.
GL_HALF_FLOAT,                //     HalfVector2,// (Floating-point format) 32-bit float format using 16 bits for the red channel and 16 bits for the green channel.
GL_HALF_FLOAT                 //     HalfVector4,// (Floating-point format) 64-bit float format using 16 bits for each channel (alpha, blue, green, red).
};


#endif

