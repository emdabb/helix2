#include <helix/gfx/VertexBuffer.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/VertexDeclaration.h>

#include <helix/core/Factory.h>
#include <cassert>

using namespace helix::core;
using namespace helix::gfx;

VertexBuffer::VertexBuffer(IGraphicsDevice* device, const VertexDeclaration* vertexDeclaration, size_t sizeInBytes)
: mGraphicsDevice(device)
, mVertexDeclaration(vertexDeclaration)
, mSizeInBytes(sizeInBytes)
, mPimpl(NULL)
, mBackup(NULL)
{
    create();
}

VertexBuffer::~VertexBuffer() {
    destroy();
}

void VertexBuffer::create() {
    if(NULL == mPimpl) {
        mPimpl = Factory<impl::IVertexBufferImpl>::getInstance().create(mGraphicsDevice->api());
        assert(mPimpl);
        mPimpl->create(this);
    }
}

void VertexBuffer::destroy() {
    if(mPimpl) {
        delete mPimpl;
        mPimpl = NULL;
    }
}

const size_t VertexBuffer::size() const {
    return mSizeInBytes;
}

const VertexDeclaration& VertexBuffer::getVertexDeclaration() const {
    return *mVertexDeclaration;
}

void VertexBuffer::onContextLost(const GraphicsResourceEventArgs& args) {
    if(mBackup == NULL) {
        mBackup = malloc(mSizeInBytes);
        mPimpl->getData(mBackup, 0, mSizeInBytes);
        destroy();
    }
}

void VertexBuffer::onContextReset(const GraphicsResourceEventArgs& args) {
    if(mBackup != NULL) {
        create();
        mPimpl->setData(mBackup, 0, mSizeInBytes);
        free(mBackup);
        mBackup = NULL;
    }
}
