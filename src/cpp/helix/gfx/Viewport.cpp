/*
 * Viewport.cpp
 *
 *  Created on: Jul 22, 2015
 *      Author: miel
 */

#include <helix/gfx/Viewport.h>
#include <helix/core/MathHelper.h>

using namespace helix::gfx;
using namespace helix::core;

void Viewport::project(const Viewport& vp, const Vector3& in, const Matrix& wvp, Vector3* out) {
    Vector3 vector = Vector3::transform(in, wvp);
    float a = ((
    		(in.X * wvp.M14) +
			(in.Y * wvp.M24)) +
    		(in.Z * wvp.M34)) +
    				wvp.M44;
    if (!MathHelper::within_epsilon(a - 1.f))
    {
        vector = (vector / a);
    }
    out->X = (((vector.X + 1.f) * 0.5f) * vp.W) + vp.X;
    out->Y = (((-vector.Y + 1.f) * 0.5f) * vp.H) + vp.Y;
    out->Z = (vector.Z * (vp.MaxDepth - vp.MinDepth)) + vp.MinDepth;

}

void Viewport::unproject(const Viewport& vp, const Vector3& in, const Matrix& wvp, Vector3* out) {
    Matrix wvpI;
    Matrix::invert(wvp, &wvpI);
    Vector3 vec;
    vec.X = (((in.X - vp.X) / ((float) vp.W)) * 2.f) - 1.f;
    vec.Y = -((((in.Y - vp.Y) / ((float) vp.H)) * 2.f) - 1.f);

    vec.Z = (in.Z - vp.MinDepth) / (vp.MaxDepth - vp.MinDepth);
    Vector3 vector = Vector3::transform(vec, wvpI);
    float a = ((
    		(vec.X * wvpI.M14) +
			(vec.Y * wvpI.M24)) +
    		(vec.Z * wvpI.M34)) +
    				 wvpI.M44;

    if (!MathHelper::within_epsilon(a - 1.f))
    {
        *out = (vector / a);
        return;
    }
    *out = vector;
}
