#ifndef HELIX_GFX_VERTEXBUFFER_H_
#define HELIX_GFX_VERTEXBUFFER_H_

#include <helix/Types.h>
#include <helix/gfx/IGraphicsResource.h>
#include <helix/core/ILockable.h>

namespace helix {
namespace gfx {

struct IGraphicsDevice;
class  VertexBuffer;
class  VertexDeclaration;

namespace impl {
    struct IVertexBufferImpl : public hxLockable {
        virtual ~IVertexBufferImpl() {}
        virtual void create(VertexBuffer*) = 0;
        virtual int  acquire() = 0;
        virtual void release() = 0;
        virtual void setData(const void*, size_t, size_t) = 0;
        virtual void getData(void*, size_t, size_t) = 0;
    };
}

class VertexBuffer : public IGraphicsResource {
    typedef impl::IVertexBufferImpl* pimpl;

    IGraphicsDevice*            mGraphicsDevice;
    const VertexDeclaration*    mVertexDeclaration;
    size_t                      mSizeInBytes;
    pimpl                       mPimpl;
    void*                       mBackup;
public:
    VertexBuffer(IGraphicsDevice*, const VertexDeclaration*, size_t);
    virtual ~VertexBuffer();

    void create();
    void destroy();

    template <typename T>
    void setData(const T* src, size_t beg, size_t end) {
        if(mPimpl->acquire()) {
            mPimpl->setData(src, beg, end * sizeof(T));
            mPimpl->release();
        }
    }

    template <typename T>
    void getData(T* dst, size_t beg, size_t end) {
        if(mPimpl->acquire()) {
            mPimpl->getData(dst, beg, end * sizeof(T));
            mPimpl->release();
        }
    }

    const size_t size() const;
    const VertexDeclaration& getVertexDeclaration() const;

    virtual void onContextLost(const GraphicsResourceEventArgs&);
    virtual void onContextReset(const GraphicsResourceEventArgs&);

    impl::IVertexBufferImpl& getImpl() const {
        return *mPimpl;
    }


};


}
}

typedef helix::gfx::VertexBuffer hxVertexBuffer;

#endif
