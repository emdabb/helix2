/**
 * @file primitive_type.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef PRIMITIVE_TYPE_H_
#define PRIMITIVE_TYPE_H_


namespace helix {
namespace gfx {

struct PrimitiveType {
    enum {
        Points,
        Lines,
        LineList,
        Triangles,
        TriangleStrip,
        TriangleFan
    };
};

}
}


#endif /* PRIMITIVE_TYPE_H_ */
