#include <helix/gfx/SpriteFont.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/core/DebugLog.h>
#include <helix/Config.h>
#include <helix/Types.h>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace helix;
using namespace helix::core;
using namespace helix::gfx;

int32_t nextPow2(const int32_t value) {
    int32_t n = value;
    n--;
    n |= n >> 1;   // Divide by 2^k for consecutive doublings of k up to 32,
    n |= n >> 2;   // and then or the results.
    n |= n >> 4;
    n |= n >> 8;
    n |= n >> 16;
    n++;           // The result is a number of 1 bits equal to the number
    // of bits in the original number, plus 1. That's the
    // next highest power of 2.
    return n;
}

float roundUp(float x, float mul) {
    return x + mul - fmod(x, mul);
}

int calc_texture_size(int count, int maxCharWidth, int maxCharHeight,
        bool powTwo) {
    int area = maxCharWidth * maxCharHeight * count;
    float len = real_sqrt((real ) area);
    len = roundUp(len,
            (maxCharWidth > maxCharHeight ? maxCharWidth : maxCharHeight));
    return powTwo ? nextPow2(len) : len;
}

struct node {
    node* left;
    node* right;
    Rectangle rc;
    int imgID;

    node() :
            imgID(-1) {
        left = NULL;
        right = NULL;
    }
    virtual ~node() {
    }
    node* insert(const Rectangle& rect) {
        if (left || right) {
            node* new_node = left->insert(rect);
            if (new_node != NULL) {
                return new_node;
            }
            return right->insert(rect);

        } else {
            if (imgID != -1) {
                return NULL;
            }
            if (Rectangle::getArea(rc) < Rectangle::getArea(rect)) {
                return NULL;
            }
            if (Rectangle::getArea(rc) == Rectangle::getArea(rect)) {
                return this;
            }

            left = new node;
            right = new node;

            unsigned int h = rect.H;
            unsigned int w = rect.W;
            int dw = rc.W - rect.W;
            int dh = rc.H - rect.H;

            if (dw > dh) {
                left->rc = { 
rc.X, 
rc.Y, 
w, 
rc.H };
                right->rc = { 
rc.X + (int)w,
rc.Y, 
rc.W - (int)w,
rc.H };
            } else {
                left->rc = { rc.X, rc.Y, rc.W, h };
                right->rc = { rc.X, rc.Y + (int)h, rc.W, rc.H - h };
            }

            return left->insert(rect);
        }
    }
};

SpriteFont::SpriteFont(GlyphInfo info[], size_t numGlyphs, Texture2D* tex) {
    DEBUG_METHOD();
    mTexture = tex;
    mInfos.resize(numGlyphs);
    for (size_t i = 0; i < numGlyphs; i++) {
        mInfos[i] = info[i];
    }
}

SpriteFont::~SpriteFont() {
}

void SpriteFont::setLineHeight(int n) {
    mMetrics.LineHeight = n;
}

int SpriteFont::getLineHeight() const {
    return mMetrics.LineHeight;
}

real SpriteFont::getSpacing() const {
    return 0;
}

char SpriteFont::getDefaultCharacter() const {
    return 0x20;
}

//void SpriteFont::setDefaultCharacter(char c) {
//}

Vector2 SpriteFont::measureString(const char* str) {
#if 0
    Vector2 size = Vector2::Zero;
    unsigned int maxx = 0;
    unsigned int maxy = 0;
    unsigned int xx=0;
    unsigned int yy = 0;

    for (size_t i = 0; i < strlen(str); i++) {
        GlyphInfo& info = mInfos[str[i] - 32];

        xx += info.Advance;
        yy  = std::max(yy, info.Rect.H);
        if(str[i] == '\n') {
           // yy += getLineHeight();
            maxx = std::max(maxx, xx);
            xx = 0;
            continue;
        }

    }

    yy  = std::max(yy, (unsigned int)getLineHeight());

    size.X = std::max(maxx, xx);
    size.Y = std::max(maxy, yy);

    return size;
#else
    int xx = 0;
    int yy = 0;
    float maxLineWidth = 0.f;
    float lineWidth = 0.0f;
    float lineHeight = (float)getLineHeight();
    Vector2 cursor = { 0.f, lineHeight };

    for (unsigned int i = 0; i < strlen(str); i++) {
		if(str[i] == '\n') {
			maxLineWidth = std::max(lineWidth, maxLineWidth);
			cursor.Y += (float)getLineHeight();
			lineWidth  = 0;
			continue; // <-- don's measure line breaks!
		}
		GlyphInfo& info = getGlyphInfo((int) str[i]);
		float px = (cursor.X + info.Bearing.X);
		float py = (cursor.Y - info.Rect.H + (info.Rect.H - info.Bearing.Y));

		lineWidth += info.Advance;
		cursor.Y  = std::max(cursor.Y, (float)info.Rect.H);
		//cursor.Y += getLineHeight();
	}

    cursor.X = std::max(lineWidth, maxLineWidth);

    //cursor.Y = std::max(cursor.Y, lineHeight);
    return cursor;
#endif
}

GlyphInfo& SpriteFont::getGlyphInfo(int c) {
    return mInfos[c - 32];
}

const GlyphInfo& SpriteFont::getGlyphInfo(int c) const {
    return const_cast<GlyphInfo&>(static_cast<const SpriteFont*>(this)->getGlyphInfo(
            c));
}

Texture2D* SpriteFont::getTexture() const {
    return mTexture;
}

SpriteFont* SpriteFont::createFromStream(IGraphicsDevice* pDevice, std::istream* is, size_t ptsize) {
    DEBUG_METHOD();

    static FT_Library library;
    static bool once = false;
    FT_Face face;

    if(!once) {
        DEBUG_MESSAGE("intializing freetype...");
        once = true;
        FT_Error err = FT_Init_FreeType(&library);
        if(FT_Err_Ok != err) {
            throw std::runtime_error("FT_Init_FreeType failed!");
        }

    }

    std::string ft_source = std::string(std::istreambuf_iterator<char>(*is), std::istreambuf_iterator<char>());

    //FT_Error err = FT_Err_Ok;
    DEBUG_MESSAGE("creating font face...");
    if(FT_New_Memory_Face(library, (const FT_Byte*)ft_source.c_str(), ft_source.length(), 0, &face)) {
        throw std::runtime_error("FT_New_Face failed!");
    }
    DEBUG_MESSAGE("char_size = %d...", ptsize << 6);
    if(FT_Set_Char_Size(face, ptsize << 6, ptsize << 6, 96, 96)) {
        throw std::runtime_error("FT_Set_Char_Size failed!");
    }

    Texture2D* GlyphAtlas = NULL;

    unsigned int w = 0;
    unsigned int h = 0;

    for (int i = 32; i < 128; i++) {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER)) {
            continue;
        }
        FT_GlyphSlot slot = face->glyph;
        FT_Bitmap& bitmap = slot->bitmap;

        w = std::max(w, bitmap.width);
        h = std::max(h, bitmap.rows);
    }

    int textureSize = calc_texture_size(128, w, h, false);
    //pDevice->createTexture2D(&GlyphAtlas, textureSize, textureSize, PixelFormat::Color);


    node root;
    root.rc = { 0, 0, (unsigned int)textureSize, (unsigned int)textureSize };

    GlyphAtlas = new Texture2D(pDevice, textureSize, textureSize, PixelFormat::Color, false);

    std::vector<GlyphInfo> infos;


    for (int i = 32; i < 128; i++) {
        if (FT_Load_Char(face, i, FT_LOAD_RENDER | FT_LOAD_FORCE_AUTOHINT | FT_LOAD_TARGET_LIGHT)) {
            continue;
        }

        FT_GlyphSlot slot = face->glyph;
        FT_Bitmap& bitmap = slot->bitmap;


        Rectangle rrc = { 0, 0, (unsigned int)bitmap.width, (unsigned int)bitmap.rows };
        node* new_node = root.insert(rrc);
        new_node->imgID = i;
        const Rectangle& rc = new_node->rc;

        if (new_node->imgID && bitmap.buffer) {
#if 1
            unsigned char* image = new unsigned char[bitmap.width * bitmap.rows * 4];
            unsigned char* ptr = &image[0];
            for (unsigned int y = 0; y < bitmap.rows; y++) {
                for (unsigned int x = 0; x < bitmap.width; x++) {
                    int i = x + y * bitmap.width;
                    *ptr++ = bitmap.buffer[i];
                    *ptr++ = bitmap.buffer[i];
                    *ptr++ = bitmap.buffer[i];
                    *ptr++ = bitmap.buffer[i];
                }
            }
            GlyphAtlas->setData<unsigned int>((unsigned int*)image, 0, rc.X, rc.Y, rc.W, rc.H);
        	delete[] image;
#else
        	GlyphAtlas->setData<unsigned char>(bitmap.buffer, 0, rc.X, rc.Y, rc.W, rc.H);
#endif

        }
        GlyphInfo info;
        info.Rect = rc;
        info.Advance = slot->advance.x >> 6;
        info.Ascent = face->ascender >> 6;
        info.Bearing.X = slot->metrics.horiBearingX >> 6;
        info.Bearing.Y = slot->metrics.horiBearingY >> 6;
        infos.push_back(info);
    }
    SpriteFont* res = new SpriteFont(&infos[0], infos.size(), GlyphAtlas);
    res->setLineHeight(face->height / 64);

    FT_Done_Face(face);

    return res;

    //return NULL;
}
