#ifndef HELIX_GFX_TEXTUREPACKER_H_
#define HELIX_GFX_TEXTUREPACKER_H_

#include <helix/core/Rectangle.h>

namespace helix {
    namespace gfx {
        class TexturePacker {
            //void* mSurfaceData;
        protected:
            void calculateTextureSize(int&, int&);
        public:
            TexturePacker();
            virtual ~TexturePacker();
            int addRect(const hxRectangle&);
            Texture2D* pack(bool forcePowerOfTwo = false);
        };
    }
}

#endif
