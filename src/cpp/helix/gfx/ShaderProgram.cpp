#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/core/Factory.h>

using namespace helix::gfx;
using namespace helix::core;

ShaderProgram::ShaderProgram(IGraphicsDevice* dev, const std::string& src)
: mSource(src)
{
    pimpl = Factory<impl::IShaderProgram>::getInstance().create(dev->api());
    assert(pimpl);
    pimpl->create(this);
    mIsActive = false;
}

ShaderProgram::~ShaderProgram() {

}

//void ShaderProgram::compileAndLink(const char* src) {
//    DEBUG_METHOD();
//    DEBUG_MESSAGE(src);
//    if(!pimpl->compile(src)) {

//    }

//    if(!pimpl->link()) {

//    }

//    pimpl->cacheUniforms(this);
//}

void ShaderProgram::onInvalidate(const UniformEventArgs& args) {
    if(this->mIsActive) {
        args.thiz->validate();
    }
}

void ShaderProgram::addUniform(ShaderParameterBase* param, int loc) {
    mUniform.push_back(param);
    mLocation.push_back(loc);
    if(mUniformNames.count(param->getName()) > 0) {
        throw std::runtime_error("duplicate uniform name found: " + param->getName());
    }
    param->OnInvalidate += event(this, &ShaderProgram::onInvalidate);
    mUniformNames[param->getName()] = param;
}

int ShaderProgram::acquire() {

    pimpl->acquire();
    mIsActive = true;
    unsigned int n = mUniform.size();
    for(unsigned int i=0; i < n; i++) {
        if(!mUniform[i]->isValid()) {
            mUniform[i]->validate();
        }
    }
    return 1;
}

void ShaderProgram::release() {
    pimpl->release();
    mIsActive = false;
}

const std::string& ShaderProgram::getSource() const {
    return mSource;
}

ShaderProgram* ShaderProgram::createFromStream(IGraphicsDevice* pDevice, std::istream* is) {
    is->seekg(0, is->beg);
    int a = is->tellg();
    is->seekg(0, is->end);
    int b = is->tellg();
    int sz = b - a;
    is->seekg(0, is->beg);
    char* src = new char[sz + 1];
    src[sz] = '\0';
    is->read(src, sz);
    ShaderProgram* out = new ShaderProgram(pDevice, src);
    //out->compileAndLink(src);
    delete[] src;
    return out;
}
