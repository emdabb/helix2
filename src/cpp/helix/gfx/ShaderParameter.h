#ifndef HELIX_GFX_SHADERPARAMETER_H_
#define HELIX_GFX_SHADERPARAMETER_H_

#include <helix/Types.h>
#include <vector>
#include <string>
#include <cassert>
#include <cstring>
#include <helix/core/EventHandler.h>

namespace helix {
namespace gfx {

class ShaderProgram;

struct IShaderParameter {
    virtual ~IShaderParameter() {}
    virtual void validate() = 0;
};

struct UniformEventArgs {
    IShaderParameter* thiz;
};


class ShaderParameterBase : public IShaderParameter{
protected:
    std::string mName;
    std::vector<ShaderProgram*> mOwner;
    std::vector<int> mLocation;
    bool mIsValid;
public:
    ShaderParameterBase(ShaderProgram* sh, const std::string& name, int loc);
    virtual ~ShaderParameterBase();
    void addOwner(ShaderProgram* sh, int loc);
    void invalidate();
    bool isValid() const;
    const std::string& getName() const {
        return mName;
    }
//    virtual void validate() = 0;
public:
    core::EventHandler<UniformEventArgs> OnInvalidate;
};

template <typename T>
class ShaderParameter : public ShaderParameterBase {
protected:
    T* mValue;
    size_t mCount;
public:
    typedef T type;
public:
    ShaderParameter(ShaderProgram* sh, const std::string& name, int loc, size_t count)
    : ShaderParameterBase(sh, name, loc)
    , mCount(count) {
        mValue = new T[mCount];
    }

    virtual ~ShaderParameter() {
        delete[] mValue;
        mValue = NULL;
    }

    void setValue(T* val) {

//        for(int i=0; i < elementCount; i++) {
//            if(mValue[i] != val[i]) {
//                mValue[i] = val[i];
//                isValid   = false;
//            }
//        }
        if(memcmp(&mValue[0], &val[0], mCount * sizeof(T))) {
            memcpy(&mValue[0], &val[0], mCount * sizeof(T));
            invalidate();
            UniformEventArgs args;
            args.thiz = this;
            OnInvalidate(args);
        }
    }

    void setValue(const T& val, size_t i) {
        assert(i < mCount);
        mValue[i] = val;
        invalidate();
        UniformEventArgs args;
        args.thiz = this;
        OnInvalidate(args);
    }

    const T& getValue(int n = 0) const {
        return mValue[n];
    }

    const T* getValues() const {
        return &mValue[0];
    }

    const size_t count() const {
        return mCount;
    }
};

}
}

template <typename T>
struct hxShaderParameter {
    typedef helix::gfx::ShaderParameter<T> Type;
};

#endif
