#include <helix/gfx/ShaderParameter.h>

using namespace helix::gfx;

ShaderParameterBase::ShaderParameterBase(ShaderProgram* sh, const std::string& name, int loc) : mName(name), mIsValid(false) {
    addOwner(sh, loc);
}
ShaderParameterBase::~ShaderParameterBase() {

}
void ShaderParameterBase::addOwner(ShaderProgram* sh, int loc) {
    //this->on_invalidate += event(sh, &ShaderProgram::on_invalidate);
}

void ShaderParameterBase::invalidate() {
    mIsValid = false;
}

bool ShaderParameterBase::isValid() const {
    return mIsValid != false;
}

