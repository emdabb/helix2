#ifndef HELIX_GFX_CLEAROPTIONS_H_
#define HELIX_GFX_CLEAROPTIONS_H_


namespace helix {
namespace gfx {

    struct ClearOptions {
        enum {
            ClearColor = 1 << 0,
            ClearDepth = 1 << 1,
            ClearStencil = 1 << 2,
            ClearAll = 7
        };
    };
    
}
}


#endif /* CLEAR_OPTIONS_H_ */
