/**
 * @file vertex_stream.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date Sep 12, 2014
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef VERTEX_STREAM_H_
#define VERTEX_STREAM_H_

namespace helix {
namespace gfx {

struct VertexStream {
    /**
     * Vertex stream position.
     */
    enum {
        Position            =  0,           //!< Position
        BlendWeight         =  1,        //!< BlendWeight
        Normal              =  2,             //!< Normal
        PrimaryColor        =  3,       //!< PrimaryColor
        SecondaryColor      =  4,     //!< SecondaryColor
        FogCoordinate       =  5,      //!< FogCoordinate
        PointSize           =  6,          //!< PointSize
        BlendIndex          =  7,         //!< BlendIndex
        TextureCoordinate0  =  8, //!< TextureCoordinate0
        TextureCoordinate1  =  9, //!< TextureCoordinate1
        TextureCoordinate2  = 10, //!< TextureCoordinate2
        TextureCoordinate3  = 11, //!< TextureCoordinate3
        TextureCoordinate4  = 12, //!< TextureCoordinate4
        TextureCoordinate5  = 13, //!< TextureCoordinate5
        TextureCoordinate6  = 14, //!< TextureCoordinate6
        TextureCoordinate7  = 15, //!< TextureCoordinate7
        //        Tangent = 16,
        //        Binormal = 17,
        //        VertexID = 18,
        //        PrimitiveID = 19,
        MaxElementAttributes = 16   //!< MaxElementAttributes
    };
};

struct VertexElementFormat {
    enum {
        /**
         *  Single-component, 32-bit floating-point, expanded to (float, 0, 0, 1).
         */
        Single,
        /**
         *  Two-component, 32-bit floating-point, expanded to (float, Float32 value, 0, 1).
         */
        Vector2,
        /**
         *  Three-component, 32-bit floating point, expanded to (float, float, float, 1).
         */
        Vector3,
        /**
         *  Four-component, 32-bit floating point, expanded to (float, float, float, float).
         */
        Vector4,
        /**
         *  16-bit floating point expanded to value. This type is valid for vertex shader version 2.0 or higher.
         */
        HalfSingle,
        /**
         *  Two-component, 16-bit floating point expanded to (value, value). This type is valid for vertex shader version 2.0 or higher.
         */
        HalfVector2,
        /**
         *  Three-component, 16-bit floating point expanded to (value, value, value). This type is valid for vertex shader version 2.0 or higher.
         */
        HalfVector3,
        /**
         *  Four-component, 16-bit floating-point expanded to (value, value, value, value). This type is valid for vertex shader version 2.0 or higher.
         */
        HalfVector4,
        /**
         *  Four-component, packed, signed int, mapped to 0 to 1 range. Input is in Int32 format (ARGB) expanded to (R, G, B, A).
         */
        //Int4,
        /**
         *  Four-component, packed, unsigned byte, mapped to 0 to 1 range. Input is in Int32 format (ABGR) expanded to (R, G, B, A).
         */
        Color,
        /**
         *  Two-component, signed short expanded to (value, value, 0, 1).
         */
        Short2,
        /**
         *  Four-component, signed short expanded to (value, value, value, value).
         */
        Short4,
        /**
         *  Normalized, two-component, signed short, expanded to (first short/32767.0, second short/32767.0, 0, 1). This type is valid for vertex shader version 2.0 or higher.
         */
        NormalizedShort2,
        /**
         *  Normalized, four-component, signed short, expanded to (first short/32767.0, second short/32767.0, third short/32767.0, fourth short/32767.0). This type is valid for vertex shader version 2.0 or higher.
         */
        NormalizedShort4,
        /**
         *  Four-component, unsigned byte.
         */
        Byte4
    };


};

}
}

typedef helix::gfx::VertexStream        hxVertexStream;
typedef helix::gfx::VertexElementFormat hxVertexElementFormat;

#endif /* VERTEX_STREAM_H_ */
