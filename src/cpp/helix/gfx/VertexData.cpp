#include <helix/gfx/VertexData.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/VertexDeclarationBuilder.h>

using namespace helix::gfx;

VertexData::VertexData() :
        mOffset(0) {
}

VertexData::~VertexData() {
}

const VertexDeclaration* VertexData::getDeclaration() {
    size_t nChannels = mChannels.size();
    if(nChannels == 0) {
        return NULL;
    }
    std::vector<VertexElement*> attr;
    VertexDeclarationBuilder builder;
    //builder.size(nChannels);

    for(size_t i=0; i < nChannels; i++) {
        IVertexChannel* c = mChannels[i];
        size_t off 	= c->offset();
        int type	= c->type();
        int binding = c->binding();

        builder.element(i, off, type, binding);
    }
    return builder.build();
}
