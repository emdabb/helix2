#include <helix/content/AssetManager.h>
#include <helix/core/DebugLog.h>
#include <CoreFoundation/CoreFoundation.h>
#include <Foundation/Foundation.h>
#include <helix/Types.h>
#include <fstream>

extern const char* PATH_DELIM;

using namespace helix::core;

namespace helix {
namespace content {
namespace impl {

class IOSAssetManager : public AssetManager {

public:
    virtual ~IOSAssetManager() {
    }
    


    
    std::istream* openStream(const std::string& fn) {
        DEBUG_METHOD();
        NSString* resources = [[NSBundle mainBundle] resourcePath];
        std::string sResourcePath([resources UTF8String]);
        //DEBUG_MESSAGE("%s", mBasedir.c_str());
        
        std::string sAssetPath = sResourcePath + PATH_DELIM + mBasedir + PATH_DELIM + fn;

        
        DEBUG_MESSAGE("opening %s...", sAssetPath.c_str());
        
        std::ifstream* fi = new std::ifstream;
        fi->open(sAssetPath.c_str());
        if(!fi->is_open() ) {
            throw std::runtime_error(std::string("File not found: ") + sAssetPath);
        }
        mStreams[fn] = fi;
        return fi;
    }
    
};
}
HXAPI HXEXPORT AssetManager* new_IOSAssetManager() {
    return new impl::IOSAssetManager;
}

}
}


