#include <helix/content/AssetManager.h>
#include <helix/Types.h>
#include <helix/core/DebugLog.h>
#include <helix/core/io/stdiostream.h>
#include <cstdio>
#include <cassert>
#include <errno.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>

using namespace helix::core;

namespace helix {
namespace content {
namespace impl {


static int android_read(void* cookie, char* buf, int size) {
	return AAsset_read((AAsset*)cookie, buf, size);
}

static int android_write(void* cookie, const char* buf, int size) {
	return EACCES; // can't provide write access to the apk
}

static fpos_t android_seek(void* cookie, fpos_t offset, int whence) {
	return AAsset_seek((AAsset*)cookie, offset, whence);
}

static int android_close(void* cookie) {
	AAsset_close((AAsset*)cookie);

	return 0;
}

static FILE* android_fopen(AAssetManager* mgr, const char* fname, const char* mode) {
	if(mode[0] == 'w') return NULL;

	AAsset* asset = AAssetManager_open(mgr, fname, 0);
	if(!asset) {
		DEBUG_METHOD();
		DEBUG_MESSAGE("File not found: \"%s\"", fname);
		return NULL;
	}
	return funopen(asset, android_read, android_write, android_seek, android_close);
}


class AndroidAssetManager : public AssetManager {
	AAssetManager* mAssetManager;
public:
	explicit AndroidAssetManager(AAssetManager* val)
	: mAssetManager(val)
	{

	}
    virtual ~AndroidAssetManager() {
    }

    virtual std::istream* openStream(const std::string& fn) {
    	assert(mAssetManager);
		FILE* fp = android_fopen(mAssetManager, fn.c_str(), "r");
		if(!fp) {
			throw std::runtime_error(fn);
		}
		return new stdiostream(fp);
    }

};


}
}
}

HXAPI HXEXPORT hxAssetManager* new_AndroidAssetManager(AAssetManager* val) {
    return new helix::content::impl::AndroidAssetManager(val);
}
