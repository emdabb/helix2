#ifndef HELIX_CONTENT_TTFTYPEREADER_H_
#define HELIX_CONTENT_TTFTYPEREADER_H_

#include <helix/content/IContentTypeReader.h>
#include <helix/content/ContentReader.h>
#include <helix/gfx/Surface.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/core/DebugLog.h>
#include <lodepng/lodepng.h>
#include <iostream>
#include <algorithm>

using namespace helix::gfx;
using namespace helix::core;

namespace helix {
namespace content {

class ImageTypeReader : public IContentTypeReader {
public:
    ImageTypeReader() {
    }

    virtual ~ImageTypeReader() {
    }

    virtual void* readInternal(ContentReader& reader) {
        DEBUG_METHOD();
        unsigned error;
        unsigned char* image;
        unsigned width, height;
        //unsigned char* png;
        size_t pngsize;
        std::istream* t = reader.getStream();
        std::string data = std::string(std::istreambuf_iterator<char>(*t), std::istreambuf_iterator<char>());
        pngsize = data.size();
        error = lodepng_decode32(&image, &width, &height, (const unsigned char*)data.c_str(), pngsize);
        if(error) {
            printf("error %u: %s\n", error, lodepng_error_text(error));
        }
        DEBUG_MESSAGE("loaded %d bytes", pngsize);

        Surface* out = new Surface(width, height, PixelFormat::Color);
        out->setData(image);
        return out;

    }
};

HXAPI HXEXPORT IContentTypeReader* new_ImageTypeReader() {
    return new ImageTypeReader;
}

}
}

#endif
 

