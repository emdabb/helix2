#include <helix/content/AssetManager.h>
#include <helix/core/DebugLog.h>
#include <helix/core/Factory.h>
#include <helix/Config.h>
#include <helix/content/ContentReader.h>
#include <helix/content/IContentTypeReader.h>
#include <helix/content/ContentTypes.h>
#include <helix/content/Utils.h>
#include <fstream>
#include <cstring>

#if defined(HELIX_PLATFORM_POSIX)
const char* PATH_DELIM  = "/";
#else
const char* PATH_DELIM  = "\\";
#endif


using namespace helix::core;
using namespace helix::content;

HXAPI HXEXPORT IContentTypeReader* new_TTFTypeReader();
HXAPI HXEXPORT IContentTypeReader* new_ModelTypeReader();
HXAPI HXEXPORT IContentTypeReader* new_ImageTypeReader();

AssetManager::AssetManager()
: mBasedir("assets")
{
    Factory<IContentTypeReader, int>::getInstance().registerClass(ContentType::SpriteFont, new_TTFTypeReader);
    Factory<IContentTypeReader, int>::getInstance().registerClass(ContentType::Model, new_ModelTypeReader);
    Factory<IContentTypeReader, int>::getInstance().registerClass(ContentType::Image, new_ImageTypeReader);

}

AssetManager::~AssetManager() {
}

void* AssetManager::loadInternal(const std::string&  fn) {
#if 0
  std::istream* instream = openStream(fn);
  ContentReader reader(instream);

  ContentTypeReader* in = Factory<ContentTypeReader>::instance().create(ext);
  if(in) {
    return in->read_internal(reader);
  }
#else
  std::string ext = FilenameUtils::getExtension(fn);
  if(!strcmp(ext.c_str(), "hxb")) {
    char type;
    std::istream* is = openStream(fn);
    is->seekg(std::istream::beg);

    ContentReader reader(is);
    reader.readBytes(&type, 1);
    IContentTypeReader* in = Factory<IContentTypeReader, int>::getInstance().create(type);
    if(in) {
            return in->readInternal(reader);
        }
    }

#endif
  return NULL;
}

std::istream* AssetManager::openStream(const std::string& fn) {
//  DEBUG_METHOD();
//  DEBUG_VALUE_AND_TYPE_OF(fn);

    DEBUG_METHOD();
    std::map<std::string, std::istream*>::iterator it = mStreams.find(fn);
    if(it != mStreams.end()) {
        return (*it).second;
    }

  std::ifstream* is = new std::ifstream;
  std::string path = mBasedir + PATH_DELIM + fn;
  is->open(path.c_str(), std::ifstream::in);
    DEBUG_MESSAGE("%s", path.c_str());
  if(!is->is_open()) {
    throw std::runtime_error(std::string("File not found: ") + path);
  }
    mStreams[fn] = is;
  return is;
}

void AssetManager::setBasedir(const std::string& str) {
    mBasedir = str;
}

const std::string& AssetManager::getBasedir() const {
    return mBasedir;
}
