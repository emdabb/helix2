#ifndef HELIX_CORE_ASSETMANAGER_H_
#define HELIX_CORE_ASSETMANAGER_H_

#include <map>
#include <string>
#include <iostream>

namespace helix {
namespace content {

class AssetManager {
    virtual void* loadInternal(const std::string&);
protected:
    std::map<std::string, void*> mAssets;
    std::map<std::string, std::istream*> mStreams;
    std::string mBasedir;

public:
    AssetManager();
    virtual ~AssetManager();
    virtual void setBasedir(const std::string&);
    const std::string& getBasedir() const;
    virtual std::istream* openStream(const std::string&);
    

    template <typename T>
    T* load(const std::string& fn) {
        return reinterpret_cast<T*>(loadInternal(fn));
    }
};

}
}

typedef helix::content::AssetManager hxAssetManager;

#endif
