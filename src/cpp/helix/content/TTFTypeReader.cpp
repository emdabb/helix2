#ifndef HELIX_CONTENT_TTFTYPEREADER_H_
#define HELIX_CONTENT_TTFTYPEREADER_H_

#include <helix/content/IContentTypeReader.h>

namespace helix {
namespace content {

class TTFTypeReader : public IContentTypeReader {
};

HXAPI HXEXPORT IContentTypeReader* new_TTFTypeReader() {
    return NULL;
}

}
}

#endif
 
