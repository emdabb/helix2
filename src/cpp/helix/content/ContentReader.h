#ifndef HELIX_CONTENT_CONTENTREADER_H_
#define HELIX_CONTENT_CONTENTREADER_H_

#include <helix/core/io/BinaryReader.h>

namespace helix {
namespace content {

class ContentReader : public core::BinaryReader {
public:
    explicit ContentReader(std::istream*);
    virtual ~ContentReader();

    template <typename T>
    T readObject() {
        T out;
        readBytes(&out, sizeof(T));
    }
};

}
}

#endif
 
