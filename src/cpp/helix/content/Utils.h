#ifndef HELIX_CONTENT_UTILS_H_
#define HELIX_CONTENT_UTILS_H_

#include <string>

namespace helix {
namespace content {

struct FilenameUtils {
    static std::string getExtension(const std::string& str); 

    static std::string getPath(const std::string& str);

    static std::string getBaseName(const std::string& str);
};

}
}

#endif 
