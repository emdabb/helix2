#ifndef HELIX_CONTENT_CONTENTTYPES_H_
#define HELIX_CONTENT_CONTENTTYPES_H_

namespace helix {
namespace content {

struct ContentType {
    enum {
        SpriteFont,
        Image,
        Model,
        Animation,
        Scene,
        Max
    };
};

}
}

#endif
