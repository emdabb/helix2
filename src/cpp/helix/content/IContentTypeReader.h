#ifndef HELIX_CONTENT_ICONTENTTYPEREADER_H_
#define HELIX_CONTENT_ICONTENTTYPEREADER_H_

#include <helix/Types.h>

namespace helix {
namespace content {

class ContentReader;

struct IContentTypeReader {
    virtual ~IContentTypeReader() {}
    virtual void* readInternal(ContentReader&) = 0;    
};

}
}

#endif

