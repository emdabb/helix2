#include <helix/content/Utils.h>

using namespace helix::content;

extern const char* PATH_DELIM;

std::string FilenameUtils::getExtension(const std::string& str) {
    int dot = str.find_last_of('.');
    return str.substr(dot + 1, std::string::npos);
}

std::string FilenameUtils::getPath(const std::string& str) {
    int dot = str.find_last_of(PATH_DELIM);
    return str.substr(0, dot);
}

std::string FilenameUtils::getBaseName(const std::string& str) {
    int dot = str.find_last_of('.');
    return str.substr(0, dot);
}

