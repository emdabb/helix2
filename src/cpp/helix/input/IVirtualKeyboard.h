/*
 * IVirtualKeyboard.h
 *
 *  Created on: Jul 31, 2015
 *      Author: miel
 */

#ifndef HELIX_INPUT_IVIRTUALKEYBOARD_H_
#define HELIX_INPUT_IVIRTUALKEYBOARD_H_

#include <helix/core/EventHandler.h>
#include <helix/core/Singleton.h>
#include <helix/base/IApplication.h>

namespace helix {
namespace input {

    struct IVirtualKeyboard {
	virtual ~IVirtualKeyboard() {}

	virtual void setVisible(bool) = 0;
	virtual bool isVisible() const = 0;
        // TODO: link with application?? or is it better to keep the two seperated?? Test!
	core::EventHandler<base::KeyEventArgs> OnKeyPressed;
	core::EventHandler<base::KeyEventArgs> OnKeyReleased;
};

}
}

typedef helix::input::IVirtualKeyboard hxVirtualKeyboard;

HXAPI HXEXPORT hxVirtualKeyboard* new_VirtualKeyboard(hxApplication*);


#endif /* HELIX_INPUT_IVIRTUALKEYBOARD_H_ */
