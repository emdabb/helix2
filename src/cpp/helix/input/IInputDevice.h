#ifndef HELIX_INPUT_IINPUTDEVICE_H_
#define HELIX_INPUT_IINPUTDEVICE_H_

#include <helix/Types.h>

namespace helix {
namespace input {

__declare_aligned(struct, 16) 
InputEventArgs {
    int     Type;
    float   x[16];    
};

struct InputDeviceType {
    enum {
        Unknown = 0,
        Joystick,
        Linear,
        Angular,
        Light,
        Proximity,
        Magnetic,
        Location,
        MaxDeviceType
    };
};

struct IInputDevice {
    virtual ~IInputDevice() {}
    virtual void create() = 0;
    virtual void destroy() = 0;
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void setAlpha(float) = 0;
    virtual void setSampleTime(int) = 0;
    virtual const int getType() const = 0;
};

}
}

typedef helix::input::IInputDevice hxInputDevice;


#endif
 
