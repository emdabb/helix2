/*
 * AndroidVirtualKeyboard.cpp
 *
 *  Created on: Jul 31, 2015
 *      Author: miel
 */

#include <jni.h>
#include <helix/input/IVirtualKeyboard.h>

namespace helix {
namespace input {
namespace impl {

class AndroidVirtualKeyboard : public IVirtualKeyboard {
    JavaVM* mVm;
    JNIEnv* mEnv;
    jclass  mKeyboardClass;
    jobject mObject;
    jmethodID mShowMethodId;
    jmethodID mHideMethodId;
    bool mIsVisible;
public:
    AndroidVirtualKeyboard(JNIEnv* jenv, jobject jobj) 
    : mVm(NULL)
    , mEnv(NULL)
    , mIsVisible(false)
    {
        jenv->GetJavaVM(&mVm);
        JNIEnv* env = jenv;
        mObject = env->NewGlobalRef(jobj);
        jclass objclass = env->GetObjectClass(mObject);
        {
            mShowMethodId = env->GetStaticMethodID(objclass, "Show", "()V");
            mHideMethodId = env->GetStaticMethodID(objclass, "Hide", "()V");
        }
        env->DeleteLocalRef(objclass);
        mKeyboardClass = env->FindClass("com/tfjoy/helix/AndroidVirtualKeyboard");
    }
    
    virtual ~AndroidVirtualKeyboard() {
        getEnv()->DeleteLocalRef(mObject);
    }

    void setVisible(bool val) {
        if(val) {
            getEnv()->CallStaticVoidMethod(mKeyboardClass, mShowMethodId);
            mIsVisible = true;
        } else {
            getEnv()->CallStaticVoidMethod(mKeyboardClass, mHideMethodId);
            mIsVisible = false;
        }
    }

    bool isVisible() const {
        return mIsVisible;
    }

protected:
    JNIEnv* getEnv() {
        JNIEnv* env = NULL;
        mVm->AttachCurrentThread(&env, NULL);
        return env;
    }
};

HXAPI HXEXPORT IVirtualKeyboard* new_AndroidVirtualKeyboard(JNIEnv* jenv, jobject jobj) {
    return new AndroidVirtualKeyboard(jenv, jobj);
}

}
}
}


