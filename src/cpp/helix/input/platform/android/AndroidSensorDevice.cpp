#include <helix/input/IInputDevice.h>
#include <android/sensor.h>

namespace helix {
namespace input {
namespace impl {

class AndroidSensorDevice : public IInputDevice {
    ASensorManager*     mSensorManager;
    ASensorEventQueue*  mEventQueue;
    ASensorRef           mSensorRef;
    const int           mSensorType;
    const int           mInputType;
    bool                mIsRunning;
protected:
    static int callback(int fd, int events, void* data) {
        AndroidSensorDevice* my = static_cast<AndroidSensorDevice*>(data);
        my->onSensorChanged();
        return 1;
    }
    const int inputTypeFromSensorType(const int stype) {
        switch (stype) {
        case ASENSOR_TYPE_ACCELEROMETER:
            return InputDeviceType::Linear;
        case ASENSOR_TYPE_GYROSCOPE:
            return InputDeviceType::Angular;
        case ASENSOR_TYPE_LIGHT:
            return InputDeviceType::Light;
        case ASENSOR_TYPE_PROXIMITY:
            return InputDeviceType::Light;
        case ASENSOR_TYPE_MAGNETIC_FIELD:
            return InputDeviceType::Magnetic;
        default:
            return InputDeviceType::Unknown;
        }
    }

void onSensorChanged() {
        ASensorEvent event;
        int numEvents = 0;
        while ((numEvents = ASensorEventQueue_getEvents(mEventQueue, &event, 1)) > 0) {
            //mData.time = event.timestamp / 1000;
            InputEventArgs args;
            memset(&args.x[0], 0, sizeof(float) * 16);
            args.Type = mInputType;

            switch (mInputType) {
            case InputDeviceType::Linear:
                args.x[0] = event.acceleration.x;
                args.x[1] = event.acceleration.y;
                args.x[2] = event.acceleration.z;
                break;
            case InputDeviceType::Angular:
                args.x[0] = event.acceleration.azimuth;
                args.x[1] = event.acceleration.pitch;
                args.x[2] = event.acceleration.roll;
                break;
            case InputDeviceType::Light:
                args.x[0] = event.light;
                break;
            case InputDeviceType::Proximity:
                args.x[0] = event.distance;
                break;
            case InputDeviceType::Magnetic:
                args.x[0] = event.magnetic.x;
                args.x[1] = event.magnetic.y;
                args.x[2] = event.magnetic.z;
                break;
            default:
                memcpy(&args.x[0], event.data, sizeof(float) * 4);
                break;
            }

//            OnInputDetected(args);
        }
    }
public:
    AndroidSensorDevice(const int stype) 
    : mSensorType(stype)
    , mInputType(inputTypeFromSensorType(stype))

    {
        mSensorManager  = ASensorManager_getInstance();
        mEventQueue     = NULL;
        mSensorRef      = NULL;
        mIsRunning      = false;
    }

    virtual ~AndroidSensorDevice() {
    }

    virtual void create() {
        ALooper* looper = ALooper_forThread();
        if(looper != NULL) {
            looper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
        }
        mEventQueue = ASensorManager_createEventQueue(mSensorManager, looper, 0, callback, this);
        mSensorRef  = ASensorManager_getDefaultSensor(mSensorManager, mSensorType);
        setSampleTime(20);
        //return mSensorRef != NULL;
    }
    
    virtual void destroy() {
        stop();
        mSensorRef = NULL;
        if(mEventQueue) {
            ASensorManager_destroyEventQueue(mSensorManager, mEventQueue);
            mEventQueue = NULL;
        }
    }
    
    virtual void setAlpha(float) {
    }

    virtual void setSampleTime(int dt) {
        if(mSensorRef != NULL) {
            ASensorEventQueue_setEventRate(mEventQueue, mSensorRef, dt * 1000);
        }
    }
    
    virtual void start() {
        if(mSensorRef != NULL) {
            mIsRunning = ASensorEventQueue_enableSensor(mEventQueue, mSensorRef) >= 0;
        }
    }

    virtual void stop() {
        if(mSensorRef != NULL) {
            ASensorEventQueue_disableSensor(mEventQueue, mSensorRef);
        }
        mIsRunning  = false;
    }

    virtual const int getType() const {
        return mSensorType;
    }
};

}
}
}

HXAPI HXEXPORT hxInputDevice* new_AndroidSensorDevice(const int sensorType) {
    return new helix::input::impl::AndroidSensorDevice(sensorType);
}
