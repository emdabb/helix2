/*
 * IOSVirtualKeyboard.h
 *
 *  Created on: Jul 31, 2015
 *      Author: miel
 */

#ifndef HELIX_INPUT_PLATFORM_IOS_IOSVIRTUALKEYBOARD_H_
#define HELIX_INPUT_PLATFORM_IOS_IOSVIRTUALKEYBOARD_H_

#include <helix/input/IVirtualKeyboard.h>

namespace helix {
namespace input {
    
class IOSVirtualKeyboard : public IVirtualKeyboard {
    void* mKeyboardView;
    bool mIsVisible;
public:
    explicit IOSVirtualKeyboard(hxApplication*, void*);
    virtual ~IOSVirtualKeyboard();
    void setVisible(bool);
    bool isVisible() const;
};

}
}

#endif /* HELIX_INPUT_PLATFORM_IOS_IOSVIRTUALKEYBOARD_H_ */
