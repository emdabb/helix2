/*
 * IOSVirtualKeyboard.cpp
 *
 *  Created on: Jul 31, 2015
 *      Author: miel
 */

#include "IOSVirtualKeyboard.h"
#include <helix/base/IApplication.h>
#import <UIKit/UIKit.h>

using namespace helix::base;
using namespace helix::input;

@interface KeyboardView : UIView<UIKeyInput> {
@private
    IOSVirtualKeyboard* keyboard;
}
-(void)initWithKeyboard:(IOSVirtualKeyboard*)kb;
@end

@implementation KeyboardView

-(void)insertText:(NSString*)text {
    const char* c_str = [text UTF8String];
    KeyEventArgs args;
    args.KeyId = (int)c_str[0];
    keyboard->OnKeyPressed(args);
}

-(void)deleteBackward {
    // delete key
    KeyEventArgs args;
    args.KeyId = KeyEventArgs::Key_BACKSPACE;
    keyboard->OnKeyPressed(args);
    
}

-(BOOL)hasText {
    return YES;
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)initWithKeyboard:(IOSVirtualKeyboard*)kb {
    keyboard = kb;
}

@end

IOSVirtualKeyboard::IOSVirtualKeyboard(IApplication*, void* parentView) {
    if(parentView) {
        mKeyboardView = [[KeyboardView alloc] initWithFrame:CGRectMake(0,0,1,1)];
        [(KeyboardView*)mKeyboardView initWithKeyboard:this];

        [(UIView*)parentView addSubview:(UIView*)mKeyboardView];
        setVisible(false);
    }
}
IOSVirtualKeyboard::~IOSVirtualKeyboard() {
    
}
void IOSVirtualKeyboard::setVisible(bool val) {
    mIsVisible  = val;
    if(val) {
        [(KeyboardView*)mKeyboardView becomeFirstResponder];
    } else {
        [(KeyboardView*)mKeyboardView resignFirstResponder];
    }
}
bool IOSVirtualKeyboard::isVisible() const {
    return mIsVisible != false;
}

HXAPI HXEXPORT IVirtualKeyboard* new_VirtualKeyboard(IApplication* app) {
    return new IOSVirtualKeyboard(app, NULL);
}

HXAPI HXEXPORT IVirtualKeyboard* new_IOSVirtualKeyboard(IApplication* app, void* view) {
    return new IOSVirtualKeyboard(app, view);
}
