#include <helix/input/platform/ios/IOSLinearDevice.h>

//@implementation SensorDelegate
//
////- (id) initWithLinearDevice:(hxIOSLinearDevice*)linearDevice {
////    mLinearDevice = linearDevice;
////    return self;
////}
////
////- (void) accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration {
////    mLinearDevice->didAccelerate(acceleration);
////}
//
//- (void) startAccelerometer()
//{
//    self.motionManager = [[CMMotionManager alloc] init];
//    self.motionManager.accelerometerUpdateInterval = .2;
//    self.motionManager.gyroUpdateInterval = .2;
//    
//    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData* data, NSError* error) {
//        
//    }]
//}
//
//
//@end

namespace helix {
namespace input {
namespace impl {

class IOSSensorDevice : public IInputDevice {
    CMMotionManager* mMotionManager;
    bool             mIsRunning;
    const int        mSensorType;
    // core::EventHandler<InputEventArgs> OnInput;
public:
    IOSSensorDevice(const int sensorType)
    : mSensorType(sensorType)
    {

        
    }
    virtual ~IOSSensorDevice() {
    }
    void create() {
        mMotionManager = [[CMMotionManager alloc] init];
        //mDelegate = [[AccelerometerDelegate alloc] initWithLinearDevice:this];
        // return true;
    }
    void destroy() {
        if(mIsRunning) {
            stop();
        }
        [mMotionManager dealloc];
        mMotionManager = nil;
    }
    void start() {
        switch(mSensorType) {
            case InputDeviceType::Linear:
                break;
            case InputDeviceType::Angular:
                break;
            default:
                break;
        }
    }
    void stop() {
    }
    void setSampleTime(int t) {
        mMotionManager.accelerometerUpdateInterval = (double)t / 1000.;
        mMotionManager.gyroUpdateInterval = (double)t / 1000.;
        //[UIAccelerometer sharedAccelerometer].updateInterval = (float)t / 1000.f;
    }
    void setAlpha(float) {
    }
    void motionDidChange() {
        //mLinearData.Time = (int)acc.timestamp * 1000;
        //mLinearData.Valid = (1 << )
       

        // OnInputDetected(mLinearData);
    }

    const int getType() const {
        
    }
    
    
};

} //impl
} //input
} //helix


HXAPI HXEXPORT hxInputDevice* new_IOSSensorDevice(const int stype) {
    return new helix::input::impl::IOSSensorDevice(stype);
}
