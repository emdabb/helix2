#ifndef HELIX_INPUT_PLATFORM_IOS_IOSLINEARDEVICE_H_
#define HELIX_INPUT_PLATFORM_IOS_IOSLINEARDEVICE_H_

#include <helix/input/IInputDevice.h>
#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

namespace helix {
namespace input {
namespace impl {
class IOSSensorDevice;
}
}
}

typedef helix::input::impl::IOSSensorDevice hxIOSSensorDevice;

//@interface SensorDelegate : NSObject<UIAccelerometerDelegate> {
//    hxIOSLinearDevice* mLinearDevice;
//    
//    - (void) startAccelerometer();
//}
////- (id)      initWithDevice(hxIOSSensorDevice*)linearDevice;
////- (void)    accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration;
//@property (strong, nonatomic) CMMotionManager* motionManager;
//@end



#endif

