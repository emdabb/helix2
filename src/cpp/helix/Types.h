#ifndef HELIX_TYPES_H_
#define HELIX_TYPES_H_

#include <stdint.h>
#include <stdlib.h>
#include <cstddef>
#include <stdexcept>
#include <numeric>
#include <limits>
#include <ios>

#if defined(_MSC_VER)
# define __aligned_type(x)  __declspec(align(x))
# define __public_type      __declspec(dllexport)
# define __private_type
//# define __forceinline      __forceinline
# define __noinline         __declspec(noinline)
#elif defined(__GNUC__)

# define __aligned_type(x)  __attribute__((aligned(x)))
# define __public_type      __attribute__((visibility("default")))
# define __private_type     __attribute__((visibility("hidden")))
# define __forceinline      __attribute__((always_inline))
# define __noinline         __attribute__((noinline))
# define __debugbreak()
#else
# define __aligned_type(x)
# define __public_type
# define __private_type
#endif

#define __declare_aligned(T, n)     T __aligned_type(n)

#if defined(__cplusplus)
#define NS_BEGIN(x) namespace x  {
#define NS_END      }
#define HXAPI       extern "C"
#define HXEXPORT    __public_type
#ifndef NULL
#define NULL		0
#endif
#else
#define NS_BEGIN(x)
#define NS_END
#define HXAPI
#define HXEXPORT
#ifndef NULL
#define NULL		(void*)0
#endif
#endif

NS_BEGIN(helix)
typedef float   real;
typedef int8_t	byte_t;
typedef uint8_t	ubyte_t;
NS_END

/**
 * Type length templates.
 */
template<typename _Tx>
struct float_length {
  const static std::streamsize value = std::numeric_limits<_Tx>::is_signed + std::numeric_limits<_Tx>::is_specialized
      + std::numeric_limits<_Tx>::digits10 * 2;
};

template<typename _Tx>
struct int_length {
  const static std::streamsize value = std::numeric_limits<_Tx>::digits + std::numeric_limits<_Tx>::digits10 + 1;
};

// Return a dummy value when we're not dealing with numbers
template<typename _Tx>
struct type_length {
  const static std::streamsize value = 14;
};

// floating point data types
template<> struct type_length<float> {
  const static std::streamsize value = float_length<float>::value;
};
template<> struct type_length<double> {
  const static std::streamsize value = float_length<double>::value;
};
template<> struct type_length<long double> {
  const static std::streamsize value = float_length<long double>::value;
};

// integer data types
template<> struct type_length<short> {
  const static std::streamsize value = int_length<short>::value;
};
template<> struct type_length<int> {
  const static std::streamsize value = int_length<int>::value;
};
template<> struct type_length<long> {
  const static std::streamsize value = int_length<long>::value;
};
template<> struct type_length<long long> {
  const static std::streamsize value = int_length<long long>::value;
};
template<> struct type_length<char> {
  const static std::streamsize value = int_length<char>::value;
};
template<> struct type_length<wchar_t> {
  const static std::streamsize value = int_length<wchar_t>::value;
};
template<> struct type_length<signed char> {
  const static std::streamsize value = int_length<signed char>::value;
};
template<> struct type_length<bool> {
  const static std::streamsize value = int_length<bool>::value;
};

template<> struct type_length<unsigned short> {
  const static std::streamsize value = int_length<unsigned short>::value;
};
template<> struct type_length<unsigned int> {
  const static std::streamsize value = int_length<unsigned int>::value;
};
template<> struct type_length<unsigned long> {
  const static std::streamsize value = int_length<unsigned long>::value;
};
template<> struct type_length<unsigned long long> {
  const static std::streamsize value = int_length<unsigned long long>::value;
};
template<> struct type_length<unsigned char> {
  const static std::streamsize value = int_length<unsigned char>::value;
};


#endif
