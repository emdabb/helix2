#ifndef HELIX_AUDIO_AUDIOINITIALIZER_H_
#define HELIX_AUDIO_AUDIOINITIALIZER_H_

namespace helix {
namespace audio {

struct AudioInitializer {
    int bitRate;
    void* userData;
    
};

}
}

#endif 
