#ifndef HELIX_AUDIO_IAUDIODEVICE_H_
#define HELIX_AUDIO_IAUDIODEVICE_H_

#include <helix/Types.h>
#include <iostream>

namespace helix {
namespace audio {

struct AudioInitializer;

struct HXEXPORT IAudioDevice {
    static const int MAX_BANKS      = 32;
    static const int MAX_STREAMS    = 8;
    static const int MAX_SOURCES    = 32;
    static const int MAX_SOUNDS     = 64;
    static const int MAX_BUFFERS_PER_SOURCE = 2;
    static const int MAX_BUFFER_SIZE= 1024;
    static const int MAX_BUFFERS    = MAX_SOURCES * MAX_BUFFERS_PER_SOURCE;

    virtual ~IAudioDevice() {}
    virtual void create(AudioInitializer const&) = 0;
    virtual void play(int, float, float) = 0;
    virtual void stop(int) = 0;
    virtual void pause(int) = 0;
    virtual void update() = 0;
    virtual void resume(int) = 0;
    virtual int  registerStream(std::istream*) = 0;
};

}
}

typedef helix::audio::IAudioDevice      hxAudioDevice;
typedef helix::audio::AudioInitializer  hxAudioInitializer;

HXAPI HXEXPORT hxAudioDevice* new_AudioDevice();

#endif
