#include <helix/audio/IAudioDevice.h>
#include <helix/audio/AudioInitializer.h>
#include <helix/Config.h>
#include <helix/core/DebugLog.h>
#include <iostream>
#include <ck/ck.h>
#include <ck/config.h>
#include <ck/bank.h>
#include <ck/sound.h>
#include <ck/customfile.h>
#include <ck/customstream.h>
#include <ck/mixer.h>
#include <ck/pathtype.h>
#include <cstring>

#if defined(HELIX_PLATFORM_ANDROID)
#include <jni.h>
#include <android/native_activity.h>
#endif

using namespace helix::core;

namespace helix {
namespace audio {
namespace impl {

class CkStlFile : public CkCustomFile {
    std::istream* is;
    typedef std::istream::char_type char_type;
public:
    CkStlFile(std::istream* ins)
    : is(ins)
    {
    }

    virtual ~CkStlFile() {
    }

    virtual bool isValid() const {
        return is != NULL;
    }

    virtual int read(void* buf, int bytes) {
        is->read(static_cast<char_type*>(buf), bytes);
        return static_cast<int>(is->gcount());
    }

    virtual int getSize() const {
        int cur = is->tellg();
        is->seekg(0, is->beg);
        int a = is->tellg();
        is->seekg(0, is->end);
        int b = is->tellg();
        is->seekg(cur, is->beg);
        return b - a;
    }

    virtual void setPos(int pos) {
        is->seekg(pos, is->beg);
    }

    virtual int getPos() const {
        return is->tellg();
    }
};

class CkAudioDevice : public IAudioDevice {
    CkConfig*   mConfig;
    CkSound*    mSoundId[MAX_SOUNDS];
    CkBank*     mAudioBank;
public:
    CkAudioDevice() {

    }
    virtual ~CkAudioDevice() {
    }
    virtual void create(AudioInitializer const& init) {
        DEBUG_METHOD();
        memset(mSoundId, 0, MAX_SOUNDS * sizeof(CkSound*));
#if defined(HELIX_PLATFORM_ANDROID)
        ANativeActivity* activity = (ANativeActivity*)init.userData;
        JavaVM* jvm = activity->vm;
        JNIEnv* env = activity->env;
        jobject obj = activity->clazz;

        mConfig = new CkConfig(jvm, obj);
        mConfig->useJavaAudio = false;
#else //IOS and vanilla Posix
        mConfig = new CkConfig;
#endif
        //mConfig->audioUpdateMs = 16;
        //mConfig->streamBufferMs = 1500.0f;

        if(CkInit(mConfig) == 0) {
            DEBUG_MESSAGE("CkInit() failed");
        }
    }
    virtual void play(int id, float gain, float pitch) {
        mSoundId[id]->setVolume(gain);
        mSoundId[id]->setPitchShift(pitch);
        mSoundId[id]->play();
    }
    virtual void stop(int id) {
        if(id < 0) {
            CkShutdown();
            return;
        }
    }
    virtual void pause(int id) {
        if(id < 0) {
            CkSuspend();
            return;
        }
    }
    virtual void update() {
        CkUpdate();
    }
    virtual void resume(int id) {
        if(id < 0) {
            CkResume();
            return;
        }
    }

    int getNextAvailableStream() {
        for(int i=0; i < MAX_SOUNDS; i++) {
            if(mSoundId[i] == NULL) {
                return i;
            }
        }
        return MAX_SOUNDS;
    }

    virtual int registerStream(std::istream* val) {
        CkSetCustomFileHandler(openFile, static_cast<void*>(val));
        int id = getNextAvailableStream();
        if(id < MAX_SOUNDS) {
            mSoundId[id] = CkSound::newStreamSound(".ogg", kCkPathType_Default);
        }
        return id;
    }

    static CkCustomFile* openFile(const char* fn, void* ins) {
        return new CkStlFile(static_cast<std::istream*>(ins));
    }
};

}
}
}

HXAPI HXEXPORT hxAudioDevice* new_CkAudioDevice() {
	return new helix::audio::impl::CkAudioDevice();
}

HXAPI HXEXPORT hxAudioDevice* new_AudioDevice() {
    return new helix::audio::impl::CkAudioDevice();
}
