/*
 * GuiContainer.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */


#include <helix/engine/gui/GuiContainer.h>
#include <helix/engine/gui/IGuiLayoutManager.h>

using namespace helix::engine;

GuiContainer::GuiContainer()
: mLayout(NULL)
{

}

GuiContainer::~GuiContainer() {

}

//IGuiComponent* GuiContainer::checkMouse() {
//
//	ComponentList::iterator it = mChildren.begin();
//	for(; it != mChildren.end(); it++) {
//		IGuiComponent* ptr = (*it)->checkMouse();
//		if(ptr) {
//			return ptr;
//		}
//	}
//	return GuiComponent::checkMouse();
//}

const ComponentList& GuiContainer::getChildren() const {
	return mChildren;
}

int GuiContainer::add(IGuiComponent* comp, int constraint) {
	comp->setParent(this);
	if(mLayout != NULL) {
		mLayout->addLayoutComponent(comp, constraint);
	}
	mChildren.push_back(comp);
	size_t res = mChildren.size() - 1;
	if(isValid()) {
		invalidate();
	}
	return res;
}

void GuiContainer::remove(IGuiComponent* ) {

}

IGuiComponent* GuiContainer::getChild(int i) {
	return mChildren[(size_t)i];
}

void GuiContainer::update(int dt) {
	GuiWidget::update(dt);
	ComponentList::iterator it = mChildren.begin();
	for(; it != mChildren.end(); it++) {
		(*it)->update(dt);
	}
}

void GuiContainer::draw(hxSpriteBatch* sb) {
	if(isVisible()) {
		GuiComponent::draw(sb);
		ComponentList::iterator it = mChildren.begin();
		for(; it != mChildren.end(); it++) {
			(*it)->draw(sb);
		}
	}
}

void GuiContainer::setLayoutManager(IGuiLayoutManager* layout) {
	mLayout = layout;
	invalidate();
}

void GuiContainer::validate() {
	validateTree();
	GuiWidget::validate();
}

void GuiContainer::validateTree() {
	if(!isValid() ) {
		if((mLayout != NULL) && (mChildren.size() != 0) ) {
			mLayout->layoutContainer(this);

			ComponentList::iterator it = mChildren.begin();
			for(; it != mChildren.end(); it++) {
				if(!(*it)->isValid()) {
					(*it)->validate();
				}
			}
		}
	}
}

const Dimension& GuiContainer::getPreferredSize() {
	// if the valid flag is set to true, and the cache is valid, return
	// the cached value.
	if(isValid() && mPreferredSize.X != -1 && mPreferredSize.Y != -1)
	{
		return mPreferredSize;
	}

	// if we have a layout manager, use it to calculate
	// the preferredSize.
	if(mLayout != 0)
	{
		mPreferredSize = mLayout->getPreferredLayoutSize(this);
	}
	else
	{
	// otherwise, just stick to the Components preferredSize.
		mPreferredSize = GuiComponent::getPreferredSize();
	}
	return mPreferredSize;
}

void GuiContainer::setAlpha(float f) {
	GuiComponent::setAlpha(f);
	ComponentList::iterator it = mChildren.begin();
	for(; it != mChildren.end(); it++) {
		(*it)->setAlpha(f);
	}
}

//void GuiContainer::handleInput() {
//	//GuiComponent::handleInput();
//	for(unsigned int i=0; i < mChildren.size(); i++) {
//		mChildren[i]->handleInput();
//	}
//}

const size_t GuiContainer::getNumberOfChildren() const {
	return mChildren.size();
}

//IGuiComponent* GuiContainer::mouseHit(const Point& mouse, IGuiWidget** res) {
//    if(*res) {
//        return *res;
//    }
//    for(int i=0; i < mChildren.size(); i++) {
//        if(*res) {
//            return *res;
//        }
//        mChildren[i]->mouseHit(mouse, res);
//    }
//    if(!*res) {
//        return GuiComponent::mouseHit(mouse, res);
//    }
//}
