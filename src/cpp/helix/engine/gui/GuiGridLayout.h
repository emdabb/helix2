/*
 * GridLayout.h
 *
 *  Created on: Jul 17, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUIGRIDLAYOUT_H_
#define HELIX_ENGINE_GUI_GUIGRIDLAYOUT_H_


#include <helix/engine/gui/IGuiLayoutManager.h>

namespace helix {
namespace engine {

class GuiGridLayout : public IGuiLayoutManager {
	int mNumRows;
	int mNumCols;
	int mHgap;
	int mVgap;
public:
	GuiGridLayout(int, int, int = DefaultHgap, int = DefaultVgap);
	virtual ~GuiGridLayout();
	virtual void layoutContainer(GuiContainer*);
	virtual void addLayoutComponent(IGuiWidget*, int);
	virtual void removeLayoutComponent(IGuiWidget*);
	virtual const Dimension getPreferredLayoutSize(GuiContainer*);
	virtual void setHgap(int);
	virtual void setVgap(int);
	virtual int  getHgap();
	virtual int  getVgap();
};

}
}



#endif /* HELIX_ENGINE_GUI_GUIGRIDLAYOUT_H_ */
