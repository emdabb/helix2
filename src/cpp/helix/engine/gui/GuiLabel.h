/*
 * GuiLabel.h
 *
 *  Created on: Jul 14, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUILABEL_H_
#define HELIX_ENGINE_GUI_GUILABEL_H_

#include <helix/engine/gui/IGuiWidget.h>
#include <helix/gfx/SpriteFont.h>
#include <string>

namespace helix {
namespace engine {

class GuiLabel : public GuiComponent {
	std::string 	mText;
//	hxSpriteFont* 	mFontRef;
	hxVector2 		mTextSize;
	int				mAlignment;
public:
	GuiLabel(const std::string& text);
	virtual ~GuiLabel();
	void setText(const std::string&);

	virtual void update(int);
	virtual void draw(hxSpriteBatch*);
	//virtual const Dimension& getPreferredSize();
	virtual void validate();
	void setAlignment(const int);
};

}
}

typedef helix::engine::GuiLabel hxGuiLabel;

#endif /* HELIX_ENGINE_GUI_GUILABEL_H_ */
