/*
 * GuiWidget.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#include <helix/engine/gui/IGuiWidget.h>
#include <helix/engine/gui/Gui.h>

using namespace helix::engine;
using namespace helix::gfx;
using namespace helix::core;

GuiWidget::GuiWidget()
: mIsValid(false)
, mIsVisible(true)
, mParent(NULL)
, mBackgroundColor(ColorPreset::White)
, mForegroundColor(ColorPreset::Black)
, mBackgroundImage(NULL)
, mMaxInterpolationTime(1000)
, mCurInterpolationTime(0)
, mSpriteFont(NULL)
, mAlpha(255)
{
	mRectangle      = (hxRectangle) { 0, 0, 0, 0 };
	mSourceRect     = (hxRectangle) { 0, 0, 0, 0 };
	mPreferredSize  = (Dimension)   { -1, -1 };
	mInsets         = (Insets)      { 0, 0, 0, 0 };
}

GuiWidget::~GuiWidget() {

}


const hxRectangle& GuiWidget::getBounds() {
	return mRectangle;
}

void GuiWidget::setSize(int w, int h) {
	mRectangle.W = w >= 0 ? (unsigned int)w : 0;
	mRectangle.H = h >= 0 ? (unsigned int)h : 0;
	invalidate();
}

void GuiWidget::setSize(const Dimension& d) {
	setSize(d.X, d.Y);
}

void GuiWidget::setBounds(int x, int y, int w, int h) {
	mRectangle.X = x;
	mRectangle.Y = y;
	mRectangle.W = (unsigned int)w;
	mRectangle.H = (unsigned int)h;

	invalidate();
}

void GuiWidget::setLocation(int x, int y) {
	mRectangle.X = x;
	mRectangle.Y = y;
}

void GuiWidget::setLocation(const Point& p) {
	setLocation(p.X, p.Y);
}

Point GuiWidget::getLocation() {
	Point p = { mRectangle.X, mRectangle.Y };

	return p;
}

Point GuiWidget::getLocationOnScreen() {
	Point p = getLocation();
	IGuiWidget* parent = getParent();
	while(parent) {
		Point p2 = parent->getLocation();
		p.X += p2.X;
		p.Y += p2.Y;
		parent = parent->getParent();
	}
	return p;
}

void GuiWidget::validate() {

	mIsValid = true;
}

void GuiWidget::invalidate() {
	mIsValid = false;
}

const bool GuiWidget::isValid() const {
	return mIsValid != false;
}

const bool GuiWidget::isVisible() const {
	return mIsVisible != false;
}

const Insets& GuiWidget::getInsets() const {
	return mInsets;
}

void GuiWidget::setInsets(const Insets& ii) {
	setInsets(ii.left, ii.right, ii.top, ii.bottom);
}
void GuiWidget::setInsets(int left, int right, int top, int bottom) {
	mInsets.left 	= left;
	mInsets.right 	= right;
	mInsets.top 	= top;
	mInsets.bottom 	= bottom;
}

const Dimension& GuiWidget::getPreferredSize()  {
	if(mPreferredSize.X != -1 && mPreferredSize.Y != -1 && isValid()) {
		return mPreferredSize;
	}

	mPreferredSize.X = mRectangle.W;
	mPreferredSize.Y = mRectangle.H;

	return mPreferredSize;
}

void GuiWidget::setPreferredSize(const Dimension& d) {
	mPreferredSize.X = d.X;
	mPreferredSize.Y = d.Y;
}

void GuiWidget::setPreferredSize(int w, int h) {
	mPreferredSize.X = w;
	mPreferredSize.Y = h;
}

IGuiComponent* GuiWidget::getParent() {
	return mParent;
}

void GuiWidget::update(int dt) {
	if(!isValid()) {
		validate();
	}
	if(!mBackgroundImage) {
		mBackgroundImage = Gui::getInstance().getDefaultTexture();
	}

//	for(size_t i=0; i < mInterpolators.size(); i++) {
//		mInterpolators[i]->update(dt);
//	}
}

int GuiWidget::getDepth() {
	int n = 0;
	IGuiWidget* ptr = getParent();
	while(ptr) {
		n++;
		ptr = ptr->getParent();
	}
	return n;
}

void GuiWidget::draw(hxSpriteBatch* list) {
	mBackgroundColor.A = mAlpha;
	mForegroundColor.A = mAlpha;

	if(isVisible() && mBackgroundImage) {
		hxRectangle dc = mRectangle;
		Point p = getLocationOnScreen();
		dc.X = p.X;
		dc.Y = p.Y;

//		hxRectangle border = {
//			p.X - 5,
//			p.Y - 5,
//			dc.W + 10,
//			dc.H + 10
//		};
//
//		list->draw(mBackgroundImage, border, &mSourceRect, ColorPreset::Black);
		list->draw(mBackgroundImage, dc, &mSourceRect, mBackgroundColor);
	}
}

void GuiWidget::setParent(IGuiWidget* p) {
	mParent = p;
}

void GuiWidget::setBackgroundImage(hxTexture2D* tex, const hxRectangle* src) {
	mBackgroundImage = tex;
	if(src) {
		mSourceRect = *src;
		//setPreferredSize(src->W, src->H);
	} else {
		mSourceRect.X = 0;
		mSourceRect.Y = 0;
		mSourceRect.W = tex->getWidth();
		mSourceRect.H = tex->getHeight();
	}
	invalidate();
}

void GuiWidget::setBackgroundColor(hxColor const& col) {
	mBackgroundColor = col;
}

void GuiWidget::setForegroundColor(hxColor const& col) {
	mForegroundColor = col;
}

void GuiWidget::setAlpha(float a) {
	mAlpha = (unsigned char)(a * 255.f);

}

void GuiWidget::setFont(hxSpriteFont* f) {
	mSpriteFont = f;
}
hxSpriteFont* GuiWidget::getFont() const {
	return mSpriteFont;
}

const bool GuiWidget::hasFocus() const {
	return Gui::getInstance().getFocus() == this;
}
//
//void GuiWidget::addInterpolator(IInterpolationController* itp) {
//	mInterpolators.push_back(itp);
//}
//
//IGuiWidget* GuiWidget::mouseHit(const Point& mouse, IGuiWidget** res) {
//    Point p = getLocationOnScreen();
//    if(hxRectangle::contains((Rectangle) { p.X, p.Y, mRectangle.W, mRectangle.H }, mouse.X, mouse.Y)) {
//        *res = this;
//        return *res;
//    }
//    return NULL;
//}
//
