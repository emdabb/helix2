/*
 * IGuiWidget.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_IGUIWIDGET_H_
#define HELIX_ENGINE_GUI_IGUIWIDGET_H_

#include <helix/Types.h>
#include <helix/core/EventHandler.h>
#include <helix/core/Int2.h>
#include <helix/core/Rectangle.h>
#include <helix/core/Easing.h>
#include <helix/engine/InterpolationController.h>
#include <helix/gfx/SpriteBatch.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/SpriteFont.h>
#include <vector>

namespace helix {
namespace engine {

typedef hxInt2 Dimension;
typedef hxInt2 Point;

struct Insets {
	int left, top;
	int right, bottom;
};

struct IGuiWidget;

struct GuiComponentEventArgs {
	IGuiWidget* Widget;
};

struct GuiComponentType {
	enum {
		Gui_Unknown = 0,
		Gui_Button,
		Gui_TextInput,
		Gui_Label,
		Gui_Radio,
		Gui_Checkbox,
		Gui_Dropdown,
		Gui_Max
	};
};

struct IGuiWidget;

typedef std::vector<IGuiWidget*> ComponentList;

struct IGuiWidget {
	struct ComponentDepth {
		enum {
			Depth_Normal = 8,
			Depth_NormalModal = 4,
			Depth_Popup = 2,
			Depth_PopupModal = 1
		};
	};
	virtual ~IGuiWidget() {}
	virtual const hxRectangle& getBounds() = 0;
	virtual void setSize(int, int) = 0;
	virtual void setSize(const Dimension&) = 0;
	virtual void setLocation(int, int) = 0;
	virtual void setLocation(const Point&) = 0;
	virtual Point getLocation() = 0;
	virtual Point getLocationOnScreen() = 0;
	virtual void setBounds(int, int, int, int) = 0;
	virtual void validate() = 0;
	virtual void invalidate() = 0;
	virtual const Dimension& getPreferredSize() = 0;
	virtual void setPreferredSize(const Dimension&) = 0;
	virtual void setPreferredSize(int, int) = 0;
	virtual const bool isValid() const = 0;
	virtual const bool isVisible() const = 0;
    virtual const bool hasFocus() const = 0;
	virtual const Insets& getInsets() const = 0;
	virtual void setInsets(const Insets&) = 0;
	virtual void setInsets(int, int, int, int) = 0;
	virtual IGuiWidget* getParent() = 0;
	virtual void update(int) = 0;
	virtual void draw(hxSpriteBatch*) = 0;
	virtual void setParent(IGuiWidget*) = 0;
	virtual void setBackgroundImage(hxTexture2D*, const hxRectangle* = NULL) = 0;
	virtual void setBackgroundColor(hxColor const&) = 0;
	virtual void setForegroundColor(hxColor const&) = 0;
	virtual void setAlpha(float) = 0;
	virtual int getDepth() = 0;
	virtual void setFont(hxSpriteFont*) = 0;
	virtual hxSpriteFont* getFont() const = 0;

	hxEventHandler<GuiComponentEventArgs>::Type OnActivate;
	hxEventHandler<GuiComponentEventArgs>::Type OnDeactivate;
	hxEventHandler<GuiComponentEventArgs>::Type OnClick;
	hxEventHandler<GuiComponentEventArgs>::Type OnFocus;
	hxEventHandler<GuiComponentEventArgs>::Type OnFocusLost;
};

typedef IGuiWidget IGuiComponent;
typedef std::vector<IInterpolationController*> InterpolatorList;

class GuiWidget : public IGuiComponent {
protected:
	bool			mIsValid;
	bool			mIsVisible;
	IGuiWidget* 	mParent;
	hxRectangle		mRectangle;
	Dimension		mPreferredSize;
	Insets			mInsets;
	hxColor			mBackgroundColor;
	hxColor			mForegroundColor;
	hxTexture2D*	mBackgroundImage;
	hxRectangle		mSourceRect;
	int 			mMaxInterpolationTime;
	int 			mCurInterpolationTime;
	hxSpriteFont*	mSpriteFont;
	InterpolatorList mInterpolators;
	unsigned char	mAlpha;
public:
	GuiWidget();
	virtual ~GuiWidget();
	virtual const hxRectangle& getBounds();
	virtual void setSize(int, int);
	virtual void setSize(const Dimension&);
	virtual void setBounds(int, int, int, int);
	virtual void setLocation(int, int);
	virtual void setLocation(const Point&);
	virtual Point getLocation();
	virtual Point getLocationOnScreen();
	virtual void validate();
	virtual void invalidate();
	virtual const Dimension& getPreferredSize();
	virtual void setPreferredSize(const Dimension&);
	virtual void setPreferredSize(int, int);
	virtual const bool isValid() const;
	virtual const bool isVisible() const;
    virtual const bool hasFocus() const;
	virtual const Insets& getInsets() const;
	virtual void setInsets(const Insets&);
	virtual void setInsets(int, int, int, int);
	IGuiComponent* getParent();
	virtual void update(int);
	virtual void draw(hxSpriteBatch*);
	virtual void setParent(IGuiWidget*);
	virtual void setBackgroundImage(hxTexture2D*, const hxRectangle* = NULL);
	virtual void setBackgroundColor(hxColor const&);
	virtual void setForegroundColor(hxColor const&);
	virtual void setAlpha(float);
	virtual int getDepth();
	virtual void setFont(hxSpriteFont*);
	virtual hxSpriteFont* getFont() const;
public:

};

typedef GuiWidget GuiComponent;

}
}

typedef helix::engine::GuiComponentEventArgs 	hxGuiComponentEventArgs;
typedef helix::engine::IGuiWidget				hxGuiComponent;


#endif /* HELIX_ENGINE_GUI_IGUIWIDGET_H_ */
