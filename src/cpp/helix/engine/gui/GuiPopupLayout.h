/*
 * GuiPopupLayout.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUIPOPUPLAYOUT_H_
#define HELIX_ENGINE_GUI_GUIPOPUPLAYOUT_H_

#include <helix/engine/gui/IGuiLayoutManager.h>

namespace helix {
namespace engine {

class PopupLayout : public IGuiLayoutManager {
public:
	PopupLayout();
	virtual ~PopupLayout();
	void addLayoutComponent(IGuiWidget*, int);
	void removeLayoutComponent(IGuiWidget*, int);
	void layoutContainer(GuiContainer*);
	const Dimension getPreferredLayoutSize(GuiContainer*);
};

}
}



#endif /* HELIX_ENGINE_GUI_GUIPOPUPLAYOUT_H_ */
