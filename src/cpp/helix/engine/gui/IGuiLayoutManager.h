/*
 * ILayoutManager.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_IGUILAYOUTMANAGER_H_
#define HELIX_ENGINE_GUI_IGUILAYOUTMANAGER_H_

#include <helix/engine/gui/IGuiWidget.h>

namespace helix {
namespace engine {

class  GuiContainer;

struct IGuiLayoutManager {
	static const int DefaultHgap = 5;
	static const int DefaultVgap = 5;

	virtual ~IGuiLayoutManager() {}
	virtual void layoutContainer(GuiContainer*) = 0;
	virtual void addLayoutComponent(IGuiWidget*, int) = 0;
	virtual void removeLayoutComponent(IGuiWidget*) = 0;
	virtual const Dimension getPreferredLayoutSize(GuiContainer*) = 0;
	virtual void setHgap(int) = 0;
	virtual void setVgap(int) = 0;
	virtual int  getHgap() = 0;
	virtual int  getVgap() = 0;

};

}
}

typedef helix::engine::IGuiLayoutManager hxGuiLayoutManager;


#endif /* HELIX_ENGINE_GUI_IGUILAYOUTMANAGER_H_ */
