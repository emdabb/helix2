/*
 * GuiBoxLayout.h
 *
 *  Created on: Jul 15, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUIBOXLAYOUT_H_
#define HELIX_ENGINE_GUI_GUIBOXLAYOUT_H_

#include <helix/engine/gui/IGuiLayoutManager.h>

namespace helix {
namespace engine {

class GuiBoxLayout : public IGuiLayoutManager {
	enum {
		Align_XAxis,
		Align_YAxis,
		Align_LineAxis,
		Align_PageAxis,
		Align_Max
	};
	int mHgap;
	int mVgap;
	const int mAlignment;
protected:
	void layoutRow(GuiContainer*);
	void layoutColumn(GuiContainer*);
public:
	GuiBoxLayout(int = DefaultHgap, int = DefaultVgap, const int = Align_XAxis);
	virtual ~GuiBoxLayout();
	virtual void layoutContainer(GuiContainer*);
	virtual void addLayoutComponent(IGuiWidget*, int);
	virtual void removeLayoutComponent(IGuiWidget*);
	virtual const Dimension getPreferredLayoutSize(GuiContainer*);
	virtual void setHgap(int);
	virtual void setVgap(int);
	virtual int  getHgap();
	virtual int  getVgap();
};

}
}



#endif /* HELIX_ENGINE_GUI_GUIBOXLAYOUT_H_ */
