/*
 * GuiPopupLayout.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#include <helix/engine/gui/GuiPopupLayout.h>
#include <helix/engine/gui/GuiContainer.h>

using namespace helix::engine;

PopupLayout::PopupLayout() {
}
PopupLayout::~PopupLayout() {
}
void PopupLayout::addLayoutComponent(IGuiWidget*, int) {
}
void PopupLayout::removeLayoutComponent(IGuiWidget*, int) {
}
const Dimension PopupLayout::getPreferredLayoutSize(GuiContainer* parent) {
	return parent->getPreferredSize();
}
void PopupLayout::layoutContainer(GuiContainer* parent) {

	ComponentList children = parent->getChildren();
	ComponentList::iterator iter;
	for(iter = children.begin(); iter != children.end(); ++iter)
	{
		(*iter)->setSize((*iter)->getPreferredSize());
		(*iter)->setLocation(0,parent->getBounds().Y + parent->getBounds().H);
	}
}
