/*
 * GridLayout.cpp
 *
 *  Created on: Jul 17, 2015
 *      Author: miel
 */

#include <helix/engine/gui/GuiGridLayout.h>
#include <helix/engine/gui/GuiContainer.h>

using namespace helix::engine;

GuiGridLayout::GuiGridLayout(int nrows, int ncols, int gaph, int gapv)
: mNumRows(nrows)
, mNumCols(ncols)
, mHgap(gaph)
, mVgap(gapv)
{
}

GuiGridLayout::~GuiGridLayout() {
}

void GuiGridLayout::layoutContainer(GuiContainer* parent) {
	Insets insets = parent->getInsets();
	int numComponents = parent->getNumberOfChildren();

	int numRows = mNumRows;
	int numCols = mNumCols;

	if (numRows > 0) {
		numCols = (numComponents + numRows - 1) / numRows;
	} else {
		numRows = (numComponents + numCols - 1) / numCols;
	}

	int width = parent->getBounds().W - (insets.left + insets.right);
	int height = parent->getBounds().H - (insets.top + insets.bottom);
	width = (width - (numCols - 1) * getHgap()) / numCols;
	height = (height - (numRows - 1) * getVgap()) / numRows;

	ComponentList childList = parent->getChildren();
	ComponentList::const_iterator iter;

	int x = insets.left;
	int y = insets.top;

	int i = 1;

	// there is probably a more efficient and correct way to do this,
	// but after a wild night out, I'm happy that it works at all.
	for (iter = childList.begin(); iter != childList.end(); ++iter) {
		(*iter)->setBounds(x, y, width, height);

		if (numCols > numRows) {
			if (i >= numCols) {
				y += height + getVgap();
				x = insets.left;
				i = 0;
			} else {
				x += width + getHgap();
			}
		} else {
			if (i >= numRows) {
				x += width + getHgap();
				y = insets.top;
				i = 0;
			} else {
				y += height + getVgap();
			}
		}
		i++;
	}
}

void GuiGridLayout::addLayoutComponent(IGuiWidget*, int) {
}

void GuiGridLayout::removeLayoutComponent(IGuiWidget*) {
}

const Dimension GuiGridLayout::getPreferredLayoutSize(GuiContainer* parent) {
	Insets insets = parent->getInsets();
	int numComponents = parent->getNumberOfChildren();

	int numRows = mNumRows;
	int numCols = mNumCols;

	if (numRows > 0) {
		numCols = (numComponents + numRows - 1) / numRows;
	} else {
		numRows = (numComponents + numCols - 1) / numCols;
	}
	ComponentList childList = parent->getChildren();

	int width = 0;
	int height = 0;

	ComponentList::const_iterator iter;
	for (iter = childList.begin(); iter != childList.end(); ++iter) {
		width = std::max((*iter)->getPreferredSize().X, width);
		height = std::max((*iter)->getPreferredSize().Y, height);
	}
	Dimension res;
	res.X = insets.left + insets.right + numCols * width
			+ (numCols - 1) * getHgap();
	res.Y = insets.top + insets.bottom + numRows * height
			+ (numRows - 1) * getVgap();

	return res;
}

void GuiGridLayout::setHgap(int val) {
	mHgap = val;
}

void GuiGridLayout::setVgap(int val) {
	mVgap = val;
}

int GuiGridLayout::getHgap() {
	return mHgap;
}

int GuiGridLayout::getVgap() {
	return mVgap;
}
