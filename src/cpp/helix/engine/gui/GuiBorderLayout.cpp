/*
 * GuiBorderLayout.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#include <helix/engine/gui/GuiBorderLayout.h>
#include <helix/engine/gui/GuiContainer.h>
#include <algorithm>

using namespace helix::engine;

GuiBorderLayout::GuiBorderLayout(bool fill, int hgap, int vgap) :
		north(0), south(0), east(0), west(0), center(0), fillCorners(fill) {
	setHgap(hgap);
	setVgap(vgap);
}

GuiBorderLayout::~GuiBorderLayout() {

}

void GuiBorderLayout::layoutContainer(GuiContainer* parent) {
	int top = parent->getInsets().top;
	int bottom = parent->getBounds().H - parent->getInsets().bottom;
	int left = parent->getInsets().left;
	int right = parent->getBounds().W - parent->getInsets().right;

	int hgap = getHgap() * 2;
	int vgap = getVgap() * 2;

	if (north != 0 && north->isVisible()) {
		north->setSize(right - left, north->getBounds().H);
		north->setBounds(left, top, right - left, north->getPreferredSize().Y);
		top += north->getPreferredSize().Y + vgap;
	}

	if (south != 0 && south->isVisible()) {
		south->setSize(right - left, south->getBounds().H);
		south->setBounds(left, bottom - south->getPreferredSize().Y, right - left, south->getPreferredSize().Y);
		bottom -= south->getPreferredSize().Y + vgap;
	}

	if (east != 0 && east->isVisible()) {
		east->setSize(east->getBounds().W, bottom - top);
		east->setBounds(right - east->getPreferredSize().X, top, east->getPreferredSize().X, bottom - top);
		right -= east->getPreferredSize().X + hgap;
	}

	if (west != 0 && west->isVisible()) {
		west->setSize(west->getBounds().W, bottom - top);
		west->setBounds(left, top, west->getPreferredSize().X,
				bottom - top);
		left += west->getPreferredSize().X + hgap;
	}

	if (center != 0 && center->isVisible()) {
		center->setBounds(left, top, right - left, bottom - top);
	}

	if (!fillCorners) {
		// resize north and south
		if (west != 0 && west->isVisible()) {
			if (north != 0 && north->isVisible()) {
				north->setBounds(north->getBounds().X + west->getBounds().W,
						north->getBounds().Y,
						north->getBounds().W - west->getBounds().W,
						north->getBounds().H);
			}

			if (south != 0 && south->isVisible()) {
				south->setBounds(south->getBounds().X + west->getBounds().W,
						south->getBounds().Y,
						south->getBounds().W - west->getBounds().W,
						south->getBounds().H);
			}
		}

		if (east != 0 && east->isVisible()) {
			if (north != 0 && north->isVisible()) {
				north->setBounds(north->getBounds().X, north->getBounds().Y,
						north->getBounds().W - east->getBounds().W,
						north->getBounds().H);
			}

			if (south != 0 && south->isVisible()) {
				south->setBounds(south->getBounds().X, south->getBounds().Y,
						south->getBounds().W - east->getBounds().W,
						south->getBounds().H);
			}
		}
	}
}

const Dimension GuiBorderLayout::getPreferredLayoutSize(GuiContainer* parent) {
	Dimension dim = (Dimension){ 0, 0 };

	int hgap = getHgap() * 2;
	int vgap = getVgap() * 2;

	if (east != 0 && east->isVisible()) {
		dim.X += east->getPreferredSize().X + hgap;
		dim.Y = std::max(east->getPreferredSize().Y, dim.Y);

		if (!fillCorners) {
			dim.X += east->getPreferredSize().X;
		}
	}
	if (west != 0 && west->isVisible()) {
		dim.X += west->getPreferredSize().X + hgap;
		dim.Y = std::max(west->getPreferredSize().Y, dim.Y);

		if (!fillCorners) {
			dim.X += west->getPreferredSize().X;
		}
	}
	if (center != 0 && center->isVisible()) {
		dim.X += center->getPreferredSize().X;
		dim.Y = std::max(center->getPreferredSize().Y, dim.Y);
	}
	if (north != 0 && north->isVisible()) {
		dim.X = std::max(north->getPreferredSize().X, dim.X);
		dim.Y += north->getPreferredSize().Y + vgap;
	}
	if (south != 0 && south->isVisible()) {
		dim.X = std::max(south->getPreferredSize().X, dim.X);
		dim.Y += south->getPreferredSize().Y + vgap;
	}

	dim.X += parent->getInsets().left + parent->getInsets().right;
	dim.Y += parent->getInsets().top + parent->getInsets().bottom;

	return dim;
}




void GuiBorderLayout::setHgap(int gap) {
	hgap = gap;
}
void GuiBorderLayout::setVgap(int gap) {
	vgap = gap;
}



void GuiBorderLayout::removeLayoutComponent(IGuiComponent* comp) {
	if (north == comp)
		north = 0;
	else if (south == comp)
		south = 0;
	else if (east == comp)
		east = 0;
	else if (west == comp)
		west = 0;
	else if (center == comp)
		center = 0;
}

void GuiBorderLayout::addLayoutComponent(IGuiComponent* comp, int constraint) {
	if (constraint == -1)
		return;

	switch (constraint) {
	case Layout_North: {
		north = comp;
		break;
	}
	case Layout_South: {
		south = comp;
		break;
	}
	case Layout_East: {
		east = comp;
		break;
	}
	case Layout_West: {
		west = comp;
		break;
	}
	case Layout_Center: {
		center = comp;
		break;
	}
	}
}

//GuiBorderLayout::GuiBorderLayout(const GuiBorderLayout& rhs) :
//		north(rhs.north), south(rhs.south), east(rhs.east), west(rhs.west), center(
//				rhs.center), fillCorners(rhs.fillCorners) {
//}
//
//void GuiBorderLayout::swap(GuiBorderLayout& rhs) throw () {
//	std::swap(north, rhs.north);
//	std::swap(south, rhs.south);
//	std::swap(east, rhs.east);
//	std::swap(west, rhs.west);
//	std::swap(center, rhs.center);
//	std::swap(fillCorners, rhs.fillCorners);
//}
//
//GuiBorderLayout& GuiBorderLayout::operator =(const GuiBorderLayout& rhs) {
//	GuiBorderLayout temp(rhs);
//	swap(temp);
//	return *this;
//}
