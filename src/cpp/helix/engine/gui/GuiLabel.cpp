/*
 * GuiLabel.cpp
 *
 *  Created on: Jul 14, 2015
 *      Author: miel
 */

#include <helix/engine/gui/GuiLabel.h>

using namespace helix::engine;
using namespace helix::gfx;

GuiLabel::GuiLabel(const std::string& text) {
	setText(text);
	setInsets(5,5,5,5);
	setBackgroundColor(ColorPreset::Transparent);
}

GuiLabel::~GuiLabel() {

}

//const Dimension& GuiLabel::getPreferredSize() {
//	if(mSpriteFont) {
//		if(!isValid()) {
//			validate();
//		}
//		mPreferredSize.X = std::max(mPreferredSize.X, (int)(mTextSize.X + 0.5f));
//		mPreferredSize.Y = std::max(mPreferredSize.Y, (int)(mTextSize.Y + 0.5f));
//	}
//	return mPreferredSize;
//}

void GuiLabel::setText(const std::string& text) {
	mText = text;
	invalidate();
}


void GuiLabel::validate() {
	if(mSpriteFont) {
		mTextSize = mSpriteFont->measureString(mText.c_str());
	}
	GuiComponent::validate();
}

void GuiLabel::update(int dt) {
	GuiComponent::update(dt);
}

void GuiLabel::draw(hxSpriteBatch* b) {
	if(isVisible()) {
		if(mSpriteFont) {
			Point textLocation;
			textLocation.X = mRectangle.W / 2 - mTextSize.X / 2;
			textLocation.Y = mRectangle.H / 2 + mTextSize.Y / 2;
			hxVector2 Vp;
			Point p = getLocationOnScreen();
			Vp.X = p.X + mRectangle.W / 2 - mTextSize.X / 2;// + textLocation.X;
			Vp.Y = p.Y + mRectangle.H / 2 - mTextSize.Y / 2 + mSpriteFont->getLineHeight();// + (float)mRectangle.H * 0.5f + mTextSize.Y * 0.5f;// + textLocation.Y;// mTextSize.Y;
			GuiComponent::draw(b);
			b->drawString(mSpriteFont, mText.c_str(), Vp, mForegroundColor);
		}
	}
}

