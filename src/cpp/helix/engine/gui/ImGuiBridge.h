/*
 * ImGuiBridge.h
 *
 *  Created on: Jul 14, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_IMGUIBRIDGE_H_
#define HELIX_ENGINE_GUI_IMGUIBRIDGE_H_

#include <imgui/imgui.h>
#include <helix/base/IApplication.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/Viewport.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/gfx/VertexBuffer.h>
#include <helix/gfx/VertexElement.h>
#include <helix/gfx/VertexStream.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/ShaderParameter.h>
#include <helix/gfx/PrimitiveType.h>
#include <helix/gfx/Sampler2D.h>
#include <helix/gfx/RenderState.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/core/Matrix.h>

namespace helix {
namespace engine {

struct ImGuiBridge {
	static hxTexture2D* 							texture;
	static hxVertexBuffer* 						vertices;
	static const hxVertexDeclaration* 		ImDrawVert_Declaration;
	static const hxVertexElement 			ImDrawVert_Elements[];
	static hxShaderProgram* 						Shader;
	static hxShaderParameter<hxMatrix>::Type* 		WVPParameter;
	static hxShaderParameter<hxSampler2D>::Type* 	BaseSamplerParameter;
	static hxGraphicsDevice*						mGraphicsDevice;

	static bool createDeviceObjects();
	static void releaseDeviceObjects();
	static void create(hxGraphicsDevice*, hxShaderProgram*);
	static void release();
	static void newFrame(int, int);
	static void renderDrawLists(ImDrawList** const, int);
};

}
}



#endif /* HELIX_ENGINE_GUI_IMGUIBRIDGE_H_ */
