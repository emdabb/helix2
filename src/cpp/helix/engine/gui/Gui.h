/*
 * Gui.h
 *
 *  Created on: Jul 15, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUI_H_
#define HELIX_ENGINE_GUI_GUI_H_

#include <helix/core/Singleton.h>
#include <helix/core/EventHandler.h>
#include <helix/base/IApplication.h>
#include <helix/engine/gui/GuiContainer.h>

namespace helix {
namespace engine {

class Gui : public GuiContainer, public hxSingleton<Gui>::Type {
	bool 	mMousePressed[5];
	Point 	mMouseLocation[5];
	Point 	mMousePressLocation[5];
	Point 	mMouseReleaseLocation[5];
	bool	mKeyDown[256];
	int		mKeyMap[16];
	bool	mKeyMod[16];
	bool	mIsDragging[5];
	IGuiWidget* mFocusItem;
	IGuiWidget* mPressedItem;
	hxTexture2D* mDefaultTexture;
public:
	Gui();
	virtual ~Gui();
	/**
	 * Helper function to translate the raw input from the application
	 * into something more meaningful.
	 */
	void onMousePressed(const hxMouseEventArgs&);
	void onMouseReleased(const hxMouseEventArgs&);
	void onMouseMoved(const hxMouseEventArgs&);
	void onKeyPressed(const hxKeyEventArgs&);
	void onKeyReleased(const hxKeyEventArgs&);

	bool isMouseDown(int) const;
	const Point& getMousePosition(int) const;
	void setFocus(IGuiComponent*);
	void setPressed(IGuiComponent*);

	IGuiComponent* getFocus() const;
	IGuiComponent* getPressed() const;

	void update(int);
	void draw(hxSpriteBatch*);
	void setDefaultTexture(hxTexture2D*);
	hxTexture2D* getDefaultTexture() const;
};

}
}



#endif /* HELIX_ENGINE_GUI_GUI_H_ */
