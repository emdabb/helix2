/*
 * GuiCompoundWidget.h
 *
 *  Created on: Jul 14, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUICOMPOUNDCOMPONENT_H_
#define HELIX_ENGINE_GUI_GUICOMPOUNDCOMPONENT_H_

#include <helix/engine/gui/GuiContainer.h>

namespace helix {
namespace engine {

class GuiCompoundComponent : public GuiContainer {
private:
	int  add(GuiComponent*, int = -1) { return -1; }
	void remove(GuiComponent*) { }
public:
	virtual ~GuiCompoundComponent() {}
};

}
}



#endif /* HELIX_ENGINE_GUI_GUICOMPOUNDCOMPONENT_H_ */
