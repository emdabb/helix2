/*
 * GuiBorderLayout.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUIBORDERLAYOUT_H_
#define HELIX_ENGINE_GUI_GUIBORDERLAYOUT_H_

#include <helix/engine/gui/IGuiLayoutManager.h>

namespace helix {
namespace engine {

class GuiBorderLayout : public IGuiLayoutManager {
	IGuiComponent* north;
	IGuiComponent* south;
	IGuiComponent* east;
	IGuiComponent* west;
	IGuiComponent* center;
	bool fillCorners;
	int hgap, vgap;
public:
	enum {
		Layout_North,
		Layout_East,
		Layout_South,
		Layout_West,
		Layout_Center
	};
public:
	GuiBorderLayout(bool fill = true, int hgap = DefaultHgap, int vgap = DefaultVgap);
	virtual ~GuiBorderLayout();
	void addLayoutComponent(IGuiComponent*, int);
	void removeLayoutComponent(IGuiComponent*);
	void layoutContainer(GuiContainer* parent);
	const Dimension getPreferredLayoutSize(GuiContainer*);

	void setHgap(int);
	void setVgap(int);

	int getHgap() { return hgap; }
	int getVgap() { return vgap; }

	//GuiBorderLayout(const GuiBorderLayout& rhs);
	//GuiBorderLayout& operator =(const GuiBorderLayout& rhs);
	//void swap(GuiBorderLayout&) throw ();

};

}
}



#endif /* HELIX_ENGINE_GUI_GUIBORDERLAYOUT_H_ */
