/*
 * GuiFlowLayout.cpp
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#include <helix/engine/gui/GuiFlowLayout.h>

using namespace helix::engine;

GuiFlowLayout::GuiFlowLayout(int hgap, int vgap, const int alignment)
: mHgap(hgap)
, mVgap(vgap)
, mAlignment(Alignment_Center)
{

}
GuiFlowLayout::~GuiFlowLayout() {

}

void GuiFlowLayout::setHgap(int) {

}

void GuiFlowLayout::setVgap(int) {

}

int  GuiFlowLayout::getHgap() {
	return mHgap;
}

int  GuiFlowLayout::getVgap() {
	return mVgap;
}

void GuiFlowLayout::addLayoutComponent(IGuiComponent*, int) {

}

void GuiFlowLayout::removeLayoutComponent(IGuiComponent*) {

}

const Dimension GuiFlowLayout::getPreferredLayoutSize(GuiContainer* parent) {
	Dimension preferredSize = (Dimension) {0, 0};

	ComponentList::iterator iter;
	ComponentList list = parent->getChildren();

	bool firstComponent = true;

	for(iter = list.begin(); iter != list.end(); ++iter)
	{
		if((*iter)->isVisible())
		{
			preferredSize.X  = std::max(preferredSize.X,(*iter)->getPreferredSize().X);
			preferredSize.Y += (*iter)->getPreferredSize().Y;

			if(firstComponent) {
				firstComponent = false;
			} else {
				preferredSize.X += getHgap();
			}
		}
	}

	Insets parentInsets = parent->getInsets();

	preferredSize.X += parentInsets.left + parentInsets.right + getHgap() + getHgap();
	preferredSize.Y += parentInsets.top + parentInsets.bottom + getVgap() + getVgap();

	return preferredSize;
}

void GuiFlowLayout::layoutContainer(GuiContainer* parent) {
	Dimension rowDimension =  { 0, 0 };
	Insets parentInsets = parent->getInsets();
	Dimension parentDimension= (Dimension) {
			(int)parent->getBounds().W - parentInsets.left - parentInsets.right,
			(int)parent->getBounds().H - parentInsets.top - parentInsets.bottom
	};

	int y = parentInsets.top;

	ComponentList::const_iterator iter;
	ComponentList list = parent->getChildren();

	ComponentList row;

	for(iter = list.begin(); iter != list.end(); ++iter) {
		if((*iter)->isVisible()) {
			(*iter)->setSize((*iter)->getPreferredSize());
			row.push_back((*iter));

			rowDimension.Y = std::max(rowDimension.Y,(*iter)->getPreferredSize().Y + getVgap());
			rowDimension.X += (*iter)->getPreferredSize().X + getHgap();

			if(rowDimension.X > (int)(parent->getBounds().W + (parentInsets.left + parentInsets.right))) {
				align(row,y,parentInsets,rowDimension,parentDimension);
				row.clear();

				y += rowDimension.Y;
				rowDimension.X = 0;
				rowDimension.Y = 0;
			}
		}
	}
	align(row,y,parentInsets,rowDimension, parentDimension);
}

void GuiFlowLayout::align(ComponentList &row, int rowHeight, Insets &parentInsets, Dimension &rowDimension, Dimension &parentDimension)
{
	Point location = {
		parentInsets.left,
		0
	};

	switch(mAlignment)	{
		case Alignment_Center: {
			location.X = (getHgap() + parentDimension.X - rowDimension.X) / 2;
			break;
		}
		case Alignment_Left: {
			location.X = getHgap();
			break;
		}
		case Alignment_Right: {
			location.X = (parentDimension.X - rowDimension.X);
			break;
		}
	}

	ComponentList::const_iterator rowIterator;

	for(rowIterator = row.begin(); rowIterator != row.end(); ++rowIterator)
	{
		hxRectangle rc = (*rowIterator)->getBounds();
		location.Y = rowHeight + ((parentDimension.Y / 2) - (rc.H / 2));
		(*rowIterator)->setLocation(location);

		location.X += rc.W + getHgap();
	}
}
