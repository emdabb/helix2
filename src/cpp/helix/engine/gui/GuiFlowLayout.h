/*
 * GuiFlowLayout.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUIFLOWLAYOUT_H_
#define HELIX_ENGINE_GUI_GUIFLOWLAYOUT_H_

#include <helix/engine/gui/IGuiLayoutManager.h>
#include <helix/engine/gui/GuiContainer.h>

namespace helix {
namespace engine {

class GuiFlowLayout : public IGuiLayoutManager {
	int mHgap;
	int mVgap;
	int mAlignment;
public:
	enum {
		Alignment_Center,
		Alignment_Left,
		Alignment_Right
	};


public:
	GuiFlowLayout(int = DefaultHgap, int = DefaultVgap, const int = Alignment_Center);
	virtual ~GuiFlowLayout();
	void addLayoutComponent(IGuiComponent*, int);
	void removeLayoutComponent(IGuiComponent*);
	void layoutContainer(GuiContainer*);
	const Dimension getPreferredLayoutSize(GuiContainer*);
	virtual void setHgap(int);
	virtual void setVgap(int);
	virtual int  getHgap();
	virtual int  getVgap();
protected:
	void align(ComponentList &row, int rowHeight, Insets &parentInsets, Dimension &rowDimension, Dimension &parentDimension);
};

}
}


#endif /* HELIX_ENGINE_GUI_GUIFLOWLAYOUT_H_ */
