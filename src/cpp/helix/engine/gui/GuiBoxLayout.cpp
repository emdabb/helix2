/*
 * GuiBoxLayout.cpp
 *
 *  Created on: Jul 15, 2015
 *      Author: miel
 */

#include <helix/engine/gui/GuiBoxLayout.h>
#include <helix/engine/gui/GuiContainer.h>

using namespace helix::engine;

GuiBoxLayout::GuiBoxLayout(int hgap, int vgap, const int align)
: mHgap(hgap)
, mVgap(vgap)
, mAlignment(align)
{
}

GuiBoxLayout::~GuiBoxLayout() {
}

void GuiBoxLayout::layoutRow(GuiContainer*) {

}

void GuiBoxLayout::layoutColumn(GuiContainer*) {

}

void GuiBoxLayout::layoutContainer(GuiContainer* obj) {
}

void GuiBoxLayout::addLayoutComponent(IGuiWidget*, int) {
}

void GuiBoxLayout::removeLayoutComponent(IGuiWidget*) {
}

const Dimension GuiBoxLayout::getPreferredLayoutSize(GuiContainer* obj) {
	return obj->getPreferredSize();
}

void GuiBoxLayout::setHgap(int n) {
	mHgap = n;
}

void GuiBoxLayout::setVgap(int n) {
	mVgap = n;
}

int GuiBoxLayout::getHgap() {
	return mHgap;
}

int GuiBoxLayout::getVgap() {
	return mVgap;
}
