/*
 * Gui.cpp
 *
 *  Created on: Jul 16, 2015
 *      Author: miel
 */

#include <helix/engine/gui/Gui.h>

using namespace helix::engine;

Gui::Gui()
: mFocusItem(NULL)
, mPressedItem(NULL)
, mDefaultTexture(NULL)
{

}

Gui::~Gui() {

}


void Gui::update(int dt) {
	if(mFocusItem) {
		mFocusItem->OnFocusLost((GuiComponentEventArgs) { mFocusItem });
	}

	mFocusItem = NULL;
    mPressedItem = NULL;



	ComponentList::iterator it = mChildren.begin();
	for(; it != mChildren.end(); it++) {
		(*it)->update(dt);
		//(*it)->handleInput();
	}
}

void Gui::draw(hxSpriteBatch* b){
	ComponentList::iterator it = mChildren.begin();
	for(; it != mChildren.end(); it++) {
		(*it)->draw(b);
	}
}

void Gui::setDefaultTexture(hxTexture2D* tex) {
	mDefaultTexture = tex;
	for(size_t i=0; i < mChildren.size(); i++) {
		mChildren[i]->invalidate();
	}
}

hxTexture2D* Gui::getDefaultTexture() const {
	return mDefaultTexture;
}

bool Gui::isMouseDown(int i) const {
	return mMousePressed[i] != false;
}


const Point& Gui::getMousePosition(int i) const {
	return mMouseLocation[i];
}

void Gui::setFocus(IGuiComponent* a) {

//	if(!mFocusItem) {
//		GuiComponentEventArgs args;
//		args.Widget = a;
//		a->OnFocus(args);
//	} else {
//		if(mFocusItem != a) {
//			mFocusItem->OnFocusLost((GuiComponentEventArgs) { mFocusItem });
//			mFocusItem->OnFocus((GuiComponentEventArgs) { a });
//		}
//	}
//
	mFocusItem = a;



//	GuiComponentEventArgs args;
//
//	if(!mFocusItem) {
//		args.Widget = a;
//		mFocusItem 	= a;
//		if(a) {
//			mFocusItem->OnFocus(args);
//		}
//		return;
//	} else {
//
//		if(mFocusItem != a) {
//			args.Widget = mFocusItem;
//			mFocusItem->OnFocusLost(args);
//			mFocusItem 	= a;
//			args.Widget = a;
//			mFocusItem->OnFocus(args);
//		}
//	}
}
void Gui::setPressed(IGuiComponent* a) {
	mPressedItem = a;
}

IGuiComponent* Gui::getPressed() const {
	return mPressedItem;
}

IGuiComponent* Gui::getFocus() const {
	return mFocusItem;
}

void Gui::onMousePressed(const hxMouseEventArgs& args) {
	mMousePressed[args.PointerId] = true;
	mMousePressLocation[args.PointerId].X = args.X;
	mMousePressLocation[args.PointerId].Y = args.Y;
}

void Gui::onMouseReleased(const hxMouseEventArgs& args) {
	mMousePressed[args.PointerId] = false;
	mIsDragging[args.PointerId] = false;
	mMouseReleaseLocation[args.PointerId].X = args.X;
	mMouseReleaseLocation[args.PointerId].Y = args.Y;
}

void Gui::onMouseMoved(const hxMouseEventArgs& args) {
	if(mMousePressed[args.PointerId] != false) {
		mIsDragging[args.PointerId] = true;
	}
	mMouseLocation[args.PointerId].X = args.X;
	mMouseLocation[args.PointerId].Y = args.Y;
}

void Gui::onKeyPressed(const hxKeyEventArgs& args) {
	int key = args.KeyId;
	if(args.KeyId >= 2048) {
		// special key!
		this->mKeyMod[key - 2048] = true;
		return;
	}
	mKeyDown[key] = true;
}

void Gui::onKeyReleased(const hxKeyEventArgs& args) {
	int key = args.KeyId;
	if(args.KeyId >= 2048) {
		// special key!
		this->mKeyMod[key - 2048] = true;
		return;
	}
	mKeyDown[key] = false;
}
