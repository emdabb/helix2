/*
 * GuiPanel.h
 *
 *  Created on: Jul 16, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUIPANEL_H_
#define HELIX_ENGINE_GUI_GUIPANEL_H_

#include <helix/engine/gui/GuiContainer.h>
#include <helix/engine/gui/GuiFlowLayout.h>

namespace helix {
namespace engine {

class GuiPanel : public GuiContainer {
	GuiFlowLayout mLayout;
public:
	GuiPanel();
	virtual ~GuiPanel();
};

}
}


#endif /* HELIX_ENGINE_GUI_GUIPANEL_H_ */
