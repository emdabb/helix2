/*
 * GuiContainer.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GUI_GUICONTAINER_H_
#define HELIX_ENGINE_GUI_GUICONTAINER_H_

#include <helix/engine/gui/IGuiWidget.h>

namespace helix {
namespace engine {

struct IGuiLayoutManager;

class GuiContainer : public GuiWidget {
protected:
	ComponentList mChildren;
	ComponentList mWidgetsToDraw;
	IGuiLayoutManager* mLayout;
public:
	typedef void(*GuiDrawCallback)(GuiComponent**, size_t);
	GuiDrawCallback Callback;
public:
	GuiContainer();
	virtual ~GuiContainer();
	virtual void 	 validate();
	virtual void 	 update(int);
	virtual void 	 draw(hxSpriteBatch*);
	const Dimension& getPreferredSize();
	void 			 validateTree();
	void 			 setLayoutManager(IGuiLayoutManager*);
	IGuiComponent*   getChild(int);
	virtual int  	 add(IGuiComponent* , int = -1);
	virtual void 	 remove(IGuiComponent*);
	virtual void 	 setAlpha(float);
	//virtual void 	 handleInput();
	const size_t 	 getNumberOfChildren() const;
	const ComponentList& getChildren() const;

	template <typename T>
	T* getChildAs(int i) {
		return reinterpret_cast<T*>(getChild(i));
	}

    //IGuiWidget* mouseHit(const Point&, IGuiWidget** );
};


}
}


typedef helix::engine::GuiContainer hxGuiContainer;


#endif /* HELIX_ENGINE_GUI_GUICONTAINER_H_ */
