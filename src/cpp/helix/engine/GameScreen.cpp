 /**
  * @file GameScreen.cpp
  * @author miel <miel@mimesis-games.com>
  * @version 1.0
  * @date May 21, 2015
  *
  * @section LICENSE
  *
  * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
  * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
  *
  * Mimesis Games 2014, All Rights Reserved
  *
  * @section DESCRIPTION
  */

#include <helix/engine/GameScreen.h>
#include <helix/engine/GameServices.h>
#include <helix/engine/GameArt.h>
#include <helix/base/IApplication.h>
#include <helix/gfx/Viewport.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/gfx/VertexBuffer.h>
#include <helix/gfx/VertexElement.h>
#include <helix/gfx/VertexStream.h>
#include <helix/gfx/VertexDeclaration.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/ShaderParameter.h>
#include <helix/gfx/PrimitiveType.h>
#include <helix/core/Matrix.h>

#include <algorithm>
#include <string>

using namespace helix::gfx;
using namespace helix::base;
using namespace helix::core;
using namespace helix::engine;

//#define MAX_VERTICES	2048
/**
 *
 * @brief GameScreen class implementation
 *
 */

GameScreenManager::GameScreenManager() {

	//ImGui_ImplHx::init();

}
GameScreenManager::~GameScreenManager() {

}
void GameScreenManager::loadContent() {
    for (size_t i = 0; i < mScreens.size(); i++) {
        mScreens[i]->loadContent();
    }
}
void GameScreenManager::unloadContent() {
    for (size_t i = 0; i < mScreens.size(); i++) {
        mScreens[i]->unloadContent();
    }
}
bool GameScreenManager::update(int dt) {
    if(mScreens.size() == 0) {
        return false;
    }
    /**
     * Make a copy of the master screen list, to avoid confusion if
     * the process of updating one screen adds or removes others
     * (or it happens on another thread)
     */
    mScreensToUpdate.clear();

    for (size_t i = 0; i < mScreens.size(); i++) {
        mScreensToUpdate.push_back(mScreens[i]);
    }

    bool otherScreenHasFocus = false; //!Game.IsActive;
    bool coveredByOtherScreen = false;

    // Loop as long as there are screens waiting to be updated.
    while (mScreensToUpdate.size() > 0) {
        // Pop the topmost screen off the waiting list.
        GameScreen* screen = mScreensToUpdate.back();
        mScreensToUpdate.pop_back();
        // Update the screen.
        screen->update(dt, otherScreenHasFocus, coveredByOtherScreen);

        const int state = screen->getState();

        if (state == ScreenState::TransitionOn || state == ScreenState::Active
        || state == ScreenState::TransitionOff
        ) {
            // If this is the first active screen we came across,
            // give it a chance to handle input and update presence.
            if (!otherScreenHasFocus) {
            	screen->handleInput();
                // screen.HandleInput(input);
                // screen.UpdatePresence(); // presence support

                otherScreenHasFocus = true;
            }
            // If this is an active non-popup, inform any subsequent
            // screens that they are covered by it.
            if (!screen->isPopup()) {
                coveredByOtherScreen = true;
            }
        }
    }

    return true;
}
void GameScreenManager::draw(int dt) {
    /**
     *
     * Make a copy of the master screen list, to avoid confusion if
     * the process of drawing one screen adds or removes others
     * (or it happens on another thread
     *
     */
    mScreensToDraw.clear();

    for (size_t i = 0; i < mScreens.size(); i++) {
        mScreensToDraw.push_back(mScreens[i]);
    }

    for (size_t i = 0; i < mScreensToDraw.size(); i++) {
        if (mScreensToDraw[i]->getState() == ScreenState::Hidden) {
            continue;
        }
        mScreensToDraw[i]->draw(dt);
    }
}

void GameScreenManager::drawRectangle(const hxRectangle& rc, const Color& color) {
	SpriteBatch* spriteBatch = GameServices::getInstance().getSpriteBatch();
    spriteBatch->begin(0, BlendState::AlphaBlend, Matrix::Identity, GameArt::getInstance().getShaderProgram("@spriteBatchDefault"));
    spriteBatch->draw(GameArt::getInstance().getTexture2D("whitePixel"), rc, NULL, color);
    spriteBatch->end();
}
void GameScreenManager::addScreen(GameScreen* screen) {
    GameScreenManager* tmp = screen->getOwner();
    if (tmp != this) {
        if (tmp) {
            tmp->removeScreen(screen);
        }
    }
    screen->setOwner(this);
    mScreens.push_back(screen);
    screen->loadContent();
}
bool GameScreenManager::removeScreen(GameScreen* screen) {
    screen->unloadContent();

    std::vector<GameScreen*>::iterator it = std::find(mScreens.begin(),
            mScreens.end(), screen);
    if (it != mScreens.end()) {
    	mScreens.erase(it);
    }


    it = std::find(mScreensToUpdate.begin(), mScreensToUpdate.end(), screen);
    if (it != mScreensToUpdate.end()) {
    	mScreensToUpdate.erase(it);
    }


    screen->OnExit(hxEventArgs::Empty);


    return true;
}
void GameScreenManager::fadeToBlack(int a) {

    Color color = (Color) { 0x00000000 };
    color.A = (unsigned char) a;

    Viewport vp = GameServices::getInstance().getGraphicsDevice()->getViewport();
    drawRectangle(vp, color);
}

int GameScreenManager::numScreens() {
	return (int)mScreens.size();
}

/**
 *
 * @brief GameScreen class implementation
 *
 */

GameScreen::GameScreen() {
    mState = ScreenState::TransitionOn;
    mTransitionOnTime = 1000;
    mTransitionOffTime = 1000;
    mOtherScreenHasFocus = false;
    mIsPopup = false;
    mIsExiting = false;
    mTransitionPosition = 1.0f;
    mOwner = NULL;
}

GameScreen::~GameScreen() {
}

const bool GameScreen::isPopup() const {
    return mIsPopup != false;
}

const bool GameScreen::isActive() const {
    return !mOtherScreenHasFocus && (mState == ScreenState::TransitionOn || mState == ScreenState::Active);
}

const int GameScreen::getState() const {
    return mState;
}

float GameScreen::getTransitionAlpha() {
    return 1.f - mTransitionPosition;
}

void GameScreen::setOwner(GameScreenManager* m) {
    mOwner = m;
}

GameScreenManager* GameScreen::getOwner() {
    return mOwner;
}

void GameScreen::exitScreen() {
    if(mTransitionOffTime == 0) {
        mOwner->removeScreen(this);
    } else {
        mIsExiting = true;
    }
}

bool GameScreen::updateTransition(int dt, int time, int direction) {
     // How much should we move by?
    float transitionDelta;

    if (time == 0) {
        transitionDelta = 1;
    } else {
        transitionDelta = (float)dt / (float)time;
    }

    // Update the transition position.
    mTransitionPosition += transitionDelta * direction;

    // Did we reach the end of the transition?
    if ((mTransitionPosition <= 0) || (mTransitionPosition >= 1)) {
        mTransitionPosition = std::min(1.f, std::max(0.f, mTransitionPosition));
        return false;
    }

    // Otherwise we are still busy transitioning.
    return true;
}

void GameScreen::update(int dt, bool otherScreenHasFocus, bool coveredByOtherScreen) {
    mOtherScreenHasFocus = otherScreenHasFocus;

    if (mIsExiting) {
        // If the screen is going away to die, it should transition off.
        mState = ScreenState::TransitionOff;

        if (!updateTransition(dt, mTransitionOffTime, 1)) {
            // When the transition finishes, remove the screen.
            mOwner->removeScreen(this);
            mIsExiting = false;
        }
    } else if (coveredByOtherScreen) {
        // If the screen is covered by another, it should transition off.
        if (updateTransition(dt, mTransitionOffTime, 1)) {
            // Still busy transitioning.
            mState = ScreenState::TransitionOff;
        } else {
            // Transition finished!
            mState = ScreenState::Hidden;
        }

    } else {
        // Otherwise the screen should transition on and become active.
        if (updateTransition(dt, mTransitionOnTime, -1)) {
            // Still busy transitioning.
            mState = ScreenState::TransitionOn;
        } else {
            // Transition finished!
            mState = ScreenState::Active;
        }
    }
}

void GameScreen::draw(int) {
}

void GameScreen::loadContent() {
}

void GameScreen::unloadContent() {
}

void GameScreen::handleInput() {

}
