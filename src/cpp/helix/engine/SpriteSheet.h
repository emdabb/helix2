/*
 * SpriteSheet.h
 *
 *  Created on: Jul 17, 2015
 *      Author: miel
 */

#ifndef CLASSES_GAME_SPRITESHEET_H_
#define CLASSES_GAME_SPRITESHEET_H_

#include <helix/gfx/Texture2D.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/core/Rectangle.h>
#include <cJSON/cJSON.h>
#include <string>
#include <vector>
#include <map>

namespace helix {
namespace engine {



class SpriteSheet {
	hxGraphicsDevice*			mGraphicsDevice;
	hxTexture2D* 				mTexture;
	std::map<std::string, int> 	mRectNames;
	std::vector<hxRectangle> 	mRects;
protected:
	static void parseJSON(cJSON*, SpriteSheet*);
public:
	SpriteSheet(hxGraphicsDevice*);
	virtual ~SpriteSheet();
	int getRectangleIdForString(const std::string&);
	const hxRectangle& getRectangleByName(const std::string&);
	const hxRectangle& getRectangleById(int);
	int addRectangle(const std::string&, const hxRectangle&);
	void setTexture(hxTexture2D*);
	hxTexture2D* getTexture() const;
	//const std::string& getTextureName() const;
	static SpriteSheet* createFromStream(hxGraphicsDevice*, std::istream*);

};

}
}

#endif /* CLASSES_GAME_SPRITESHEET_H_ */
