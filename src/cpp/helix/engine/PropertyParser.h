/*
 * PropertyParser.h
 *
 *  Created on: Jul 29, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_PROPERTYPARSER_H_
#define HELIX_ENGINE_PROPERTYPARSER_H_

#include <helix/core/Properties.h>

namespace helix {
namespace engine {

struct PropertyParser {
	static hxProperties parseJSON(const char*);
};

}
}



#endif /* HELIX_ENGINE_PROPERTYPARSER_H_ */
