/*
 * LuaRestoreStack.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#include <helix/engine/lua/LuaVirtualMachine.h>
#include <helix/engine/lua/LuaRestoreStack.h>

using namespace helix::engine;

LuaRestoreStack::LuaRestoreStack(LuaVirtualMachine& vm) : mVM(vm), mTop(-1) {
    if(mVM.isValid() ) {
        mTop  = lua_gettop((lua_State*)mVM);
    }
}

LuaRestoreStack::~LuaRestoreStack() {
    lua_settop((lua_State*)mVM, mTop);
}


