/*
 * LuaVirtualMachine.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUAVIRTUALMACHINE_H_
#define LUAVIRTUALMACHINE_H_
extern "C" {
#include <lua.h>
#include <lauxlib.h>
}

namespace helix {
namespace engine {

class LuaDebugger;

class LuaVirtualMachine {
	lua_State* mState;
	bool mIsOK;
	LuaDebugger* mDebugger;
public:
	LuaVirtualMachine();
	virtual ~LuaVirtualMachine();
	/**
	 * Initialize the virtual machine
	 * @return
	 */
	bool initializeVM();
	/**
	 * Destroy the virtual machine.
	 * @return
	 */
	bool destroyVM();
	/**
	 * Run a file containing LUA script (unused at the moment).
	 * @param
	 * @return
	 */
	/** bool runFile(const char*); */
	/**
	 * Run a buffer (LUA byte code).
	 * @param buffer
	 * @param len
	 * @param name
	 * @return
	 */
	bool runBuffer(const char*, size_t, const char* = 0);
	/**
	 * C-API into script.
	 *
	 * @param
	 * @param
	 * @return
	 */
	bool call(int, int = 0);
	/**
	 *
	 * @param
	 */
	static void panic(lua_State*);
	/**
	 *
	 * @return
	 */
	virtual bool isValid() const;
	/**
	 * Attach a debugger to this VM.
	 * @param
	 */
	void attachDebugger(LuaDebugger*);
	/**
	 * Cast operator to lua_State.
	 */
	operator lua_State*() {
		return mState;
	}
};

}
}

typedef helix::engine::LuaVirtualMachine hxLuaVirtualMachine;
#endif /* LUAVIRTUALMACHINE_H_ */
