/*
 * LuaDebugger.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUADEBUGGER_H_
#define LUADEBUGGER_H_

namespace helix {
namespace engine {

class LuaVirtualMachine;

class LuaDebugger {
    int 				mCountMask;
    LuaVirtualMachine& 	mVM;
public:
    explicit LuaDebugger(LuaVirtualMachine&);
    virtual ~LuaDebugger();
    void setHook(int);
    void setCount(int);
    void error(int);
};

}
}

typedef helix::engine::LuaDebugger hxLuaDebugger;

#endif /* LUADEBUGGER_H_ */
