/*
 * LuaScript.h
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#ifndef LUASCRIPT_H_
#define LUASCRIPT_H_

#include <helix/Types.h>
//#include <core/event.h>
extern "C" {
#include <lua.h>
}
namespace helix {
namespace engine {

class LuaVirtualMachine;

struct ScriptEvent {
	//LuaVirtualMachine& VM;
	lua_State* L;
	int id;
	const char* Msg;
};

class LuaScript {
	int mNumMethods;
	int mThisRef;
	int mNumArgs;
	const char* mFunctionName;
protected:
	LuaVirtualMachine& mVM;
protected:
//      virtual int scriptCall(LuaVirtualMachine&, int);
//      virtual void handleReturns(LuaVirtualMachine&, const char*);
public:
	explicit LuaScript(LuaVirtualMachine&);
	virtual ~LuaScript();

	bool compile(const char*, size_t);
	int registerFunction(const char*);
	bool selectScriptFunction(const char*);
	void addParam(int);
	void addParam(float);
	void addParam(const char*);
	bool go(int = 0);
	bool hasFunction(const char*);
	int numMethods() const;
	virtual void initializeFunctions() = 0;
	virtual int scriptCalling(lua_State*, int) = 0;
	virtual void handleReturns(lua_State*, const char*) = 0;

//      event_handler<ScriptEvent> OnScriptCall;
//      event_handler<ScriptEvent> OnScriptReturn;

	LuaVirtualMachine& getVM() {
		return mVM;
	}
};

}
}

typedef helix::engine::LuaScript hxLuaScript;

#endif /* LUASCRIPT_H_ */
