/*
 * LuaVirtualMachine.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#include <helix/engine/lua/LuaVirtualMachine.h>
#include <helix/engine/lua/LuaDebugger.h>
#include <helix/engine/lua/LuaHelper.h>
#include <cassert>
#include <cstring>

using namespace helix::engine;
using namespace helix::core;

LuaVirtualMachine::LuaVirtualMachine()
: mState(0)
, mIsOK(false)
, mDebugger(0) {
    DEBUG_METHOD();
}

LuaVirtualMachine::~LuaVirtualMachine() {
    if (mState != 0) {
        lua_close(mState);
    }
}

void LuaVirtualMachine::panic(lua_State*) {
	DEBUG_METHOD();
}

bool LuaVirtualMachine::initializeVM() {
    DEBUG_METHOD();
    DEBUG_MESSAGE("LUA: initializing virtual machine...");
    if (isValid()) {
    	DEBUG_MESSAGE("LUA: failed! already initialized!");
        destroyVM();
    }
    mState = luaL_newstate();
    if (mState) {
        mIsOK = true;
        /**
         * Load utility libraries into LUA.
         */
#ifdef __HAVE_LUA_OPEN_DIRECT__
        luaopen_base(mState);
        luaopen_table(mState);
        luaopen_string(mState);
        luaopen_math(mState);
        luaopen_debug(mState);
        luaopen_io(mState);
        luaopen_package(mState);
#else
        DEBUG_MESSAGE("LUA: opening LUA libraries...");
        luaL_openlibs(mState);
#endif
        DEBUG_MESSAGE("LUA: setting up trace...");
        /**
         * Setup global printing (trace).
         */
        lua_pushcclosure(mState, LuaHelper::printMessage, 0);
        lua_setglobal(mState, "trace");
        lua_atpanic(mState, (lua_CFunction) LuaVirtualMachine::panic);
    }
    return mIsOK != false;
}

bool LuaVirtualMachine::destroyVM() {
    DEBUG_METHOD();
    if (mState) {
        lua_close(mState);
        mState = 0;
        mIsOK = false;
    }
    return mIsOK != true;
}

//bool LuaVirtualMachine::runFile(const char* filename) {
//	bool success = false;
//	int err = 0;
//	if ((err = luaL_loadfile(mState, filename)) == 0) {
//		/**
//		 * Call 'main'.
//		 */
//		if ((err = lua_pcall(mState, 0, LUA_MULTRET, 0)) == 0) {
//			success = true;
//		}
//	}
//	if (!success) {
//		if (mDebugger) {
//			mDebugger->error(err);
//		}
//	}
//	return success != false;
//}

bool LuaVirtualMachine::runBuffer(const char* buffer, size_t len, const char* name) {
    bool success = false;
    int err = 0;
    if (name == NULL) {
        name = "Temp";
    }
    if ((err = luaL_loadbuffer(mState, buffer, len, name)) == 0) {
        if ((err = lua_pcall(mState, 0, LUA_MULTRET, 0)) == 0) {
            success = true;
        }
    }
    if (!success) {
        if (mDebugger) {
            mDebugger->error(err);
        }
    }
    return success != false;
}

bool LuaVirtualMachine::call(int args, int ret) {
    bool success = false;
    if (lua_isfunction(mState, -args - 1)) {
        int err = 0;
        if ((err = lua_pcall(mState, args, ret, 0)) == 0) {
            success = true;
        }
        else {
          DEBUG_METHOD();
          DEBUG_MESSAGE("lua call failed.");
        }

        if (!success) {
            if (mDebugger) {
                mDebugger->error(err);
            }
        }
    }
    return success != false;
}

bool LuaVirtualMachine::isValid() const {
    return mIsOK != false;
}

void LuaVirtualMachine::attachDebugger(LuaDebugger* dbg) {
    mDebugger = dbg;
}
