/*
 * LuaDebugger.cpp
 *
 *  Created on: Jul 29, 2013
 *      Author: miel
 */

#include "LuaDebugger.h"
#include "LuaVirtualMachine.h"
#include "LuaHelper.h"
#include <helix/core/DebugLog.h>
extern "C" {
#include <lua.h>
}
#include <cassert>

using namespace helix::core;
using namespace helix::engine;

LuaDebugger::LuaDebugger(LuaVirtualMachine& vm) : mCountMask(0), mVM(vm){
    if(vm.isValid()) {
        vm.attachDebugger(this);
        lua_sethook((lua_State*)vm, LuaHelper::luaHook, 0, mCountMask);
    }

}

LuaDebugger::~LuaDebugger() {
    if(mVM.isValid() ) {
        lua_sethook((lua_State*)mVM, LuaHelper::luaHook, 0, mCountMask);
    }
}

void LuaDebugger::setHook(int flags) {
    lua_sethook((lua_State*)mVM, LuaHelper::luaHook, flags, mCountMask);
}

void LuaDebugger::setCount(int value) {
    mCountMask = value;
}

void LuaDebugger::error(int value) {
    DEBUG_METHOD();
    switch(value) {
    case LUA_ERRRUN:
        DEBUG_MESSAGE("LUA_ERRRUN");
        break;
    case LUA_ERRMEM:
        DEBUG_MESSAGE("LUA_ERRMEM");
        break;
    case LUA_ERRERR:
        DEBUG_MESSAGE("LUA_ERRERR");
        break;
    }
    DEBUG_MESSAGE("%s", lua_tostring((lua_State*)mVM, -1));
}

