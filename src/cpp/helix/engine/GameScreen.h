/**
 * @file GameScreen.h
 * @author miel <miel@mimesis-games.com>
 * @version 1.0
 * @date May 21, 2015
 *
 * @section LICENSE
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Mimesis Games 2014, All Rights Reserved
 *
 * @section DESCRIPTION
 */

#ifndef GAME_UI_GAMESCREEN_H_
#define GAME_UI_GAMESCREEN_H_

#include <helix/core/Rectangle.h>
#include <helix/core/EventHandler.h>
#include <helix/base/IApplication.h>
#include <helix/gfx/Color.h>
#include <vector>


struct ScreenState {
    enum {
        TransitionOn,
        Active,
        TransitionOff,
        Hidden
    };
};

class GameScreenManager;

HXAPI HXEXPORT void ImGui_newFrame(int, int);
HXAPI HXEXPORT void ImGui_mouseMoved(const hxMouseEventArgs&);
HXAPI HXEXPORT void ImGui_mousePressed(const hxMouseEventArgs&);
HXAPI HXEXPORT void ImGui_mouseReleased(const hxMouseEventArgs&);
HXAPI HXEXPORT void ImGui_keyPressed(const hxKeyEventArgs& args);
HXAPI HXEXPORT void ImGui_keyReleased(const hxKeyEventArgs& args);

class GameScreen {
protected:
    int   mState;
    int   mTransitionOnTime;
    int   mTransitionOffTime;
    bool  mOtherScreenHasFocus;
    bool  mIsPopup;
    bool  mIsExiting;
    float mTransitionPosition;
    GameScreenManager* mOwner;
protected:
    bool updateTransition(int, int, int);
public:
    GameScreen();
    virtual ~GameScreen();

    const bool isPopup() const;
    const bool isActive() const;
    const int getState() const;
    float getTransitionAlpha();
    void setOwner(GameScreenManager* m);
    GameScreenManager* getOwner();
    virtual void exitScreen();
    virtual void update(int, bool, bool);
    virtual void draw(int);
    virtual void loadContent();
    virtual void unloadContent();
    virtual void handleInput();
public:
    hxEventHandler<hxEventArgs>::Type OnExit;
};

class GameScreenManager {
    std::vector<GameScreen*> mScreens;
    std::vector<GameScreen*> mScreensToUpdate;
    std::vector<GameScreen*> mScreensToDraw;
protected:

public:
    GameScreenManager();
    virtual ~GameScreenManager();
    void loadContent();
    void unloadContent();
    bool update(int);
    void draw(int);
    void drawRectangle(const hxRectangle&, const hxColor&);
    void addScreen(GameScreen*);
    bool removeScreen(GameScreen*);
    void fadeToBlack(int);
    int  numScreens();
public:
    //EventHandler<EventArgs> OnScreenRemoved;
};




#endif /* GAME_UI_GAMESCREEN_H_ */
