/*
 * GameServices.cpp
 *
 *  Created on: Jul 20, 2015
 *      Author: miel
 */

#include <helix/engine/GameServices.h>
#include <helix/engine/GameArt.h>

using namespace helix::engine;
using namespace helix::gfx;


GameServices::~GameServices() {

}

void GameServices::setProvider(hxApplication* app) {
	mGraphicsDevice = app->getGraphicsDevice();
	mAssetManager   = app->getAssetManager();
	mAudioDevice	= app->getAudioDevice();
	mSpriteBatch    = new hxSpriteBatch(mGraphicsDevice);

}

HXAPI HXEXPORT void Engine_initialize(hxApplication* app) {
	GameServices::getInstance().setProvider(app);
	GameArt::getInstance();
}
