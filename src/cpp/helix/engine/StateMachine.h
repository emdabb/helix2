#ifndef AGARIO_GAME_STATEMACHINE_H_
#define AGARIO_GAME_STATEMACHINE_H_

#include <helix/Types.h>
#include <stack>

namespace helix {
namespace engine {

template <typename T>
struct IState {
    virtual ~IState() {}
    virtual void onEnter(T*) = 0;
    virtual void onUpdate(T*, int) = 0;
    virtual void onExit(T*) = 0;
};

template <typename T>
class StateMachine {
    typedef IState<T> state_type;
    std::stack<state_type*> mStates;
    T* mOwner;

public:
    StateMachine()
    : mOwner(NULL)
    {
    }

    virtual ~StateMachine() {

    }

    void setOwner(T* thiz) {
        mOwner = thiz;
    }

    void push(state_type* s) {
        if(mStates.size() > 0) {
            mStates.top()->onExit(mOwner);
            mStates.pop();
        }
        mStates.push(s);
        mStates.top()->onEnter(mOwner);
    }

    state_type* pop() {
        state_type* res = NULL;
        if(mStates.size() > 0) {
            res = mStates.top();
            res->onExit(mOwner);
            mStates.pop();
        }
        return res;
    }

    state_type* top() {
        state_type* res = NULL;
        if(mStates.size() > 0) {
            res = mStates.top();
        }
        return res;
    }

    void update(int dt) {
        if(mStates.size() > 0) {
            mStates.top()->onUpdate(mOwner, dt);
        }
    }
};

}
}

#endif
