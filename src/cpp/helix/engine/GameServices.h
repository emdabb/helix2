/*
 * GameServices.h
 *
 *  Created on: Jul 13, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_GAMESERVICES_H_
#define HELIX_ENGINE_GAMESERVICES_H_

#include <helix/core/Singleton.h>
#include <helix/base/IApplication.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/SpriteBatch.h>
#include <helix/content/AssetManager.h>
#include <helix/audio/IAudioDevice.h>

namespace helix {
namespace engine {

class GameServices : public hxSingleton<GameServices>::Type {
	hxAssetManager* 	mAssetManager;
	hxGraphicsDevice* 	mGraphicsDevice;
	hxAudioDevice*		mAudioDevice;
	hxSpriteBatch*		mSpriteBatch;
public:
	virtual ~GameServices();
	void setProvider(hxApplication*);
	hxAssetManager* getAssetManager() {
		return mAssetManager;
	}
	hxGraphicsDevice* getGraphicsDevice() {
		return mGraphicsDevice;
	}
	hxAudioDevice* getAudioDevice() {
		return mAudioDevice;
	}
	hxSpriteBatch* getSpriteBatch() {
		return mSpriteBatch;
	}
};

}
}



#endif /* HELIX_ENGINE_GAMESERVICES_H_ */
