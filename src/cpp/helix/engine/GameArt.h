#ifndef AGARIO_GAME_GAMEART_H_
#define AGARIO_GAME_GAMEART_H_

#include <helix/core/Singleton.h>
#include <helix/gfx/Texture2D.h>
#include <helix/gfx/ShaderProgram.h>
#include <helix/gfx/SpriteFont.h>
#include <helix/content/AssetManager.h>
#include "SpriteSheet.h"

#include <map>
#include <string>
#include <vector>
#include <algorithm>

namespace helix {
namespace engine {


class GameArt : public hxSingleton<GameArt>::Type {
	std::vector<SpriteSheet*> 		mSpriteSheets;
    std::vector<hxTexture2D*> 		mTextures;
    std::vector<hxShaderProgram*> 	mShaders;
    std::vector<hxSpriteFont*> 		mFonts;
    std::vector<int>				mAudio;

    std::map<std::string, size_t> mSpriteSheetNames;
    std::map<std::string, size_t> mTextureNames;
    std::map<std::string, size_t> mShaderNames;
    std::map<std::string, size_t> mFontNames;
    std::map<std::string, size_t> mAudioNames;

    std::map<std::string, std::string> mSpriteSheetAliases;
    std::map<std::string, std::string> mTextureAliases;
    std::map<std::string, std::string> mFontAliases;
    std::map<std::string, std::string> mShaderAliases;
    std::map<std::string, std::string> mAudioAliases;
public:
    GameArt();
    virtual ~GameArt();
    void loadTextures(const std::string& jsonPath);
    void loadFonts(const std::string& jsonPath, int = 16);
    void loadShaders(const std::string& jsonPath);
    void loadSpriteSheets(const std::string& jsonPath);
    void loadAudio(const std::string& jsonPath);

    SpriteSheet*	 getSpriteSheet(const std::string& name);
    hxShaderProgram* getShaderProgram(const std::string& name);
    hxTexture2D*     getTexture2D(const std::string&);
    hxSpriteFont*    getFont(const std::string&);
    int				 getAudio(const std::string&);

    size_t getSpriteSheetId(const std::string&);
    size_t getTextureId(const std::string&);
    size_t getFontId(const std::string&);
    size_t getShaderId(const std::string&);
    size_t getAudioId(const std::string&);

    SpriteSheet* getSpriteSheetById(size_t);
    hxTexture2D* getTextureById(size_t);
    hxSpriteFont* getFontById(size_t);
    hxShaderProgram* getShaderById(size_t);
    int getAudioById(size_t);

    void addSpriteSheet(SpriteSheet*, const std::string&);
    void addTexture(hxTexture2D*, const std::string&);
    void addFont(hxSpriteFont*, const std::string&);
    void addShader(hxShaderProgram*, const std::string&);
    void addAudio(int, const std::string&);
};


}
}

#endif
