/*
 * PropertyParser.cpp
 *
 *  Created on: Jul 29, 2015
 *      Author: miel
 */

#include <helix/engine/PropertyParser.h>
#include <cJSON/cJSON.h>

using namespace helix::core;
using namespace helix::engine;

void PropertyParser_parseJSON(cJSON* json, Properties* props) {
	if(json->string) {
		if(json->valuestring) {
			props->insert(json->string, json->valuestring);
		} else {
			int k = 0;
		}
	}

	if(json->child) {
		PropertyParser_parseJSON(json->child, props);
	}

	if(json->next) {
		PropertyParser_parseJSON(json->next, props);
	}
}

hxProperties PropertyParser::parseJSON(const char* src) {
	hxProperties props;
	cJSON* json = cJSON_Parse(src);
	PropertyParser_parseJSON(json, &props);

	return props;
}
