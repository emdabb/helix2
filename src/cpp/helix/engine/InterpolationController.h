/*
 * Interpolator.h
 *
 *  Created on: Jul 17, 2015
 *      Author: miel
 */

#ifndef HELIX_ENGINE_INTERPOLATIONCONTROLLER_H_
#define HELIX_ENGINE_INTERPOLATIONCONTROLLER_H_

#include <helix/core/Easing.h>
#include <cassert>

namespace helix {
namespace engine {

struct IInterpolationController {
	virtual ~IInterpolationController() {}
	virtual void update(int) = 0;
};

template <typename T>
class InterpolationController : public IInterpolationController {
	typedef core::Easing::EasingFunction InterpolateFn;
	T* mRef;
	T  mStartValue;
	T  mEndValue;
	InterpolateFn mFn;
	int mTotalTime;
	int mCurrentTime;
	bool mIsLoop;
public:
	InterpolationController(T* ref, const T& startValue, const T& endValue, InterpolateFn fn, int totalTime, bool isLoop = false)
	: mRef(ref)
	, mStartValue(startValue)
 	, mEndValue(endValue)
	, mFn(fn)
	, mTotalTime(totalTime)
 	, mCurrentTime(0)
	, mIsLoop(isLoop)
	{
		assert(mTotalTime > 0);
	}

	virtual ~InterpolationController() {

	}

	void update(int dt) {
		mCurrentTime += dt;
		if(mIsLoop && mCurrentTime >= mTotalTime) {
			mCurrentTime = 0;
		}
		float fItp = mFn((float)mCurrentTime / (float)mTotalTime);
		*mRef = mStartValue + (mEndValue - mStartValue) * fItp;
	}
};

}
}



#endif /* HELIX_ENGINE_INTERPOLATIONCONTROLLER_H_ */
