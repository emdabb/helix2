/*
 * SpriteSheet.cpp
 *
 *  Created on: Jul 17, 2015
 *      Author: miel
 */

#include <helix/engine/SpriteSheet.h>
#include <helix/engine/GameArt.h>
#include <helix/core/DebugLog.h>
#include <iostream>
#include <sstream>

using namespace helix::engine;
using namespace helix::core;
using namespace helix::gfx;

template <typename T>
T lexical_cast(const std::string& str)
{
    T var;
    std::istringstream iss;
    iss.str(str);
    iss >> var;
    // deal with any error bits that may have been set on the stream
    return var;
}

using namespace helix::gfx;
using namespace helix::core;

SpriteSheet::SpriteSheet(hxGraphicsDevice* pDevice)
: mGraphicsDevice(pDevice)
, mTexture(NULL)
{
}
SpriteSheet::~SpriteSheet() {
}

int SpriteSheet::getRectangleIdForString(const std::string& name) {
	std::map<std::string, int>::iterator it = mRectNames.find(name);
	if(it != mRectNames.end()) {
		return (*it).second;
	}
	return -1;
}

const hxRectangle& SpriteSheet::getRectangleByName(const std::string& name) {
	int id = getRectangleIdForString(name);
	if(id >= 0) {
		return mRects[id];
	}
	return (Rectangle) { -1, -1, 0, 0 };
}

const hxRectangle& SpriteSheet::getRectangleById(int id) {
	return mRects[id];
}

int SpriteSheet::addRectangle(const std::string& name, const hxRectangle& rc) {
	if(getRectangleIdForString(name) < 0) {
		int id = (int)mRects.size();
		mRectNames[name] = id;//.insert(std::make_pair<std::string, int>(name, id));
		mRects.push_back(rc);
		return id;
	}
	return -1;
}

void SpriteSheet::setTexture(hxTexture2D* tex) {
	mTexture = tex;
}
hxTexture2D* SpriteSheet::getTexture() const {
	return mTexture;
}

//const std::string& SpriteSheet::getTextureName() const {
//
//}

void SpriteSheet::parseJSON(cJSON* json, SpriteSheet* res) {

	if(json->string) {
		//DEBUG_METHOD();
		//DEBUG_MESSAGE("%s=[%s]", json->string, json->valuestring);
		if(!strcmp("TextureAtlas", json->string)) {

		}
		if(!strcmp("imagePath", json->string)) {
			res->setTexture(GameArt::getInstance().getTexture2D(json->valuestring));
		}
		if(!strcmp("name", json->string)) {
			std::map<std::string, std::string> props;
			cJSON* ptr = json->next;
			while(ptr) {
				props[ptr->string] = ptr->valuestring;
				ptr = ptr->next;
			}
			hxRectangle rc;
			rc.X = lexical_cast<int>(props["x"]);
			rc.Y = lexical_cast<int>(props["y"]);
			rc.W = lexical_cast<int>(props["width"]);
			rc.H = lexical_cast<int>(props["height"]);
			res->addRectangle(std::string(json->valuestring), rc);
		}

	}

	if(json->next) {
		parseJSON(json->next, res);
	}

	if(json->child) {
		parseJSON(json->child, res);
	}
}
SpriteSheet* SpriteSheet::createFromStream(IGraphicsDevice* pDevice, std::istream* is) {
	SpriteSheet* res = new SpriteSheet(pDevice);

	std::istreambuf_iterator<char> eos;
	std::string s(std::istreambuf_iterator<char>(*is), eos);

	cJSON* root = cJSON_Parse(s.c_str());
	parseJSON(root, res);

	return res;
}

