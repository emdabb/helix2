#include <helix/engine/GameArt.h>
#include <helix/engine/GameServices.h>
#include <cJSON/cJSON.h>
#include <helix/gfx/Surface.h>
#include <helix/gfx/PixelFormat.h>
#include <helix/core/DebugLog.h>

using namespace helix::core;
using namespace helix::engine;
using namespace helix::gfx;

static void GameArt_parseJSON(cJSON* json, std::map<std::string, std::string>* ptr) {
    if(json->string) {
        ptr->insert(std::make_pair<std::string, std::string>(json->string, json->valuestring));
    }

    if(json->next) {
        GameArt_parseJSON(json->next, ptr);
    }
}

GameArt::GameArt() {
	Texture2D* tex= new Texture2D(GameServices::getInstance().getGraphicsDevice(), 1, 1, PixelFormat::Color, false);
	tex->setData<Color>(&ColorPreset::White, 0, 0, 0, 1, 1);
	addTexture(tex, "whitePixel");
}

GameArt::~GameArt() {
}

void GameArt::loadShaders(const std::string& path) {
    std::istream* is = GameServices::getInstance().getAssetManager()->openStream(path);
    if(is) {
        std::string src(std::istreambuf_iterator<char>(*is), (std::istreambuf_iterator<char>()));
        cJSON* json = cJSON_Parse(src.c_str());
        GameArt_parseJSON(json->child, &mShaderAliases);
    }

    std::map<std::string, std::string>::iterator it = mShaderAliases.begin();
    for(; it != mShaderAliases.end(); it++) {
        std::istream* iss = GameServices::getInstance().getAssetManager()->openStream((*it).second);
        hxShaderProgram* p = hxShaderProgram::createFromStream(GameServices::getInstance().getGraphicsDevice(), iss);
        addShader(p, (*it).first);
        //addTexture(t, (*it).first);

    }
}

void GameArt::loadTextures(const std::string& path) {
    std::istream* is = GameServices::getInstance().getAssetManager()->openStream(path);
    if(is) {
        std::string src(std::istreambuf_iterator<char>(*is), (std::istreambuf_iterator<char>()));
        cJSON* json = cJSON_Parse(src.c_str());
        GameArt_parseJSON(json->child, &mTextureAliases);
    }

    std::map<std::string, std::string>::iterator it = mTextureAliases.begin();
    for(; it != mTextureAliases.end(); it++) {
        //std::istream* in = GameServices::assets.openStream((*it).second);
        hxSurface* s    = GameServices::getInstance().getAssetManager()->load<hxSurface>((*it).second);
        hxTexture2D* t  = hxTexture2D::createFromSurface(GameServices::getInstance().getGraphicsDevice(), *s);
        addTexture(t, (*it).first);

    }
}

void GameArt::loadFonts(const std::string& path, int size) {
    std::istream* is = GameServices::getInstance().getAssetManager()->openStream(path);
    if(is) {
        std::string src(std::istreambuf_iterator<char>(*is), (std::istreambuf_iterator<char>()));
        cJSON* json = cJSON_Parse(src.c_str());
        GameArt_parseJSON(json->child, &mFontAliases);
    }

    std::map<std::string, std::string>::iterator it = mFontAliases.begin();
    for(; it != mFontAliases.end(); it++) {
        std::istream* in = GameServices::getInstance().getAssetManager()->openStream((*it).second);
        hxSpriteFont* f = hxSpriteFont::createFromStream(GameServices::getInstance().getGraphicsDevice(), in, size);
        addFont(f, (*it).first);
    }
}

void GameArt::loadAudio(const std::string& jsonPath) {
	std::istream* is = GameServices::getInstance().getAssetManager()->openStream(jsonPath);
	if(is) {
		std::string src(std::istreambuf_iterator<char>(*is), (std::istreambuf_iterator<char>()));
		cJSON* json = cJSON_Parse(src.c_str());
		GameArt_parseJSON(json->child, &mAudioAliases);
	}

	 std::map<std::string, std::string>::iterator it = mAudioAliases.begin();
	for(; it != mAudioAliases.end(); it++) {
		std::istream* in = GameServices::getInstance().getAssetManager()->openStream((*it).second);
		//hxSpriteFont* f = hxSpriteFont::createFromStream(GameServices::getInstance().getGraphicsDevice(), in, size);
		int n = GameServices::getInstance().getAudioDevice()->registerStream(in);
		addAudio(n, (*it).first);
	}
}

void GameArt::loadSpriteSheets(const std::string& path) {
	std::istream* is = GameServices::getInstance().getAssetManager()->openStream(path);
	 if(is) {
		std::string src(std::istreambuf_iterator<char>(*is), (std::istreambuf_iterator<char>()));
		cJSON* json = cJSON_Parse(src.c_str());
		GameArt_parseJSON(json->child, &mSpriteSheetAliases);
	}

	std::map<std::string, std::string>::iterator it = mSpriteSheetAliases.begin();
	for(; it != mSpriteSheetAliases.end(); it++) {
		std::istream* in = GameServices::getInstance().getAssetManager()->openStream((*it).second);
		SpriteSheet* ss = SpriteSheet::createFromStream(GameServices::getInstance().getGraphicsDevice(), in);
		addSpriteSheet(ss, (*it).first);
	}
}

SpriteSheet* GameArt::getSpriteSheet(const std::string& name) {
    return mSpriteSheets[getSpriteSheetId(name)];
}

hxTexture2D* GameArt::getTexture2D(const std::string& name) {
    // mTextures[getTextureId(name)];
    
    std::map<std::string, size_t>::iterator it = mTextureNames.find(name);
    if(it != mTextureNames.end()) {
        return mTextures[(*it).second];
    }
    return NULL;
}

hxShaderProgram* GameArt::getShaderProgram(const std::string& name) {
    return mShaders[getShaderId(name)];
}

hxSpriteFont* GameArt::getFont(const std::string& name) {
    return mFonts[getFontId(name)];
}

int GameArt::getAudio(const std::string& name) {
	return mAudio[getAudioId(name)];
}

size_t GameArt::getSpriteSheetId(const std::string& name) {
    return mSpriteSheetNames[name];
}

size_t GameArt::getTextureId(const std::string& name) {
    return mTextureNames[name];
}

size_t GameArt::getFontId(const std::string& name) {
    return mFontNames[name];
}

size_t GameArt::getShaderId(const std::string& name) {
    return mShaderNames[name];
}

size_t GameArt::getAudioId(const std::string& name) {
	return mAudioNames[name];
}

SpriteSheet* GameArt::getSpriteSheetById(size_t i) {
    return mSpriteSheets[i];
}

hxTexture2D* GameArt::getTextureById(size_t i) {
    return mTextures[i];
}

hxSpriteFont* GameArt::getFontById(size_t i) {
    return mFonts[i];
}

int GameArt::getAudioById(size_t i) {
	return mAudio[i];
}

void GameArt::addSpriteSheet(SpriteSheet* val, const std::string& name) {
    mSpriteSheets.push_back(val);
    mSpriteSheetNames[name] = mSpriteSheets.size() - 1;
}

void GameArt::addTexture(hxTexture2D* val, const std::string& name) {
	DEBUG_METHOD();
    mTextures.push_back(val);
    mTextureNames[name] = mTextures.size() - 1;
    DEBUG_MESSAGE("texture [\"%s\"] =@[%d]", name.c_str(), mTextures.size() - 1);
}

void GameArt::addFont(hxSpriteFont* val, const std::string& name) {
    mFonts.push_back(val);
    mFontNames[name] = mFonts.size() - 1;
}

void GameArt::addShader(hxShaderProgram* val, const std::string& name) {
    mShaders.push_back(val);
    mShaderNames[name] = mShaders.size() - 1;
}

void GameArt::addAudio(int val, const std::string& name) {
	mAudio.push_back(val);
	mAudioNames[name] = mAudio.size() - 1;
}
