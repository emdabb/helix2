#ifndef HELIX_CONFIG_H_
#define HELIX_CONFIG_H_

#cmakedefine _DEBUG
#cmakedefine NDEBUG

#define SYSTEM_NAME     "@CMAKE_SYSTEM_NAME@"
#define PACKAGE_NAME    "@PROJECT_NAME@"
#define VERSION         "@VERSION@"


#ifndef             HELIX_PLATFORM_POSIX
#cmakedefine        HELIX_PLATFORM_POSIX
#endif

#ifndef             HELIX_PLATFORM_ANDROID
#cmakedefine        HELIX_PLATFORM_ANDROID
#endif

#ifndef             HELIX_PLATFORM_IOS
#cmakedefine        HELIX_PLATFORM_IOS
#endif

#ifndef             HELIX_PLATFORM_WINRT
#cmakedefine        HELIX_PLATFORM_WINRT
#endif

#cmakedefine        ANDROID_NDK_API_LEVEL   ${ANDROID_NDK_API_LEVEL}

#cmakedefine        HELIX_FIXED_POINT
#cmakedefine        HELIX_RTTI
#cmakedefine        HELIX_EXCEPTIONS
#cmakedefine        HELIX_HAS_SSE

#endif
