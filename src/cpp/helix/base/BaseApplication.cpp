#include "BaseApplication.h"

using namespace helix;
using namespace helix::audio;
using namespace helix::base;
using namespace helix::gfx;
using namespace helix::content;
using namespace helix::input;

BaseApplication::BaseApplication()
: mAssetManager(NULL)
, mGraphicsContext(NULL)
, mGraphicsDevice(NULL)
, mVirtualKeyboard(NULL)
, mAudioDevice(NULL)
, mApplet(NULL)
, mIsMouseRelative(false)
, mLastMouseX(0)
, mLastMouseY(0)
, mWillSkipNextMouseMove(false)
, mWidth(0)
, mHeight(0)
, mPositionX(0)
, mPositionY(0)
{
    for(int i=0; i < InputDeviceType::MaxDeviceType; i++) {
        mInputDevice[i] = NULL;
    }
}

BaseApplication::~BaseApplication() {
}

AssetManager* BaseApplication::getAssetManager() const {
    return mAssetManager;
}

IGraphicsContext* BaseApplication::getGraphicsContext() const {
    return mGraphicsContext;
}

IGraphicsDevice* BaseApplication::getGraphicsDevice() const {
    return mGraphicsDevice;
}

void BaseApplication::setApplet(IApplet* applet) {
    mApplet = applet;
}

IApplet* BaseApplication::getApplet() const {
    return mApplet;
}

void* BaseApplication::getDisplay() const {
    return NULL;
}

void* BaseApplication::getSurface() const {
    return NULL;
}

void BaseApplication::create() {
//    createContextAndRenderDevice();
}

void BaseApplication::destroy() {
}

IInputDevice* BaseApplication::getInputDevice(const int id) const {
    return mInputDevice[id];
}

IVirtualKeyboard* BaseApplication::getVirtualKeyboard() const {
    return mVirtualKeyboard;
}

IAudioDevice* BaseApplication::getAudioDevice() const {
	return mAudioDevice;
}

void BaseApplication::setMouseRelative(bool val) {
    mIsMouseRelative = val;
}

bool BaseApplication::isMouseRelative() const {
    return mIsMouseRelative != false;
}

void BaseApplication::postKeyPress(int keySym) {
    KeyEventArgs args;
    args.KeyId = keySym;
    OnKeyPressed(args);
}

void BaseApplication::postKeyRelease(int keySym) {
    KeyEventArgs args;
    args.KeyId = keySym;
    OnKeyReleased(args);
}


void BaseApplication::postMouseMove(int x, int y, int pointerId) {
    MouseEventArgs args;
    args.X = x;
    args.Y = y;
    args.PointerId = pointerId;
    OnMouseMoved(args);
}
void BaseApplication::postMousePressed(int x, int y, int pointerId) {
    MouseEventArgs args;
    args.X = x;
    args.Y = y;
    args.PointerId = pointerId;;
    OnMousePressed(args);
}
void BaseApplication::postMouseReleased(int x, int y, int pointerId) {
    MouseEventArgs args;
    args.X = x;
    args.Y = y;
    args.PointerId =pointerId;
    OnMouseReleased(args);
}

void BaseApplication::resize(int w, int h) {
    mWidth = w;
    mHeight = h;
}
void BaseApplication::setPosition(int x, int y) {
    mPositionX = x;
    mPositionY = y;
}

int BaseApplication::getWidth() const {
    return mWidth;
}
int BaseApplication::getHeight() const {
    return mHeight;
}
