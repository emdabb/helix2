#ifndef HELIX_BASE_IAPPLICATION_H_
#define HELIX_BASE_IAPPLICATION_H_

#include <helix/Types.h>
#include <helix/core/EventHandler.h>

namespace helix {
	namespace audio {
		struct IAudioDevice;
	}
    namespace content {
        class AssetManager;
    }
    namespace gfx {
        struct IGraphicsDevice;
        struct IGraphicsContext;
    }
    namespace input {
        struct IInputDevice;
        struct IVirtualKeyboard;
    }
    namespace base {

        struct IApplet;

        struct MouseEventArgs {
            int X;
            int Y;
            int PointerId;
            int ButtonId;
        };

        struct KeyEventArgs {
            int KeyId;

            enum {
                Key_ENTER = 2048,// = 10,     //!< Key_ENTER
                Key_TAB,// = 8,        //!< Key_TAB
                Key_SPACE,// = 32,     //!< Key_SPACE

                Key_LEFT,// = 1024,    //!< Key_LEFT
                Key_RIGHT,          //!< Key_RIGHT
                Key_UP,             //!< Key_UP
                Key_DOWN,           //!< Key_DOWN

                // Keyboard keys
                Key_ESC,// = 2048,     //!< Key_ESC
                Key_PAUSE,          //!< Key_PAUSE
                Key_SHIFT,          //!< Key_SHIFT
                Key_CTRL,           //!< Key_CTRL
                Key_ALT,            //!< Key_ALT
                Key_SPECIAL,        //!< Key_SPECIAL
                Key_BACKSPACE,      //!< Key_BACKSPACE
                Key_DELETE,         //!< Key_DELETE

                // Cellphone keys
                Key_SOFTLEFT,// = 4096,//!< Key_SOFTLEFT
                Key_SOFTRIGHT,      //!< Key_SOFTRIGHT
                Key_ACTION,         //!< Key_ACTION
                Key_BACK,           //!< Key_BACK
                Key_MENU,           //!< Key_MENU
                Key_SEARCH,         //!< Key_SEARCH
            };

        };

        struct IApplication {
            virtual ~IApplication() {}
            virtual void create() = 0;
            virtual void destroy() = 0;
            virtual void setApplet(IApplet*) = 0;
            virtual void* getDisplay() const = 0;
            virtual void* getSurface() const = 0;
            virtual void start() = 0;
            virtual void stop() = 0;
            virtual audio::IAudioDevice* getAudioDevice() const = 0;
            virtual gfx::IGraphicsDevice* getGraphicsDevice() const = 0;
            virtual gfx::IGraphicsContext* getGraphicsContext() const = 0;
            virtual content::AssetManager* getAssetManager() const  = 0;
            virtual input::IInputDevice* getInputDevice(const int) const = 0;
            virtual input::IVirtualKeyboard* getVirtualKeyboard() const = 0;
            virtual void setMouseRelative(bool) = 0;
            virtual bool isMouseRelative() const = 0;
            virtual void resize(int, int) = 0;
            virtual void setPosition(int, int) = 0;
            virtual int getWidth() const = 0;
            virtual int getHeight() const = 0;

            core::EventHandler<MouseEventArgs>  OnMousePressed;
            core::EventHandler<MouseEventArgs>  OnMouseReleased;
            core::EventHandler<MouseEventArgs>  OnMouseMoved;

            core::EventHandler<KeyEventArgs>    OnKeyPressed;
            core::EventHandler<KeyEventArgs>    OnKeyReleased;
        };

    }
}

typedef helix::base::IApplication   hxApplication;
typedef helix::base::KeyEventArgs   hxKeyEventArgs;
typedef helix::base::MouseEventArgs hxMouseEventArgs;

#endif
