#ifndef HELIX_BASE_PLATFORM_APPLICATION_H_
#define HELIX_BASE_PLATFORM_APPLICATION_H_

#include <helix/Types.h>

#if defined(HELIX_PLATFORM_IOS)
#include <helix/base/platform/ios/IOSApplication.h>
namespace helix {
namespace base {
typedef impl::IOSApplication PlatformApplication;
}
}
#elif defined (HELIX_PLATFORM_ANDROID)
#include <helix/base/platform/android/AndroidApplication.h>
namespace helix {
namespace base {
typedef impl::AndroidApplication PlatformApplication;
}
}
#elif defined(HELIX_PLATFORM_X11)
#include <helix/base/platform/x11/X11Application.h>
namespace helix {
namespace base {
typedef impl::X11Application PlatformApplication;
}
}
#elif defined(HELIX_PLATFORM_WINDOWS_PHONE)
#include <helix/base/platform/wp8/WP8Application.h>
namespace helix {
namespace base {
typedef impl::WP8Application PlatformApplication;
}
}
#endif

#endif
