#ifndef HELIX_BASE_BASEAPPLICATION_H_
#define HELIX_BASE_BASEAPPLICATION_H_

#include "IApplication.h"
#include <helix/input/IInputDevice.h>


namespace helix {
namespace content {
class AssetManager;
}
namespace gfx {
struct IGraphicsDevice;
struct IGraphicsContext;
}
namespace base {

struct GraphicsPlugin {
    typedef gfx::IGraphicsDevice*(*PFNCREATEGRAPHICSDEVICE)(void);
    typedef gfx::IGraphicsContext*(*PFNCREATEGRAPHICSCONTEXT)(void);

    PFNCREATEGRAPHICSCONTEXT    mCreateGraphicsContextDelegate;
    PFNCREATEGRAPHICSDEVICE     mCreateGraphicsDeviceDelegate;

    GraphicsPlugin(PFNCREATEGRAPHICSCONTEXT = NULL, PFNCREATEGRAPHICSDEVICE = NULL);
};

class BaseApplication : public IApplication {
protected:
    content::AssetManager*   mAssetManager;
    gfx::IGraphicsContext*   mGraphicsContext;
    gfx::IGraphicsDevice*    mGraphicsDevice;
    input::IInputDevice*     mInputDevice[input::InputDeviceType::MaxDeviceType];
    input::IVirtualKeyboard* mVirtualKeyboard;
    audio::IAudioDevice*	 mAudioDevice;
    IApplet*                 mApplet;
    bool                     mIsMouseRelative;
    int                      mLastMouseX;
    int                      mLastMouseY;
    bool                     mWillSkipNextMouseMove;
    int                      mWidth;
    int                      mHeight;
    int                      mPositionX;
    int                      mPositionY;
protected:
    void postMouseMove(int, int, int);
    void postMousePressed(int, int, int);
    void postMouseReleased(int, int, int);
    void postKeyPress(int);
    void postKeyRelease(int);
public:
    BaseApplication();
    virtual ~BaseApplication();
    virtual content::AssetManager* getAssetManager() const;
    virtual gfx::IGraphicsContext* getGraphicsContext() const;
    virtual gfx::IGraphicsDevice*  getGraphicsDevice() const;
    virtual input::IInputDevice*   getInputDevice(const int) const;
    virtual input::IVirtualKeyboard* getVirtualKeyboard() const;
    virtual audio::IAudioDevice* getAudioDevice() const;
    virtual void setApplet(IApplet*);
    virtual IApplet* getApplet() const;
    virtual void* getDisplay() const;
    virtual void* getSurface() const;
    virtual void create();
    virtual void destroy();
    virtual void setMouseRelative(bool);
    virtual bool isMouseRelative() const;
    virtual void resize(int, int);
    virtual void setPosition(int, int);
    virtual int getWidth() const;
    virtual int getHeight() const;
};

}
}

#endif
