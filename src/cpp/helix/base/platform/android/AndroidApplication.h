#ifndef HELIX_BASE_PLATFORM_ANDROIDAPPLICATION_H_
#define HELIX_BASE_PLATFORM_ANDROIDAPPLICATION_H_

#include <helix/base/BaseApplication.h>
#include <helix/core/IMutex.h>
#include <helix/core/IRunnable.h>
#include <helix/core/IThread.h>
#include <android/native_activity.h>
#include <android/native_window.h>
#include <android/configuration.h>
#include <android/input.h>
#include <android/sensor.h>
#include <android/window.h>

namespace helix {
namespace base {
namespace impl {

class HXEXPORT AndroidApplication : public BaseApplication, public hxRunnable {
	ANativeActivity*	    mNativeActivity;
    AConfiguration*         mConfig;
    ALooper*                mLooper;
    AInputQueue*			mQueue;
    hxMutex*                mLock;
    hxThread*				mThread;
    ANativeWindow*          mWindow;
    bool                    mIsRunning;
	unsigned int			mAsyncMessage;
public:
	AndroidApplication();
	virtual ~AndroidApplication();
	virtual void create();
    virtual void start();
    virtual void stop();
    virtual void pause();
    virtual void resume();
    virtual void* getDisplay() const;
    virtual void* getSurface() const;
    int run();
	void done();
	void setNativeActivity(ANativeActivity*);
    void setWindow(ANativeWindow*);
	void pushAsyncMessage(const int);
	void popAsyncMessage(const int);
protected:
    void handleInput();
protected:
    static void onDestroy(ANativeActivity *activity);
    static void onStart(ANativeActivity *activity);
    static void onStop(ANativeActivity *activity);
    static void onNativeWindowCreated(ANativeActivity *activity,ANativeWindow *window);
    static void onNativeWindowDestroyed(ANativeActivity *activity,ANativeWindow *window);
    static void onNativeWindowResized(ANativeActivity *activity,ANativeWindow *window);
    static void onInputQueueCreated(ANativeActivity *activity,AInputQueue *queue);
    static void onInputQueueDestroyed(ANativeActivity *activity,AInputQueue *queue);
    static void onConfigurationChanged(ANativeActivity* activity);
    static void onResume(ANativeActivity* activity);
    static void onPause(ANativeActivity* activity);
};

}
}
}

#endif
