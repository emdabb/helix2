#include "AndroidApplication.h"
#include <helix/core/DebugLog.h>
#include <helix/Config.h>
#include <helix/base/IApplet.h>
#include <helix/gfx/IGraphicsContext.h>
#include <helix/gfx/IGraphicsDevice.h>

//#include <helix/content/platform/android/AndroidAssetManager.h>

using namespace helix::core;
using namespace helix::content;
using namespace helix::base;
using namespace helix::base::impl;
using namespace helix::gfx;
using namespace helix::input;

#define FLAGS_FOR_ACTION(a) (((a)) & AMOTION_EVENT_ACTION_MASK)
#define INDEX_FOR_ACTION(a) ((((a)) & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT)
#define CONCAT2 (a, b) a ## b
#define CONCAT(a, b) CONCAT(a, b)
#define NDK_BRIDGE(x) CONCAT(CONCAT(CONCAT(Java_, PACKAGE_NAME), _), x)

HXAPI HXEXPORT IGraphicsDevice*     new_GLGraphicsDevice();
HXAPI HXEXPORT IGraphicsContext*    new_EGLGraphicsContext();
HXAPI HXEXPORT AssetManager*        new_AndroidAssetManager(AAssetManager*);
HXAPI HXEXPORT IInputDevice*        new_AndroidSensorDevice(const int);

struct AsyncMessage {
    enum {
        Msg_None                = 0,
        Msg_WindowCreated       = 1 << 0,
        Msg_WindowResized       = 1 << 1,
        Msg_WindowDestroyed     = 1 << 2,
        Msg_QueueCreated        = 1 << 3,
        Msg_QueueDesroyed       = 1 << 4
    };
};

AndroidApplication::AndroidApplication()
:mNativeActivity(NULL)
,mConfig(NULL)
,mLooper(NULL)
, mQueue(NULL)
,mLock(new_Mutex())
,mThread(new_Thread(this))
,mWindow(NULL)
,mIsRunning(false)
{

}

AndroidApplication::~AndroidApplication() {
}

void AndroidApplication::create() {
    mAssetManager = new_AndroidAssetManager(mNativeActivity->assetManager);
}

void AndroidApplication::start() {
    if(!mIsRunning) {
        mIsRunning = true;
        mThread->start();
    }
}

void AndroidApplication::handleInput() {
	int nevents = 0;
	while (ALooper_pollAll(0, NULL, &nevents, NULL) >= 0) {
		AInputEvent* ev = NULL;
		int handled = 0;
		if (AInputQueue_getEvent(mQueue, &ev) >= 0) {
			if (AInputQueue_preDispatchEvent(mQueue, ev)) {
				return;
			}
			//int nhandled = 0;
			switch (AInputEvent_getType(ev)) {
			case AINPUT_EVENT_TYPE_KEY: {
				break;
			}
			case AINPUT_EVENT_TYPE_MOTION: {
				int action = AMotionEvent_getAction(ev);
				int flags = FLAGS_FOR_ACTION(action);
				int index = INDEX_FOR_ACTION(action);
				//int npointers = AMotionEvent_getPointerCount(ev);
				int pointer = AMotionEvent_getPointerId(ev, index);
				//float psi = AMotionEvent_getPressure(ev, index);
				int pos_x = AMotionEvent_getX(ev, index);
				int pos_y = AMotionEvent_getY(ev, index);
				switch (flags) {
				case AMOTION_EVENT_ACTION_MOVE: {
	//                    if (is_mouse_relative) {
	//                        postMouseMove(pmousex - pos_x, pmousey - pos_y, 0, pointer);
	//                        pmousex = pos_x;
	//                        pmousey = pos_y;
	//
	//                    } else {
						postMouseMove(pos_x, pos_y, pointer);
	//                    }
					break;
				}
				case AMOTION_EVENT_ACTION_DOWN:
				case AMOTION_EVENT_ACTION_POINTER_DOWN: {
					if (mIsMouseRelative) {
						postMouseMove(0, 0, pointer);//, 0, pointer);
						mLastMouseX = pos_x;
						mLastMouseX = pos_y;
					} else {
						postMouseMove(pos_x, pos_y, pointer);//, 0, pointer);
					}
					postMousePressed(pos_x, pos_y, pointer);
					break;
				}
				case AMOTION_EVENT_ACTION_UP:
				case AMOTION_EVENT_ACTION_POINTER_UP: {
					postMouseReleased(pos_x, pos_y, pointer);

				}
				}
				break;
			}
			}
			AInputQueue_finishEvent(mQueue, ev, handled);
		}
	}
}

void AndroidApplication::done(){
    stop();
}

void AndroidApplication::pushAsyncMessage(const int val) {
    hxScopedLock<IMutex>::Type guard(mLock);
    mAsyncMessage |= val;
}

void AndroidApplication::popAsyncMessage(const int val) {
    hxScopedLock<IMutex>::Type guard(mLock); // TODO: <-- deadlock??
    mAsyncMessage |= val;
}

int AndroidApplication::run() {
    DEBUG_METHOD();
	mLooper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
	if (mQueue != NULL) {
		DEBUG_MESSAGE("Attaching looper to input queue...");
		AInputQueue_attachLooper(mQueue, mLooper, 0, NULL, this);
	}

    while(mIsRunning) {
    	handleInput();
        hxScopedLock<hxMutex>::Type guard(mLock);


        if(mAsyncMessage & AsyncMessage::Msg_WindowCreated) {
            mGraphicsContext = new_EGLGraphicsContext();
			mGraphicsDevice  = new_GLGraphicsDevice();
			mGraphicsContext->create(getDisplay(), getSurface());
			mGraphicsDevice->createWithContext(mGraphicsContext);
			if(mApplet != NULL) {
				mApplet->create();
			}
            popAsyncMessage(AsyncMessage::Msg_WindowCreated);
        }

        if(mAsyncMessage & AsyncMessage::Msg_WindowDestroyed) {
            mGraphicsContext->destroy();
            popAsyncMessage(AsyncMessage::Msg_WindowDestroyed);
        }

    	if(mWindow && mApplet) {
    		mApplet->update(16);
    		mApplet->draw();
    	}
    	//mLock->release();
    }
	return 0;
}

void AndroidApplication::stop() {
    mIsRunning = false;
}

void AndroidApplication::pause() {
}

void AndroidApplication::resume() {
}

void* AndroidApplication::getDisplay() const {
    return (NULL);
}

void* AndroidApplication::getSurface() const {
    return static_cast<void*>(mWindow);
}


void AndroidApplication::setNativeActivity(ANativeActivity* activity) {
    activity->instance=this;
    activity->callbacks->onDestroy              = onDestroy;
    activity->callbacks->onStart                = onStart;
    activity->callbacks->onResume               = onResume;
    activity->callbacks->onPause                = onPause;
    activity->callbacks->onStop                 = onStop;
    activity->callbacks->onConfigurationChanged = onConfigurationChanged;
    activity->callbacks->onNativeWindowCreated  = onNativeWindowCreated;
    activity->callbacks->onNativeWindowDestroyed= onNativeWindowDestroyed;
    activity->callbacks->onNativeWindowResized  = onNativeWindowResized;
    activity->callbacks->onInputQueueCreated    = onInputQueueCreated;
    activity->callbacks->onInputQueueDestroyed  = onInputQueueDestroyed;
    this->mNativeActivity                       = activity;
}

void AndroidApplication::onDestroy(ANativeActivity *activity) {

}
void AndroidApplication::onStart(ANativeActivity *activity) {
    DEBUG_METHOD();
    ANativeActivity_setWindowFlags(activity, AWINDOW_FLAG_KEEP_SCREEN_ON, 0);
//    static_cast<AndroidApplication*>(activity->instance)->start();
}
void AndroidApplication::onStop(ANativeActivity *activity) {
    DEBUG_METHOD();
//    static_cast<AndroidApplication*>(activity->instance)->stop();
}

void AndroidApplication::onPause(ANativeActivity *activity) {
    DEBUG_METHOD();
//    static_cast<AndroidApplication*>(activity->instance)->pause();
}
void AndroidApplication::onResume(ANativeActivity *activity) {
    DEBUG_METHOD();
//    static_cast<AndroidApplication*>(activity->instance)->resume();
}



void AndroidApplication::setWindow(ANativeWindow* win) {
    ScopedLock<IMutex> guard(mLock);
    mWindow = win;
}

void AndroidApplication::onNativeWindowCreated(ANativeActivity *activity, ANativeWindow *win) {
    DEBUG_METHOD();
    //static_cast<AndroidApplication*>(activity->instance)->onNativeWindowCreated(window);
    AndroidApplication* app =static_cast<AndroidApplication*>(activity->instance);
    app->setWindow(win);
    app->pushAsyncMessage(AsyncMessage::Msg_WindowCreated);

}

void AndroidApplication::onNativeWindowDestroyed(ANativeActivity *activity, ANativeWindow *window) {
    DEBUG_METHOD();
    //hxScopedLock<IMutex>::Type guard(mLock);
    AndroidApplication* app =static_cast<AndroidApplication*>(activity->instance);
    app->pushAsyncMessage(AsyncMessage::Msg_WindowDestroyed);
}
void AndroidApplication::onNativeWindowResized(ANativeActivity *activity, ANativeWindow *window) {
    DEBUG_METHOD();
    //hxScopedLock<IMutex>::Type guard(mLock);
    AndroidApplication* app =static_cast<AndroidApplication*>(activity->instance);
    app->pushAsyncMessage(AsyncMessage::Msg_WindowResized);
//    static_cast<AndroidApplication*>(activity->instance)->notifyWindowResized(window);
}
void AndroidApplication::onInputQueueCreated(ANativeActivity *activity, AInputQueue *queue) {
    DEBUG_METHOD();
    //hxScopedLock<IMutex>::Type guard(mLock);
    AndroidApplication* app =static_cast<AndroidApplication*>(activity->instance);
    app->pushAsyncMessage(AsyncMessage::Msg_QueueCreated);

//    static_cast<AndroidApplication*>(activity->instance)->notifyQueueCreated(queue);
}
void AndroidApplication::onInputQueueDestroyed(ANativeActivity *activity, AInputQueue *queue) {
    DEBUG_METHOD();
    //hxScopedLock<IMutex>::Type guard(mLock);
    AndroidApplication* app = static_cast<AndroidApplication*>(activity->instance);
    app->pushAsyncMessage(AsyncMessage::Msg_QueueDesroyed);
//    static_cast<AndroidApplication*>(activity->instance)->notifyQueueDestroyed(queue);
}
void AndroidApplication::onConfigurationChanged(ANativeActivity* activity) {
    DEBUG_METHOD();
}
