#ifndef HELIX_PLATFORM_PLATFORM_IOS_IOSAPPLICATION_H_
#define HELIX_PLATFORM_PLATFORM_IOS_IOSAPPLICATION_H_

#include <helix/base/BaseApplication.h>

namespace helix {
namespace base {
namespace impl {

class HXEXPORT IOSApplication : public BaseApplication {
	void*	mWindow;
	void* 	mView;
    void*   mViewController;
	void* 	mPool;
public:
    IOSApplication();
	virtual ~IOSApplication();
	virtual void create();
    virtual void* getDisplay() const;
    virtual void* getSurface() const;
    virtual void start();
    virtual void stop();
    void setWindow(void*);
    void update(int);
    void draw();
    void createGraphicsContextAndDevice();

    void mousePressed(int, int, int);
    void mouseMoved(int, int, int);
    void mouseReleased(int, int, int);
};

}
}
}


#endif

