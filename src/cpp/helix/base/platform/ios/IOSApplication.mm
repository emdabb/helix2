#include "IOSApplication.h"
#include <helix/core/DebugLog.h>
#include <helix/base/IApplet.h>
#include <helix/audio/IAudioDevice.h>
#include <helix/audio/AudioInitializer.h>
#include <helix/gfx/IGraphicsContext.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/Viewport.h>
#include <helix/input/IInputDevice.h>
#import <UIKit/UIKit.h>
#include <sys/time.h>

#define SCREEN_WIDTH    (IOS_VERSION_LOWER_THAN_8 \
                        ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) \
                        ? [[UIScreen mainScreen] bounds].size.width \
                        : [[UIScreen mainScreen] bounds].size.height) \
                        : [[UIScreen mainScreen] bounds].size.width)

#define SCREEN_HEIGHT   (IOS_VERSION_LOWER_THAN_8 \
                        ? (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation) \
                        ? [[UIScreen mainScreen] bounds].size.height \
                        : [[UIScreen mainScreen] bounds].size.width) \
                        : [[UIScreen mainScreen] bounds].size.height)

#define IOS_VERSION_LOWER_THAN_8    (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1)
#define IOS_IS_LANDSCAPE            (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))

using namespace helix;
using namespace helix::content;
using namespace helix::gfx;
using namespace helix::base;
using namespace helix::base::impl;
using namespace helix::input;
using namespace helix::core;
using namespace helix::audio;

HXAPI IGraphicsContext* new_EAGLGraphicsContext();
HXAPI IGraphicsDevice*  new_GLGraphicsDevice();
HXAPI AssetManager*     new_IOSAssetManager();
HXAPI IInputDevice*     new_IOSSensorDevice(const int);
HXAPI IVirtualKeyboard* new_IOSVirtualKeyboard(IApplication*, void*);
HXAPI IAudioDevice*     new_CkAudioDevice();

uint64_t mtime() {
        struct timeval now;
        gettimeofday(&now,0);
        return ((uint64_t)now.tv_sec)*1000+((uint64_t)(now.tv_usec))/1000;
}


@interface ApplicationView : UIView  {
@private
        IOSApplication*         mApplication;
        NSTimer*                mTimer;
        uint64_t                mLastTime;
        CGPoint                 mLastLocation;
}

- (id) initWithApplication:(IOSApplication*)application frame:(CGRect)rect;
- (void) update;
@end

@implementation ApplicationView
+ (Class)layerClass {
        return [CAEAGLLayer class];
}

- (id) initWithApplication : (IOSApplication*)application frame:(CGRect)rect {

    if((self=[super initWithFrame:rect])) {
            self.opaque=YES,
			mLastTime = mtime();
			mApplication = application;
	}
	return self;
}
- (void) layoutSubviews {
    DEBUG_METHOD();
    int w = [self bounds].size.width;
    int h = [self bounds].size.height;

}


- (void) dealloc{
	[mTimer invalidate];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

- (void) update {
//    mApplication->update(16);
    uint64_t currentTime=mtime();
    int dt = currentTime - mLastTime;
    //if(mApplication->isActive()){
    mApplication->update(dt);
    mApplication->draw();
//        if(mApplication->getRenderDevice()!=NULL){
//            mApplication->render();
//        }
//        if(mApplication->getAudioDevice()!=NULL){
//            mApplication->getAudioDevice()->update(dt);
//        }
    //}
    mLastTime=currentTime;

}

- (void) start {
    if(mTimer == nil) {
        mTimer = [NSTimer scheduledTimerWithTimeInterval:0.016666 target:self selector:@selector(update) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:mTimer forMode:NSDefaultRunLoopMode];
    }
}

- (void) stop {
    [mTimer invalidate];
    mTimer = nil;
}

// Handles the start of a touch
- (void) touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    CGRect fr = [self frame];
    CGRect rc = [self bounds];
    UITouch* touch      = [[event touchesForView:self] anyObject];
    CGPoint location    = [touch locationInView:self];
    mLastLocation       = location;
    mApplication->mousePressed(location.x,location.y,0);
}

// Handles the continuation of a touch.
- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event{
    UITouch* touch  = [[event touchesForView:self] anyObject];
    CGPoint location= [touch locationInView:self];
    mLastLocation   = location;
    mApplication->mouseMoved(location.x,location.y, 0);
}

// Handles the end of a touch event when the touch is a tap.
- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event{
    UITouch* touch  =[[event touchesForView:self] anyObject];
    CGPoint location=[touch locationInView:self];
    if(location.x!=mLastLocation.x || location.y!=mLastLocation.y){
        mApplication->mouseMoved(location.x,location.y, 0);
    }
    mApplication->mouseReleased(location.x,location.y,0);
}

// Handles the end of a touch event.
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch* touch=[[event touchesForView:self] anyObject];
    CGPoint location=[touch locationInView:self];
    if(location.x!=mLastLocation.x || location.y!=mLastLocation.y){
        mApplication->mouseMoved(location.x,location.y, 0);
    }
    mApplication->mouseReleased(location.x,location.y,0);
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return YES;
//}


@end



@interface ViewController : UIViewController {
    @public
    ApplicationView* mView;
    IOSApplication* mApplication;

}
-(id) initWithApplication:(IOSApplication*)application;
-(CGSize) getScreenSize;
//@property (nonatomic, retain) ApplicationView* mView;
@end

@implementation ViewController
-(id) initWithApplication:(IOSApplication*) application {
    mApplication = application;
    return self;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void) loadView {
    self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
}
-(void) viewDidLoad {
    [super viewDidLoad];
    CGSize rc = [self getScreenSize];
    mView = [[ApplicationView alloc] initWithApplication:mApplication frame:CGRectMake(0, 0, rc.width, rc.height)];//[UIScreen mainScreen].bounds];
    [self.view addSubview:(UIView*)mView];
    mApplication->createGraphicsContextAndDevice();
}
-(void) viewDidAppear:(BOOL)animated {
    CGRect rc = [[self view] bounds];
    CGRect frame = [[self view] frame];
    int k = 0;
}
- (CGSize) getScreenSize {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    if (IOS_VERSION_LOWER_THAN_8 && IOS_IS_LANDSCAPE)
    {
        return CGSizeMake(screenSize.height, screenSize.width);
    }
    return screenSize;
    //int screenSizeX = SCREEN_WIDTH;
    //int screenSizeY = SCREEN_HEIGHT;
    //return CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
}
@end

IOSApplication::IOSApplication()
: mWindow(NULL)
, mViewController(NULL)
, mPool(NULL)
{

}

IOSApplication::~IOSApplication() {

}

void IOSApplication::create() {
    // pre-create; set windows, views etc.

    if(mWindow == nil) {
        [UIApplication sharedApplication];
        mPool = [[NSAutoreleasePool alloc] init];
        //        CGRect rect = [[UIScreen mainScreen] applicationFrame];
        //
        CGRect rect = [UIScreen mainScreen].bounds;
        if(IOS_VERSION_LOWER_THAN_8 && IOS_IS_LANDSCAPE) {
            CGFloat tmp = rect.size.width;
            rect.size.width = rect.size.height;
            rect.size.height= tmp;
        }
        
        mWindow = [[UIWindow alloc] initWithFrame:rect];
        
        
        if(IOS_IS_LANDSCAPE) {
            ((UIWindow*)mWindow).center = CGPointMake(rect.size.height / 2.0f, rect.size.width / 2.0f);
            [mWindow setTransform:CGAffineTransformMakeRotation(M_PI / 2.0f)];
        } else {
            ((UIWindow*)mWindow).center = CGPointMake(rect.size.width / 2.0f, rect.size.height / 2.0f);
        }
    }

    mAudioDevice = new_CkAudioDevice();
    AudioInitializer init;
    init.userData = this;
    init.bitRate  = 22800;
    mAudioDevice->create(init);

    mViewController = (void*)[[ViewController alloc]initWithApplication:this];

    [mWindow setRootViewController:(ViewController*)mViewController];
    [mWindow addSubview:[(ViewController*)mViewController view]];
    [mWindow makeKeyAndVisible];


    this->mInputDevice[InputDeviceType::Linear] = new_IOSSensorDevice(InputDeviceType::Linear);
    this->mInputDevice[InputDeviceType::Angular]= new_IOSSensorDevice(InputDeviceType::Angular);

    for(int i=0; i < InputDeviceType::MaxDeviceType; i++) {
        if(mInputDevice[i] != NULL) {
            mInputDevice[i]->create();
        }
    }
    

}

void IOSApplication::createGraphicsContextAndDevice() {
    DEBUG_METHOD();
    mView = (void*)[((ViewController*)mViewController)->mView retain];
    mVirtualKeyboard = new_IOSVirtualKeyboard(this, mView);
    
//    mVirtualKeyboard->OnKeyPress += event(IApplication:)
    
    CGSize sz = [(ViewController*)mViewController getScreenSize];
    CGRect rc =  CGRectMake(0, 0, sz.width, sz.height);

    DEBUG_MESSAGE("UIWindow.size is      [%3.2fx%3.2f]", sz.width, sz.height);

    mWidth = rc.size.width;
    mHeight = rc.size.height;


    // create graphics context and device
    mGraphicsContext = new_EAGLGraphicsContext();
    mGraphicsDevice  = new_GLGraphicsDevice();

    mGraphicsContext->create(getDisplay(), getSurface());
    mGraphicsDevice->createWithContext(mGraphicsContext);
    mAssetManager = new_IOSAssetManager();

    Viewport vp;
    vp.X = 0;
    vp.Y = 0;
    vp.W = mWidth;
    vp.H = mHeight;
    vp.MinDepth = 0.1f;
    vp.MaxDepth = 1000.0f;
    mGraphicsDevice->setViewport(vp);

}

void IOSApplication::setWindow(void* window) {
    mWindow = window;
    [(NSObject*)mWindow retain];
}


void* IOSApplication::getDisplay() const {
    return NULL;
}

void* IOSApplication::getSurface() const {
    return (CAEAGLLayer*)[(UIView*)mView layer];
}

void IOSApplication::update(int dt) {
    if(mApplet != NULL) {
        mApplet->update(dt);
    }
}

void IOSApplication::draw() {
    if(mApplet != NULL) {
        mApplet->draw();
    }
}

void IOSApplication::start() {
    if(mApplet != nil) {
        mApplet->create();
    }
    int sizeX = [(ApplicationView*)mView bounds].size.width;
    int sizeY = [(ApplicationView*)mView bounds].size.height;

    [(ApplicationView*)mView start];
    if(mPool != nil) {
        [(UIWindow*)mWindow makeKeyAndVisible];
        [[UIApplication sharedApplication] run];
    }
}

void IOSApplication::stop() {
    [(ApplicationView*)mView stop];
}

void IOSApplication::mousePressed(int x, int y, int pointerId) {
    postMousePressed(x, y, pointerId);
}

void IOSApplication::mouseMoved(int x, int y, int pointer) {
    if(isMouseRelative()){
        mLastMouseX = x;
        mLastMouseY = y;
        postMouseMove(mLastMouseX-x,mLastMouseY-y, pointer);
    } else {
        postMouseMove(x, y, pointer);

    }
    mLastMouseX = x;
    mLastMouseY = y;
}

void IOSApplication::mouseReleased(int x, int y, int pointerId) {
    postMouseReleased(x, y, pointerId);
}
