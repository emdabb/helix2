#include "X11Application.h"
#include <X11/X.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>
#include <X11/extensions/XInput.h>
#include <linux/joystick.h>
#include <linux/gameport.h>
#include <helix/base/IApplet.h>
#include <helix/gfx/IGraphicsContext.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/Viewport.h>
#include <helix/audio/IAudioDevice.h>
#include <helix/audio/AudioInitializer.h>
#include <helix/core/DebugLog.h>
#include <helix/content/AssetManager.h>

HXAPI hxGraphicsContext* new_EGLGraphicsContext();
HXAPI hxGraphicsDevice*  new_GLGraphicsDevice();
HXAPI hxAudioDevice*	 new_CkAudioDevice();

using namespace helix::audio;
using namespace helix::core;
using namespace helix::content;
using namespace helix::gfx;

namespace helix {
namespace base {
namespace impl {

class X11Application :public BaseApplication {

    Window      mWindow;
    Display*    mDisplay;
    Cursor      mBlankCursor;
    Atom        mDestroyWindowAtom;
    const char* mWindowName;
    bool        mIsRunning; // TODO:  <-- move to BaseApplication??

public:
    X11Application()
    : mWindow(None)
    , mDisplay(NULL)
    , mBlankCursor(0)
    , mDestroyWindowAtom(0)
    {
    }

    virtual ~X11Application() {
    }

    virtual void create() {
        createWindow();
        createCursor();

        Viewport vp;
        vp.X = vp.Y = 0;
        vp.W = mWidth;
        vp.H = mHeight;


        mGraphicsContext = new_EGLGraphicsContext();
        mGraphicsDevice  = new_GLGraphicsDevice();

        mGraphicsContext->create(getDisplay(), getSurface());
        mGraphicsDevice->createWithContext(mGraphicsContext);
        mGraphicsDevice->setViewport(vp);

        AudioInitializer with;
        with.userData = this;
        with.bitRate  = 22800;
        mAudioDevice = new_CkAudioDevice();
        mAudioDevice->create(with);

        mAssetManager = new AssetManager();
    }

    virtual void destroy() {
    }

    void createCursor() {
        DEBUG_METHOD();
        static char cursorData[1] = {0};
        XColor cursorColor;
        Pixmap cursorPixmap = XCreateBitmapFromData(mDisplay, mWindow, cursorData, 1, 1);
        mBlankCursor = XCreatePixmapCursor(mDisplay, cursorPixmap, cursorPixmap, &cursorColor, &cursorColor, 0, 0);
        XFreePixmap(mDisplay, cursorPixmap);
    }

    void createWindow() {
        DEBUG_METHOD();
        mDisplay = XOpenDisplay(getenv("DISPLAY"));
        XVisualInfo* vinfo;
        int scrnum = XDefaultScreen(mDisplay);
        XVisualInfo info;
        if (!XMatchVisualInfo(mDisplay, scrnum, 24, TrueColor, &info)) {
            throw std::runtime_error("XMatchVisualInfo failed!");
        }
        info.screen = 0;
        int n = 0;
        vinfo = XGetVisualInfo(mDisplay, VisualAllMask, &info, &n);
        if (vinfo == None) {
            throw std::runtime_error("XGetVisualInfo failed!");
        }
        int x, y, w, h;
        x = mPositionX;
        y = mPositionY;
        w = mWidth;
        h = mHeight;
        Bool fullscreen = False; //mGraphicsInitializer.fullscreen() ? True : False;
        Window root = DefaultRootWindow(mDisplay);
        if (fullscreen) {
            x = y = 0;
            w = DisplayWidth(mDisplay, DefaultScreen(mDisplay));
            h = DisplayHeight(mDisplay, DefaultScreen(mDisplay));
        }
        // Set the window attributes
        XSetWindowAttributes attrs;
        unsigned long mask;
        attrs.background_pixel = 0;
        attrs.border_pixel = 0;
        attrs.colormap = XCreateColormap(mDisplay, root, vinfo->visual, AllocNone);
        attrs.event_mask = StructureNotifyMask | ExposureMask | KeyReleaseMask | KeyPressMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask | ExposureMask;
        attrs.override_redirect = False; //mGraphicsInitializer.fullscreen() ? True : False;
        mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask
        | CWOverrideRedirect;
        mWindow = XCreateWindow(mDisplay, root, x, y, w, h, 0, vinfo->depth, InputOutput, vinfo->visual, mask, &attrs);


        XFree(vinfo);
        /* set hints and properties */
        {
            XSizeHints sizehints;
            sizehints.x = x;
            sizehints.y = y;
            sizehints.width = sizehints.min_width = sizehints.max_width = w;
            sizehints.height = sizehints.min_height = sizehints.max_height = h;
            sizehints.flags = PPosition | USSize |PMinSize | PMaxSize;
            XSetNormalHints(mDisplay, mWindow, &sizehints);
            XSetStandardProperties(mDisplay, mWindow, mWindowName, mWindowName, None, (char **) NULL, 0, &sizehints);
        }
        XMapWindow(mDisplay, mWindow);
        XFlush(mDisplay);
        /* QQQ: enforce window positioning */
        XMoveWindow(mDisplay, mWindow, x, y);


        mDestroyWindowAtom=XInternAtom(mDisplay, "WM_DELETE_WINDOW", false);
        XSetWMProtocols(mDisplay,mWindow,&mDestroyWindowAtom,1);
    }
    virtual void start() {
        if(!mIsRunning) {
            mIsRunning = true;
            run();
        }
    }
    virtual void stop() {
        mIsRunning = false;
    }

    void internal_mouseMoved(int x, int y, int pointerId) {
    	if (mWillSkipNextMouseMove) {
    	    mLastMouseX = x;
    	    mLastMouseY = y;
    	    mWillSkipNextMouseMove = false;
    	    return;
    	  }
    	  //MouseEventArgs ee;
    	  if (mIsMouseRelative) {
    	    // We check to see if we are at the warp-point, instead of using SkipNextMove
    	    //  since it seems on X11 that user mouse-move commands can get processed before the skip, getting things confused
    	    // We still use SkipNextMove when the DifferenceMouse is turned on to avoid the first jump
    	    if (x == mWidth / 2 && y == mHeight / 2) {
    	    	mLastMouseX = x;
    	    	mLastMouseY = y;
    	      return;
    	    }
    	    int dx = x - mLastMouseX, dy = y - mLastMouseY;
    	    XWarpPointer(mDisplay, None, mWindow, 0, 0, 0, 0, mWidth / 2, mHeight / 2);
    	    x = dx;
    	    y = dy;
    	  }
    	  postMouseMove(x, y, pointerId);
    }

    void processInput() {
        XEvent ev;
//        KeySym key;

        while (mDisplay && XPending(mDisplay)) {
            XNextEvent(mDisplay, &ev);
            switch (ev.type) {
            default:
                break;
            case ConfigureNotify:
//                configured(ev.xconfigure.x, ev.xconfigure.y, ev.xconfigure.width,
//                        ev.xconfigure.height);
                    //setPosition(ev.xconfigure.x, ev.xconfigure.y);
                    //resize(ev.xconfigure.width, ev.xconfigure.height);
                break;
            case MotionNotify:
                //postMouseMove(ev.xmotion.x, ev.xmotion.y, 0);
            	internal_mouseMoved(ev.xmotion.x, ev.xmotion.y, 0);
                break;
            case ButtonPress:
                switch (ev.xbutton.button) {
                case Button1:
                    //internal_mouse_pressed(ev.xbutton.x, ev.xbutton.y, 0, 0);
                    postMousePressed(ev.xbutton.x, ev.xbutton.y, 0);
                    break;
                case Button2:
                    //internal_mouse_pressed(ev.xbutton.x, ev.xbutton.y, 0, 1);
                    postMousePressed(ev.xbutton.x, ev.xbutton.y, 1);
                    break;
                case Button3:
                    //internal_mouse_pressed(ev.xbutton.x, ev.xbutton.y, 0, 2);
                    postMousePressed(ev.xbutton.x, ev.xbutton.y, 2);
                    break;
                case Button4:
                    //postMouseScroll(ev.xbutton.x, ev.xbutton.y, 1);
                    break;
                case Button5:
                    //postMouseScroll(ev.xbutton.x, ev.xbutton.y, -1);
                    break;
                default:
                    break;
                }
                break;

            case ButtonRelease:
                switch (ev.xbutton.button) {
                case Button1:
                    //internal_mouse_released(ev.xbutton.x, ev.xbutton.y, 0, 0);
                    postMouseReleased(ev.xbutton.x, ev.xbutton.y, 0);
                    break;
                case Button2:
                    //internal_mouse_released(ev.xbutton.x, ev.xbutton.y, 0, 1);
                    postMouseReleased(ev.xbutton.x, ev.xbutton.y, 1);
                    break;
                case Button3:
                    //internal_mouse_released(ev.xbutton.x, ev.xbutton.y, 0, 2);
                    postMouseReleased(ev.xbutton.x, ev.xbutton.y, 2);
                    break;
                case Button4:
                    break;
                case Button5:
                    break;
                default:
                    break;
                }
                break;
            case KeyPress: {
                KeySym key = XkbKeycodeToKeysym(mDisplay, ev.xkey.keycode, 0, 0);
                //internal_key_pressed(translate_key(key));
                postKeyPress(translateKey(key));
                break;
                }

            case KeyRelease: {
                KeySym key = XkbKeycodeToKeysym(mDisplay, ev.xkey.keycode, 0, 0);
                //internal_key_released(translate_key(key));
                postKeyRelease(translateKey(key));
                break;
                }
            case ResizeRequest:
                break;
            case ClientMessage:
                if(ev.xclient.data.l[0] == (long)mDestroyWindowAtom){
                    stop();
                    break;
                }
                break;
            }
        }
    }

    int timer() {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
    }

    virtual void run() {
        int dt = (int)(1000. / 60.);
        int timeCurrentMs = 0;
        int timeAccumulatedMs = dt;
        int timeDeltaMs = 0;
        int timeLastMs = timer();

        if(mApplet != NULL) {
            mApplet->create();
        }

        while(mIsRunning) {
            if(mApplet != NULL) {
                processInput();
                timeCurrentMs = timer();
                timeDeltaMs = timeCurrentMs - timeLastMs;
                timeAccumulatedMs += timeDeltaMs;
                while(timeAccumulatedMs >= dt) {
                    mApplet->update(dt);
                    timeAccumulatedMs -= dt;
                }
                mApplet->draw();
                timeLastMs = timeCurrentMs;
            }
        }
    }
    virtual void* getDisplay() const {
        return reinterpret_cast<void*>(mDisplay);
    }
    virtual void* getSurface() const {
        return reinterpret_cast<void*>(mWindow);
    }

    virtual void setMouseRelative(bool val) {
        //mIsMouseRelative = val;
        BaseApplication::setMouseRelative(val);
        mWillSkipNextMouseMove = true;
        if (mWindow && mDisplay) {
            if (val) {
                XDefineCursor(mDisplay, mWindow, mBlankCursor);
            } else {
                XUndefineCursor(mDisplay, mWindow);
            }
        }
    }
    virtual void resize(int w, int h) {
        DEBUG_METHOD();
        BaseApplication::resize(w, h);
        if (mWindow != None) {
            XResizeWindow(mDisplay, mWindow, mWidth, mHeight);
            XFlush(mDisplay);
        }
    }

    virtual void setPosition(int x, int y) {
        DEBUG_METHOD();
        BaseApplication::setPosition(x, y);
        if (mWindow != None) {
            XMoveWindow(mDisplay, mWindow, mPositionX, mPositionY);
            XFlush(mDisplay);
        }
    }

    void destroyWindow() {
       if(mBlankCursor){
            XFreeCursor(mDisplay, mBlankCursor);
            mBlankCursor=None;
        }

        // Just to be safe
        if(mDisplay){
            XSync(mDisplay,true);
        }

        //originalResolution();

        //originalEnv();

        if(mWindow){
            XDestroyWindow(mDisplay,mWindow);
            mWindow=None;
        }

        if(mDisplay){
            XCloseDisplay(mDisplay);
            mDisplay=None;
        }
    }

    int translateKey(int keysym) {
        switch (keysym) {
        case XK_Escape:
            return KeyEventArgs::Key_ESC;
        case XK_Pause:
            return KeyEventArgs::Key_PAUSE;
        case XK_Left:
            return KeyEventArgs::Key_LEFT;
        case XK_Right:
            return KeyEventArgs::Key_RIGHT;
        case XK_Up:
            return KeyEventArgs::Key_UP;
        case XK_Down:
            return KeyEventArgs::Key_DOWN;
        case XK_Shift_L:
        case XK_Shift_R:
            return KeyEventArgs::Key_SHIFT;
        case XK_Control_L:
        case XK_Control_R:
            return KeyEventArgs::Key_CTRL;
        case XK_Alt_L:
        case XK_Alt_R:
            return KeyEventArgs::Key_ALT;
        case XK_Super_L:
        case XK_Super_R:
            return KeyEventArgs::Key_SPECIAL;
        case XK_Return:
            return KeyEventArgs::Key_ENTER;
        case XK_Tab:
            return KeyEventArgs::Key_TAB;
        case XK_BackSpace:
            return KeyEventArgs::Key_BACKSPACE;
        case XK_Delete:
            return KeyEventArgs::Key_DELETE;
        case 32:
            return KeyEventArgs::Key_SPACE;
        default:
            break;
    }

    return keysym;
    }
};

}
}
}

typedef helix::base::impl::X11Application hxPlatformApplication;

HXAPI HXEXPORT hxApplication* new_PlatformApplication() {
    return new hxPlatformApplication;
}

