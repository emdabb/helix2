#include <helix/base/BaseApplication.h>
#include <helix/gfx/IGraphicsDevice.h>
#include <helix/gfx/IGraphicsContext.h>
#include <helix/core/DebugLog.h>
#include <wrl.h>
#include <wrl/client.h>
#include <d3d11_2.h>
#include <d2d1_2.h>
#include <d2d1effects_1.h>
#include <dwrite_2.h>
#include <wincodec.h>
#include <DirectXColors.h>
#include <DirectXMath.h>
#include <memory>
//#include <agile.h>
#include <concrt.h>
#include <collection.h>
#include <ppltasks.h>

HXAPI hxGraphicsDevice*     new_GLGraphicsDevice();
HXAPI hxGraphicsContext*    new_EGLGraphicsContext();

using namespace helix::core;

namespace helix {
namespace base {
namespace impl {

	class WP8Application;

	public ref class WP8AppDelegate sealed : public Windows::ApplicationModel::Core::IFrameworkView {
		WP8Application* 				mHandle;
		Windows::UI::Core::CoreWindow^	mWindowRef;
	public:
		WP8AppDelegate();
		virtual ~WP8AppDelegate();

		virtual void Initialize(Windows::ApplicationModel::Core::CoreApplicationView^ applicationView);
		virtual void SetWindow(Windows::UI::Core::CoreWindow^ window);
		virtual void Load(Platform::String^ entryPoint);

		virtual void Run();
		virtual void Uninitialize() {

		}
	internal:
		void setApplication(WP8Application* app) {
			mHandle = app;
		}

		void* getWindow() const;

	};

	class WP8Application : public BaseApplication {
		WP8AppDelegate^ mAppDelegate;
	public:
		virtual void create();
		/*virtual void setApplet(IApplet*)
		{

		}*/
		virtual void* getDisplay() const {
			return NULL;
		}
		virtual void* getSurface() const {
			return mAppDelegate->getWindow();
		}
		virtual void start() {

		}
		virtual void stop() {

		}

	};

	WP8AppDelegate::WP8AppDelegate() {
	}

	 WP8AppDelegate::~WP8AppDelegate() {
	}

	void WP8AppDelegate::Initialize(Windows::ApplicationModel::Core::CoreApplicationView^ applicationView) {
		DEBUG_METHOD();
	}

	void WP8AppDelegate::SetWindow(Windows::UI::Core::CoreWindow^ window) {
		DEBUG_METHOD();
		mWindowRef = window;
	}

	void* WP8AppDelegate::getWindow() const {
		DEBUG_METHOD();
		return reinterpret_cast<void*>(mWindowRef);
	}

	void WP8AppDelegate::Run() {
		DEBUG_METHOD();
		mHandle->start();
	}

	void WP8AppDelegate::Load(Platform::String^ entryPoint) {
		DEBUG_METHOD();
	}


}
}
}

HXAPI HXEXPORT hxApplication* new_WP8Application() {
    return new helix::base::impl::WP8Application();
}
