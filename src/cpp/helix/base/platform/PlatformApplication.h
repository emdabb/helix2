#ifndef HELIX_BASE_PLATFORM_PLATFORMAPPLICATION_H_
#define HELIX_BASE_PLATFORM_PLATFORMAPPLICATION_H_

#include <helix/Types.h>
#include <helix/base/IApplication.h>

HXAPI HXEXPORT hxApplication* new_PlatformApplication();

#endif
