#ifndef HELIX_BASE_IAPPLET_H_
#define HELIX_BASE_IAPPLET_H_

namespace helix {
namespace base {

struct IApplet {
    virtual ~IApplet() {}
    virtual void create() = 0;
    virtual void destroy() = 0;
    virtual void update(int) = 0;
    virtual void draw() = 0;
};

}
}

typedef helix::base::IApplet hxApplet;

#endif
